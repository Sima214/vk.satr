---
DisableFormat: false
BasedOnStyle: Google
Language: Cpp
Standard: Cpp11

## Indent settings
TabWidth: 4
UseTab: Never
IndentWidth: 4
ColumnLimit: 96 # But don't overdo it.
NamespaceIndentation: None
IndentPPDirectives: BeforeHash
ContinuationIndentWidth: 5
IndentCaseLabels: true
IndentWrappedFunctionNames: false
ConstructorInitializerIndentWidth: 4

PenaltyBreakBeforeFirstCallParameter: 1
PenaltyBreakFirstLessLess: 120
PenaltyBreakString: 1000
PenaltyExcessCharacter: 9001
PenaltyReturnTypeOnItsOwnLine: 200
PenaltyBreakComment: 300

## Comments
ReflowComments: true
SpacesBeforeTrailingComments: 2
AlignTrailingComments: true
CommentPragmas: '^ pragma: '

## Include file sorting
SortIncludes: true
IncludeCategories:
  - Regex: '^<[A-Z].*\.h>'
    Priority: 3
  - Regex: '^<.*\.hpp>'
    Priority: 1
  - Regex: '^<[a-z].*>'
    Priority: 2
  - Regex: '".*"'
    Priority: 0
IncludeIsMainRegex: '([-_](test|unittest))?$'

## Macros
AlignEscapedNewlines: Right
MacroBlockBegin: ''
MacroBlockEnd: ''
ForEachMacros: [foreach]

## Template
AlwaysBreakTemplateDeclarations: MultiLine
SpaceAfterTemplateKeyword: false
SpacesInAngles: false

## Pointers
DerivePointerAlignment: false
PointerAlignment: Left

## Braces
BreakBeforeBraces: Custom
BraceWrapping:
  AfterClass: false
  AfterControlStatement: false
  AfterEnum: false
  AfterFunction: false
  AfterNamespace: false
  AfterStruct: false
  AfterUnion: false
  BeforeCatch: true
  BeforeElse: true
  IndentBraces: false
Cpp11BracedListStyle: true
AllowShortBlocksOnASingleLine: false
AllowShortIfStatementsOnASingleLine: false
AllowShortLoopsOnASingleLine: false
SpacesInSquareBrackets: false

## Parenthesis
AlignAfterOpenBracket: Align
AllowAllParametersOfDeclarationOnNextLine: true
SpaceBeforeParens: ControlStatements
SpaceInEmptyParentheses: false
SpaceAfterCStyleCast: true
SpacesInCStyleCastParentheses: false
SpacesInParentheses: false

## Functions
AllowShortFunctionsOnASingleLine: Empty
AlwaysBreakAfterDefinitionReturnType: None
AlwaysBreakAfterReturnType: None
BinPackArguments: true
BinPackParameters: true

## C++ concepts
RequiresClausePosition: WithPreceding
IndentRequiresClause: false

## Operators/Expressions
BreakBeforeBinaryOperators: None
BreakBeforeTernaryOperators: false
SpaceBeforeAssignmentOperators: true

## Alignments
AlignConsecutiveAssignments: false
AlignConsecutiveDeclarations: false
AlignOperands: true

## Misc
AccessModifierOffset: -1
AllowShortCaseLabelsOnASingleLine: true
AlwaysBreakBeforeMultilineStrings: false
BreakConstructorInitializers: AfterColon
ConstructorInitializerAllOnOneLineOrOnePerLine: false
KeepEmptyLinesAtTheStartOfBlocks: true
BreakStringLiterals: true
MaxEmptyLinesToKeep: 1
