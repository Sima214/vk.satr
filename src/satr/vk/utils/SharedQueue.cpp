#include "SharedQueue.hpp"

#include <logger/Logger.hpp>
#include <vulkan/vulkan_to_string.hpp>

namespace satr::vk {

SharedQueue::SharedQueue(::vk::Queue handle) : _handle(handle) {}
SharedQueue::SharedQueue(::vk::Device& dev, QueueIndex index) :
    SharedQueue(dev.getQueue(index.family_index, index.index)) {}

SharedQueue::SharedQueue(SharedQueue&& o) noexcept {
    std::scoped_lock _lock(_mutex, o._mutex);
    _handle = std::exchange(o._handle, nullptr);
}
SharedQueue& SharedQueue::operator=(SharedQueue&& o) noexcept {
    std::scoped_lock _lock(_mutex, o._mutex);
    _handle = std::exchange(o._handle, nullptr);
    return *this;
}
SharedQueue::~SharedQueue() {
    if (_handle) {
        // Wait until this object is not used anymore.
        std::lock_guard _lock(_mutex);
        _handle = nullptr;
    }
}

bool SharedQueue::submit(const ::vk::SubmitInfo& info, ::vk::Fence fence) const {
    std::lock_guard g(_mutex);
    ::vk::Result r = _handle.submit(info, fence);
    if (r != ::vk::Result::eSuccess) {
        logger.loge("Couldn't submit command buffer to queue(", to_string(r), ")!");
        return false;
    }
    return true;
}
::vk::Result SharedQueue::submit(const ::vk::PresentInfoKHR& info) const {
    std::lock_guard g(_mutex);
    ::vk::Result r = _handle.presentKHR(info);
    return r;
}

}  // namespace satr::vk
