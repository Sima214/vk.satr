#include "CommandBuffer.hpp"

#include <SharedQueue.hpp>
#include <logger/Logger.hpp>
#include <trace/Trace.hpp>
#include <vulkan/vulkan_to_string.hpp>

#include <utility>

namespace satr::vk {

bool CommandBufferContext::_start(::vk::CommandBufferUsageFlags flags) {
    _cmd_buf.reset();
    ::vk::CommandBufferBeginInfo binfo(flags);
    ::vk::Result r = _cmd_buf.begin(binfo);
    if (r != ::vk::Result::eSuccess) [[unlikely]] {
        logger.loge("Couldn't begin command recording(", to_string(r), ")!");
        return false;
    }
    return true;
}
CommandBufferContext::CommandBufferContext(::vk::CommandBuffer cmd_buf,
                                           ::vk::Fence cmd_complete,
                                           ::vk::CommandBufferUsageFlags flags) :
    _cmd_buf(cmd_buf),
    _cmd_complete(cmd_complete) {
    if (!_start(flags)) {
        _cmd_buf = nullptr;
        _cmd_complete = nullptr;
    }
}

CommandBufferContext::CommandBufferContext(CommandBufferContext&& o) noexcept :
    _cmd_buf(std::exchange(o._cmd_buf, nullptr)),
    _cmd_complete(std::exchange(o._cmd_complete, nullptr)),
    _res_lst_buffer(std::move(o._res_lst_buffer)), _res_lst_img1d(std::move(o._res_lst_img1d)),
    _res_lst_img2d(std::move(o._res_lst_img2d)), _res_lst_img3d(std::move(o._res_lst_img3d)) {}
CommandBufferContext& CommandBufferContext::operator=(CommandBufferContext&& o) {
    if (_cmd_buf != nullptr) [[unlikely]] {
        logger.logf("Cannot replace non-empty CommandBufferContext!");
    }

    _cmd_buf = std::exchange(o._cmd_buf, nullptr);
    _cmd_complete = std::exchange(o._cmd_complete, nullptr);
    _res_lst_buffer = std::move(o._res_lst_buffer);
    _res_lst_img1d = std::move(o._res_lst_img1d);
    _res_lst_img2d = std::move(o._res_lst_img2d);
    _res_lst_img3d = std::move(o._res_lst_img3d);

    return *this;
}

CommandBufferContext::~CommandBufferContext() {
    if (_cmd_buf != nullptr) {
        logger.logw("Destructing non-empty CommandBufferContext!");
        // Invalidate.
        _cmd_buf = nullptr;
        _cmd_complete = nullptr;
    }
}

void CommandBufferContext::register_resource(Buffer& res) {
    _res_lst_buffer.push_front(res);
}
void CommandBufferContext::register_resource(Image1D& res) {
    _res_lst_img1d.push_front(res);
}
void CommandBufferContext::register_resource(Image2D& res) {
    _res_lst_img2d.push_front(res);
}
void CommandBufferContext::register_resource(Image3D& res) {
    _res_lst_img3d.push_front(res);
}

bool CommandBufferContext::submit(const SharedQueue& queue,
                                  ::vk::SubmitInfo& submit_info) const {
    {
        ::vk::Result r = _cmd_buf.end();
        if (r != ::vk::Result::eSuccess) {
            logger.loge("Couldn't end command recording(", to_string(r), ")!");
            return false;
        }
    }

    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &_cmd_buf;

    return queue.submit(submit_info, _cmd_complete);
}
bool CommandBufferContext::wait(::vk::Device dev) {
    if (!_cmd_complete) {
        // Skip
        return true;
    }
    // Wait for command execution completion.
    ::vk::Result fence_result;
    do {
        fence_result =
             dev.waitForFences({_cmd_complete}, true, std::numeric_limits<uint64_t>::max());
    } while (fence_result == ::vk::Result::eTimeout);
    if (fence_result != ::vk::Result::eSuccess) {
        logger.loge("Error while waiting on fence(", to_string(fence_result), ")!");
        return false;
    }
    dev.resetFences({_cmd_complete});

    _cmd_buf = nullptr;
    _cmd_complete = nullptr;
    _res_lst_buffer.clear();
    _res_lst_img1d.clear();
    _res_lst_img2d.clear();
    _res_lst_img3d.clear();

    return true;
}

SingleUseCommandBufferContext::SingleUseCommandBufferContext(Context& engine,
                                                             QueueSlotRole role,
                                                             size_t sub_index) :
    _engine(engine),
    _queue_role(role), _queue_sub_index(sub_index),
    _cmd_pool(engine.create_command_pool(role, sub_index)) {
    _cmd_buf = engine.create_command_buffer(_cmd_pool);
    _cmd_complete = engine.create_fence(false);
    if (!_start(::vk::CommandBufferUsageFlagBits::eOneTimeSubmit)) {
        ::vk::Device dev = engine.get_active_device();
        dev.destroy(_cmd_pool);
        dev.destroy(_cmd_complete);
        _cmd_pool = nullptr;
        _cmd_buf = nullptr;
        _cmd_complete = nullptr;
    }
}
SingleUseCommandBufferContext::SingleUseCommandBufferContext(
     SingleUseCommandBufferContext&& o) :
    CommandBufferContext(std::forward<CommandBufferContext>(o)),
    _engine(o._engine), _queue_role(o._queue_role), _queue_sub_index(o._queue_sub_index),
    _cmd_pool(std::exchange(o._cmd_pool, nullptr)) {}
SingleUseCommandBufferContext& SingleUseCommandBufferContext::operator=(
     SingleUseCommandBufferContext&& o) {
    CommandBufferContext::operator=(std::forward<CommandBufferContext>(o));
    _engine = o._engine;
    _queue_role = o._queue_role;
    _queue_sub_index = o._queue_sub_index;
    _cmd_pool = std::exchange(o._cmd_pool, nullptr);
    return *this;
}
SingleUseCommandBufferContext::~SingleUseCommandBufferContext() {
    if (_cmd_pool != nullptr) {
        _cmd_pool = nullptr;
    }
}

bool SingleUseCommandBufferContext::submit() {
    return CommandBufferContext::submit(_engine.get().get_queue(_queue_role, _queue_sub_index));
}
bool SingleUseCommandBufferContext::wait() {
    ::vk::Device dev = _engine.get().get_active_device();
    ::vk::Fence cmd_complete = _cmd_complete;
    bool status = CommandBufferContext::wait(dev);
    dev.destroy(_cmd_pool);
    dev.destroy(cmd_complete);
    _cmd_pool = nullptr;
    return status;
}

}  // namespace satr::vk
