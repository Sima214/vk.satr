#ifndef SATR_VK_FORMATS_HPP
#define SATR_VK_FORMATS_HPP
/**
 * @file
 * @brief Utilities that (de)compose vulkan data format enums.
 */

#include <Attr.hpp>
#include <vulkan/vulkan.hpp>

#include <cstdint>
#include <string>

namespace satr::vk {

enum class NumericFormat : uint8_t {
    Invalid,
    /** Integer */
    Uint,
    Sint,
    /** Floating */
    Ufloat,
    Sfloat,
    /** Integer formats with float representation */
    Unorm,
    Snorm,
    Uscaled,
    Sscaled,
    Srgb,
    /** Proprietary */
    NvI10F5
};

enum class ComponentFormatClass : uint8_t {
    Invalid,
    /** Variations of RGBA */
    R,
    RG,
    RGB,
    RGBA,
    RX,
    RXGX,
    RXGXBXAX,
    ARGB,
    ABGR,
    BGRA,
    BGR,
    GBR,
    EBGR,
    GBGR,
    BGRG,
    BXGXRXGX,
    GXBXRX,
    GXBXGXRX,
    /** Depth & Stencil */
    D,
    S,
    XD,
    DS,
    /** Compressed */
    BC,
    ETC2,
    EAC,
    ASTC,
    PVRTC
};

struct ComponentFormat {
    ComponentFormatClass type;
    uint8_t params[8];
};

packed_struct Packing {
    uint8_t m;
    uint16_t nn;
};

enum class PlanePacking : uint8_t { Undefined, _444, _422, _420 };

struct Planes {
    uint8_t n;
    PlanePacking p;
};

struct Format {
    enum class PrintStyles {
        /**
         * How the enums are formatted in the original C headers.
         */
        OGFormat,
        /**
         * C header/original formatting, but without the `VK_FORMAT_` prefix.
         */
        SnakeCase,
        /**
         * How the enums are formatted in the C++ header library.
         */
        PPFormat,
        /**
         * C++ header formatting, but without the `e` prefix.
         */
        CamelCase,
    };

    NumericFormat numeric_format;
    ComponentFormat component_format;
    bool block : 1;
    bool undefined : 1;
    uint8_t __reserved : 6;
    Packing packing;
    Planes planes;
};

NumericFormat decompose_numeric_format(::vk::Format fmt);
ComponentFormat decompose_component_format(::vk::Format fmt);
bool decompose_block(::vk::Format fmt);
Packing decompose_packing(::vk::Format fmt);
Planes decompose_planes(::vk::Format fmt);
Format decompose(::vk::Format fmt);

::vk::Format recompose(Format fmt);

std::string to_string(NumericFormat v,
                      Format::PrintStyles style = Format::PrintStyles::CamelCase);
std::string to_string(ComponentFormat v,
                      Format::PrintStyles style = Format::PrintStyles::CamelCase);
std::string to_string(Packing v, Format::PrintStyles style = Format::PrintStyles::CamelCase);
std::string to_string(Planes v, Format::PrintStyles style = Format::PrintStyles::CamelCase);
std::string to_string(Format fmt, Format::PrintStyles style = Format::PrintStyles::CamelCase);

/**
 * Returns the bit depth of the corresponding channel.
 */
int get_red_bits(ComponentFormat component_format);
int get_green_bits(ComponentFormat component_format);
int get_blue_bits(ComponentFormat component_format);
int get_alpha_bits(ComponentFormat component_format);
size_t get_bits_per_pixel(ComponentFormat fmt);

// TODO: is_format_supported_for_usage

}  // namespace satr::vk

#endif /*SATR_VK_FORMATS_HPP*/