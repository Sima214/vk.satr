#ifndef SATR_VK_TYPECONVERSIONS_HPP
#define SATR_VK_TYPECONVERSIONS_HPP
/**
 * @file
 * @brief Conversion utilities between vulkan and other types.
 */
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/vec1.hpp>
#include <vulkan/vulkan.hpp>

#include <array>
#include <cstdint>
#include <cstring>
#include <stdexcept>
#include <type_traits>

namespace satr::vk {

template<typename O> constexpr O extent_to_vec(::vk::Extent2D);
template<typename O> constexpr O extent_to_vec(::vk::Extent3D);

template<typename O> O constexpr vec_to_extent(uint32_t);
template<typename O> O constexpr vec_to_extent(glm::uvec1);
template<typename O> O constexpr vec_to_extent(glm::uvec2);
template<typename O> O constexpr vec_to_extent(glm::uvec3);

template<> constexpr uint32_t extent_to_vec(::vk::Extent2D extent) {
    return extent.width;
}
template<> constexpr uint32_t extent_to_vec(::vk::Extent3D extent) {
    return extent.width;
}
template<> constexpr glm::uvec1 extent_to_vec(::vk::Extent2D extent) {
    return glm::uvec1(extent.width);
}
template<> constexpr glm::uvec1 extent_to_vec(::vk::Extent3D extent) {
    return glm::uvec1(extent.width);
}
template<> constexpr glm::uvec2 extent_to_vec(::vk::Extent2D extent) {
    return glm::uvec2(extent.width, extent.height);
}
template<> constexpr glm::uvec2 extent_to_vec(::vk::Extent3D extent) {
    return glm::uvec2(extent.width, extent.height);
}
template<> constexpr glm::uvec3 extent_to_vec(::vk::Extent2D extent) {
    return glm::uvec3(extent.width, extent.height, 1);
}
template<> constexpr glm::uvec3 extent_to_vec(::vk::Extent3D extent) {
    return glm::uvec3(extent.width, extent.height, extent.depth);
}

template<> constexpr ::vk::Extent2D vec_to_extent(uint32_t v) {
    return ::vk::Extent2D(v, 1);
}
template<> constexpr ::vk::Extent3D vec_to_extent(uint32_t v) {
    return ::vk::Extent3D(v, 1, 1);
}
template<> constexpr ::vk::Extent2D vec_to_extent(glm::uvec1 v) {
    return ::vk::Extent2D(v.x, 1);
}
template<> constexpr ::vk::Extent3D vec_to_extent(glm::uvec1 v) {
    return ::vk::Extent3D(v.x, 1, 1);
}
template<> constexpr ::vk::Extent2D vec_to_extent(glm::uvec2 v) {
    return ::vk::Extent2D(v.x, v.y);
}
template<> constexpr ::vk::Extent3D vec_to_extent(glm::uvec2 v) {
    return ::vk::Extent3D(v.x, v.y, 1);
}
template<> constexpr ::vk::Extent2D vec_to_extent(glm::uvec3 v) {
    return ::vk::Extent2D(v.x, v.y /**, v.z */);
}
template<> constexpr ::vk::Extent3D vec_to_extent(glm::uvec3 v) {
    return ::vk::Extent3D(v.x, v.y, v.z);
}

template<typename O> constexpr O offset_to_vec(::vk::Offset2D);
template<typename O> constexpr O offset_to_vec(::vk::Offset3D);

template<typename O> constexpr O vec_to_offset(int32_t);
template<typename O> constexpr O vec_to_offset(glm::ivec1);
template<typename O> constexpr O vec_to_offset(glm::ivec2);
template<typename O> constexpr O vec_to_offset(glm::ivec3);

template<> constexpr int32_t offset_to_vec(::vk::Offset2D offset) {
    return offset.x;
}
template<> constexpr int32_t offset_to_vec(::vk::Offset3D offset) {
    return offset.x;
}
template<> constexpr glm::ivec1 offset_to_vec(::vk::Offset2D offset) {
    return glm::uvec1(offset.x);
}
template<> constexpr glm::ivec1 offset_to_vec(::vk::Offset3D offset) {
    return glm::uvec1(offset.x);
}
template<> constexpr glm::ivec2 offset_to_vec(::vk::Offset2D offset) {
    return glm::uvec2(offset.x, offset.y);
}
template<> constexpr glm::ivec2 offset_to_vec(::vk::Offset3D offset) {
    return glm::uvec2(offset.x, offset.y);
}
template<> constexpr glm::ivec3 offset_to_vec(::vk::Offset2D offset) {
    return glm::uvec3(offset.x, offset.y, 0);
}
template<> constexpr glm::ivec3 offset_to_vec(::vk::Offset3D offset) {
    return glm::uvec3(offset.x, offset.y, offset.z);
}

template<> constexpr ::vk::Offset2D vec_to_offset(int32_t v) {
    return ::vk::Offset2D(v, 0);
}
template<> constexpr ::vk::Offset3D vec_to_offset(int32_t v) {
    return ::vk::Offset3D(v, 0, 0);
}
template<> constexpr ::vk::Offset2D vec_to_offset(glm::ivec1 v) {
    return ::vk::Offset2D(v.x, 0);
}
template<> constexpr ::vk::Offset3D vec_to_offset(glm::ivec1 v) {
    return ::vk::Offset3D(v.x, 0);
}
template<> constexpr ::vk::Offset2D vec_to_offset(glm::ivec2 v) {
    return ::vk::Offset2D(v.x, v.y);
}
template<> constexpr ::vk::Offset3D vec_to_offset(glm::ivec2 v) {
    return ::vk::Offset3D(v.x, v.y, 0);
}
template<> constexpr ::vk::Offset2D vec_to_offset(glm::ivec3 v) {
    return ::vk::Offset2D(v.x, v.y /**, v.z */);
}
template<> constexpr ::vk::Offset3D vec_to_offset(glm::ivec3 v) {
    return ::vk::Offset3D(v.x, v.y, v.z);
}

constexpr std::pair<glm::ivec2, glm::uvec2> rect_to_vec(::vk::Rect2D rect) {
    return {offset_to_vec<glm::ivec2>(rect.offset), extent_to_vec<glm::uvec2>(rect.extent)};
}

constexpr ::vk::Rect2D vec_to_rect(glm::ivec2 offset, glm::uvec2 extent) {
    return ::vk::Rect2D(vec_to_offset<::vk::Offset2D>(offset),
                        vec_to_extent<::vk::Extent2D>(extent));
}

constexpr ::vk::Rect2D vec_to_rect(glm::uvec2 extent) {
    return ::vk::Rect2D({0, 0}, vec_to_extent<::vk::Extent2D>(extent));
}

template<int L, typename T, glm::qualifier Q>
constexpr std::array<T, L> vec2array(const glm::vec<L, T, Q>& v) {
    std::array<T, L> a;
    std::memcpy(a.data(), glm::value_ptr(v), sizeof(T) * L);
    return a;
}

template<int L, typename T, glm::qualifier Q>
constexpr void vec2array(const glm::vec<L, T, Q>& v, T a[L]) {
    std::memcpy(a, glm::value_ptr(v), sizeof(T) * L);
}

constexpr glm::vec3 rgb_hex_to_vec(uint32_t c) {
    uint8_t b = c & 0xff;
    uint8_t g = (c >> 8) & 0xff;
    uint8_t r = (c >> 16) & 0xff;
    return glm::vec3(((float) r) / 255.0f, ((float) g) / 255.0f, ((float) b) / 255.0f);
}

constexpr glm::vec4 rgba_hex_to_vec(uint32_t c) {
    uint8_t a = c & 0xff;
    uint8_t b = (c >> 8) & 0xff;
    uint8_t g = (c >> 16) & 0xff;
    uint8_t r = (c >> 24) & 0xff;
    return glm::vec4(((float) r) / 255.0f, ((float) g) / 255.0f, ((float) b) / 255.0f,
                     ((float) a) / 255.0f);
}

template<typename T>
constexpr std::enable_if_t<std::is_unsigned_v<T>, T> max_from_unsigned_bits(size_t n) {
    T v = 0;
    for (size_t i = 0; i < n; i++) {
        v <<= 1;
        v |= 1;
    }
    return v;
}

template<typename T>
constexpr std::enable_if_t<std::is_signed_v<T>, T> max_from_signed_bits(size_t n) {
    T v = 0;
    for (size_t i = 0; i < (n - 1); i++) {
        v <<= 1;
        v |= 1;
    }
    return v;
}

template<typename T>
constexpr std::enable_if_t<std::is_signed_v<T>, T> min_from_signed_bits(size_t n) {
    T v = 0;
    v = ~v;
    for (size_t i = 0; i < (n - 1); i++) {
        v <<= 1;
    }
    return v;
}

}  // namespace satr::vk

#endif /*SATR_VK_TYPECONVERSIONS_HPP*/