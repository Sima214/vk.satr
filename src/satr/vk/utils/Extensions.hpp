#ifndef SATR_VK_EXTENSIONS_HPP
#define SATR_VK_EXTENSIONS_HPP
/**
 * @file
 * @brief Instance and (logical) Device dynamic handling.
 */

#include <Glfw.hpp>
#include <Utils.hpp>

#include <string>
#include <vector>

namespace satr::vk {

class Extensions : spec::INonCopyable {
   protected:
    std::vector<const char*> _extension_set;

   public:
    /** Empty/default constructor. */
    constexpr Extensions() noexcept : _extension_set() {}
    /** Empty/default constructor with preallocated space. */
    Extensions(size_t n);
    /** Lifecycle constructors. */
    Extensions(const Extensions& o) noexcept;
    Extensions& operator=(const Extensions& o) noexcept;

    Extensions(Extensions&& o) noexcept;
    Extensions& operator=(Extensions&& o) noexcept;

    ~Extensions();

    /**
     * Create a new Extensions set with a single extension.
     */
    Extensions(const char* extension);
    /**
     * Create a new Extensions set with all the extensions
     * GLFW requires for window surface creation.
     */
    Extensions(satr::nui::GLFW& glfw);

    /**
     * Append a single new extension to the set.
     */
    void append(const char* extension);
    /**
     * Combine two sets of extensions.
     */
    void append(const Extensions& o);
    /**
     * Append extensions GLFW requires for window surface creation.
     */
    void append(satr::nui::GLFW& glfw);
    /**
     * Append supported extensions using vk::EnumerateInstanceExtensionProperties.
     */
    bool append_supported() {
        return append_supported(nullptr);
    }
    /**
     * Append supported extensions using vk::EnumerateInstanceExtensionProperties.
     */
    bool append_supported(const std::string& layer) {
        return append_supported(layer.c_str());
    }
    /**
     * Append supported extensions using vk::EnumerateInstanceExtensionProperties.
     */
    bool append_supported(const char* layer);

    bool contains(const std::string& extension) const {
        return contains(extension.c_str());
    }
    bool contains(const char* extension) const;

    /**
     * Perform an AND operation on this with another object.
     * In the end, this will contain only the extensions
     * that were contained on both objects.
     */
    void mask(const Extensions& o);

    /** append(...) aliases. */
    Extensions& operator+=(const Extensions& o) {
        append(o);
        return *this;
    }
    Extensions& operator+=(const char* extension) {
        append(extension);
        return *this;
    }
    Extensions& operator+=(satr::nui::GLFW& glfw) {
        append(glfw);
        return *this;
    }
    Extensions& operator&=(const Extensions& o) {
        mask(o);
        return *this;
    }

    std::pair<size_t, const char* const*> get_data() const {
        return {_extension_set.size(), _extension_set.data()};
    }

   protected:
    static bool _compare(const char* const e1, const char* const e2);
};

template<typename CTy, typename CTr>
std::basic_ostream<CTy, CTr>& operator<<(std::basic_ostream<CTy, CTr>& o, const Extensions& v) {
    auto [n, a] = v.get_data();
    o << "Extensions(";
    for (decltype(n) i = 0; i < n; i++) {
        o << a[i];
        if (i != n - 1) {
            // Append comma, unless if at last element.
            o << ", ";
        }
    }
    o << ")";
    return o;
}

}  // namespace satr::vk

#endif /*SATR_VK_EXTENSIONS_HPP*/