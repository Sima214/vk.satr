#ifndef SATR_VK_DEVICECAPABILITIES_HPP
#define SATR_VK_DEVICECAPABILITIES_HPP
/**
 * @file
 * @brief vk::PhysicalDevice interface and (high level) filtering.
 * Also uses vk::PhysicalDeviceProperties.
 */

#include <Extensions.hpp>
#include <Formats.hpp>
#include <SharedQueue.hpp>
#include <Utils.hpp>
#include <vulkan/vulkan.hpp>

#include <cstdint>
#include <optional>
#include <ostream>
#include <tuple>

#include <macros.h>

namespace satr::vk {

class DeviceCapabilities : public spec::INonCopyable {
   public:
    class Filter {
       protected:
        const uint32_t DEVICE_TYPE_MASK_OTHER = MASK_CREATE(0);
        const uint32_t DEVICE_TYPE_MASK_INTEGRATED_GPU = MASK_CREATE(1);
        const uint32_t DEVICE_TYPE_MASK_DISCRETE_GPU = MASK_CREATE(2);
        const uint32_t DEVICE_TYPE_MASK_VIRTUAL_GPU = MASK_CREATE(3);
        const uint32_t DEVICE_TYPE_MASK_CPU = MASK_CREATE(4);
        const uint32_t DEVICE_TYPE_MASK_ALL =
             DEVICE_TYPE_MASK_OTHER | DEVICE_TYPE_MASK_INTEGRATED_GPU |
             DEVICE_TYPE_MASK_DISCRETE_GPU | DEVICE_TYPE_MASK_VIRTUAL_GPU |
             DEVICE_TYPE_MASK_CPU;

       protected:
        /** Core device limits. */
        uint32_t api_version = VK_API_VERSION_1_0;
        uint32_t device_type_mask = DEVICE_TYPE_MASK_ALL;

        Extensions required_extensions;

        /** Memory limits. */
        ::vk::DeviceSize min_device_local = 0;
        ::vk::DeviceSize min_device_local_host_visible = 0;

       public:
        /** Creates an empty/default filter object. */
        Filter() = default;

        void set_min_api_version(uint32_t v) {
            api_version = v;
        };

        void set_device_type_other_allowed(bool allow) {
            MASK_SET(device_type_mask, DEVICE_TYPE_MASK_OTHER, allow);
        }
        void set_device_type_integrated_gpu_allowed(bool allow) {
            MASK_SET(device_type_mask, DEVICE_TYPE_MASK_INTEGRATED_GPU, allow);
        }
        void set_device_type_discrete_gpu_allowed(bool allow) {
            MASK_SET(device_type_mask, DEVICE_TYPE_MASK_DISCRETE_GPU, allow);
        }
        void set_device_type_virtual_gpu_allowed(bool allow) {
            MASK_SET(device_type_mask, DEVICE_TYPE_MASK_VIRTUAL_GPU, allow);
        }
        void set_device_type_cpu_allowed(bool allow) {
            MASK_SET(device_type_mask, DEVICE_TYPE_MASK_CPU, allow);
        }

        const Extensions& get_required_extensions() const {
            return required_extensions;
        }
        Extensions& get_required_extensions() {
            return required_extensions;
        }
        void append_required_extensions(const Extensions& ext) {
            required_extensions.append(ext);
        }
        void append_required_extension(const char* ext) {
            required_extensions.append(ext);
        }

        void set_min_heap_device_local(::vk::DeviceSize v) {
            min_device_local = v;
        }
        void set_min_heap_device_local_host_visible(::vk::DeviceSize v) {
            min_device_local_host_visible = v;
        }

       protected:
        bool is_device_type_accepted(::vk::PhysicalDeviceType dev_type) const;

        friend class DeviceCapabilities;
    };

    class Requests {
       protected:
        /** Features requested for logical device creation. */
        ::vk::PhysicalDeviceFeatures _features;

       public:
        /** Creates an empty `Requests` object. */
        Requests() = default;

       protected:
        const ::vk::PhysicalDeviceFeatures& get_requested_features() const {
            return _features;
        }

        friend class DeviceCapabilities;
    };

   protected:
    ::vk::PhysicalDevice _handle = nullptr;
    ::vk::PhysicalDeviceProperties _props;
    ::vk::PhysicalDeviceFeatures _features;
    ::vk::PhysicalDeviceMemoryProperties _mem_props;

    static const Extensions DEFAULT_EXTENSIONS;

   public:
    /** Object lifecycle. */
    DeviceCapabilities(::vk::PhysicalDevice phys_dev);
    ~DeviceCapabilities() = default;

    /** Invalid constructor. */
    constexpr DeviceCapabilities() = default;
    /** Move constructor/operator. */
    DeviceCapabilities(DeviceCapabilities&& o) noexcept = default;
    DeviceCapabilities& operator=(DeviceCapabilities&& o) noexcept = default;

    /** Setters/Getters */
    ::vk::PhysicalDevice get_handle() const;

    uint32_t get_api_version() const;
    uint32_t get_driver_version() const;
    uint32_t get_vendor_id() const;
    uint32_t get_device_id() const;
    ::vk::PhysicalDeviceType get_device_type() const;
    const char* get_device_name() const;
    ::vk::DeviceSize get_device_local_bytes() const;
    ::vk::DeviceSize get_device_local_host_visible_bytes() const;

    const ::vk::PhysicalDeviceLimits& get_limits() const;
    const ::vk::PhysicalDeviceSparseProperties& get_sparse_properties() const;
    const ::vk::PhysicalDeviceFeatures& get_features() const;
    const ::vk::PhysicalDeviceMemoryProperties& get_memory_properties() const;

    ::vk::FormatProperties get_format_support(::vk::Format fmt) const;
    ::vk::FormatProperties get_format_support(Format fmt) const {
        return get_format_support(recompose(fmt));
    }

    /**
     * Retrieves the list of extensions the device supports.
     */
    Extensions get_extensions(const char* layer_name) const;
    Extensions get_extensions(const std::string& layer_name) const {
        return get_extensions(layer_name.c_str());
    }
    Extensions get_extensions() const {
        return get_extensions(nullptr);
    }

    /** Supported operations. */
    bool match(const Filter& f1, const Requests& f2) const;
    std::optional<::vk::Device> create_logical(
         const Requests& filter, const Extensions& extensions,
         std::pair<uint32_t, const char* const*> layers,
         std::tuple<size_t, const QueueSlotRole*, QueueSlotIndex*> queue_slots) const;

   protected:
    ::vk::DeviceSize measure_total_heap_for_flags(::vk::MemoryPropertyFlags flags) const;

    static QueueSlotIndex select_queue_index(
         size_t slot_index,
         std::tuple<size_t, const QueueSlotRole*, const QueueSlotIndex*> queue_slots,
         const ::vk::QueueFamilyProperties* phys_dev_queue_families,
         uint32_t dedicated_graphics_family, uint32_t dedicated_transfer_family,
         uint32_t dedicated_compute_family, uint32_t* phys_dev_queue_family_allocations);
};

template<typename CTy, typename CTr>
std::basic_ostream<CTy, CTr>& operator<<(std::basic_ostream<CTy, CTr>& o,
                                         const DeviceCapabilities& v) {
    o << v.get_device_name() << "(" << v.get_api_version() << ")";
    return o;
}

}  // namespace satr::vk

#endif /*SATR_VK_DEVICECAPABILITIES_HPP*/