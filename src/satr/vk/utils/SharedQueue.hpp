#ifndef SATR_VK_SHAREDQUEUE_HPP
#define SATR_VK_SHAREDQUEUE_HPP
/**
 * @file
 * @brief Utilities to allocate and later synchronize
 * a single Vulkan Queue between multiple threads.
 */

#include <Utils.hpp>
#include <trace/Trace.hpp>
#include <vulkan/vulkan.hpp>

#include <cstdint>
#include <mutex>
#include <utility>

namespace satr::vk {

struct QueueIndex {
    uint32_t family_index;
    uint32_t index;

    QueueIndex() : family_index(-1U), index(-1U) {}
    QueueIndex(uint32_t family_index, uint32_t index) :
        family_index(family_index), index(index) {}

    bool operator==(const QueueIndex& o) const {
        return family_index == o.family_index && index == o.index;
    }
};

template<typename CTy, typename CTr>
std::basic_ostream<CTy, CTr>& operator<<(std::basic_ostream<CTy, CTr>& o, const QueueIndex& v) {
    o << v.family_index << ':' << v.index;
    return o;
}

/**
 * SlotRoles concept allows for modular and dynamic queue selection.
 */
enum class QueueSlotRole {
    Graphics,
    Compute,
    Transfer,
    Convert,
};

inline std::string to_string(QueueSlotRole v) {
    switch (v) {
        case QueueSlotRole::Graphics: {
            return "Graphics";
        } break;
        case QueueSlotRole::Compute: {
            return "Compute";
        } break;
        case QueueSlotRole::Transfer: {
            return "Transfer";
        } break;
        case QueueSlotRole::Convert: {
            return "Convert";
        } break;
        default: {
            spec::fatal("to_string: unknown value for QueueSlotRole!");
        }
    }
}
template<typename CTy, typename CTr>
std::basic_ostream<CTy, CTr>& operator<<(std::basic_ostream<CTy, CTr>& o,
                                         const QueueSlotRole& v) {
    auto str = to_string(v);
    return o << str;
}

struct QueueSlotIndex {
    /** Maps to vulkan physical device queue families. */
    QueueIndex phys;
    /** Used to index a SharedQueue from an array pool. */
    uint32_t slot_index;

    QueueSlotIndex() : phys(), slot_index(-1U) {}
    QueueSlotIndex(uint32_t family_index, uint32_t index) :
        phys(family_index, index), slot_index(-1U) {}
    QueueSlotIndex(uint32_t family_index, uint32_t index, uint32_t slot_index) :
        phys(family_index, index), slot_index(slot_index) {}
};

template<typename CTy, typename CTr>
std::basic_ostream<CTy, CTr>& operator<<(std::basic_ostream<CTy, CTr>& o,
                                         const QueueSlotIndex& v) {
    o << v.phys << '#' << v.slot_index;
    return o;
}

/**
 * Interface to vk::Queue, but CPU barriers using mutexes.
 */
class SharedQueue : public spec::INonCopyable {
   protected:
    ::vk::Queue _handle = nullptr;
    mutable std::mutex _mutex;

   public:
    /** Constructors */
    constexpr SharedQueue() = default;
    SharedQueue(SharedQueue&& o) noexcept;
    SharedQueue& operator=(SharedQueue&& o) noexcept;

    SharedQueue(::vk::Queue handle);
    SharedQueue(::vk::Device& dev, QueueIndex index);

    ~SharedQueue();

    /** Object validity check. */
    operator bool() const {
        return (bool) _handle;
    }

    /** Setters/Getters. */
    /**
     * Get the vulkan queue handle.
     * NOTE: this bypasses queue submit synchronization.
     */
    ::vk::Queue get_handle() const {
        return _handle;
    }

    /** Operations */
    bool submit(const ::vk::SubmitInfo& info, ::vk::Fence fence = {}) const;
    ::vk::Result submit(const ::vk::PresentInfoKHR& info) const;
};

}  // namespace satr::vk

#endif /*SATR_VK_SHAREDQUEUE_HPP*/
