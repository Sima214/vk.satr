#include "Extensions.hpp"

#include <trace/Trace.hpp>
#include <vulkan/vulkan_to_string.hpp>

#include <cstring>
#include <utility>

namespace satr::vk {

Extensions::Extensions(size_t n) : Extensions() {
    _extension_set.reserve(n);
}
Extensions::Extensions(const Extensions& o) noexcept : Extensions(o._extension_set.size()) {
    for (const auto& e : o._extension_set) {
        size_t l = std::strlen(e);
        char* cpy_e = new char[l + 1];
        std::memcpy(cpy_e, e, l + 1);
        _extension_set.push_back(cpy_e);
    }
}
Extensions& Extensions::operator=(const Extensions& o) noexcept {
    // Clear existing.
    for (auto& e : _extension_set) {
        delete[] e;
    }
    _extension_set.clear();
    _extension_set.reserve(o._extension_set.size());
    // Copy from other object.
    for (const auto& e : o._extension_set) {
        size_t l = std::strlen(e);
        char* cpy_e = new char[l + 1];
        std::memcpy(cpy_e, e, l + 1);
        _extension_set.push_back(cpy_e);
    }
    return *this;
}
Extensions::Extensions(Extensions&& o) noexcept : _extension_set(std::move(o._extension_set)) {}
Extensions& Extensions::operator=(Extensions&& o) noexcept {
    // Clear existing.
    for (auto& e : _extension_set) {
        delete[] e;
    }
    _extension_set.clear();
    // Move other object's raw data.
    _extension_set = std::move(o._extension_set);
    return *this;
}
Extensions::~Extensions() {
    // Clear existing.
    for (auto& e : _extension_set) {
        delete[] e;
    }
    _extension_set.clear();
}

Extensions::Extensions(const char* extension) : Extensions(1) {
    append(extension);
}
Extensions::Extensions(satr::nui::GLFW& glfw) : Extensions() {
    append(glfw);
}

void Extensions::append(const char* extension) {
    auto pos =
         std::lower_bound(_extension_set.begin(), _extension_set.end(), extension, _compare);
    if (pos == _extension_set.end() || std::strcmp(*pos, extension) != 0) {
        size_t l = std::strlen(extension);
        char* cpy_e = new char[l + 1];
        std::memcpy(cpy_e, extension, l + 1);
        _extension_set.insert(pos, cpy_e);
    }
}
void Extensions::append(const Extensions& o) {
    _extension_set.reserve(_extension_set.size() + o._extension_set.size());
    for (auto& e : o._extension_set) {
        append(e);
    }
}
void Extensions::append(satr::nui::GLFW& glfw) {
    auto glfw_extensions = glfw.get_required_instance_extensions();
    _extension_set.reserve(_extension_set.size() + glfw_extensions.size());
    for (auto& e : glfw_extensions) {
        append(e);
    }
}
bool Extensions::append_supported(const char* layer) {
    uint32_t count = 0;
    ::vk::Result r = ::vk::enumerateInstanceExtensionProperties(layer, &count, nullptr);
    if (r == ::vk::Result::eErrorLayerNotPresent) {
        spec::trace("Extensions::append_supported -> LayerNotPresent!");
        return false;
    }
    std::vector<::vk::ExtensionProperties> props(count);
    r = ::vk::enumerateInstanceExtensionProperties(layer, &count, props.data());
    if (r == ::vk::Result::eIncomplete) {
        spec::trace("Extensions::append_supported -> Incomplete.");
    }
    else if (r != ::vk::Result::eSuccess) {
        spec::trace("Extensions::append_supported -> ", to_string(r), ".");
        return false;
    }
    _extension_set.reserve(_extension_set.size() + count);
    for (uint32_t i = 0; i < count; i++) {
        append(props[i].extensionName);
    }
    return true;
}
bool Extensions::contains(const char* extension) const {
    auto pos =
         std::lower_bound(_extension_set.begin(), _extension_set.end(), extension, _compare);
    if (pos == _extension_set.end() || std::strcmp(*pos, extension) != 0) {
        return false;
    }
    return true;
}
void Extensions::mask(const Extensions& o) {
    for (size_t i = _extension_set.size(); i != 0; i--) {
        auto e = _extension_set.begin() + (i - 1);
        if (!o.contains(*e)) {
            _extension_set.erase(e);
        }
    }
}

bool Extensions::_compare(const char* const e1, const char* const e2) {
    return std::strcmp(e1, e2) < 0;
}

}  // namespace satr::vk
