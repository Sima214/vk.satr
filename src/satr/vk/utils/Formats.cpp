#include "Formats.hpp"

#include <trace/Trace.hpp>
#include <vulkan/vulkan.hpp>

#include <algorithm>
#include <cstring>
#include <stdexcept>
#include <string>

#include <macros.h>

namespace satr::vk {

NumericFormat decompose_numeric_format(::vk::Format fmt) {
    switch (fmt) {
        case ::vk::Format::eUndefined: {
            return NumericFormat::Invalid;
        }

        case ::vk::Format::eR4G4UnormPack8:
        case ::vk::Format::eR4G4B4A4UnormPack16:
        case ::vk::Format::eB4G4R4A4UnormPack16:
        case ::vk::Format::eR5G6B5UnormPack16:
        case ::vk::Format::eB5G6R5UnormPack16:
        case ::vk::Format::eR5G5B5A1UnormPack16:
        case ::vk::Format::eB5G5R5A1UnormPack16:
        case ::vk::Format::eA1R5G5B5UnormPack16:
        case ::vk::Format::eR8Unorm:
        case ::vk::Format::eR8G8Unorm:
        case ::vk::Format::eR8G8B8Unorm:
        case ::vk::Format::eB8G8R8Unorm:
        case ::vk::Format::eR8G8B8A8Unorm:
        case ::vk::Format::eB8G8R8A8Unorm:
        case ::vk::Format::eA8B8G8R8UnormPack32:
        case ::vk::Format::eA2R10G10B10UnormPack32:
        case ::vk::Format::eA2B10G10R10UnormPack32:
        case ::vk::Format::eR16Unorm:
        case ::vk::Format::eR16G16Unorm:
        case ::vk::Format::eR16G16B16Unorm:
        case ::vk::Format::eR16G16B16A16Unorm:
        case ::vk::Format::eD16Unorm:
        case ::vk::Format::eX8D24UnormPack32:
        case ::vk::Format::eD16UnormS8Uint:
        case ::vk::Format::eD24UnormS8Uint:
        case ::vk::Format::eBc1RgbUnormBlock:
        case ::vk::Format::eBc1RgbaUnormBlock:
        case ::vk::Format::eBc2UnormBlock:
        case ::vk::Format::eBc3UnormBlock:
        case ::vk::Format::eBc4UnormBlock:
        case ::vk::Format::eBc5UnormBlock:
        case ::vk::Format::eBc7UnormBlock:
        case ::vk::Format::eEtc2R8G8B8UnormBlock:
        case ::vk::Format::eEtc2R8G8B8A1UnormBlock:
        case ::vk::Format::eEtc2R8G8B8A8UnormBlock:
        case ::vk::Format::eEacR11UnormBlock:
        case ::vk::Format::eEacR11G11UnormBlock:
        case ::vk::Format::eAstc4x4UnormBlock:
        case ::vk::Format::eAstc5x4UnormBlock:
        case ::vk::Format::eAstc5x5UnormBlock:
        case ::vk::Format::eAstc6x5UnormBlock:
        case ::vk::Format::eAstc6x6UnormBlock:
        case ::vk::Format::eAstc8x5UnormBlock:
        case ::vk::Format::eAstc8x6UnormBlock:
        case ::vk::Format::eAstc8x8UnormBlock:
        case ::vk::Format::eAstc10x5UnormBlock:
        case ::vk::Format::eAstc10x6UnormBlock:
        case ::vk::Format::eAstc10x8UnormBlock:
        case ::vk::Format::eAstc10x10UnormBlock:
        case ::vk::Format::eAstc12x10UnormBlock:
        case ::vk::Format::eAstc12x12UnormBlock:
        case ::vk::Format::eG8B8G8R8422Unorm:
        case ::vk::Format::eB8G8R8G8422Unorm:
        case ::vk::Format::eG8B8R83Plane420Unorm:
        case ::vk::Format::eG8B8R82Plane420Unorm:
        case ::vk::Format::eG8B8R83Plane422Unorm:
        case ::vk::Format::eG8B8R82Plane422Unorm:
        case ::vk::Format::eG8B8R83Plane444Unorm:
        case ::vk::Format::eR10X6UnormPack16:
        case ::vk::Format::eR10X6G10X6Unorm2Pack16:
        case ::vk::Format::eR10X6G10X6B10X6A10X6Unorm4Pack16:
        case ::vk::Format::eG10X6B10X6G10X6R10X6422Unorm4Pack16:
        case ::vk::Format::eB10X6G10X6R10X6G10X6422Unorm4Pack16:
        case ::vk::Format::eG10X6B10X6R10X63Plane420Unorm3Pack16:
        case ::vk::Format::eG10X6B10X6R10X62Plane420Unorm3Pack16:
        case ::vk::Format::eG10X6B10X6R10X63Plane422Unorm3Pack16:
        case ::vk::Format::eG10X6B10X6R10X62Plane422Unorm3Pack16:
        case ::vk::Format::eG10X6B10X6R10X63Plane444Unorm3Pack16:
        case ::vk::Format::eR12X4UnormPack16:
        case ::vk::Format::eR12X4G12X4Unorm2Pack16:
        case ::vk::Format::eR12X4G12X4B12X4A12X4Unorm4Pack16:
        case ::vk::Format::eG12X4B12X4G12X4R12X4422Unorm4Pack16:
        case ::vk::Format::eB12X4G12X4R12X4G12X4422Unorm4Pack16:
        case ::vk::Format::eG12X4B12X4R12X43Plane420Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X42Plane420Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X43Plane422Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X42Plane422Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X43Plane444Unorm3Pack16:
        case ::vk::Format::eG16B16G16R16422Unorm:
        case ::vk::Format::eB16G16R16G16422Unorm:
        case ::vk::Format::eG16B16R163Plane420Unorm:
        case ::vk::Format::eG16B16R162Plane420Unorm:
        case ::vk::Format::eG16B16R163Plane422Unorm:
        case ::vk::Format::eG16B16R162Plane422Unorm:
        case ::vk::Format::eG16B16R163Plane444Unorm:
        case ::vk::Format::eG8B8R82Plane444Unorm:
        case ::vk::Format::eG10X6B10X6R10X62Plane444Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X42Plane444Unorm3Pack16:
        case ::vk::Format::eG16B16R162Plane444Unorm:
        case ::vk::Format::eA4R4G4B4UnormPack16:
        case ::vk::Format::eA4B4G4R4UnormPack16:
        case ::vk::Format::ePvrtc12BppUnormBlockIMG:
        case ::vk::Format::ePvrtc14BppUnormBlockIMG:
        case ::vk::Format::ePvrtc22BppUnormBlockIMG:
        case ::vk::Format::ePvrtc24BppUnormBlockIMG: {
            return NumericFormat::Unorm;
        }

        case ::vk::Format::eR8Snorm:
        case ::vk::Format::eR8G8Snorm:
        case ::vk::Format::eR8G8B8Snorm:
        case ::vk::Format::eB8G8R8Snorm:
        case ::vk::Format::eR8G8B8A8Snorm:
        case ::vk::Format::eB8G8R8A8Snorm:
        case ::vk::Format::eA8B8G8R8SnormPack32:
        case ::vk::Format::eA2R10G10B10SnormPack32:
        case ::vk::Format::eA2B10G10R10SnormPack32:
        case ::vk::Format::eR16Snorm:
        case ::vk::Format::eR16G16Snorm:
        case ::vk::Format::eR16G16B16Snorm:
        case ::vk::Format::eR16G16B16A16Snorm:
        case ::vk::Format::eBc4SnormBlock:
        case ::vk::Format::eBc5SnormBlock:
        case ::vk::Format::eEacR11SnormBlock:
        case ::vk::Format::eEacR11G11SnormBlock: {
            return NumericFormat::Snorm;
        }

        case ::vk::Format::eR8Uscaled:
        case ::vk::Format::eR8G8Uscaled:
        case ::vk::Format::eR8G8B8Uscaled:
        case ::vk::Format::eB8G8R8Uscaled:
        case ::vk::Format::eR8G8B8A8Uscaled:
        case ::vk::Format::eB8G8R8A8Uscaled:
        case ::vk::Format::eA8B8G8R8UscaledPack32:
        case ::vk::Format::eA2R10G10B10UscaledPack32:
        case ::vk::Format::eA2B10G10R10UscaledPack32:
        case ::vk::Format::eR16Uscaled:
        case ::vk::Format::eR16G16Uscaled:
        case ::vk::Format::eR16G16B16Uscaled:
        case ::vk::Format::eR16G16B16A16Uscaled: {
            return NumericFormat::Uscaled;
        }

        case ::vk::Format::eR8Sscaled:
        case ::vk::Format::eR8G8Sscaled:
        case ::vk::Format::eR8G8B8Sscaled:
        case ::vk::Format::eB8G8R8Sscaled:
        case ::vk::Format::eR8G8B8A8Sscaled:
        case ::vk::Format::eB8G8R8A8Sscaled:
        case ::vk::Format::eA8B8G8R8SscaledPack32:
        case ::vk::Format::eA2R10G10B10SscaledPack32:
        case ::vk::Format::eA2B10G10R10SscaledPack32:
        case ::vk::Format::eR16Sscaled:
        case ::vk::Format::eR16G16Sscaled:
        case ::vk::Format::eR16G16B16Sscaled:
        case ::vk::Format::eR16G16B16A16Sscaled: {
            return NumericFormat::Sscaled;
        }

        case ::vk::Format::eR8Uint:
        case ::vk::Format::eR8G8Uint:
        case ::vk::Format::eR8G8B8Uint:
        case ::vk::Format::eB8G8R8Uint:
        case ::vk::Format::eR8G8B8A8Uint:
        case ::vk::Format::eB8G8R8A8Uint:
        case ::vk::Format::eA8B8G8R8UintPack32:
        case ::vk::Format::eA2R10G10B10UintPack32:
        case ::vk::Format::eA2B10G10R10UintPack32:
        case ::vk::Format::eR16Uint:
        case ::vk::Format::eR16G16Uint:
        case ::vk::Format::eR16G16B16Uint:
        case ::vk::Format::eR16G16B16A16Uint:
        case ::vk::Format::eR32Uint:
        case ::vk::Format::eR32G32Uint:
        case ::vk::Format::eR32G32B32Uint:
        case ::vk::Format::eR32G32B32A32Uint:
        case ::vk::Format::eR64Uint:
        case ::vk::Format::eR64G64Uint:
        case ::vk::Format::eR64G64B64Uint:
        case ::vk::Format::eR64G64B64A64Uint:
        case ::vk::Format::eS8Uint: {
            return NumericFormat::Uint;
        }

        case ::vk::Format::eR8Sint:
        case ::vk::Format::eR8G8Sint:
        case ::vk::Format::eR8G8B8Sint:
        case ::vk::Format::eB8G8R8Sint:
        case ::vk::Format::eR8G8B8A8Sint:
        case ::vk::Format::eB8G8R8A8Sint:
        case ::vk::Format::eA8B8G8R8SintPack32:
        case ::vk::Format::eA2R10G10B10SintPack32:
        case ::vk::Format::eA2B10G10R10SintPack32:
        case ::vk::Format::eR16Sint:
        case ::vk::Format::eR16G16Sint:
        case ::vk::Format::eR16G16B16Sint:
        case ::vk::Format::eR16G16B16A16Sint:
        case ::vk::Format::eR32Sint:
        case ::vk::Format::eR32G32Sint:
        case ::vk::Format::eR32G32B32Sint:
        case ::vk::Format::eR32G32B32A32Sint:
        case ::vk::Format::eR64Sint:
        case ::vk::Format::eR64G64Sint:
        case ::vk::Format::eR64G64B64Sint:
        case ::vk::Format::eR64G64B64A64Sint: {
            return NumericFormat::Sint;
        }

        case ::vk::Format::eB10G11R11UfloatPack32:
        case ::vk::Format::eE5B9G9R9UfloatPack32:
        case ::vk::Format::eBc6HUfloatBlock: {
            return NumericFormat::Ufloat;
        }

        case ::vk::Format::eD32SfloatS8Uint:
        case ::vk::Format::eR16Sfloat:
        case ::vk::Format::eR16G16Sfloat:
        case ::vk::Format::eR16G16B16Sfloat:
        case ::vk::Format::eR16G16B16A16Sfloat:
        case ::vk::Format::eR32Sfloat:
        case ::vk::Format::eR32G32Sfloat:
        case ::vk::Format::eR32G32B32Sfloat:
        case ::vk::Format::eR32G32B32A32Sfloat:
        case ::vk::Format::eR64Sfloat:
        case ::vk::Format::eR64G64Sfloat:
        case ::vk::Format::eR64G64B64Sfloat:
        case ::vk::Format::eR64G64B64A64Sfloat:
        case ::vk::Format::eD32Sfloat:
        case ::vk::Format::eBc6HSfloatBlock:
        case ::vk::Format::eAstc4x4SfloatBlock:
        case ::vk::Format::eAstc5x4SfloatBlock:
        case ::vk::Format::eAstc5x5SfloatBlock:
        case ::vk::Format::eAstc6x5SfloatBlock:
        case ::vk::Format::eAstc6x6SfloatBlock:
        case ::vk::Format::eAstc8x5SfloatBlock:
        case ::vk::Format::eAstc8x6SfloatBlock:
        case ::vk::Format::eAstc8x8SfloatBlock:
        case ::vk::Format::eAstc10x5SfloatBlock:
        case ::vk::Format::eAstc10x6SfloatBlock:
        case ::vk::Format::eAstc10x8SfloatBlock:
        case ::vk::Format::eAstc10x10SfloatBlock:
        case ::vk::Format::eAstc12x10SfloatBlock:
        case ::vk::Format::eAstc12x12SfloatBlock: {
            return NumericFormat::Sfloat;
        }

        case ::vk::Format::eR8Srgb:
        case ::vk::Format::eR8G8Srgb:
        case ::vk::Format::eR8G8B8Srgb:
        case ::vk::Format::eB8G8R8Srgb:
        case ::vk::Format::eR8G8B8A8Srgb:
        case ::vk::Format::eB8G8R8A8Srgb:
        case ::vk::Format::eA8B8G8R8SrgbPack32:
        case ::vk::Format::eBc1RgbSrgbBlock:
        case ::vk::Format::eBc1RgbaSrgbBlock:
        case ::vk::Format::eBc2SrgbBlock:
        case ::vk::Format::eBc3SrgbBlock:
        case ::vk::Format::eBc7SrgbBlock:
        case ::vk::Format::eEtc2R8G8B8SrgbBlock:
        case ::vk::Format::eEtc2R8G8B8A1SrgbBlock:
        case ::vk::Format::eEtc2R8G8B8A8SrgbBlock:
        case ::vk::Format::eAstc4x4SrgbBlock:
        case ::vk::Format::eAstc5x4SrgbBlock:
        case ::vk::Format::eAstc5x5SrgbBlock:
        case ::vk::Format::eAstc6x5SrgbBlock:
        case ::vk::Format::eAstc6x6SrgbBlock:
        case ::vk::Format::eAstc8x5SrgbBlock:
        case ::vk::Format::eAstc8x6SrgbBlock:
        case ::vk::Format::eAstc8x8SrgbBlock:
        case ::vk::Format::eAstc10x5SrgbBlock:
        case ::vk::Format::eAstc10x6SrgbBlock:
        case ::vk::Format::eAstc10x8SrgbBlock:
        case ::vk::Format::eAstc10x10SrgbBlock:
        case ::vk::Format::eAstc12x10SrgbBlock:
        case ::vk::Format::eAstc12x12SrgbBlock:
        case ::vk::Format::ePvrtc12BppSrgbBlockIMG:
        case ::vk::Format::ePvrtc14BppSrgbBlockIMG:
        case ::vk::Format::ePvrtc22BppSrgbBlockIMG:
        case ::vk::Format::ePvrtc24BppSrgbBlockIMG: {
            return NumericFormat::Srgb;
        }

        case ::vk::Format::eR16G16S105NV: {
            return NumericFormat::NvI10F5;
        }
    }

    spec::trace("decompose_numeric_format encountered unknown vulkan format value: ",
                static_cast<int64_t>(fmt), "!");
    return NumericFormat::Invalid;
}

ComponentFormat decompose_component_format(::vk::Format fmt) {
    switch (fmt) {
        case ::vk::Format::eUndefined: {
            return {ComponentFormatClass::Invalid, {}};
        }
        case ::vk::Format::eR8Unorm:
        case ::vk::Format::eR8Snorm:
        case ::vk::Format::eR8Uscaled:
        case ::vk::Format::eR8Sscaled:
        case ::vk::Format::eR8Uint:
        case ::vk::Format::eR8Sint:
        case ::vk::Format::eR8Srgb: {
            return {ComponentFormatClass::R, {8}};
        }
        case ::vk::Format::eR16Unorm:
        case ::vk::Format::eR16Snorm:
        case ::vk::Format::eR16Uscaled:
        case ::vk::Format::eR16Sscaled:
        case ::vk::Format::eR16Uint:
        case ::vk::Format::eR16Sint:
        case ::vk::Format::eR16Sfloat: {
            return {ComponentFormatClass::R, {16}};
        }
        case ::vk::Format::eR32Uint:
        case ::vk::Format::eR32Sint:
        case ::vk::Format::eR32Sfloat: {
            return {ComponentFormatClass::R, {32}};
        }
        case ::vk::Format::eR64Uint:
        case ::vk::Format::eR64Sint:
        case ::vk::Format::eR64Sfloat: {
            return {ComponentFormatClass::R, {64}};
        }

        case ::vk::Format::eR4G4UnormPack8: {
            return {ComponentFormatClass::RG, {4, 4}};
        }
        case ::vk::Format::eR8G8Unorm:
        case ::vk::Format::eR8G8Snorm:
        case ::vk::Format::eR8G8Uscaled:
        case ::vk::Format::eR8G8Sscaled:
        case ::vk::Format::eR8G8Uint:
        case ::vk::Format::eR8G8Sint:
        case ::vk::Format::eR8G8Srgb: {
            return {ComponentFormatClass::RG, {8, 8}};
        }
        case ::vk::Format::eR16G16Unorm:
        case ::vk::Format::eR16G16Snorm:
        case ::vk::Format::eR16G16Uscaled:
        case ::vk::Format::eR16G16Sscaled:
        case ::vk::Format::eR16G16Uint:
        case ::vk::Format::eR16G16Sint:
        case ::vk::Format::eR16G16Sfloat:
        case ::vk::Format::eR16G16S105NV: {
            return {ComponentFormatClass::RG, {16, 16}};
        }
        case ::vk::Format::eR32G32Uint:
        case ::vk::Format::eR32G32Sint:
        case ::vk::Format::eR32G32Sfloat: {
            return {ComponentFormatClass::RG, {32, 32}};
        }
        case ::vk::Format::eR64G64Uint:
        case ::vk::Format::eR64G64Sint:
        case ::vk::Format::eR64G64Sfloat: {
            return {ComponentFormatClass::RG, {64, 64}};
        }

        case ::vk::Format::eR5G6B5UnormPack16: {
            return {ComponentFormatClass::RGB, {5, 6, 5}};
        }
        case ::vk::Format::eR8G8B8Unorm:
        case ::vk::Format::eR8G8B8Snorm:
        case ::vk::Format::eR8G8B8Uscaled:
        case ::vk::Format::eR8G8B8Sscaled:
        case ::vk::Format::eR8G8B8Uint:
        case ::vk::Format::eR8G8B8Sint:
        case ::vk::Format::eR8G8B8Srgb: {
            return {ComponentFormatClass::RGB, {8, 8, 8}};
        }
        case ::vk::Format::eR16G16B16Unorm:
        case ::vk::Format::eR16G16B16Snorm:
        case ::vk::Format::eR16G16B16Uscaled:
        case ::vk::Format::eR16G16B16Sscaled:
        case ::vk::Format::eR16G16B16Uint:
        case ::vk::Format::eR16G16B16Sint:
        case ::vk::Format::eR16G16B16Sfloat: {
            return {ComponentFormatClass::RGB, {16, 16, 16}};
        }
        case ::vk::Format::eR32G32B32Uint:
        case ::vk::Format::eR32G32B32Sint:
        case ::vk::Format::eR32G32B32Sfloat: {
            return {ComponentFormatClass::RGB, {32, 32, 32}};
        }
        case ::vk::Format::eR64G64B64Uint:
        case ::vk::Format::eR64G64B64Sint:
        case ::vk::Format::eR64G64B64Sfloat: {
            return {ComponentFormatClass::RGB, {64, 64, 64}};
        }

        case ::vk::Format::eR4G4B4A4UnormPack16: {
            return {ComponentFormatClass::RGBA, {4, 4, 4, 4}};
        }
        case ::vk::Format::eR5G5B5A1UnormPack16: {
            return {ComponentFormatClass::RGBA, {5, 5, 5, 1}};
        }
        case ::vk::Format::eR8G8B8A8Unorm:
        case ::vk::Format::eR8G8B8A8Snorm:
        case ::vk::Format::eR8G8B8A8Uscaled:
        case ::vk::Format::eR8G8B8A8Sscaled:
        case ::vk::Format::eR8G8B8A8Uint:
        case ::vk::Format::eR8G8B8A8Sint:
        case ::vk::Format::eR8G8B8A8Srgb: {
            return {ComponentFormatClass::RGBA, {8, 8, 8, 8}};
        }
        case ::vk::Format::eR16G16B16A16Unorm:
        case ::vk::Format::eR16G16B16A16Snorm:
        case ::vk::Format::eR16G16B16A16Uscaled:
        case ::vk::Format::eR16G16B16A16Sscaled:
        case ::vk::Format::eR16G16B16A16Uint:
        case ::vk::Format::eR16G16B16A16Sint:
        case ::vk::Format::eR16G16B16A16Sfloat: {
            return {ComponentFormatClass::RGBA, {16, 16, 16, 16}};
        }
        case ::vk::Format::eR32G32B32A32Uint:
        case ::vk::Format::eR32G32B32A32Sint:
        case ::vk::Format::eR32G32B32A32Sfloat: {
            return {ComponentFormatClass::RGBA, {32, 32, 32, 32}};
        }
        case ::vk::Format::eR64G64B64A64Uint:
        case ::vk::Format::eR64G64B64A64Sint:
        case ::vk::Format::eR64G64B64A64Sfloat: {
            return {ComponentFormatClass::RGBA, {64, 64, 64, 64}};
        }

        case ::vk::Format::eR10X6UnormPack16: {
            return {ComponentFormatClass::RX, {10, 6}};
        }
        case ::vk::Format::eR12X4UnormPack16: {
            return {ComponentFormatClass::RX, {12, 4}};
        }

        case ::vk::Format::eR10X6G10X6Unorm2Pack16: {
            return {ComponentFormatClass::RXGX, {10, 6, 10, 6}};
        }
        case ::vk::Format::eR12X4G12X4Unorm2Pack16: {
            return {ComponentFormatClass::RXGX, {12, 4, 12, 4}};
        }

        case ::vk::Format::eR10X6G10X6B10X6A10X6Unorm4Pack16: {
            return {ComponentFormatClass::RXGXBXAX, {10, 6, 10, 6, 10, 6, 10, 6}};
        }
        case ::vk::Format::eR12X4G12X4B12X4A12X4Unorm4Pack16: {
            return {ComponentFormatClass::RXGXBXAX, {12, 4, 12, 4, 12, 4, 12, 4}};
        }

        case ::vk::Format::eA1R5G5B5UnormPack16: {
            return {ComponentFormatClass::ARGB, {1, 5, 5, 5}};
        }
        case ::vk::Format::eA4R4G4B4UnormPack16: {
            return {ComponentFormatClass::ARGB, {4, 4, 4, 4}};
        }
        case ::vk::Format::eA2R10G10B10UnormPack32:
        case ::vk::Format::eA2R10G10B10SnormPack32:
        case ::vk::Format::eA2R10G10B10UscaledPack32:
        case ::vk::Format::eA2R10G10B10SscaledPack32:
        case ::vk::Format::eA2R10G10B10UintPack32:
        case ::vk::Format::eA2R10G10B10SintPack32: {
            return {ComponentFormatClass::ARGB, {2, 10, 10, 10}};
        }

        case ::vk::Format::eA4B4G4R4UnormPack16: {
            return {ComponentFormatClass::ABGR, {4, 4, 4, 4}};
        }
        case ::vk::Format::eA8B8G8R8UnormPack32:
        case ::vk::Format::eA8B8G8R8SnormPack32:
        case ::vk::Format::eA8B8G8R8UscaledPack32:
        case ::vk::Format::eA8B8G8R8SscaledPack32:
        case ::vk::Format::eA8B8G8R8UintPack32:
        case ::vk::Format::eA8B8G8R8SintPack32:
        case ::vk::Format::eA8B8G8R8SrgbPack32: {
            return {ComponentFormatClass::ABGR, {8, 8, 8, 8}};
        }
        case ::vk::Format::eA2B10G10R10UnormPack32:
        case ::vk::Format::eA2B10G10R10SnormPack32:
        case ::vk::Format::eA2B10G10R10UscaledPack32:
        case ::vk::Format::eA2B10G10R10SscaledPack32:
        case ::vk::Format::eA2B10G10R10UintPack32:
        case ::vk::Format::eA2B10G10R10SintPack32: {
            return {ComponentFormatClass::ABGR, {2, 10, 10, 10}};
        }

        case ::vk::Format::eB4G4R4A4UnormPack16: {
            return {ComponentFormatClass::BGRA, {4, 4, 4, 4}};
        }
        case ::vk::Format::eB5G5R5A1UnormPack16: {
            return {ComponentFormatClass::BGRA, {5, 5, 5, 1}};
        }
        case ::vk::Format::eB8G8R8A8Unorm:
        case ::vk::Format::eB8G8R8A8Snorm:
        case ::vk::Format::eB8G8R8A8Uscaled:
        case ::vk::Format::eB8G8R8A8Sscaled:
        case ::vk::Format::eB8G8R8A8Uint:
        case ::vk::Format::eB8G8R8A8Sint:
        case ::vk::Format::eB8G8R8A8Srgb: {
            return {ComponentFormatClass::BGRA, {8, 8, 8, 8}};
        }

        case ::vk::Format::eB5G6R5UnormPack16: {
            return {ComponentFormatClass::BGR, {5, 6, 5}};
        }
        case ::vk::Format::eB8G8R8Unorm:
        case ::vk::Format::eB8G8R8Snorm:
        case ::vk::Format::eB8G8R8Uscaled:
        case ::vk::Format::eB8G8R8Sscaled:
        case ::vk::Format::eB8G8R8Uint:
        case ::vk::Format::eB8G8R8Sint:
        case ::vk::Format::eB8G8R8Srgb: {
            return {ComponentFormatClass::BGR, {8, 8, 8}};
        }
        case ::vk::Format::eB10G11R11UfloatPack32: {
            return {ComponentFormatClass::BGR, {10, 11, 11}};
        }

        case ::vk::Format::eG8B8R82Plane420Unorm:
        case ::vk::Format::eG8B8R82Plane422Unorm:
        case ::vk::Format::eG8B8R82Plane444Unorm:
        case ::vk::Format::eG8B8R83Plane420Unorm:
        case ::vk::Format::eG8B8R83Plane422Unorm:
        case ::vk::Format::eG8B8R83Plane444Unorm: {
            return {ComponentFormatClass::GBR, {8, 8, 8}};
        }
        case ::vk::Format::eG16B16R162Plane420Unorm:
        case ::vk::Format::eG16B16R162Plane422Unorm:
        case ::vk::Format::eG16B16R162Plane444Unorm:
        case ::vk::Format::eG16B16R163Plane420Unorm:
        case ::vk::Format::eG16B16R163Plane422Unorm:
        case ::vk::Format::eG16B16R163Plane444Unorm: {
            return {ComponentFormatClass::GBR, {16, 16, 16}};
        }

        case ::vk::Format::eE5B9G9R9UfloatPack32: {
            return {ComponentFormatClass::EBGR, {5, 9, 9, 9}};
        }

        case ::vk::Format::eG8B8G8R8422Unorm: {
            return {ComponentFormatClass::GBGR, {8, 8, 8, 8}};
        }
        case ::vk::Format::eG16B16G16R16422Unorm: {
            return {ComponentFormatClass::GBGR, {16, 16, 16, 16}};
        }

        case ::vk::Format::eB8G8R8G8422Unorm: {
            return {ComponentFormatClass::BGRG, {8, 8, 8, 8}};
        }
        case ::vk::Format::eB16G16R16G16422Unorm: {
            return {ComponentFormatClass::BGRG, {16, 16, 16, 16}};
        }

        case ::vk::Format::eB10X6G10X6R10X6G10X6422Unorm4Pack16: {
            return {ComponentFormatClass::BXGXRXGX, {10, 6, 10, 6, 10, 6, 10, 6}};
        }
        case ::vk::Format::eB12X4G12X4R12X4G12X4422Unorm4Pack16: {
            return {ComponentFormatClass::BXGXRXGX, {12, 4, 12, 4, 12, 4, 12, 4}};
        }

        case ::vk::Format::eG10X6B10X6R10X62Plane420Unorm3Pack16:
        case ::vk::Format::eG10X6B10X6R10X62Plane422Unorm3Pack16:
        case ::vk::Format::eG10X6B10X6R10X62Plane444Unorm3Pack16:
        case ::vk::Format::eG10X6B10X6R10X63Plane420Unorm3Pack16:
        case ::vk::Format::eG10X6B10X6R10X63Plane422Unorm3Pack16:
        case ::vk::Format::eG10X6B10X6R10X63Plane444Unorm3Pack16: {
            return {ComponentFormatClass::GXBXRX, {10, 6, 10, 6, 10, 6}};
        }
        case ::vk::Format::eG12X4B12X4R12X42Plane420Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X42Plane422Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X42Plane444Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X43Plane420Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X43Plane422Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X43Plane444Unorm3Pack16: {
            return {ComponentFormatClass::GXBXRX, {12, 4, 12, 4, 12, 4}};
        }

        case ::vk::Format::eG10X6B10X6G10X6R10X6422Unorm4Pack16: {
            return {ComponentFormatClass::GXBXGXRX, {10, 6, 10, 6, 10, 6, 10, 6}};
        }
        case ::vk::Format::eG12X4B12X4G12X4R12X4422Unorm4Pack16: {
            return {ComponentFormatClass::GXBXGXRX, {12, 4, 12, 4, 12, 4, 12, 4}};
        }

        case ::vk::Format::eD16Unorm: {
            return {ComponentFormatClass::D, {16}};
        }
        case ::vk::Format::eD32Sfloat: {
            return {ComponentFormatClass::D, {32}};
        }

        case ::vk::Format::eS8Uint: {
            return {ComponentFormatClass::S, {8}};
        }

        case ::vk::Format::eX8D24UnormPack32: {
            return {ComponentFormatClass::XD, {8, 24}};
        }

        case ::vk::Format::eD16UnormS8Uint: {
            return {ComponentFormatClass::DS, {16, 8}};
        }
        case ::vk::Format::eD24UnormS8Uint: {
            return {ComponentFormatClass::DS, {24, 8}};
        }
        case ::vk::Format::eD32SfloatS8Uint: {
            return {ComponentFormatClass::DS, {32, 8}};
        }

        case ::vk::Format::eBc1RgbUnormBlock:
        case ::vk::Format::eBc1RgbSrgbBlock: {
            return {ComponentFormatClass::BC, {1, (uint8_t) ComponentFormatClass::RGB}};
        }
        case ::vk::Format::eBc1RgbaUnormBlock:
        case ::vk::Format::eBc1RgbaSrgbBlock: {
            return {ComponentFormatClass::BC, {1, (uint8_t) ComponentFormatClass::RGBA}};
        }
        case ::vk::Format::eBc2UnormBlock:
        case ::vk::Format::eBc2SrgbBlock: {
            return {ComponentFormatClass::BC, {2}};
        }
        case ::vk::Format::eBc3UnormBlock:
        case ::vk::Format::eBc3SrgbBlock: {
            return {ComponentFormatClass::BC, {3}};
        }
        case ::vk::Format::eBc4UnormBlock:
        case ::vk::Format::eBc4SnormBlock: {
            return {ComponentFormatClass::BC, {4}};
        }
        case ::vk::Format::eBc5UnormBlock:
        case ::vk::Format::eBc5SnormBlock: {
            return {ComponentFormatClass::BC, {5}};
        }
        case ::vk::Format::eBc6HUfloatBlock:
        case ::vk::Format::eBc6HSfloatBlock: {
            return {ComponentFormatClass::BC, {6}};
        }
        case ::vk::Format::eBc7UnormBlock:
        case ::vk::Format::eBc7SrgbBlock: {
            return {ComponentFormatClass::BC, {7}};
        }

        case ::vk::Format::eEtc2R8G8B8UnormBlock:
        case ::vk::Format::eEtc2R8G8B8SrgbBlock: {
            return {ComponentFormatClass::ETC2, {(uint8_t) ComponentFormatClass::RGB, 8, 8, 8}};
        }
        case ::vk::Format::eEtc2R8G8B8A1UnormBlock:
        case ::vk::Format::eEtc2R8G8B8A1SrgbBlock: {
            return {ComponentFormatClass::ETC2,
                    {(uint8_t) ComponentFormatClass::RGBA, 8, 8, 8, 1}};
        }
        case ::vk::Format::eEtc2R8G8B8A8UnormBlock:
        case ::vk::Format::eEtc2R8G8B8A8SrgbBlock: {
            return {ComponentFormatClass::ETC2,
                    {(uint8_t) ComponentFormatClass::RGBA, 8, 8, 8, 8}};
        }

        case ::vk::Format::eEacR11UnormBlock:
        case ::vk::Format::eEacR11SnormBlock: {
            return {ComponentFormatClass::EAC, {(uint8_t) ComponentFormatClass::R, 11}};
        }
        case ::vk::Format::eEacR11G11UnormBlock:
        case ::vk::Format::eEacR11G11SnormBlock: {
            return {ComponentFormatClass::EAC, {(uint8_t) ComponentFormatClass::RG, 11, 11}};
        }

        case ::vk::Format::eAstc4x4UnormBlock:
        case ::vk::Format::eAstc4x4SrgbBlock:
        case ::vk::Format::eAstc4x4SfloatBlock: {
            return {ComponentFormatClass::ASTC, {4, 4}};
        }
        case ::vk::Format::eAstc5x4UnormBlock:
        case ::vk::Format::eAstc5x4SrgbBlock:
        case ::vk::Format::eAstc5x4SfloatBlock: {
            return {ComponentFormatClass::ASTC, {5, 4}};
        }
        case ::vk::Format::eAstc5x5UnormBlock:
        case ::vk::Format::eAstc5x5SrgbBlock:
        case ::vk::Format::eAstc5x5SfloatBlock: {
            return {ComponentFormatClass::ASTC, {5, 5}};
        }
        case ::vk::Format::eAstc6x5UnormBlock:
        case ::vk::Format::eAstc6x5SrgbBlock:
        case ::vk::Format::eAstc6x5SfloatBlock: {
            return {ComponentFormatClass::ASTC, {6, 5}};
        }
        case ::vk::Format::eAstc6x6UnormBlock:
        case ::vk::Format::eAstc6x6SrgbBlock:
        case ::vk::Format::eAstc6x6SfloatBlock: {
            return {ComponentFormatClass::ASTC, {6, 6}};
        }
        case ::vk::Format::eAstc8x5UnormBlock:
        case ::vk::Format::eAstc8x5SrgbBlock:
        case ::vk::Format::eAstc8x5SfloatBlock: {
            return {ComponentFormatClass::ASTC, {8, 5}};
        }
        case ::vk::Format::eAstc8x6UnormBlock:
        case ::vk::Format::eAstc8x6SrgbBlock:
        case ::vk::Format::eAstc8x6SfloatBlock: {
            return {ComponentFormatClass::ASTC, {8, 6}};
        }
        case ::vk::Format::eAstc8x8UnormBlock:
        case ::vk::Format::eAstc8x8SrgbBlock:
        case ::vk::Format::eAstc8x8SfloatBlock: {
            return {ComponentFormatClass::ASTC, {8, 8}};
        }
        case ::vk::Format::eAstc10x5UnormBlock:
        case ::vk::Format::eAstc10x5SrgbBlock:
        case ::vk::Format::eAstc10x5SfloatBlock: {
            return {ComponentFormatClass::ASTC, {10, 5}};
        }
        case ::vk::Format::eAstc10x6UnormBlock:
        case ::vk::Format::eAstc10x6SrgbBlock:
        case ::vk::Format::eAstc10x6SfloatBlock: {
            return {ComponentFormatClass::ASTC, {10, 6}};
        }
        case ::vk::Format::eAstc10x8UnormBlock:
        case ::vk::Format::eAstc10x8SrgbBlock:
        case ::vk::Format::eAstc10x8SfloatBlock: {
            return {ComponentFormatClass::ASTC, {10, 8}};
        }
        case ::vk::Format::eAstc10x10UnormBlock:
        case ::vk::Format::eAstc10x10SrgbBlock:
        case ::vk::Format::eAstc10x10SfloatBlock: {
            return {ComponentFormatClass::ASTC, {10, 10}};
        }
        case ::vk::Format::eAstc12x10UnormBlock:
        case ::vk::Format::eAstc12x10SrgbBlock:
        case ::vk::Format::eAstc12x10SfloatBlock: {
            return {ComponentFormatClass::ASTC, {12, 10}};
        }
        case ::vk::Format::eAstc12x12UnormBlock:
        case ::vk::Format::eAstc12x12SrgbBlock:
        case ::vk::Format::eAstc12x12SfloatBlock: {
            return {ComponentFormatClass::ASTC, {12, 12}};
        }

        case ::vk::Format::ePvrtc12BppUnormBlockIMG:
        case ::vk::Format::ePvrtc12BppSrgbBlockIMG: {
            return {ComponentFormatClass::PVRTC, {1, 2}};
        }
        case ::vk::Format::ePvrtc14BppUnormBlockIMG:
        case ::vk::Format::ePvrtc14BppSrgbBlockIMG: {
            return {ComponentFormatClass::PVRTC, {1, 4}};
        }
        case ::vk::Format::ePvrtc22BppUnormBlockIMG:
        case ::vk::Format::ePvrtc22BppSrgbBlockIMG: {
            return {ComponentFormatClass::PVRTC, {2, 2}};
        }
        case ::vk::Format::ePvrtc24BppUnormBlockIMG:
        case ::vk::Format::ePvrtc24BppSrgbBlockIMG: {
            return {ComponentFormatClass::PVRTC, {2, 4}};
        };
    }

    spec::trace("decompose_component_format encountered unknown vulkan format value: ",
                static_cast<int64_t>(fmt), "!");
    return {ComponentFormatClass::Invalid, {}};
}

bool decompose_block(::vk::Format fmt) {
    switch (fmt) {
        case ::vk::Format::eUndefined:
        case ::vk::Format::eR4G4UnormPack8:
        case ::vk::Format::eR4G4B4A4UnormPack16:
        case ::vk::Format::eB4G4R4A4UnormPack16:
        case ::vk::Format::eR5G6B5UnormPack16:
        case ::vk::Format::eB5G6R5UnormPack16:
        case ::vk::Format::eR5G5B5A1UnormPack16:
        case ::vk::Format::eB5G5R5A1UnormPack16:
        case ::vk::Format::eA1R5G5B5UnormPack16:
        case ::vk::Format::eR8Unorm:
        case ::vk::Format::eR8Snorm:
        case ::vk::Format::eR8Uscaled:
        case ::vk::Format::eR8Sscaled:
        case ::vk::Format::eR8Uint:
        case ::vk::Format::eR8Sint:
        case ::vk::Format::eR8Srgb:
        case ::vk::Format::eR8G8Unorm:
        case ::vk::Format::eR8G8Snorm:
        case ::vk::Format::eR8G8Uscaled:
        case ::vk::Format::eR8G8Sscaled:
        case ::vk::Format::eR8G8Uint:
        case ::vk::Format::eR8G8Sint:
        case ::vk::Format::eR8G8Srgb:
        case ::vk::Format::eR8G8B8Unorm:
        case ::vk::Format::eR8G8B8Snorm:
        case ::vk::Format::eR8G8B8Uscaled:
        case ::vk::Format::eR8G8B8Sscaled:
        case ::vk::Format::eR8G8B8Uint:
        case ::vk::Format::eR8G8B8Sint:
        case ::vk::Format::eR8G8B8Srgb:
        case ::vk::Format::eB8G8R8Unorm:
        case ::vk::Format::eB8G8R8Snorm:
        case ::vk::Format::eB8G8R8Uscaled:
        case ::vk::Format::eB8G8R8Sscaled:
        case ::vk::Format::eB8G8R8Uint:
        case ::vk::Format::eB8G8R8Sint:
        case ::vk::Format::eB8G8R8Srgb:
        case ::vk::Format::eR8G8B8A8Unorm:
        case ::vk::Format::eR8G8B8A8Snorm:
        case ::vk::Format::eR8G8B8A8Uscaled:
        case ::vk::Format::eR8G8B8A8Sscaled:
        case ::vk::Format::eR8G8B8A8Uint:
        case ::vk::Format::eR8G8B8A8Sint:
        case ::vk::Format::eR8G8B8A8Srgb:
        case ::vk::Format::eB8G8R8A8Unorm:
        case ::vk::Format::eB8G8R8A8Snorm:
        case ::vk::Format::eB8G8R8A8Uscaled:
        case ::vk::Format::eB8G8R8A8Sscaled:
        case ::vk::Format::eB8G8R8A8Uint:
        case ::vk::Format::eB8G8R8A8Sint:
        case ::vk::Format::eB8G8R8A8Srgb:
        case ::vk::Format::eA8B8G8R8UnormPack32:
        case ::vk::Format::eA8B8G8R8SnormPack32:
        case ::vk::Format::eA8B8G8R8UscaledPack32:
        case ::vk::Format::eA8B8G8R8SscaledPack32:
        case ::vk::Format::eA8B8G8R8UintPack32:
        case ::vk::Format::eA8B8G8R8SintPack32:
        case ::vk::Format::eA8B8G8R8SrgbPack32:
        case ::vk::Format::eA2R10G10B10UnormPack32:
        case ::vk::Format::eA2R10G10B10SnormPack32:
        case ::vk::Format::eA2R10G10B10UscaledPack32:
        case ::vk::Format::eA2R10G10B10SscaledPack32:
        case ::vk::Format::eA2R10G10B10UintPack32:
        case ::vk::Format::eA2R10G10B10SintPack32:
        case ::vk::Format::eA2B10G10R10UnormPack32:
        case ::vk::Format::eA2B10G10R10SnormPack32:
        case ::vk::Format::eA2B10G10R10UscaledPack32:
        case ::vk::Format::eA2B10G10R10SscaledPack32:
        case ::vk::Format::eA2B10G10R10UintPack32:
        case ::vk::Format::eA2B10G10R10SintPack32:
        case ::vk::Format::eR16Unorm:
        case ::vk::Format::eR16Snorm:
        case ::vk::Format::eR16Uscaled:
        case ::vk::Format::eR16Sscaled:
        case ::vk::Format::eR16Uint:
        case ::vk::Format::eR16Sint:
        case ::vk::Format::eR16Sfloat:
        case ::vk::Format::eR16G16Unorm:
        case ::vk::Format::eR16G16Snorm:
        case ::vk::Format::eR16G16Uscaled:
        case ::vk::Format::eR16G16Sscaled:
        case ::vk::Format::eR16G16Uint:
        case ::vk::Format::eR16G16Sint:
        case ::vk::Format::eR16G16Sfloat:
        case ::vk::Format::eR16G16B16Unorm:
        case ::vk::Format::eR16G16B16Snorm:
        case ::vk::Format::eR16G16B16Uscaled:
        case ::vk::Format::eR16G16B16Sscaled:
        case ::vk::Format::eR16G16B16Uint:
        case ::vk::Format::eR16G16B16Sint:
        case ::vk::Format::eR16G16B16Sfloat:
        case ::vk::Format::eR16G16B16A16Unorm:
        case ::vk::Format::eR16G16B16A16Snorm:
        case ::vk::Format::eR16G16B16A16Uscaled:
        case ::vk::Format::eR16G16B16A16Sscaled:
        case ::vk::Format::eR16G16B16A16Uint:
        case ::vk::Format::eR16G16B16A16Sint:
        case ::vk::Format::eR16G16B16A16Sfloat:
        case ::vk::Format::eR32Uint:
        case ::vk::Format::eR32Sint:
        case ::vk::Format::eR32Sfloat:
        case ::vk::Format::eR32G32Uint:
        case ::vk::Format::eR32G32Sint:
        case ::vk::Format::eR32G32Sfloat:
        case ::vk::Format::eR32G32B32Uint:
        case ::vk::Format::eR32G32B32Sint:
        case ::vk::Format::eR32G32B32Sfloat:
        case ::vk::Format::eR32G32B32A32Uint:
        case ::vk::Format::eR32G32B32A32Sint:
        case ::vk::Format::eR32G32B32A32Sfloat:
        case ::vk::Format::eR64Uint:
        case ::vk::Format::eR64Sint:
        case ::vk::Format::eR64Sfloat:
        case ::vk::Format::eR64G64Uint:
        case ::vk::Format::eR64G64Sint:
        case ::vk::Format::eR64G64Sfloat:
        case ::vk::Format::eR64G64B64Uint:
        case ::vk::Format::eR64G64B64Sint:
        case ::vk::Format::eR64G64B64Sfloat:
        case ::vk::Format::eR64G64B64A64Uint:
        case ::vk::Format::eR64G64B64A64Sint:
        case ::vk::Format::eR64G64B64A64Sfloat:
        case ::vk::Format::eB10G11R11UfloatPack32:
        case ::vk::Format::eE5B9G9R9UfloatPack32:
        case ::vk::Format::eD16Unorm:
        case ::vk::Format::eX8D24UnormPack32:
        case ::vk::Format::eD32Sfloat:
        case ::vk::Format::eS8Uint:
        case ::vk::Format::eD16UnormS8Uint:
        case ::vk::Format::eD24UnormS8Uint:
        case ::vk::Format::eD32SfloatS8Uint:
        case ::vk::Format::eG8B8G8R8422Unorm:
        case ::vk::Format::eB8G8R8G8422Unorm:
        case ::vk::Format::eG8B8R83Plane420Unorm:
        case ::vk::Format::eG8B8R82Plane420Unorm:
        case ::vk::Format::eG8B8R83Plane422Unorm:
        case ::vk::Format::eG8B8R82Plane422Unorm:
        case ::vk::Format::eG8B8R83Plane444Unorm:
        case ::vk::Format::eR10X6UnormPack16:
        case ::vk::Format::eR10X6G10X6Unorm2Pack16:
        case ::vk::Format::eR10X6G10X6B10X6A10X6Unorm4Pack16:
        case ::vk::Format::eG10X6B10X6G10X6R10X6422Unorm4Pack16:
        case ::vk::Format::eB10X6G10X6R10X6G10X6422Unorm4Pack16:
        case ::vk::Format::eG10X6B10X6R10X63Plane420Unorm3Pack16:
        case ::vk::Format::eG10X6B10X6R10X62Plane420Unorm3Pack16:
        case ::vk::Format::eG10X6B10X6R10X63Plane422Unorm3Pack16:
        case ::vk::Format::eG10X6B10X6R10X62Plane422Unorm3Pack16:
        case ::vk::Format::eG10X6B10X6R10X63Plane444Unorm3Pack16:
        case ::vk::Format::eR12X4UnormPack16:
        case ::vk::Format::eR12X4G12X4Unorm2Pack16:
        case ::vk::Format::eR12X4G12X4B12X4A12X4Unorm4Pack16:
        case ::vk::Format::eG12X4B12X4G12X4R12X4422Unorm4Pack16:
        case ::vk::Format::eB12X4G12X4R12X4G12X4422Unorm4Pack16:
        case ::vk::Format::eG12X4B12X4R12X43Plane420Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X42Plane420Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X43Plane422Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X42Plane422Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X43Plane444Unorm3Pack16:
        case ::vk::Format::eG16B16G16R16422Unorm:
        case ::vk::Format::eB16G16R16G16422Unorm:
        case ::vk::Format::eG16B16R163Plane420Unorm:
        case ::vk::Format::eG16B16R162Plane420Unorm:
        case ::vk::Format::eG16B16R163Plane422Unorm:
        case ::vk::Format::eG16B16R162Plane422Unorm:
        case ::vk::Format::eG16B16R163Plane444Unorm:
        case ::vk::Format::eG8B8R82Plane444Unorm:
        case ::vk::Format::eG10X6B10X6R10X62Plane444Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X42Plane444Unorm3Pack16:
        case ::vk::Format::eG16B16R162Plane444Unorm:
        case ::vk::Format::eA4R4G4B4UnormPack16:
        case ::vk::Format::eA4B4G4R4UnormPack16:
        case ::vk::Format::eR16G16S105NV: {
            return false;
        }
        case ::vk::Format::eBc1RgbUnormBlock:
        case ::vk::Format::eBc1RgbSrgbBlock:
        case ::vk::Format::eBc1RgbaUnormBlock:
        case ::vk::Format::eBc1RgbaSrgbBlock:
        case ::vk::Format::eBc2UnormBlock:
        case ::vk::Format::eBc2SrgbBlock:
        case ::vk::Format::eBc3UnormBlock:
        case ::vk::Format::eBc3SrgbBlock:
        case ::vk::Format::eBc4UnormBlock:
        case ::vk::Format::eBc4SnormBlock:
        case ::vk::Format::eBc5UnormBlock:
        case ::vk::Format::eBc5SnormBlock:
        case ::vk::Format::eBc6HUfloatBlock:
        case ::vk::Format::eBc6HSfloatBlock:
        case ::vk::Format::eBc7UnormBlock:
        case ::vk::Format::eBc7SrgbBlock:
        case ::vk::Format::eEtc2R8G8B8UnormBlock:
        case ::vk::Format::eEtc2R8G8B8SrgbBlock:
        case ::vk::Format::eEtc2R8G8B8A1UnormBlock:
        case ::vk::Format::eEtc2R8G8B8A1SrgbBlock:
        case ::vk::Format::eEtc2R8G8B8A8UnormBlock:
        case ::vk::Format::eEtc2R8G8B8A8SrgbBlock:
        case ::vk::Format::eEacR11UnormBlock:
        case ::vk::Format::eEacR11SnormBlock:
        case ::vk::Format::eEacR11G11UnormBlock:
        case ::vk::Format::eEacR11G11SnormBlock:
        case ::vk::Format::eAstc4x4UnormBlock:
        case ::vk::Format::eAstc4x4SrgbBlock:
        case ::vk::Format::eAstc5x4UnormBlock:
        case ::vk::Format::eAstc5x4SrgbBlock:
        case ::vk::Format::eAstc5x5UnormBlock:
        case ::vk::Format::eAstc5x5SrgbBlock:
        case ::vk::Format::eAstc6x5UnormBlock:
        case ::vk::Format::eAstc6x5SrgbBlock:
        case ::vk::Format::eAstc6x6UnormBlock:
        case ::vk::Format::eAstc6x6SrgbBlock:
        case ::vk::Format::eAstc8x5UnormBlock:
        case ::vk::Format::eAstc8x5SrgbBlock:
        case ::vk::Format::eAstc8x6UnormBlock:
        case ::vk::Format::eAstc8x6SrgbBlock:
        case ::vk::Format::eAstc8x8UnormBlock:
        case ::vk::Format::eAstc8x8SrgbBlock:
        case ::vk::Format::eAstc10x5UnormBlock:
        case ::vk::Format::eAstc10x5SrgbBlock:
        case ::vk::Format::eAstc10x6UnormBlock:
        case ::vk::Format::eAstc10x6SrgbBlock:
        case ::vk::Format::eAstc10x8UnormBlock:
        case ::vk::Format::eAstc10x8SrgbBlock:
        case ::vk::Format::eAstc10x10UnormBlock:
        case ::vk::Format::eAstc10x10SrgbBlock:
        case ::vk::Format::eAstc12x10UnormBlock:
        case ::vk::Format::eAstc12x10SrgbBlock:
        case ::vk::Format::eAstc12x12UnormBlock:
        case ::vk::Format::eAstc12x12SrgbBlock:
        case ::vk::Format::eAstc4x4SfloatBlock:
        case ::vk::Format::eAstc5x4SfloatBlock:
        case ::vk::Format::eAstc5x5SfloatBlock:
        case ::vk::Format::eAstc6x5SfloatBlock:
        case ::vk::Format::eAstc6x6SfloatBlock:
        case ::vk::Format::eAstc8x5SfloatBlock:
        case ::vk::Format::eAstc8x6SfloatBlock:
        case ::vk::Format::eAstc8x8SfloatBlock:
        case ::vk::Format::eAstc10x5SfloatBlock:
        case ::vk::Format::eAstc10x6SfloatBlock:
        case ::vk::Format::eAstc10x8SfloatBlock:
        case ::vk::Format::eAstc10x10SfloatBlock:
        case ::vk::Format::eAstc12x10SfloatBlock:
        case ::vk::Format::eAstc12x12SfloatBlock:
        case ::vk::Format::ePvrtc12BppUnormBlockIMG:
        case ::vk::Format::ePvrtc14BppUnormBlockIMG:
        case ::vk::Format::ePvrtc22BppUnormBlockIMG:
        case ::vk::Format::ePvrtc24BppUnormBlockIMG:
        case ::vk::Format::ePvrtc12BppSrgbBlockIMG:
        case ::vk::Format::ePvrtc14BppSrgbBlockIMG:
        case ::vk::Format::ePvrtc22BppSrgbBlockIMG:
        case ::vk::Format::ePvrtc24BppSrgbBlockIMG: {
            return true;
        }
    }

    spec::trace("decompose_block encountered unknown vulkan format value: ",
                static_cast<int64_t>(fmt), "!");
    return false;
}

Packing decompose_packing(::vk::Format fmt) {
    switch (fmt) {
        case ::vk::Format::eUndefined:
        case ::vk::Format::eR8Unorm:
        case ::vk::Format::eR8Snorm:
        case ::vk::Format::eR8Uscaled:
        case ::vk::Format::eR8Sscaled:
        case ::vk::Format::eR8Uint:
        case ::vk::Format::eR8Sint:
        case ::vk::Format::eR8Srgb:
        case ::vk::Format::eR8G8Unorm:
        case ::vk::Format::eR8G8Snorm:
        case ::vk::Format::eR8G8Uscaled:
        case ::vk::Format::eR8G8Sscaled:
        case ::vk::Format::eR8G8Uint:
        case ::vk::Format::eR8G8Sint:
        case ::vk::Format::eR8G8Srgb:
        case ::vk::Format::eR8G8B8Unorm:
        case ::vk::Format::eR8G8B8Snorm:
        case ::vk::Format::eR8G8B8Uscaled:
        case ::vk::Format::eR8G8B8Sscaled:
        case ::vk::Format::eR8G8B8Uint:
        case ::vk::Format::eR8G8B8Sint:
        case ::vk::Format::eR8G8B8Srgb:
        case ::vk::Format::eB8G8R8Unorm:
        case ::vk::Format::eB8G8R8Snorm:
        case ::vk::Format::eB8G8R8Uscaled:
        case ::vk::Format::eB8G8R8Sscaled:
        case ::vk::Format::eB8G8R8Uint:
        case ::vk::Format::eB8G8R8Sint:
        case ::vk::Format::eB8G8R8Srgb:
        case ::vk::Format::eR8G8B8A8Unorm:
        case ::vk::Format::eR8G8B8A8Snorm:
        case ::vk::Format::eR8G8B8A8Uscaled:
        case ::vk::Format::eR8G8B8A8Sscaled:
        case ::vk::Format::eR8G8B8A8Uint:
        case ::vk::Format::eR8G8B8A8Sint:
        case ::vk::Format::eR8G8B8A8Srgb:
        case ::vk::Format::eB8G8R8A8Unorm:
        case ::vk::Format::eB8G8R8A8Snorm:
        case ::vk::Format::eB8G8R8A8Uscaled:
        case ::vk::Format::eB8G8R8A8Sscaled:
        case ::vk::Format::eB8G8R8A8Uint:
        case ::vk::Format::eB8G8R8A8Sint:
        case ::vk::Format::eB8G8R8A8Srgb:
        case ::vk::Format::eR16Unorm:
        case ::vk::Format::eR16Snorm:
        case ::vk::Format::eR16Uscaled:
        case ::vk::Format::eR16Sscaled:
        case ::vk::Format::eR16Uint:
        case ::vk::Format::eR16Sint:
        case ::vk::Format::eR16Sfloat:
        case ::vk::Format::eR16G16Unorm:
        case ::vk::Format::eR16G16Snorm:
        case ::vk::Format::eR16G16Uscaled:
        case ::vk::Format::eR16G16Sscaled:
        case ::vk::Format::eR16G16Uint:
        case ::vk::Format::eR16G16Sint:
        case ::vk::Format::eR16G16Sfloat:
        case ::vk::Format::eR16G16B16Unorm:
        case ::vk::Format::eR16G16B16Snorm:
        case ::vk::Format::eR16G16B16Uscaled:
        case ::vk::Format::eR16G16B16Sscaled:
        case ::vk::Format::eR16G16B16Uint:
        case ::vk::Format::eR16G16B16Sint:
        case ::vk::Format::eR16G16B16Sfloat:
        case ::vk::Format::eR16G16B16A16Unorm:
        case ::vk::Format::eR16G16B16A16Snorm:
        case ::vk::Format::eR16G16B16A16Uscaled:
        case ::vk::Format::eR16G16B16A16Sscaled:
        case ::vk::Format::eR16G16B16A16Uint:
        case ::vk::Format::eR16G16B16A16Sint:
        case ::vk::Format::eR16G16B16A16Sfloat:
        case ::vk::Format::eR32Uint:
        case ::vk::Format::eR32Sint:
        case ::vk::Format::eR32Sfloat:
        case ::vk::Format::eR32G32Uint:
        case ::vk::Format::eR32G32Sint:
        case ::vk::Format::eR32G32Sfloat:
        case ::vk::Format::eR32G32B32Uint:
        case ::vk::Format::eR32G32B32Sint:
        case ::vk::Format::eR32G32B32Sfloat:
        case ::vk::Format::eR32G32B32A32Uint:
        case ::vk::Format::eR32G32B32A32Sint:
        case ::vk::Format::eR32G32B32A32Sfloat:
        case ::vk::Format::eR64Uint:
        case ::vk::Format::eR64Sint:
        case ::vk::Format::eR64Sfloat:
        case ::vk::Format::eR64G64Uint:
        case ::vk::Format::eR64G64Sint:
        case ::vk::Format::eR64G64Sfloat:
        case ::vk::Format::eR64G64B64Uint:
        case ::vk::Format::eR64G64B64Sint:
        case ::vk::Format::eR64G64B64Sfloat:
        case ::vk::Format::eR64G64B64A64Uint:
        case ::vk::Format::eR64G64B64A64Sint:
        case ::vk::Format::eR64G64B64A64Sfloat:
        case ::vk::Format::eD16Unorm:
        case ::vk::Format::eD32Sfloat:
        case ::vk::Format::eS8Uint:
        case ::vk::Format::eD16UnormS8Uint:
        case ::vk::Format::eD24UnormS8Uint:
        case ::vk::Format::eD32SfloatS8Uint:
        case ::vk::Format::eBc1RgbUnormBlock:
        case ::vk::Format::eBc1RgbSrgbBlock:
        case ::vk::Format::eBc1RgbaUnormBlock:
        case ::vk::Format::eBc1RgbaSrgbBlock:
        case ::vk::Format::eBc2UnormBlock:
        case ::vk::Format::eBc2SrgbBlock:
        case ::vk::Format::eBc3UnormBlock:
        case ::vk::Format::eBc3SrgbBlock:
        case ::vk::Format::eBc4UnormBlock:
        case ::vk::Format::eBc4SnormBlock:
        case ::vk::Format::eBc5UnormBlock:
        case ::vk::Format::eBc5SnormBlock:
        case ::vk::Format::eBc6HUfloatBlock:
        case ::vk::Format::eBc6HSfloatBlock:
        case ::vk::Format::eBc7UnormBlock:
        case ::vk::Format::eBc7SrgbBlock:
        case ::vk::Format::eEtc2R8G8B8UnormBlock:
        case ::vk::Format::eEtc2R8G8B8SrgbBlock:
        case ::vk::Format::eEtc2R8G8B8A1UnormBlock:
        case ::vk::Format::eEtc2R8G8B8A1SrgbBlock:
        case ::vk::Format::eEtc2R8G8B8A8UnormBlock:
        case ::vk::Format::eEtc2R8G8B8A8SrgbBlock:
        case ::vk::Format::eEacR11UnormBlock:
        case ::vk::Format::eEacR11SnormBlock:
        case ::vk::Format::eEacR11G11UnormBlock:
        case ::vk::Format::eEacR11G11SnormBlock:
        case ::vk::Format::eAstc4x4UnormBlock:
        case ::vk::Format::eAstc4x4SrgbBlock:
        case ::vk::Format::eAstc5x4UnormBlock:
        case ::vk::Format::eAstc5x4SrgbBlock:
        case ::vk::Format::eAstc5x5UnormBlock:
        case ::vk::Format::eAstc5x5SrgbBlock:
        case ::vk::Format::eAstc6x5UnormBlock:
        case ::vk::Format::eAstc6x5SrgbBlock:
        case ::vk::Format::eAstc6x6UnormBlock:
        case ::vk::Format::eAstc6x6SrgbBlock:
        case ::vk::Format::eAstc8x5UnormBlock:
        case ::vk::Format::eAstc8x5SrgbBlock:
        case ::vk::Format::eAstc8x6UnormBlock:
        case ::vk::Format::eAstc8x6SrgbBlock:
        case ::vk::Format::eAstc8x8UnormBlock:
        case ::vk::Format::eAstc8x8SrgbBlock:
        case ::vk::Format::eAstc10x5UnormBlock:
        case ::vk::Format::eAstc10x5SrgbBlock:
        case ::vk::Format::eAstc10x6UnormBlock:
        case ::vk::Format::eAstc10x6SrgbBlock:
        case ::vk::Format::eAstc10x8UnormBlock:
        case ::vk::Format::eAstc10x8SrgbBlock:
        case ::vk::Format::eAstc10x10UnormBlock:
        case ::vk::Format::eAstc10x10SrgbBlock:
        case ::vk::Format::eAstc12x10UnormBlock:
        case ::vk::Format::eAstc12x10SrgbBlock:
        case ::vk::Format::eAstc12x12UnormBlock:
        case ::vk::Format::eAstc12x12SrgbBlock:
        case ::vk::Format::eG8B8G8R8422Unorm:
        case ::vk::Format::eB8G8R8G8422Unorm:
        case ::vk::Format::eG8B8R83Plane420Unorm:
        case ::vk::Format::eG8B8R82Plane420Unorm:
        case ::vk::Format::eG8B8R83Plane422Unorm:
        case ::vk::Format::eG8B8R82Plane422Unorm:
        case ::vk::Format::eG8B8R83Plane444Unorm:
        case ::vk::Format::eG16B16G16R16422Unorm:
        case ::vk::Format::eB16G16R16G16422Unorm:
        case ::vk::Format::eG16B16R163Plane420Unorm:
        case ::vk::Format::eG16B16R162Plane420Unorm:
        case ::vk::Format::eG16B16R163Plane422Unorm:
        case ::vk::Format::eG16B16R162Plane422Unorm:
        case ::vk::Format::eG16B16R163Plane444Unorm:
        case ::vk::Format::eG8B8R82Plane444Unorm:
        case ::vk::Format::eG16B16R162Plane444Unorm:
        case ::vk::Format::eAstc4x4SfloatBlock:
        case ::vk::Format::eAstc5x4SfloatBlock:
        case ::vk::Format::eAstc5x5SfloatBlock:
        case ::vk::Format::eAstc6x5SfloatBlock:
        case ::vk::Format::eAstc6x6SfloatBlock:
        case ::vk::Format::eAstc8x5SfloatBlock:
        case ::vk::Format::eAstc8x6SfloatBlock:
        case ::vk::Format::eAstc8x8SfloatBlock:
        case ::vk::Format::eAstc10x5SfloatBlock:
        case ::vk::Format::eAstc10x6SfloatBlock:
        case ::vk::Format::eAstc10x8SfloatBlock:
        case ::vk::Format::eAstc10x10SfloatBlock:
        case ::vk::Format::eAstc12x10SfloatBlock:
        case ::vk::Format::eAstc12x12SfloatBlock:
        case ::vk::Format::ePvrtc12BppUnormBlockIMG:
        case ::vk::Format::ePvrtc14BppUnormBlockIMG:
        case ::vk::Format::ePvrtc22BppUnormBlockIMG:
        case ::vk::Format::ePvrtc24BppUnormBlockIMG:
        case ::vk::Format::ePvrtc12BppSrgbBlockIMG:
        case ::vk::Format::ePvrtc14BppSrgbBlockIMG:
        case ::vk::Format::ePvrtc22BppSrgbBlockIMG:
        case ::vk::Format::ePvrtc24BppSrgbBlockIMG:
        case ::vk::Format::eR16G16S105NV: {
            return {0, 0};
        }

        case ::vk::Format::eR4G4UnormPack8: {
            return {1, 8};
        }

        case ::vk::Format::eR4G4B4A4UnormPack16:
        case ::vk::Format::eB4G4R4A4UnormPack16:
        case ::vk::Format::eR5G6B5UnormPack16:
        case ::vk::Format::eB5G6R5UnormPack16:
        case ::vk::Format::eR5G5B5A1UnormPack16:
        case ::vk::Format::eB5G5R5A1UnormPack16:
        case ::vk::Format::eA1R5G5B5UnormPack16:
        case ::vk::Format::eR10X6UnormPack16:
        case ::vk::Format::eR12X4UnormPack16:
        case ::vk::Format::eA4R4G4B4UnormPack16:
        case ::vk::Format::eA4B4G4R4UnormPack16: {
            return {1, 16};
        }

        case ::vk::Format::eR10X6G10X6Unorm2Pack16:
        case ::vk::Format::eR12X4G12X4Unorm2Pack16: {
            return {2, 16};
        }

        case ::vk::Format::eG10X6B10X6R10X63Plane420Unorm3Pack16:
        case ::vk::Format::eG10X6B10X6R10X62Plane420Unorm3Pack16:
        case ::vk::Format::eG10X6B10X6R10X63Plane422Unorm3Pack16:
        case ::vk::Format::eG10X6B10X6R10X62Plane422Unorm3Pack16:
        case ::vk::Format::eG10X6B10X6R10X63Plane444Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X43Plane420Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X42Plane420Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X43Plane422Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X42Plane422Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X43Plane444Unorm3Pack16:
        case ::vk::Format::eG10X6B10X6R10X62Plane444Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X42Plane444Unorm3Pack16: {
            return {3, 16};
        }

        case ::vk::Format::eR10X6G10X6B10X6A10X6Unorm4Pack16:
        case ::vk::Format::eG10X6B10X6G10X6R10X6422Unorm4Pack16:
        case ::vk::Format::eB10X6G10X6R10X6G10X6422Unorm4Pack16:
        case ::vk::Format::eR12X4G12X4B12X4A12X4Unorm4Pack16:
        case ::vk::Format::eG12X4B12X4G12X4R12X4422Unorm4Pack16:
        case ::vk::Format::eB12X4G12X4R12X4G12X4422Unorm4Pack16: {
            return {4, 16};
        }

        case ::vk::Format::eA8B8G8R8UnormPack32:
        case ::vk::Format::eA8B8G8R8SnormPack32:
        case ::vk::Format::eA8B8G8R8UscaledPack32:
        case ::vk::Format::eA8B8G8R8SscaledPack32:
        case ::vk::Format::eA8B8G8R8UintPack32:
        case ::vk::Format::eA8B8G8R8SintPack32:
        case ::vk::Format::eA8B8G8R8SrgbPack32:
        case ::vk::Format::eA2R10G10B10UnormPack32:
        case ::vk::Format::eA2R10G10B10SnormPack32:
        case ::vk::Format::eA2R10G10B10UscaledPack32:
        case ::vk::Format::eA2R10G10B10SscaledPack32:
        case ::vk::Format::eA2R10G10B10UintPack32:
        case ::vk::Format::eA2R10G10B10SintPack32:
        case ::vk::Format::eA2B10G10R10UnormPack32:
        case ::vk::Format::eA2B10G10R10SnormPack32:
        case ::vk::Format::eA2B10G10R10UscaledPack32:
        case ::vk::Format::eA2B10G10R10SscaledPack32:
        case ::vk::Format::eA2B10G10R10UintPack32:
        case ::vk::Format::eA2B10G10R10SintPack32:
        case ::vk::Format::eB10G11R11UfloatPack32:
        case ::vk::Format::eE5B9G9R9UfloatPack32:
        case ::vk::Format::eX8D24UnormPack32: {
            return {1, 32};
        }
    }

    spec::trace("decompose_packing encountered unknown vulkan format value: ",
                static_cast<int64_t>(fmt), "!");
    return {0, 0};
}

Planes decompose_planes(::vk::Format fmt) {
    switch (fmt) {
        case ::vk::Format::eUndefined:
        case ::vk::Format::eR4G4UnormPack8:
        case ::vk::Format::eR4G4B4A4UnormPack16:
        case ::vk::Format::eB4G4R4A4UnormPack16:
        case ::vk::Format::eR5G6B5UnormPack16:
        case ::vk::Format::eB5G6R5UnormPack16:
        case ::vk::Format::eR5G5B5A1UnormPack16:
        case ::vk::Format::eB5G5R5A1UnormPack16:
        case ::vk::Format::eA1R5G5B5UnormPack16:
        case ::vk::Format::eR8Unorm:
        case ::vk::Format::eR8Snorm:
        case ::vk::Format::eR8Uscaled:
        case ::vk::Format::eR8Sscaled:
        case ::vk::Format::eR8Uint:
        case ::vk::Format::eR8Sint:
        case ::vk::Format::eR8Srgb:
        case ::vk::Format::eR8G8Unorm:
        case ::vk::Format::eR8G8Snorm:
        case ::vk::Format::eR8G8Uscaled:
        case ::vk::Format::eR8G8Sscaled:
        case ::vk::Format::eR8G8Uint:
        case ::vk::Format::eR8G8Sint:
        case ::vk::Format::eR8G8Srgb:
        case ::vk::Format::eR8G8B8Unorm:
        case ::vk::Format::eR8G8B8Snorm:
        case ::vk::Format::eR8G8B8Uscaled:
        case ::vk::Format::eR8G8B8Sscaled:
        case ::vk::Format::eR8G8B8Uint:
        case ::vk::Format::eR8G8B8Sint:
        case ::vk::Format::eR8G8B8Srgb:
        case ::vk::Format::eB8G8R8Unorm:
        case ::vk::Format::eB8G8R8Snorm:
        case ::vk::Format::eB8G8R8Uscaled:
        case ::vk::Format::eB8G8R8Sscaled:
        case ::vk::Format::eB8G8R8Uint:
        case ::vk::Format::eB8G8R8Sint:
        case ::vk::Format::eB8G8R8Srgb:
        case ::vk::Format::eR8G8B8A8Unorm:
        case ::vk::Format::eR8G8B8A8Snorm:
        case ::vk::Format::eR8G8B8A8Uscaled:
        case ::vk::Format::eR8G8B8A8Sscaled:
        case ::vk::Format::eR8G8B8A8Uint:
        case ::vk::Format::eR8G8B8A8Sint:
        case ::vk::Format::eR8G8B8A8Srgb:
        case ::vk::Format::eB8G8R8A8Unorm:
        case ::vk::Format::eB8G8R8A8Snorm:
        case ::vk::Format::eB8G8R8A8Uscaled:
        case ::vk::Format::eB8G8R8A8Sscaled:
        case ::vk::Format::eB8G8R8A8Uint:
        case ::vk::Format::eB8G8R8A8Sint:
        case ::vk::Format::eB8G8R8A8Srgb:
        case ::vk::Format::eA8B8G8R8UnormPack32:
        case ::vk::Format::eA8B8G8R8SnormPack32:
        case ::vk::Format::eA8B8G8R8UscaledPack32:
        case ::vk::Format::eA8B8G8R8SscaledPack32:
        case ::vk::Format::eA8B8G8R8UintPack32:
        case ::vk::Format::eA8B8G8R8SintPack32:
        case ::vk::Format::eA8B8G8R8SrgbPack32:
        case ::vk::Format::eA2R10G10B10UnormPack32:
        case ::vk::Format::eA2R10G10B10SnormPack32:
        case ::vk::Format::eA2R10G10B10UscaledPack32:
        case ::vk::Format::eA2R10G10B10SscaledPack32:
        case ::vk::Format::eA2R10G10B10UintPack32:
        case ::vk::Format::eA2R10G10B10SintPack32:
        case ::vk::Format::eA2B10G10R10UnormPack32:
        case ::vk::Format::eA2B10G10R10SnormPack32:
        case ::vk::Format::eA2B10G10R10UscaledPack32:
        case ::vk::Format::eA2B10G10R10SscaledPack32:
        case ::vk::Format::eA2B10G10R10UintPack32:
        case ::vk::Format::eA2B10G10R10SintPack32:
        case ::vk::Format::eR16Unorm:
        case ::vk::Format::eR16Snorm:
        case ::vk::Format::eR16Uscaled:
        case ::vk::Format::eR16Sscaled:
        case ::vk::Format::eR16Uint:
        case ::vk::Format::eR16Sint:
        case ::vk::Format::eR16Sfloat:
        case ::vk::Format::eR16G16Unorm:
        case ::vk::Format::eR16G16Snorm:
        case ::vk::Format::eR16G16Uscaled:
        case ::vk::Format::eR16G16Sscaled:
        case ::vk::Format::eR16G16Uint:
        case ::vk::Format::eR16G16Sint:
        case ::vk::Format::eR16G16Sfloat:
        case ::vk::Format::eR16G16B16Unorm:
        case ::vk::Format::eR16G16B16Snorm:
        case ::vk::Format::eR16G16B16Uscaled:
        case ::vk::Format::eR16G16B16Sscaled:
        case ::vk::Format::eR16G16B16Uint:
        case ::vk::Format::eR16G16B16Sint:
        case ::vk::Format::eR16G16B16Sfloat:
        case ::vk::Format::eR16G16B16A16Unorm:
        case ::vk::Format::eR16G16B16A16Snorm:
        case ::vk::Format::eR16G16B16A16Uscaled:
        case ::vk::Format::eR16G16B16A16Sscaled:
        case ::vk::Format::eR16G16B16A16Uint:
        case ::vk::Format::eR16G16B16A16Sint:
        case ::vk::Format::eR16G16B16A16Sfloat:
        case ::vk::Format::eR32Uint:
        case ::vk::Format::eR32Sint:
        case ::vk::Format::eR32Sfloat:
        case ::vk::Format::eR32G32Uint:
        case ::vk::Format::eR32G32Sint:
        case ::vk::Format::eR32G32Sfloat:
        case ::vk::Format::eR32G32B32Uint:
        case ::vk::Format::eR32G32B32Sint:
        case ::vk::Format::eR32G32B32Sfloat:
        case ::vk::Format::eR32G32B32A32Uint:
        case ::vk::Format::eR32G32B32A32Sint:
        case ::vk::Format::eR32G32B32A32Sfloat:
        case ::vk::Format::eR64Uint:
        case ::vk::Format::eR64Sint:
        case ::vk::Format::eR64Sfloat:
        case ::vk::Format::eR64G64Uint:
        case ::vk::Format::eR64G64Sint:
        case ::vk::Format::eR64G64Sfloat:
        case ::vk::Format::eR64G64B64Uint:
        case ::vk::Format::eR64G64B64Sint:
        case ::vk::Format::eR64G64B64Sfloat:
        case ::vk::Format::eR64G64B64A64Uint:
        case ::vk::Format::eR64G64B64A64Sint:
        case ::vk::Format::eR64G64B64A64Sfloat:
        case ::vk::Format::eB10G11R11UfloatPack32:
        case ::vk::Format::eE5B9G9R9UfloatPack32:
        case ::vk::Format::eD16Unorm:
        case ::vk::Format::eX8D24UnormPack32:
        case ::vk::Format::eD32Sfloat:
        case ::vk::Format::eS8Uint:
        case ::vk::Format::eD16UnormS8Uint:
        case ::vk::Format::eD24UnormS8Uint:
        case ::vk::Format::eD32SfloatS8Uint:
        case ::vk::Format::eBc1RgbUnormBlock:
        case ::vk::Format::eBc1RgbSrgbBlock:
        case ::vk::Format::eBc1RgbaUnormBlock:
        case ::vk::Format::eBc1RgbaSrgbBlock:
        case ::vk::Format::eBc2UnormBlock:
        case ::vk::Format::eBc2SrgbBlock:
        case ::vk::Format::eBc3UnormBlock:
        case ::vk::Format::eBc3SrgbBlock:
        case ::vk::Format::eBc4UnormBlock:
        case ::vk::Format::eBc4SnormBlock:
        case ::vk::Format::eBc5UnormBlock:
        case ::vk::Format::eBc5SnormBlock:
        case ::vk::Format::eBc6HUfloatBlock:
        case ::vk::Format::eBc6HSfloatBlock:
        case ::vk::Format::eBc7UnormBlock:
        case ::vk::Format::eBc7SrgbBlock:
        case ::vk::Format::eEtc2R8G8B8UnormBlock:
        case ::vk::Format::eEtc2R8G8B8SrgbBlock:
        case ::vk::Format::eEtc2R8G8B8A1UnormBlock:
        case ::vk::Format::eEtc2R8G8B8A1SrgbBlock:
        case ::vk::Format::eEtc2R8G8B8A8UnormBlock:
        case ::vk::Format::eEtc2R8G8B8A8SrgbBlock:
        case ::vk::Format::eEacR11UnormBlock:
        case ::vk::Format::eEacR11SnormBlock:
        case ::vk::Format::eEacR11G11UnormBlock:
        case ::vk::Format::eEacR11G11SnormBlock:
        case ::vk::Format::eAstc4x4UnormBlock:
        case ::vk::Format::eAstc4x4SrgbBlock:
        case ::vk::Format::eAstc5x4UnormBlock:
        case ::vk::Format::eAstc5x4SrgbBlock:
        case ::vk::Format::eAstc5x5UnormBlock:
        case ::vk::Format::eAstc5x5SrgbBlock:
        case ::vk::Format::eAstc6x5UnormBlock:
        case ::vk::Format::eAstc6x5SrgbBlock:
        case ::vk::Format::eAstc6x6UnormBlock:
        case ::vk::Format::eAstc6x6SrgbBlock:
        case ::vk::Format::eAstc8x5UnormBlock:
        case ::vk::Format::eAstc8x5SrgbBlock:
        case ::vk::Format::eAstc8x6UnormBlock:
        case ::vk::Format::eAstc8x6SrgbBlock:
        case ::vk::Format::eAstc8x8UnormBlock:
        case ::vk::Format::eAstc8x8SrgbBlock:
        case ::vk::Format::eAstc10x5UnormBlock:
        case ::vk::Format::eAstc10x5SrgbBlock:
        case ::vk::Format::eAstc10x6UnormBlock:
        case ::vk::Format::eAstc10x6SrgbBlock:
        case ::vk::Format::eAstc10x8UnormBlock:
        case ::vk::Format::eAstc10x8SrgbBlock:
        case ::vk::Format::eAstc10x10UnormBlock:
        case ::vk::Format::eAstc10x10SrgbBlock:
        case ::vk::Format::eAstc12x10UnormBlock:
        case ::vk::Format::eAstc12x10SrgbBlock:
        case ::vk::Format::eAstc12x12UnormBlock:
        case ::vk::Format::eAstc12x12SrgbBlock:
        case ::vk::Format::eR10X6UnormPack16:
        case ::vk::Format::eR10X6G10X6Unorm2Pack16:
        case ::vk::Format::eR10X6G10X6B10X6A10X6Unorm4Pack16:
        case ::vk::Format::eR12X4UnormPack16:
        case ::vk::Format::eR12X4G12X4Unorm2Pack16:
        case ::vk::Format::eR12X4G12X4B12X4A12X4Unorm4Pack16:
        case ::vk::Format::eA4R4G4B4UnormPack16:
        case ::vk::Format::eA4B4G4R4UnormPack16:
        case ::vk::Format::eAstc4x4SfloatBlock:
        case ::vk::Format::eAstc5x4SfloatBlock:
        case ::vk::Format::eAstc5x5SfloatBlock:
        case ::vk::Format::eAstc6x5SfloatBlock:
        case ::vk::Format::eAstc6x6SfloatBlock:
        case ::vk::Format::eAstc8x5SfloatBlock:
        case ::vk::Format::eAstc8x6SfloatBlock:
        case ::vk::Format::eAstc8x8SfloatBlock:
        case ::vk::Format::eAstc10x5SfloatBlock:
        case ::vk::Format::eAstc10x6SfloatBlock:
        case ::vk::Format::eAstc10x8SfloatBlock:
        case ::vk::Format::eAstc10x10SfloatBlock:
        case ::vk::Format::eAstc12x10SfloatBlock:
        case ::vk::Format::eAstc12x12SfloatBlock:
        case ::vk::Format::ePvrtc12BppUnormBlockIMG:
        case ::vk::Format::ePvrtc14BppUnormBlockIMG:
        case ::vk::Format::ePvrtc22BppUnormBlockIMG:
        case ::vk::Format::ePvrtc24BppUnormBlockIMG:
        case ::vk::Format::ePvrtc12BppSrgbBlockIMG:
        case ::vk::Format::ePvrtc14BppSrgbBlockIMG:
        case ::vk::Format::ePvrtc22BppSrgbBlockIMG:
        case ::vk::Format::ePvrtc24BppSrgbBlockIMG:
        case ::vk::Format::eR16G16S105NV: {
            return {0, PlanePacking::Undefined};
        }

        case ::vk::Format::eG8B8R82Plane444Unorm:
        case ::vk::Format::eG10X6B10X6R10X62Plane444Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X42Plane444Unorm3Pack16:
        case ::vk::Format::eG16B16R162Plane444Unorm: {
            return {2, PlanePacking::_444};
        }

        case ::vk::Format::eG8B8R83Plane444Unorm:
        case ::vk::Format::eG10X6B10X6R10X63Plane444Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X43Plane444Unorm3Pack16:
        case ::vk::Format::eG16B16R163Plane444Unorm: {
            return {3, PlanePacking::_444};
        }

        case ::vk::Format::eG8B8G8R8422Unorm:
        case ::vk::Format::eB8G8R8G8422Unorm:
        case ::vk::Format::eG10X6B10X6G10X6R10X6422Unorm4Pack16:
        case ::vk::Format::eB10X6G10X6R10X6G10X6422Unorm4Pack16:
        case ::vk::Format::eG12X4B12X4G12X4R12X4422Unorm4Pack16:
        case ::vk::Format::eB12X4G12X4R12X4G12X4422Unorm4Pack16:
        case ::vk::Format::eG16B16G16R16422Unorm:
        case ::vk::Format::eB16G16R16G16422Unorm: {
            return {1, PlanePacking::_422};
        }

        case ::vk::Format::eG8B8R82Plane422Unorm:
        case ::vk::Format::eG10X6B10X6R10X62Plane422Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X42Plane422Unorm3Pack16:
        case ::vk::Format::eG16B16R162Plane422Unorm: {
            return {2, PlanePacking::_422};
        }

        case ::vk::Format::eG8B8R83Plane422Unorm:
        case ::vk::Format::eG10X6B10X6R10X63Plane422Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X43Plane422Unorm3Pack16:
        case ::vk::Format::eG16B16R163Plane422Unorm: {
            return {3, PlanePacking::_422};
        }

        case ::vk::Format::eG8B8R82Plane420Unorm:
        case ::vk::Format::eG10X6B10X6R10X62Plane420Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X42Plane420Unorm3Pack16:
        case ::vk::Format::eG16B16R162Plane420Unorm: {
            return {2, PlanePacking::_420};
        }

        case ::vk::Format::eG8B8R83Plane420Unorm:
        case ::vk::Format::eG10X6B10X6R10X63Plane420Unorm3Pack16:
        case ::vk::Format::eG12X4B12X4R12X43Plane420Unorm3Pack16:
        case ::vk::Format::eG16B16R163Plane420Unorm: {
            return {3, PlanePacking::_420};
        }
    }

    spec::trace("decompose_planes encountered unknown vulkan format value: ",
                static_cast<int64_t>(fmt), "!");
    return {0, PlanePacking::Undefined};
}

Format decompose(::vk::Format fmt) {
    NumericFormat numeric_format = decompose_numeric_format(fmt);
    ComponentFormat component_format = decompose_component_format(fmt);
    bool block = decompose_block(fmt);
    bool undefined = fmt == ::vk::Format::eUndefined;
    Packing packing = decompose_packing(fmt);
    Planes planes = decompose_planes(fmt);
    return {numeric_format, component_format, block, undefined, 41, packing, planes};
}

::vk::Format recompose(Format fmt) {
    if (fmt.undefined) {
        return ::vk::Format::eUndefined;
    }
    switch (fmt.component_format.type) {
        case ComponentFormatClass::R: {
            if (!fmt.block && fmt.planes.p == PlanePacking::Undefined && fmt.planes.n == 0 &&
                fmt.packing.m == 0 && fmt.packing.nn == 0) {
                switch (fmt.numeric_format) {
                    case NumericFormat::Unorm: {
                        switch (fmt.component_format.params[0]) {
                            case 8: return ::vk::Format::eR8Unorm;
                            case 16: return ::vk::Format::eR16Unorm;
                        }
                    } break;
                    case NumericFormat::Snorm: {
                        switch (fmt.component_format.params[0]) {
                            case 8: return ::vk::Format::eR8Snorm;
                            case 16: return ::vk::Format::eR16Snorm;
                        }
                    } break;
                    case NumericFormat::Uscaled: {
                        switch (fmt.component_format.params[0]) {
                            case 8: return ::vk::Format::eR8Uscaled;
                            case 16: return ::vk::Format::eR16Uscaled;
                        }
                    } break;
                    case NumericFormat::Sscaled: {
                        switch (fmt.component_format.params[0]) {
                            case 8: return ::vk::Format::eR8Sscaled;
                            case 16: return ::vk::Format::eR16Sscaled;
                        }
                    } break;
                    case NumericFormat::Uint: {
                        switch (fmt.component_format.params[0]) {
                            case 8: return ::vk::Format::eR8Uint;
                            case 16: return ::vk::Format::eR16Uint;
                            case 32: return ::vk::Format::eR32Uint;
                            case 64: return ::vk::Format::eR64Uint;
                        }
                    } break;
                    case NumericFormat::Sint: {
                        switch (fmt.component_format.params[0]) {
                            case 8: return ::vk::Format::eR8Sint;
                            case 16: return ::vk::Format::eR16Sint;
                            case 32: return ::vk::Format::eR32Sint;
                            case 64: return ::vk::Format::eR64Sint;
                        }
                    } break;
                    case NumericFormat::Sfloat: {
                        switch (fmt.component_format.params[0]) {
                            case 16: return ::vk::Format::eR16Sfloat;
                            case 32: return ::vk::Format::eR32Sfloat;
                            case 64: return ::vk::Format::eR64Sfloat;
                        }
                    } break;
                    case NumericFormat::Srgb: {
                        if (fmt.component_format.params[0] == 8) {
                            return ::vk::Format::eR8Srgb;
                        }
                    } break;
                    default: break;
                }
            }

        } break;
        case ComponentFormatClass::RG: {
            if (!fmt.block && fmt.planes.p == PlanePacking::Undefined && fmt.planes.n == 0) {
                if (fmt.packing.m == 0 && fmt.packing.nn == 0 &&
                    fmt.component_format.params[0] == fmt.component_format.params[1]) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: {
                            switch (fmt.component_format.params[0]) {
                                case 8: return ::vk::Format::eR8G8Unorm;
                                case 16: return ::vk::Format::eR16G16Unorm;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Snorm: {
                            switch (fmt.component_format.params[0]) {
                                case 8: return ::vk::Format::eR8G8Snorm;
                                case 16: return ::vk::Format::eR16G16Snorm;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Uscaled: {
                            switch (fmt.component_format.params[0]) {
                                case 8: return ::vk::Format::eR8G8Uscaled;
                                case 16: return ::vk::Format::eR16G16Uscaled;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Sscaled: {
                            switch (fmt.component_format.params[0]) {
                                case 8: return ::vk::Format::eR8G8Sscaled;
                                case 16: return ::vk::Format::eR16G16Sscaled;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Uint: {
                            switch (fmt.component_format.params[0]) {
                                case 8: return ::vk::Format::eR8G8Uint;
                                case 16: return ::vk::Format::eR16G16Uint;
                                case 32: return ::vk::Format::eR32G32Uint;
                                case 64: return ::vk::Format::eR64G64Uint;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Sint: {
                            switch (fmt.component_format.params[0]) {
                                case 8: return ::vk::Format::eR8G8Sint;
                                case 16: return ::vk::Format::eR16G16Sint;
                                case 32: return ::vk::Format::eR32G32Sint;
                                case 64: return ::vk::Format::eR64G64Sint;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Sfloat: {
                            switch (fmt.component_format.params[0]) {
                                case 16: return ::vk::Format::eR16G16Sfloat;
                                case 32: return ::vk::Format::eR32G32Sfloat;
                                case 64: return ::vk::Format::eR64G64Sfloat;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Srgb: {
                            if (fmt.component_format.params[0] == 8) {
                                return ::vk::Format::eR8G8Srgb;
                            }
                        } break;
                        case NumericFormat::NvI10F5: {
                            if (fmt.component_format.params[0] == 16) {
                                return ::vk::Format::eR16G16S105NV;
                            }
                        } break;
                        default: break;
                    }
                }
                else if (fmt.packing.m == 1 && fmt.packing.nn == 8 &&
                         fmt.numeric_format == NumericFormat::Unorm &&
                         fmt.component_format.params[0] == 4 &&
                         fmt.component_format.params[1] == 4) {
                    return ::vk::Format::eR4G4UnormPack8;
                }
            }
        } break;
        case ComponentFormatClass::RGB: {
            if (!fmt.block && fmt.planes.p == PlanePacking::Undefined && fmt.planes.n == 0) {
                if (fmt.packing.m == 0 && fmt.packing.nn == 0 &&
                    fmt.component_format.params[0] == fmt.component_format.params[1] &&
                    fmt.component_format.params[1] == fmt.component_format.params[2]) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: {
                            switch (fmt.component_format.params[0]) {
                                case 8: return ::vk::Format::eR8G8B8Unorm;
                                case 16: return ::vk::Format::eR16G16B16Unorm;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Snorm: {
                            switch (fmt.component_format.params[0]) {
                                case 8: return ::vk::Format::eR8G8B8Snorm;
                                case 16: return ::vk::Format::eR16G16B16Snorm;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Uscaled: {
                            switch (fmt.component_format.params[0]) {
                                case 8: return ::vk::Format::eR8G8B8Uscaled;
                                case 16: return ::vk::Format::eR16G16B16Uscaled;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Sscaled: {
                            switch (fmt.component_format.params[0]) {
                                case 8: return ::vk::Format::eR8G8B8Sscaled;
                                case 16: return ::vk::Format::eR16G16B16Sscaled;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Uint: {
                            switch (fmt.component_format.params[0]) {
                                case 8: return ::vk::Format::eR8G8B8Uint;
                                case 16: return ::vk::Format::eR16G16B16Uint;
                                case 32: return ::vk::Format::eR32G32B32Uint;
                                case 64: return ::vk::Format::eR64G64B64Uint;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Sint: {
                            switch (fmt.component_format.params[0]) {
                                case 8: return ::vk::Format::eR8G8B8Sint;
                                case 16: return ::vk::Format::eR16G16B16Sint;
                                case 32: return ::vk::Format::eR32G32B32Sint;
                                case 64: return ::vk::Format::eR64G64B64Sint;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Sfloat: {
                            switch (fmt.component_format.params[0]) {
                                case 16: return ::vk::Format::eR16G16B16Sfloat;
                                case 32: return ::vk::Format::eR32G32B32Sfloat;
                                case 64: return ::vk::Format::eR64G64B64Sfloat;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Srgb: {
                            if (fmt.component_format.params[0] == 8) {
                                return ::vk::Format::eR8G8B8Srgb;
                            }
                        } break;
                        default: break;
                    }
                }
                else if (fmt.packing.m == 1 && fmt.packing.nn == 16 &&
                         fmt.numeric_format == NumericFormat::Unorm &&
                         fmt.component_format.params[0] == 5 &&
                         fmt.component_format.params[1] == 6 &&
                         fmt.component_format.params[2] == 5) {
                    return ::vk::Format::eR5G6B5UnormPack16;
                }
            }
        } break;
        case ComponentFormatClass::RGBA: {
            if (!fmt.block && fmt.planes.p == PlanePacking::Undefined && fmt.planes.n == 0) {
                if (fmt.packing.m == 0 && fmt.packing.nn == 0 &&
                    fmt.component_format.params[0] == fmt.component_format.params[1] &&
                    fmt.component_format.params[1] == fmt.component_format.params[2] &&
                    fmt.component_format.params[2] == fmt.component_format.params[3]) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: {
                            switch (fmt.component_format.params[0]) {
                                case 8: return ::vk::Format::eR8G8B8A8Unorm;
                                case 16: return ::vk::Format::eR16G16B16A16Unorm;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Snorm: {
                            switch (fmt.component_format.params[0]) {
                                case 8: return ::vk::Format::eR8G8B8A8Snorm;
                                case 16: return ::vk::Format::eR16G16B16A16Snorm;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Uscaled: {
                            switch (fmt.component_format.params[0]) {
                                case 8: return ::vk::Format::eR8G8B8A8Uscaled;
                                case 16: return ::vk::Format::eR16G16B16A16Uscaled;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Sscaled: {
                            switch (fmt.component_format.params[0]) {
                                case 8: return ::vk::Format::eR8G8B8A8Sscaled;
                                case 16: return ::vk::Format::eR16G16B16A16Sscaled;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Uint: {
                            switch (fmt.component_format.params[0]) {
                                case 8: return ::vk::Format::eR8G8B8A8Uint;
                                case 16: return ::vk::Format::eR16G16B16A16Uint;
                                case 32: return ::vk::Format::eR32G32B32A32Uint;
                                case 64: return ::vk::Format::eR64G64B64A64Uint;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Sint: {
                            switch (fmt.component_format.params[0]) {
                                case 8: return ::vk::Format::eR8G8B8A8Sint;
                                case 16: return ::vk::Format::eR16G16B16A16Sint;
                                case 32: return ::vk::Format::eR32G32B32A32Sint;
                                case 64: return ::vk::Format::eR64G64B64A64Sint;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Sfloat: {
                            switch (fmt.component_format.params[0]) {
                                case 16: return ::vk::Format::eR16G16B16A16Sfloat;
                                case 32: return ::vk::Format::eR32G32B32A32Sfloat;
                                case 64: return ::vk::Format::eR64G64B64A64Sfloat;
                                default: break;
                            }
                        } break;
                        case NumericFormat::Srgb: {
                            if (fmt.component_format.params[0] == 8) {
                                return ::vk::Format::eR8G8B8A8Srgb;
                            }
                        } break;
                        default: break;
                    }
                }
                else if (fmt.packing.m == 1 && fmt.packing.nn == 16 &&
                         fmt.numeric_format == NumericFormat::Unorm) {
                    if (fmt.component_format.params[0] == 4 &&
                        fmt.component_format.params[1] == 4 &&
                        fmt.component_format.params[2] == 4 &&
                        fmt.component_format.params[3] == 4) {
                        return ::vk::Format::eR4G4B4A4UnormPack16;
                    }
                    else if (fmt.component_format.params[0] == 5 &&
                             fmt.component_format.params[1] == 5 &&
                             fmt.component_format.params[2] == 5 &&
                             fmt.component_format.params[3] == 1) {
                        return ::vk::Format::eR5G5B5A1UnormPack16;
                    }
                }
            }
        } break;
        case ComponentFormatClass::RX: {
            if (!fmt.block && fmt.numeric_format == NumericFormat::Unorm &&
                fmt.planes.p == PlanePacking::Undefined && fmt.planes.n == 0 &&
                fmt.packing.m == 1 && fmt.packing.nn == 16) {
                if (fmt.component_format.params[0] == 10 &&
                    fmt.component_format.params[1] == 6) {
                    return ::vk::Format::eR10X6UnormPack16;
                }
                else if (fmt.component_format.params[0] == 12 &&
                         fmt.component_format.params[1] == 4) {
                    return ::vk::Format::eR12X4UnormPack16;
                }
            }
        } break;
        case ComponentFormatClass::RXGX: {
            if (!fmt.block && fmt.numeric_format == NumericFormat::Unorm &&
                fmt.planes.p == PlanePacking::Undefined && fmt.planes.n == 0 &&
                fmt.packing.m == 2 && fmt.packing.nn == 16) {
                if (fmt.component_format.params[0] == 10 &&
                    fmt.component_format.params[1] == 6 &&
                    fmt.component_format.params[2] == 10 &&
                    fmt.component_format.params[3] == 6) {
                    return ::vk::Format::eR10X6G10X6Unorm2Pack16;
                }
                else if (fmt.component_format.params[0] == 12 &&
                         fmt.component_format.params[1] == 4 &&
                         fmt.component_format.params[2] == 12 &&
                         fmt.component_format.params[3] == 4) {
                    return ::vk::Format::eR12X4G12X4Unorm2Pack16;
                }
            }
        } break;
        case ComponentFormatClass::RXGXBXAX: {
            if (!fmt.block && fmt.numeric_format == NumericFormat::Unorm &&
                fmt.planes.p == PlanePacking::Undefined && fmt.planes.n == 0 &&
                fmt.packing.m == 4 && fmt.packing.nn == 16) {
                if (fmt.component_format.params[0] == 10 &&
                    fmt.component_format.params[1] == 6 &&
                    fmt.component_format.params[2] == 10 &&
                    fmt.component_format.params[3] == 6 &&
                    fmt.component_format.params[4] == 10 &&
                    fmt.component_format.params[5] == 6 &&
                    fmt.component_format.params[6] == 10 &&
                    fmt.component_format.params[7] == 6) {
                    return ::vk::Format::eR10X6G10X6B10X6A10X6Unorm4Pack16;
                }
                else if (fmt.component_format.params[0] == 12 &&
                         fmt.component_format.params[1] == 4 &&
                         fmt.component_format.params[2] == 12 &&
                         fmt.component_format.params[3] == 4 &&
                         fmt.component_format.params[4] == 12 &&
                         fmt.component_format.params[5] == 4 &&
                         fmt.component_format.params[6] == 12 &&
                         fmt.component_format.params[7] == 4) {
                    return ::vk::Format::eR12X4G12X4B12X4A12X4Unorm4Pack16;
                }
            }
        } break;
        case ComponentFormatClass::ARGB: {
            if (!fmt.block && fmt.planes.p == PlanePacking::Undefined && fmt.planes.n == 0 &&
                fmt.packing.m == 1) {
                if (fmt.packing.nn == 16 && fmt.numeric_format == NumericFormat::Unorm) {
                    if (fmt.component_format.params[0] == 1 &&
                        fmt.component_format.params[1] == 5 &&
                        fmt.component_format.params[2] == 5 &&
                        fmt.component_format.params[3] == 5) {
                        return ::vk::Format::eA1R5G5B5UnormPack16;
                    }
                    else if (fmt.component_format.params[0] == 4 &&
                             fmt.component_format.params[1] == 4 &&
                             fmt.component_format.params[2] == 4 &&
                             fmt.component_format.params[3] == 4) {
                        return ::vk::Format::eA4R4G4B4UnormPack16;
                    }
                }
                else if (fmt.packing.nn == 32 && fmt.component_format.params[0] == 2 &&
                         fmt.component_format.params[1] == 10 &&
                         fmt.component_format.params[2] == 10 &&
                         fmt.component_format.params[3] == 10) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: return ::vk::Format::eA2R10G10B10UnormPack32;
                        case NumericFormat::Snorm: return ::vk::Format::eA2R10G10B10SnormPack32;
                        case NumericFormat::Uscaled:
                            return ::vk::Format::eA2R10G10B10UscaledPack32;
                        case NumericFormat::Sscaled:
                            return ::vk::Format::eA2R10G10B10SscaledPack32;
                        case NumericFormat::Uint: return ::vk::Format::eA2R10G10B10UintPack32;
                        case NumericFormat::Sint: return ::vk::Format::eA2R10G10B10SintPack32;
                        default: break;
                    }
                }
            }
        } break;
        case ComponentFormatClass::ABGR: {
            if (!fmt.block && fmt.planes.p == PlanePacking::Undefined && fmt.planes.n == 0 &&
                fmt.packing.m == 1) {
                if (fmt.packing.nn == 16 && fmt.numeric_format == NumericFormat::Unorm &&
                    fmt.component_format.params[0] == 4 &&
                    fmt.component_format.params[1] == 4 &&
                    fmt.component_format.params[2] == 4 &&
                    fmt.component_format.params[3] == 4) {
                    return ::vk::Format::eA4B4G4R4UnormPack16;
                }
                else if (fmt.packing.nn == 32) {
                    if (fmt.component_format.params[0] == 8 &&
                        fmt.component_format.params[1] == 8 &&
                        fmt.component_format.params[2] == 8 &&
                        fmt.component_format.params[3] == 8) {
                        switch (fmt.numeric_format) {
                            case NumericFormat::Unorm:
                                return ::vk::Format::eA8B8G8R8UnormPack32;
                            case NumericFormat::Snorm:
                                return ::vk::Format::eA8B8G8R8SnormPack32;
                            case NumericFormat::Uscaled:
                                return ::vk::Format::eA8B8G8R8UscaledPack32;
                            case NumericFormat::Sscaled:
                                return ::vk::Format::eA8B8G8R8SscaledPack32;
                            case NumericFormat::Uint: return ::vk::Format::eA8B8G8R8UintPack32;
                            case NumericFormat::Sint: return ::vk::Format::eA8B8G8R8SintPack32;
                            case NumericFormat::Srgb: return ::vk::Format::eA8B8G8R8SrgbPack32;
                            default: break;
                        }
                    }
                    else if (fmt.component_format.params[0] == 2 &&
                             fmt.component_format.params[1] == 10 &&
                             fmt.component_format.params[2] == 10 &&
                             fmt.component_format.params[3] == 10) {
                        switch (fmt.numeric_format) {
                            case NumericFormat::Unorm:
                                return ::vk::Format::eA2B10G10R10UnormPack32;
                            case NumericFormat::Snorm:
                                return ::vk::Format::eA2B10G10R10SnormPack32;
                            case NumericFormat::Uscaled:
                                return ::vk::Format::eA2B10G10R10UscaledPack32;
                            case NumericFormat::Sscaled:
                                return ::vk::Format::eA2B10G10R10SscaledPack32;
                            case NumericFormat::Uint:
                                return ::vk::Format::eA2B10G10R10UintPack32;
                            case NumericFormat::Sint:
                                return ::vk::Format::eA2B10G10R10SintPack32;
                            default: break;
                        }
                    }
                }
            }
        } break;
        case ComponentFormatClass::BGRA: {
            if (!fmt.block && fmt.planes.p == PlanePacking::Undefined && fmt.planes.n == 0) {
                if (fmt.packing.m == 1 && fmt.packing.nn == 16 &&
                    fmt.numeric_format == NumericFormat::Unorm) {
                    if (fmt.component_format.params[0] == 4 &&
                        fmt.component_format.params[1] == 4 &&
                        fmt.component_format.params[2] == 4 &&
                        fmt.component_format.params[3] == 4) {
                        return ::vk::Format::eB4G4R4A4UnormPack16;
                    }
                    else if (fmt.component_format.params[0] == 5 &&
                             fmt.component_format.params[1] == 5 &&
                             fmt.component_format.params[2] == 5 &&
                             fmt.component_format.params[3] == 1)
                        return ::vk::Format::eB5G5R5A1UnormPack16;
                }
                else if (fmt.packing.m == 0 && fmt.packing.nn == 0 &&
                         fmt.component_format.params[0] == 8 &&
                         fmt.component_format.params[1] == 8 &&
                         fmt.component_format.params[2] == 8 &&
                         fmt.component_format.params[3] == 8) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: return ::vk::Format::eB8G8R8A8Unorm;
                        case NumericFormat::Snorm: return ::vk::Format::eB8G8R8A8Snorm;
                        case NumericFormat::Uscaled: return ::vk::Format::eB8G8R8A8Uscaled;
                        case NumericFormat::Sscaled: return ::vk::Format::eB8G8R8A8Sscaled;
                        case NumericFormat::Uint: return ::vk::Format::eB8G8R8A8Uint;
                        case NumericFormat::Sint: return ::vk::Format::eB8G8R8A8Sint;
                        case NumericFormat::Srgb: return ::vk::Format::eB8G8R8A8Srgb;
                        default: break;
                    }
                }
            }
        } break;
        case ComponentFormatClass::BGR: {
            if (!fmt.block && fmt.planes.p == PlanePacking::Undefined && fmt.planes.n == 0) {
                if (fmt.packing.m == 1 && fmt.packing.nn == 16 &&
                    fmt.numeric_format == NumericFormat::Unorm &&
                    fmt.component_format.params[0] == 5 &&
                    fmt.component_format.params[1] == 6 &&
                    fmt.component_format.params[2] == 5) {
                    return ::vk::Format::eB5G6R5UnormPack16;
                }
                else if (fmt.packing.m == 1 && fmt.packing.nn == 32 &&
                         fmt.numeric_format == NumericFormat::Ufloat &&
                         fmt.component_format.params[0] == 10 &&
                         fmt.component_format.params[1] == 11 &&
                         fmt.component_format.params[2] == 11) {
                    return ::vk::Format::eB10G11R11UfloatPack32;
                }
                else if (fmt.packing.m == 0 && fmt.packing.nn == 0 &&
                         fmt.component_format.params[0] == 8 &&
                         fmt.component_format.params[1] == 8 &&
                         fmt.component_format.params[2] == 8) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: return ::vk::Format::eB8G8R8Unorm;
                        case NumericFormat::Snorm: return ::vk::Format::eB8G8R8Snorm;
                        case NumericFormat::Uscaled: return ::vk::Format::eB8G8R8Uscaled;
                        case NumericFormat::Sscaled: return ::vk::Format::eB8G8R8Sscaled;
                        case NumericFormat::Uint: return ::vk::Format::eB8G8R8Uint;
                        case NumericFormat::Sint: return ::vk::Format::eB8G8R8Sint;
                        case NumericFormat::Srgb: return ::vk::Format::eB8G8R8Srgb;
                        default: break;
                    }
                }
            }
        } break;
        case ComponentFormatClass::GBR: {
            if (!fmt.block && fmt.numeric_format == NumericFormat::Unorm) {
                if (fmt.component_format.params[0] == 8 &&
                    fmt.component_format.params[1] == 8 &&
                    fmt.component_format.params[2] == 8) {
                    if (fmt.planes.n == 2) {
                        switch (fmt.planes.p) {
                            case PlanePacking::_444: return ::vk::Format::eG8B8R82Plane444Unorm;
                            case PlanePacking::_422: return ::vk::Format::eG8B8R82Plane422Unorm;
                            case PlanePacking::_420: return ::vk::Format::eG8B8R82Plane420Unorm;
                            default: break;
                        }
                    }
                    else if (fmt.planes.n == 3) {
                        switch (fmt.planes.p) {
                            case PlanePacking::_444: return ::vk::Format::eG8B8R83Plane444Unorm;
                            case PlanePacking::_422: return ::vk::Format::eG8B8R83Plane422Unorm;
                            case PlanePacking::_420: return ::vk::Format::eG8B8R83Plane420Unorm;
                            default: break;
                        }
                    }
                }
                else if (fmt.component_format.params[0] == 16 &&
                         fmt.component_format.params[1] == 16 &&
                         fmt.component_format.params[2] == 16) {
                    if (fmt.planes.n == 2) {
                        switch (fmt.planes.p) {
                            case PlanePacking::_444:
                                return ::vk::Format::eG16B16R162Plane444Unorm;
                            case PlanePacking::_422:
                                return ::vk::Format::eG16B16R162Plane422Unorm;
                            case PlanePacking::_420:
                                return ::vk::Format::eG16B16R162Plane420Unorm;
                            default: break;
                        }
                    }
                    else if (fmt.planes.n == 3) {
                        switch (fmt.planes.p) {
                            case PlanePacking::_444:
                                return ::vk::Format::eG16B16R163Plane444Unorm;
                            case PlanePacking::_422:
                                return ::vk::Format::eG16B16R163Plane422Unorm;
                            case PlanePacking::_420:
                                return ::vk::Format::eG16B16R163Plane420Unorm;
                            default: break;
                        }
                    }
                }
            }
        } break;
        case ComponentFormatClass::EBGR: {
            if (!fmt.block && fmt.numeric_format == NumericFormat::Ufloat &&
                fmt.component_format.params[0] == 5 && fmt.component_format.params[1] == 9 &&
                fmt.component_format.params[2] == 9 && fmt.component_format.params[3] == 9 &&
                fmt.packing.m == 1 && fmt.packing.nn == 32 && fmt.planes.n == 0 &&
                fmt.planes.p == PlanePacking::Undefined) {
                return ::vk::Format::eE5B9G9R9UfloatPack32;
            }
        } break;
        case ComponentFormatClass::GBGR: {
            if (!fmt.block && fmt.numeric_format == NumericFormat::Unorm &&
                fmt.packing.m == 0 && fmt.packing.nn == 0 && fmt.planes.n == 1 &&
                fmt.planes.p == PlanePacking::_422) {
                if (fmt.component_format.params[0] == 8 &&
                    fmt.component_format.params[1] == 8 &&
                    fmt.component_format.params[2] == 8 &&
                    fmt.component_format.params[3] == 8) {
                    return ::vk::Format::eG8B8G8R8422Unorm;
                }
                else if (fmt.component_format.params[0] == 16 &&
                         fmt.component_format.params[1] == 16 &&
                         fmt.component_format.params[2] == 16 &&
                         fmt.component_format.params[3] == 16) {
                    return ::vk::Format::eG16B16G16R16422Unorm;
                }
            }
        } break;
        case ComponentFormatClass::BGRG: {
            if (!fmt.block && fmt.numeric_format == NumericFormat::Unorm &&
                fmt.packing.m == 0 && fmt.packing.nn == 0 && fmt.planes.n == 1 &&
                fmt.planes.p == PlanePacking::_422) {
                if (fmt.component_format.params[0] == 8 &&
                    fmt.component_format.params[1] == 8 &&
                    fmt.component_format.params[2] == 8 &&
                    fmt.component_format.params[3] == 8) {
                    return ::vk::Format::eB8G8R8G8422Unorm;
                }
                else if (fmt.component_format.params[0] == 16 &&
                         fmt.component_format.params[1] == 16 &&
                         fmt.component_format.params[2] == 16 &&
                         fmt.component_format.params[3] == 16) {
                    return ::vk::Format::eB16G16R16G16422Unorm;
                }
            }
        } break;
        case ComponentFormatClass::BXGXRXGX: {
            if (!fmt.block && fmt.numeric_format == NumericFormat::Unorm &&
                fmt.packing.m == 4 && fmt.packing.nn == 16 && fmt.planes.n == 1 &&
                fmt.planes.p == PlanePacking::_422) {
                if (fmt.component_format.params[0] == 10 &&
                    fmt.component_format.params[1] == 6 &&
                    fmt.component_format.params[2] == 10 &&
                    fmt.component_format.params[3] == 6 &&
                    fmt.component_format.params[4] == 10 &&
                    fmt.component_format.params[5] == 6 &&
                    fmt.component_format.params[6] == 10 &&
                    fmt.component_format.params[7] == 6) {
                    return ::vk::Format::eB10X6G10X6R10X6G10X6422Unorm4Pack16;
                }
                else if (fmt.component_format.params[0] == 12 &&
                         fmt.component_format.params[1] == 4 &&
                         fmt.component_format.params[2] == 12 &&
                         fmt.component_format.params[3] == 4 &&
                         fmt.component_format.params[4] == 12 &&
                         fmt.component_format.params[5] == 4 &&
                         fmt.component_format.params[6] == 12 &&
                         fmt.component_format.params[7] == 4) {
                    return ::vk::Format::eB12X4G12X4R12X4G12X4422Unorm4Pack16;
                }
            }
        } break;
        case ComponentFormatClass::GXBXRX: {
            if (!fmt.block && fmt.numeric_format == NumericFormat::Unorm &&
                fmt.packing.m == 3 && fmt.packing.nn == 16) {
                if (fmt.component_format.params[0] == 10 &&
                    fmt.component_format.params[1] == 6 &&
                    fmt.component_format.params[2] == 10 &&
                    fmt.component_format.params[3] == 6 &&
                    fmt.component_format.params[4] == 10 &&
                    fmt.component_format.params[5] == 6) {
                    if (fmt.planes.n == 2) {
                        switch (fmt.planes.p) {
                            case PlanePacking::_444:
                                return ::vk::Format::eG10X6B10X6R10X62Plane444Unorm3Pack16;
                            case PlanePacking::_422:
                                return ::vk::Format::eG10X6B10X6R10X62Plane422Unorm3Pack16;
                            case PlanePacking::_420:
                                return ::vk::Format::eG10X6B10X6R10X62Plane420Unorm3Pack16;
                            default: break;
                        }
                    }
                    else if (fmt.planes.n == 3) {
                        switch (fmt.planes.p) {
                            case PlanePacking::_444:
                                return ::vk::Format::eG10X6B10X6R10X63Plane444Unorm3Pack16;
                            case PlanePacking::_422:
                                return ::vk::Format::eG10X6B10X6R10X63Plane422Unorm3Pack16;
                            case PlanePacking::_420:
                                return ::vk::Format::eG10X6B10X6R10X63Plane420Unorm3Pack16;
                            default: break;
                        }
                    }
                }
                else if (fmt.component_format.params[0] == 12 &&
                         fmt.component_format.params[1] == 4 &&
                         fmt.component_format.params[2] == 12 &&
                         fmt.component_format.params[3] == 4 &&
                         fmt.component_format.params[4] == 12 &&
                         fmt.component_format.params[5] == 4) {
                    if (fmt.planes.n == 2) {
                        switch (fmt.planes.p) {
                            case PlanePacking::_444:
                                return ::vk::Format::eG12X4B12X4R12X42Plane444Unorm3Pack16;
                            case PlanePacking::_422:
                                return ::vk::Format::eG12X4B12X4R12X42Plane422Unorm3Pack16;
                            case PlanePacking::_420:
                                return ::vk::Format::eG12X4B12X4R12X42Plane420Unorm3Pack16;
                            default: break;
                        }
                    }
                    else if (fmt.planes.n == 3) {
                        switch (fmt.planes.p) {
                            case PlanePacking::_444:
                                return ::vk::Format::eG12X4B12X4R12X43Plane444Unorm3Pack16;
                            case PlanePacking::_422:
                                return ::vk::Format::eG12X4B12X4R12X43Plane422Unorm3Pack16;
                            case PlanePacking::_420:
                                return ::vk::Format::eG12X4B12X4R12X43Plane420Unorm3Pack16;
                            default: break;
                        }
                    }
                }
            }
        } break;
        case ComponentFormatClass::GXBXGXRX: {
            if (!fmt.block && fmt.numeric_format == NumericFormat::Unorm &&
                fmt.packing.m == 4 && fmt.packing.nn == 16 && fmt.planes.n == 1 &&
                fmt.planes.p == PlanePacking::_422) {
                if (fmt.component_format.params[0] == 10 &&
                    fmt.component_format.params[1] == 6 &&
                    fmt.component_format.params[2] == 10 &&
                    fmt.component_format.params[3] == 6 &&
                    fmt.component_format.params[4] == 10 &&
                    fmt.component_format.params[5] == 6 &&
                    fmt.component_format.params[6] == 10 &&
                    fmt.component_format.params[7] == 6) {
                    return ::vk::Format::eG10X6B10X6G10X6R10X6422Unorm4Pack16;
                }
                else if (fmt.component_format.params[0] == 12 &&
                         fmt.component_format.params[1] == 4 &&
                         fmt.component_format.params[2] == 12 &&
                         fmt.component_format.params[3] == 4 &&
                         fmt.component_format.params[4] == 12 &&
                         fmt.component_format.params[5] == 4 &&
                         fmt.component_format.params[6] == 12 &&
                         fmt.component_format.params[7] == 4) {
                    return ::vk::Format::eG12X4B12X4G12X4R12X4422Unorm4Pack16;
                }
            }
        } break;
        case ComponentFormatClass::D: {
            if (!fmt.block && fmt.packing.m == 0 && fmt.packing.nn == 0 && fmt.planes.n == 0 &&
                fmt.planes.p == PlanePacking::Undefined) {
                if (fmt.numeric_format == NumericFormat::Unorm &&
                    fmt.component_format.params[0] == 16) {
                    return ::vk::Format::eD16Unorm;
                }
                else if (fmt.numeric_format == NumericFormat::Sfloat &&
                         fmt.component_format.params[0] == 32) {
                    return ::vk::Format::eD32Sfloat;
                }
            }
        } break;
        case ComponentFormatClass::S: {
            if (!fmt.block && fmt.numeric_format == NumericFormat::Uint &&
                fmt.component_format.params[0] == 8 && fmt.packing.m == 0 &&
                fmt.packing.nn == 0 && fmt.planes.n == 0 &&
                fmt.planes.p == PlanePacking::Undefined) {
                return ::vk::Format::eS8Uint;
            }
        } break;
        case ComponentFormatClass::XD: {
            if (!fmt.block && fmt.numeric_format == NumericFormat::Unorm &&
                fmt.component_format.params[0] == 8 && fmt.component_format.params[1] == 24 &&
                fmt.packing.m == 1 && fmt.packing.nn == 32 && fmt.planes.n == 0 &&
                fmt.planes.p == PlanePacking::Undefined) {
                return ::vk::Format::eX8D24UnormPack32;
            }
        } break;
        case ComponentFormatClass::DS: {
            if (!fmt.block && fmt.component_format.params[1] == 8 && fmt.packing.m == 0 &&
                fmt.packing.nn == 0 && fmt.planes.n == 0 &&
                fmt.planes.p == PlanePacking::Undefined) {
                if (fmt.numeric_format == NumericFormat::Unorm) {
                    if (fmt.component_format.params[0] == 16) {
                        return ::vk::Format::eD16UnormS8Uint;
                    }
                    else if (fmt.component_format.params[0] == 24) {
                        return ::vk::Format::eD24UnormS8Uint;
                    }
                }
                else if (fmt.numeric_format == NumericFormat::Sfloat &&
                         fmt.component_format.params[0] == 32) {
                    return ::vk::Format::eD32SfloatS8Uint;
                }
            }
        } break;
        case ComponentFormatClass::BC: {
            if (fmt.block && fmt.packing.m == 0 && fmt.packing.nn == 0 && fmt.planes.n == 0 &&
                fmt.planes.p == PlanePacking::Undefined) {
                switch (fmt.component_format.params[0]) {
                    case 1: {
                        if (fmt.component_format.params[1] ==
                            (uint8_t) ComponentFormatClass::RGB) {
                            if (fmt.numeric_format == NumericFormat::Unorm) {
                                return ::vk::Format::eBc1RgbUnormBlock;
                            }
                            else if (fmt.numeric_format == NumericFormat::Srgb) {
                                return ::vk::Format::eBc1RgbSrgbBlock;
                            }
                        }
                        else if (fmt.component_format.params[1] ==
                                 (uint8_t) ComponentFormatClass::RGBA) {
                            if (fmt.numeric_format == NumericFormat::Unorm) {
                                return ::vk::Format::eBc1RgbaUnormBlock;
                            }
                            else if (fmt.numeric_format == NumericFormat::Srgb) {
                                return ::vk::Format::eBc1RgbaSrgbBlock;
                            }
                        }
                    } break;
                    case 2: {
                        if (fmt.numeric_format == NumericFormat::Unorm) {
                            return ::vk::Format::eBc2UnormBlock;
                        }
                        else if (fmt.numeric_format == NumericFormat::Srgb) {
                            return ::vk::Format::eBc2SrgbBlock;
                        }
                    } break;
                    case 3: {
                        if (fmt.numeric_format == NumericFormat::Unorm) {
                            return ::vk::Format::eBc3UnormBlock;
                        }
                        else if (fmt.numeric_format == NumericFormat::Srgb) {
                            return ::vk::Format::eBc3SrgbBlock;
                        }
                    } break;
                    case 4: {
                        if (fmt.numeric_format == NumericFormat::Unorm) {
                            return ::vk::Format::eBc4UnormBlock;
                        }
                        else if (fmt.numeric_format == NumericFormat::Snorm) {
                            return ::vk::Format::eBc4SnormBlock;
                        }
                    } break;
                    case 5: {
                        if (fmt.numeric_format == NumericFormat::Unorm) {
                            return ::vk::Format::eBc5UnormBlock;
                        }
                        else if (fmt.numeric_format == NumericFormat::Snorm) {
                            return ::vk::Format::eBc5SnormBlock;
                        }
                    } break;
                    case 6: {
                        if (fmt.numeric_format == NumericFormat::Ufloat) {
                            return ::vk::Format::eBc6HUfloatBlock;
                        }
                        else if (fmt.numeric_format == NumericFormat::Sfloat) {
                            return ::vk::Format::eBc6HSfloatBlock;
                        }
                    } break;
                    case 7: {
                        if (fmt.numeric_format == NumericFormat::Unorm) {
                            return ::vk::Format::eBc7UnormBlock;
                        }
                        else if (fmt.numeric_format == NumericFormat::Srgb) {
                            return ::vk::Format::eBc7SrgbBlock;
                        }
                    } break;
                    default: break;
                }
            }
        } break;
        case ComponentFormatClass::ETC2: {
            if (fmt.block && fmt.packing.m == 0 && fmt.packing.nn == 0 && fmt.planes.n == 0 &&
                fmt.planes.p == PlanePacking::Undefined) {
                if (fmt.component_format.params[0] == (uint8_t) ComponentFormatClass::RGB &&
                    fmt.component_format.params[1] == 8 &&
                    fmt.component_format.params[2] == 8 &&
                    fmt.component_format.params[3] == 8) {
                    if (fmt.numeric_format == NumericFormat::Unorm) {
                        return ::vk::Format::eEtc2R8G8B8UnormBlock;
                    }
                    else if (fmt.numeric_format == NumericFormat::Srgb) {
                        return ::vk::Format::eEtc2R8G8B8SrgbBlock;
                    }
                }
                else if (fmt.component_format.params[0] ==
                         (uint8_t) ComponentFormatClass::RGBA) {
                    if (fmt.component_format.params[1] == 8 &&
                        fmt.component_format.params[2] == 8 &&
                        fmt.component_format.params[3] == 8 &&
                        fmt.component_format.params[4] == 1) {
                        if (fmt.numeric_format == NumericFormat::Unorm) {
                            return ::vk::Format::eEtc2R8G8B8A1UnormBlock;
                        }
                        else if (fmt.numeric_format == NumericFormat::Srgb) {
                            return ::vk::Format::eEtc2R8G8B8A1SrgbBlock;
                        }
                    }
                    else if (fmt.component_format.params[1] == 8 &&
                             fmt.component_format.params[2] == 8 &&
                             fmt.component_format.params[3] == 8 &&
                             fmt.component_format.params[4] == 8) {
                        if (fmt.numeric_format == NumericFormat::Unorm) {
                            return ::vk::Format::eEtc2R8G8B8A8UnormBlock;
                        }
                        else if (fmt.numeric_format == NumericFormat::Srgb) {
                            return ::vk::Format::eEtc2R8G8B8A8SrgbBlock;
                        }
                    }
                }
            }
        } break;
        case ComponentFormatClass::EAC: {
            if (fmt.block && fmt.packing.m == 0 && fmt.packing.nn == 0 && fmt.planes.n == 0 &&
                fmt.planes.p == PlanePacking::Undefined) {
                if (fmt.component_format.params[0] == (uint8_t) ComponentFormatClass::R &&
                    fmt.component_format.params[1] == 11) {
                    if (fmt.numeric_format == NumericFormat::Unorm) {
                        return ::vk::Format::eEacR11UnormBlock;
                    }
                    else if (fmt.numeric_format == NumericFormat::Snorm) {
                        return ::vk::Format::eEacR11SnormBlock;
                    }
                }
                else if (fmt.component_format.params[0] == (uint8_t) ComponentFormatClass::RG &&
                         fmt.component_format.params[1] == 11 &&
                         fmt.component_format.params[2] == 11) {
                    if (fmt.numeric_format == NumericFormat::Unorm) {
                        return ::vk::Format::eEacR11G11UnormBlock;
                    }
                    else if (fmt.numeric_format == NumericFormat::Snorm) {
                        return ::vk::Format::eEacR11G11SnormBlock;
                    }
                }
            }
        } break;
        case ComponentFormatClass::ASTC: {
            if (fmt.block && fmt.packing.m == 0 && fmt.packing.nn == 0 && fmt.planes.n == 0 &&
                fmt.planes.p == PlanePacking::Undefined) {
                if (fmt.component_format.params[0] == 4 &&
                    fmt.component_format.params[1] == 4) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: return ::vk::Format::eAstc4x4UnormBlock;
                        case NumericFormat::Srgb: return ::vk::Format::eAstc4x4SrgbBlock;
                        case NumericFormat::Sfloat: return ::vk::Format::eAstc4x4SfloatBlock;
                        default: break;
                    }
                }
                else if (fmt.component_format.params[0] == 5 &&
                         fmt.component_format.params[1] == 4) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: return ::vk::Format::eAstc5x4UnormBlock;
                        case NumericFormat::Srgb: return ::vk::Format::eAstc5x4SrgbBlock;
                        case NumericFormat::Sfloat: return ::vk::Format::eAstc5x4SfloatBlock;
                        default: break;
                    }
                }
                else if (fmt.component_format.params[0] == 5 &&
                         fmt.component_format.params[1] == 5) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: return ::vk::Format::eAstc5x5UnormBlock;
                        case NumericFormat::Srgb: return ::vk::Format::eAstc5x5SrgbBlock;
                        case NumericFormat::Sfloat: return ::vk::Format::eAstc5x5SfloatBlock;
                        default: break;
                    }
                }
                else if (fmt.component_format.params[0] == 6 &&
                         fmt.component_format.params[1] == 5) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: return ::vk::Format::eAstc6x5UnormBlock;
                        case NumericFormat::Srgb: return ::vk::Format::eAstc6x5SrgbBlock;
                        case NumericFormat::Sfloat: return ::vk::Format::eAstc6x5SfloatBlock;
                        default: break;
                    }
                }
                else if (fmt.component_format.params[0] == 6 &&
                         fmt.component_format.params[1] == 6) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: return ::vk::Format::eAstc6x6UnormBlock;
                        case NumericFormat::Srgb: return ::vk::Format::eAstc6x6SrgbBlock;
                        case NumericFormat::Sfloat: return ::vk::Format::eAstc6x6SfloatBlock;
                        default: break;
                    }
                }
                else if (fmt.component_format.params[0] == 8 &&
                         fmt.component_format.params[1] == 5) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: return ::vk::Format::eAstc8x5UnormBlock;
                        case NumericFormat::Srgb: return ::vk::Format::eAstc8x5SrgbBlock;
                        case NumericFormat::Sfloat: return ::vk::Format::eAstc8x5SfloatBlock;
                        default: break;
                    }
                }
                else if (fmt.component_format.params[0] == 8 &&
                         fmt.component_format.params[1] == 6) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: return ::vk::Format::eAstc8x6UnormBlock;
                        case NumericFormat::Srgb: return ::vk::Format::eAstc8x6SrgbBlock;
                        case NumericFormat::Sfloat: return ::vk::Format::eAstc8x6SfloatBlock;
                        default: break;
                    }
                }
                else if (fmt.component_format.params[0] == 8 &&
                         fmt.component_format.params[1] == 8) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: return ::vk::Format::eAstc8x8UnormBlock;
                        case NumericFormat::Srgb: return ::vk::Format::eAstc8x8SrgbBlock;
                        case NumericFormat::Sfloat: return ::vk::Format::eAstc8x8SfloatBlock;
                        default: break;
                    }
                }
                else if (fmt.component_format.params[0] == 10 &&
                         fmt.component_format.params[1] == 5) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: return ::vk::Format::eAstc10x5UnormBlock;
                        case NumericFormat::Srgb: return ::vk::Format::eAstc10x5SrgbBlock;
                        case NumericFormat::Sfloat: return ::vk::Format::eAstc10x5SfloatBlock;
                        default: break;
                    }
                }
                else if (fmt.component_format.params[0] == 10 &&
                         fmt.component_format.params[1] == 6) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: return ::vk::Format::eAstc10x6UnormBlock;
                        case NumericFormat::Srgb: return ::vk::Format::eAstc10x6SrgbBlock;
                        case NumericFormat::Sfloat: return ::vk::Format::eAstc10x6SfloatBlock;
                        default: break;
                    }
                }
                else if (fmt.component_format.params[0] == 10 &&
                         fmt.component_format.params[1] == 8) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: return ::vk::Format::eAstc10x8UnormBlock;
                        case NumericFormat::Srgb: return ::vk::Format::eAstc10x8SrgbBlock;
                        case NumericFormat::Sfloat: return ::vk::Format::eAstc10x8SfloatBlock;
                        default: break;
                    }
                }
                else if (fmt.component_format.params[0] == 10 &&
                         fmt.component_format.params[1] == 10) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: return ::vk::Format::eAstc10x10UnormBlock;
                        case NumericFormat::Srgb: return ::vk::Format::eAstc10x10SrgbBlock;
                        case NumericFormat::Sfloat: return ::vk::Format::eAstc10x10SfloatBlock;
                        default: break;
                    }
                }
                else if (fmt.component_format.params[0] == 12 &&
                         fmt.component_format.params[1] == 10) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: return ::vk::Format::eAstc12x10UnormBlock;
                        case NumericFormat::Srgb: return ::vk::Format::eAstc12x10SrgbBlock;
                        case NumericFormat::Sfloat: return ::vk::Format::eAstc12x10SfloatBlock;
                        default: break;
                    }
                }
                else if (fmt.component_format.params[0] == 12 &&
                         fmt.component_format.params[1] == 12) {
                    switch (fmt.numeric_format) {
                        case NumericFormat::Unorm: return ::vk::Format::eAstc12x12UnormBlock;
                        case NumericFormat::Srgb: return ::vk::Format::eAstc12x12SrgbBlock;
                        case NumericFormat::Sfloat: return ::vk::Format::eAstc12x12SfloatBlock;
                        default: break;
                    }
                }
            }
        } break;
        case ComponentFormatClass::PVRTC: {
            if (fmt.block && fmt.packing.m == 0 && fmt.packing.nn == 0 && fmt.planes.n == 0 &&
                fmt.planes.p == PlanePacking::Undefined) {
                switch (fmt.component_format.params[0]) {
                    case 1: {
                        switch (fmt.component_format.params[1]) {
                            case 2: {
                                if (fmt.numeric_format == NumericFormat::Unorm) {
                                    return ::vk::Format::ePvrtc12BppUnormBlockIMG;
                                }
                                else if (fmt.numeric_format == NumericFormat::Srgb) {
                                    return ::vk::Format::ePvrtc12BppSrgbBlockIMG;
                                }
                            } break;
                            case 4: {
                                if (fmt.numeric_format == NumericFormat::Unorm) {
                                    return ::vk::Format::ePvrtc14BppUnormBlockIMG;
                                }
                                else if (fmt.numeric_format == NumericFormat::Srgb) {
                                    return ::vk::Format::ePvrtc14BppSrgbBlockIMG;
                                }
                            } break;
                            default: break;
                        }
                    } break;
                    case 2: {
                        switch (fmt.component_format.params[1]) {
                            case 2: {
                                if (fmt.numeric_format == NumericFormat::Unorm) {
                                    return ::vk::Format::ePvrtc22BppUnormBlockIMG;
                                }
                                else if (fmt.numeric_format == NumericFormat::Srgb) {
                                    return ::vk::Format::ePvrtc22BppSrgbBlockIMG;
                                }
                            } break;
                            case 4: {
                                if (fmt.numeric_format == NumericFormat::Unorm) {
                                    return ::vk::Format::ePvrtc24BppUnormBlockIMG;
                                }
                                else if (fmt.numeric_format == NumericFormat::Srgb) {
                                    return ::vk::Format::ePvrtc24BppSrgbBlockIMG;
                                }
                            } break;
                            default: break;
                        }
                    } break;
                    default: break;
                }
            }
        } break;
        default: /** Fall to default return */;
    }

    spec::trace("recompose failed constructing requested format!");
    return ::vk::Format::eUndefined;
}

static std::string _to_string_invalid(Format::PrintStyles style) {
    switch (style) {
        case Format::PrintStyles::OGFormat: {
            return "VK_FORMAT_UNDEFINED";
        } break;
        case Format::PrintStyles::SnakeCase: {
            return "UNDEFINED";
        } break;
        case Format::PrintStyles::PPFormat: {
            return "eUndefined";
        } break;
        case Format::PrintStyles::CamelCase: {
            return "Undefined";
        } break;
        default: {
            spec::fatal("Invalid Format::PrintStyles"
                        " argument passed to to_string!");
        }
    }
}

static std::string _to_string(ComponentFormatClass cls, Format::PrintStyles style) {
    switch (style) {
        case Format::PrintStyles::OGFormat:
        case Format::PrintStyles::SnakeCase: {
            switch (cls) {
                case ComponentFormatClass::Invalid: return "INVALID";
                case ComponentFormatClass::R: return "R";
                case ComponentFormatClass::RG: return "RG";
                case ComponentFormatClass::RGB: return "RGB";
                case ComponentFormatClass::RGBA: return "RGBA";
                case ComponentFormatClass::RX: return "RX";
                case ComponentFormatClass::RXGX: return "RXGX";
                case ComponentFormatClass::RXGXBXAX: return "RXGXBXA";
                case ComponentFormatClass::ARGB: return "ARGB";
                case ComponentFormatClass::ABGR: return "ABGR";
                case ComponentFormatClass::BGRA: return "BGRA";
                case ComponentFormatClass::BGR: return "BGR";
                case ComponentFormatClass::GBR: return "GBR";
                case ComponentFormatClass::EBGR: return "EBGR";
                case ComponentFormatClass::GBGR: return "GBGR";
                case ComponentFormatClass::BGRG: return "BGRG";
                case ComponentFormatClass::BXGXRXGX: return "BXGXRXGX";
                case ComponentFormatClass::GXBXRX: return "GXBXRX";
                case ComponentFormatClass::GXBXGXRX: return "GXBXGXRX";
                case ComponentFormatClass::D: return "D";
                case ComponentFormatClass::S: return "S";
                case ComponentFormatClass::XD: return "XD";
                case ComponentFormatClass::DS: return "DS";
                case ComponentFormatClass::BC: return "BC";
                case ComponentFormatClass::ETC2: return "ETC2";
                case ComponentFormatClass::EAC: return "EAC";
                case ComponentFormatClass::ASTC: return "ASTC";
                case ComponentFormatClass::PVRTC: return "PVRTC";
            }
        } break;
        case Format::PrintStyles::PPFormat:
        case Format::PrintStyles::CamelCase: {
            switch (cls) {
                case ComponentFormatClass::Invalid: return "Invalid";
                case ComponentFormatClass::R: return "R";
                case ComponentFormatClass::RG: return "Rg";
                case ComponentFormatClass::RGB: return "Rgb";
                case ComponentFormatClass::RGBA: return "Rgba";
                case ComponentFormatClass::RX: return "Rx";
                case ComponentFormatClass::RXGX: return "Rxgx";
                case ComponentFormatClass::RXGXBXAX: return "Rxgxbxa";
                case ComponentFormatClass::ARGB: return "Argb";
                case ComponentFormatClass::ABGR: return "Abgr";
                case ComponentFormatClass::BGRA: return "Bgra";
                case ComponentFormatClass::BGR: return "Bgr";
                case ComponentFormatClass::GBR: return "Gbr";
                case ComponentFormatClass::EBGR: return "Ebgr";
                case ComponentFormatClass::GBGR: return "Gbgr";
                case ComponentFormatClass::BGRG: return "Bgrg";
                case ComponentFormatClass::BXGXRXGX: return "Bxgxrxgx";
                case ComponentFormatClass::GXBXRX: return "Gxbxrx";
                case ComponentFormatClass::GXBXGXRX: return "Gxbxgxrx";
                case ComponentFormatClass::D: return "D";
                case ComponentFormatClass::S: return "S";
                case ComponentFormatClass::XD: return "Xd";
                case ComponentFormatClass::DS: return "Ds";
                case ComponentFormatClass::BC: return "Bc";
                case ComponentFormatClass::ETC2: return "Etc2";
                case ComponentFormatClass::EAC: return "Eac";
                case ComponentFormatClass::ASTC: return "Astc";
                case ComponentFormatClass::PVRTC: return "Pvrtc";
            }
        } break;
    }
    spec::fatal("Unreachable@" FILE_LINE_STR);
}
static std::string _to_string(ComponentFormat v, Format::PrintStyles style, uint8_t planes_n) {
    auto params = v.params;
    switch (v.type) {
        case ComponentFormatClass::R: {
            std::string cf_r("R");
            cf_r.append(std::to_string(params[0]));
            return cf_r;
        } break;
        case ComponentFormatClass::RG: {
            std::string cf_r("R");
            cf_r.append(std::to_string(params[0]));
            std::string cf_g("G");
            cf_g.append(std::to_string(params[1]));
            return cf_r + cf_g;
        } break;
        case ComponentFormatClass::RGB: {
            std::string cf_r("R");
            cf_r.append(std::to_string(params[0]));
            std::string cf_g("G");
            cf_g.append(std::to_string(params[1]));
            std::string cf_b("B");
            cf_b.append(std::to_string(params[2]));
            return cf_r + cf_g + cf_b;
        } break;
        case ComponentFormatClass::RGBA: {
            std::string cf_r("R");
            cf_r.append(std::to_string(params[0]));
            std::string cf_g("G");
            cf_g.append(std::to_string(params[1]));
            std::string cf_b("B");
            cf_b.append(std::to_string(params[2]));
            std::string cf_a("A");
            cf_a.append(std::to_string(params[3]));
            return cf_r + cf_g + cf_b + cf_a;
        } break;
        case ComponentFormatClass::RX: {
            std::string cf_r("R");
            cf_r.append(std::to_string(params[0]));
            cf_r.append("X");
            cf_r.append(std::to_string(params[1]));
            return cf_r;
        } break;
        case ComponentFormatClass::RXGX: {
            std::string cf_r("R");
            cf_r.append(std::to_string(params[0]));
            cf_r.append("X");
            cf_r.append(std::to_string(params[1]));
            std::string cf_g("G");
            cf_g.append(std::to_string(params[2]));
            cf_g.append("X");
            cf_g.append(std::to_string(params[3]));
            return cf_r + cf_g;
        } break;
        case ComponentFormatClass::RXGXBXAX: {
            std::string cf_r("R");
            cf_r.append(std::to_string(params[0]));
            cf_r.append("X");
            cf_r.append(std::to_string(params[1]));
            std::string cf_g("G");
            cf_g.append(std::to_string(params[2]));
            cf_g.append("X");
            cf_g.append(std::to_string(params[3]));
            std::string cf_b("B");
            cf_b.append(std::to_string(params[4]));
            cf_b.append("X");
            cf_b.append(std::to_string(params[5]));
            std::string cf_a("A");
            cf_a.append(std::to_string(params[6]));
            cf_a.append("X");
            cf_a.append(std::to_string(params[7]));
            return cf_r + cf_g + cf_b + cf_a;
        } break;
        case ComponentFormatClass::ARGB: {
            std::string cf_a("A");
            cf_a.append(std::to_string(params[0]));
            std::string cf_r("R");
            cf_r.append(std::to_string(params[1]));
            std::string cf_g("G");
            cf_g.append(std::to_string(params[2]));
            std::string cf_b("B");
            cf_b.append(std::to_string(params[3]));
            return cf_a + cf_r + cf_g + cf_b;
        } break;
        case ComponentFormatClass::ABGR: {
            std::string cf_a("A");
            cf_a.append(std::to_string(params[0]));
            std::string cf_b("B");
            cf_b.append(std::to_string(params[1]));
            std::string cf_g("G");
            cf_g.append(std::to_string(params[2]));
            std::string cf_r("R");
            cf_r.append(std::to_string(params[3]));
            return cf_a + cf_b + cf_g + cf_r;
        } break;
        case ComponentFormatClass::BGRA: {
            std::string cf_b("B");
            cf_b.append(std::to_string(params[0]));
            std::string cf_g("G");
            cf_g.append(std::to_string(params[1]));
            std::string cf_r("R");
            cf_r.append(std::to_string(params[2]));
            std::string cf_a("A");
            cf_a.append(std::to_string(params[3]));
            return cf_b + cf_g + cf_r + cf_a;
        } break;
        case ComponentFormatClass::BGR: {
            std::string cf_b("B");
            cf_b.append(std::to_string(params[0]));
            std::string cf_g("G");
            cf_g.append(std::to_string(params[1]));
            std::string cf_r("R");
            cf_r.append(std::to_string(params[2]));
            return cf_b + cf_g + cf_r;
        } break;
        case ComponentFormatClass::GBR: {
            std::string cf_g("G");
            cf_g.append(std::to_string(params[0]));
            std::string cf_b("B");
            cf_b.append(std::to_string(params[1]));
            std::string cf_r("R");
            cf_r.append(std::to_string(params[2]));
            if (style == Format::PrintStyles::OGFormat ||
                style == Format::PrintStyles::SnakeCase) {
                if (planes_n == 3) {
                    return cf_g + "_" + cf_b + "_" + cf_r;
                }
                else if (planes_n == 2) {
                    return cf_g + "_" + cf_b + cf_r;
                }
            }
            return cf_g + cf_b + cf_r;
        } break;
        case ComponentFormatClass::EBGR: {
            std::string cf_e("E");
            cf_e.append(std::to_string(params[0]));
            std::string cf_b("B");
            cf_b.append(std::to_string(params[1]));
            std::string cf_g("G");
            cf_g.append(std::to_string(params[2]));
            std::string cf_r("R");
            cf_r.append(std::to_string(params[3]));
            return cf_e + cf_b + cf_g + cf_r;
        } break;
        case ComponentFormatClass::GBGR: {
            std::string cf_g1("G");
            cf_g1.append(std::to_string(params[0]));
            std::string cf_b("B");
            cf_b.append(std::to_string(params[1]));
            std::string cf_g2("G");
            cf_g2.append(std::to_string(params[2]));
            std::string cf_r("R");
            cf_r.append(std::to_string(params[3]));
            return cf_g1 + cf_b + cf_g2 + cf_r;
        } break;
        case ComponentFormatClass::BGRG: {
            std::string cf_b("B");
            cf_b.append(std::to_string(params[0]));
            std::string cf_g1("G");
            cf_g1.append(std::to_string(params[1]));
            std::string cf_r("R");
            cf_r.append(std::to_string(params[2]));
            std::string cf_g2("G");
            cf_g2.append(std::to_string(params[3]));
            return cf_b + cf_g1 + cf_r + cf_g2;
        } break;
        case ComponentFormatClass::BXGXRXGX: {
            std::string cf_b("B");
            cf_b.append(std::to_string(params[0]));
            cf_b.append("X");
            cf_b.append(std::to_string(params[1]));
            std::string cf_g1("G");
            cf_g1.append(std::to_string(params[2]));
            cf_g1.append("X");
            cf_g1.append(std::to_string(params[3]));
            std::string cf_r("R");
            cf_r.append(std::to_string(params[4]));
            cf_r.append("X");
            cf_r.append(std::to_string(params[5]));
            std::string cf_g2("G");
            cf_g2.append(std::to_string(params[6]));
            cf_g2.append("X");
            cf_g2.append(std::to_string(params[7]));
            return cf_b + cf_g1 + cf_r + cf_g2;
        } break;
        case ComponentFormatClass::GXBXRX: {
            std::string cf_g("G");
            cf_g.append(std::to_string(params[0]));
            cf_g.append("X");
            cf_g.append(std::to_string(params[1]));
            std::string cf_b("B");
            cf_b.append(std::to_string(params[2]));
            cf_b.append("X");
            cf_b.append(std::to_string(params[3]));
            std::string cf_r("R");
            cf_r.append(std::to_string(params[4]));
            cf_r.append("X");
            cf_r.append(std::to_string(params[5]));
            if (style == Format::PrintStyles::OGFormat ||
                style == Format::PrintStyles::SnakeCase) {
                if (planes_n == 3) {
                    return cf_g + "_" + cf_b + "_" + cf_r;
                }
                else if (planes_n == 2) {
                    return cf_g + "_" + cf_b + cf_r;
                }
            }
            return cf_g + cf_b + cf_r;
        } break;
        case ComponentFormatClass::GXBXGXRX: {
            std::string cf_g1("G");
            cf_g1.append(std::to_string(params[0]));
            cf_g1.append("X");
            cf_g1.append(std::to_string(params[1]));
            std::string cf_b("B");
            cf_b.append(std::to_string(params[2]));
            cf_b.append("X");
            cf_b.append(std::to_string(params[3]));
            std::string cf_g2("G");
            cf_g2.append(std::to_string(params[4]));
            cf_g2.append("X");
            cf_g2.append(std::to_string(params[5]));
            std::string cf_r("R");
            cf_r.append(std::to_string(params[6]));
            cf_r.append("X");
            cf_r.append(std::to_string(params[7]));
            return cf_g1 + cf_b + cf_g2 + cf_r;
        } break;
        case ComponentFormatClass::D: {
            return "D" + std::to_string(params[0]);
        } break;
        case ComponentFormatClass::S: {
            return "S" + std::to_string(params[0]);
        } break;
        case ComponentFormatClass::XD: {
            std::string cf_d = "X";
            cf_d.append(std::to_string(params[0]));
            if (style == Format::PrintStyles::OGFormat ||
                style == Format::PrintStyles::SnakeCase) {
                cf_d.append("_");
            }
            cf_d.append("D");
            cf_d.append(std::to_string(params[1]));
            return cf_d;
        } break;
        case ComponentFormatClass::DS: {
            std::string cf_d("D");
            cf_d.append(std::to_string(params[0]));
            std::string cf_s("S");
            cf_s.append(std::to_string(params[1]));
            switch (style) {
                case Format::PrintStyles::OGFormat:
                case Format::PrintStyles::SnakeCase: {
                    return cf_d + "_" + cf_s;
                } break;
                case Format::PrintStyles::PPFormat:
                case Format::PrintStyles::CamelCase: {
                    return cf_d + cf_s;
                } break;
            }
        } break;
        case ComponentFormatClass::BC: {
            std::string cf_bc;
            switch (style) {
                case Format::PrintStyles::OGFormat:
                case Format::PrintStyles::SnakeCase: {
                    cf_bc = "BC";
                } break;
                case Format::PrintStyles::PPFormat:
                case Format::PrintStyles::CamelCase: {
                    cf_bc = "Bc";
                } break;
            }
            if (params[0] == 1) {
                switch (style) {
                    case Format::PrintStyles::OGFormat:
                    case Format::PrintStyles::SnakeCase: {
                        cf_bc.append("1_");
                    } break;
                    case Format::PrintStyles::PPFormat:
                    case Format::PrintStyles::CamelCase: {
                        cf_bc.append("1");
                    } break;
                }
                cf_bc.append(_to_string(static_cast<ComponentFormatClass>(params[1]), style));
            }
            else if (params[0] == 6) {
                cf_bc.append("6H");
            }
            else {
                cf_bc.append(std::to_string(params[0]));
            }
            return cf_bc;
        } break;
        case ComponentFormatClass::ETC2: {
            std::string cf_etc2;
            switch (style) {
                case Format::PrintStyles::OGFormat:
                case Format::PrintStyles::SnakeCase: {
                    cf_etc2 = "ETC2_";
                } break;
                case Format::PrintStyles::PPFormat:
                case Format::PrintStyles::CamelCase: {
                    cf_etc2 = "Etc2";
                } break;
            }
            ComponentFormat etc2_fmt;
            etc2_fmt.type = static_cast<ComponentFormatClass>(params[0]);
            std::memcpy(etc2_fmt.params, params + 1, sizeof(etc2_fmt.params) - 1);
            cf_etc2.append(to_string(etc2_fmt, style));
            return cf_etc2;
        } break;
        case ComponentFormatClass::EAC: {
            std::string cf_eac;
            switch (style) {
                case Format::PrintStyles::OGFormat:
                case Format::PrintStyles::SnakeCase: {
                    cf_eac = "EAC_";
                } break;
                case Format::PrintStyles::PPFormat:
                case Format::PrintStyles::CamelCase: {
                    cf_eac = "Eac";
                } break;
            }
            ComponentFormat eac_fmt;
            eac_fmt.type = static_cast<ComponentFormatClass>(params[0]);
            std::memcpy(eac_fmt.params, params + 1, sizeof(eac_fmt.params) - 1);
            cf_eac.append(to_string(eac_fmt, style));
            return cf_eac;
        } break;
        case ComponentFormatClass::ASTC: {
            std::string cf_astc;
            switch (style) {
                case Format::PrintStyles::OGFormat:
                case Format::PrintStyles::SnakeCase: {
                    cf_astc = "ASTC_";
                    cf_astc.append(std::to_string(params[0]) + "x" + std::to_string(params[1]));
                } break;
                case Format::PrintStyles::PPFormat:
                case Format::PrintStyles::CamelCase: {
                    cf_astc = "Astc";
                    cf_astc.append(std::to_string(params[0]) + "x" + std::to_string(params[1]));
                } break;
            }
            return cf_astc;
        } break;
        case ComponentFormatClass::PVRTC: {
            std::string cf_pvrtc;
            switch (style) {
                case Format::PrintStyles::OGFormat:
                case Format::PrintStyles::SnakeCase: {
                    cf_pvrtc = "PVRTC";
                    cf_pvrtc.append(std::to_string(params[0]) + "_");
                    cf_pvrtc.append(std::to_string(params[1]) + "BPP");
                } break;
                case Format::PrintStyles::PPFormat:
                case Format::PrintStyles::CamelCase: {
                    cf_pvrtc = "Pvrtc";
                    cf_pvrtc.append(std::to_string(params[0]) + std::to_string(params[1]) +
                                    "Bpp");
                } break;
            }
            return cf_pvrtc;
        } break;
        case ComponentFormatClass::Invalid:
        default: {
            spec::trace("to_string got invalid component format type: ",
                        static_cast<int>(v.type), '!');
            return "";
        } break;
    }
    spec::fatal("Unreachable@" FILE_LINE_STR);
}
static std::string _to_string(PlanePacking cls, Format::PrintStyles) {
    switch (cls) {
        case PlanePacking::_420: {
            return "420";
        } break;
        case PlanePacking::_422: {
            return "422";
        } break;
        case PlanePacking::_444: {
            return "444";
        } break;
        case PlanePacking::Undefined: {
            return "";
        } break;
    }
    spec::fatal("Unreachable@" FILE_LINE_STR);
}

std::string to_string(NumericFormat v, Format::PrintStyles style) {
    switch (style) {
        case Format::PrintStyles::OGFormat:
        case Format::PrintStyles::SnakeCase: {
            switch (v) {
                case NumericFormat::Unorm: {
                    return "UNORM";
                }
                case NumericFormat::Snorm: {
                    return "SNORM";
                }
                case NumericFormat::Uscaled: {
                    return "USCALED";
                }
                case NumericFormat::Sscaled: {
                    return "SSCALED";
                }
                case NumericFormat::Uint: {
                    return "UINT";
                }
                case NumericFormat::Sint: {
                    return "SINT";
                }
                case NumericFormat::Ufloat: {
                    return "UFLOAT";
                }
                case NumericFormat::Sfloat: {
                    return "SFLOAT";
                }
                case NumericFormat::Srgb: {
                    return "SRGB";
                }
                case NumericFormat::NvI10F5: {
                    return "S10_5_NV";
                }
                default: {
                    spec::fatal("to_string got invalid numeric format(", static_cast<int>(v),
                                ")!");
                }
            }
        } break;
        case Format::PrintStyles::PPFormat:
        case Format::PrintStyles::CamelCase: {
            switch (v) {
                case NumericFormat::Unorm: {
                    return "Unorm";
                }
                case NumericFormat::Snorm: {
                    return "Snorm";
                }
                case NumericFormat::Uscaled: {
                    return "Uscaled";
                }
                case NumericFormat::Sscaled: {
                    return "Sscaled";
                }
                case NumericFormat::Uint: {
                    return "Uint";
                }
                case NumericFormat::Sint: {
                    return "Sint";
                }
                case NumericFormat::Ufloat: {
                    return "Ufloat";
                }
                case NumericFormat::Sfloat: {
                    return "Sfloat";
                }
                case NumericFormat::Srgb: {
                    return "Srgb";
                }
                case NumericFormat::NvI10F5: {
                    return "S105NV";
                }
                default: {
                    spec::fatal("to_string got invalid numeric format(", static_cast<int>(v),
                                ")!");
                }
            }
        } break;
        default: {
            spec::fatal("Invalid Format::PrintStyles"
                        " argument passed to to_string!");
        } break;
    }
    spec::fatal("Unreachable@" FILE_LINE_STR);
}
std::string to_string(ComponentFormat v, Format::PrintStyles style) {
    return _to_string(v, style, 0);
}
std::string to_string(Packing v, Format::PrintStyles style) {
    std::string fmt;
    if (v.m >= 1) {
        if (v.m > 1) {
            fmt.append(std::to_string(v.m));
        }
        switch (style) {
            case Format::PrintStyles::OGFormat:
            case Format::PrintStyles::SnakeCase: {
                fmt.append("PACK");
            } break;
            case Format::PrintStyles::PPFormat:
            case Format::PrintStyles::CamelCase: {
                fmt.append("Pack");
            } break;
        }
        fmt.append(std::to_string(v.nn));
    }
    return fmt;
}
std::string to_string(Planes v, Format::PrintStyles style) {
    std::string fmt;
    if (v.n >= 1) {
        if (v.n == 1) {
            // Without different resolution planes.
            fmt = _to_string(v.p, style);
        }
        else {
            fmt.append(std::to_string(v.n));
            switch (style) {
                case Format::PrintStyles::OGFormat:
                case Format::PrintStyles::SnakeCase: {
                    fmt.append("PLANE_");
                } break;
                case Format::PrintStyles::PPFormat:
                case Format::PrintStyles::CamelCase: {
                    fmt.append("Plane");
                } break;
            }
            fmt.append(_to_string(v.p, style));
        }
    }
    return fmt;
}

std::string to_string(Format fmt, Format::PrintStyles style) {
    std::string invalid_name = _to_string_invalid(style);

    if (fmt.undefined || fmt.numeric_format == NumericFormat::Invalid) {
        return invalid_name;
    }

    std::string str;
    if (fmt.component_format.type != ComponentFormatClass::DS) {
        std::string numeric_format = to_string(fmt.numeric_format, style);
        std::string component_format = _to_string(fmt.component_format, style, fmt.planes.n);
        // Plane specifier goes between component and numeric format.
        if (fmt.planes.n > 0) {
            std::string plane_format = to_string(fmt.planes, style);
            switch (style) {
                case Format::PrintStyles::OGFormat:
                case Format::PrintStyles::SnakeCase: {
                    str = component_format + "_" + plane_format + "_" + numeric_format;
                } break;
                case Format::PrintStyles::PPFormat:
                case Format::PrintStyles::CamelCase: {
                    str = component_format + plane_format + numeric_format;
                } break;
            }
        }
        else {
            // Combine numeric and color/compression format components.
            switch (style) {
                case Format::PrintStyles::OGFormat:
                case Format::PrintStyles::SnakeCase: {
                    str = component_format + "_" + numeric_format;
                } break;
                case Format::PrintStyles::PPFormat:
                case Format::PrintStyles::CamelCase: {
                    str = component_format + numeric_format;
                } break;
            }
        }
    }
    else {
        // Special case for Depth-Stencil combined formats.
        std::string numeric_format = to_string(fmt.numeric_format, style);
        std::string cf_d("D");
        cf_d.append(std::to_string(fmt.component_format.params[0]));
        std::string cf_s("S");
        cf_s.append(std::to_string(fmt.component_format.params[1]));
        switch (style) {
            case Format::PrintStyles::OGFormat:
            case Format::PrintStyles::SnakeCase: {
                str = cf_d + "_" + numeric_format + "_" + cf_s + "_UINT";
            } break;
            case Format::PrintStyles::PPFormat:
            case Format::PrintStyles::CamelCase: {
                str = cf_d + numeric_format + cf_s + "Uint";
            } break;
        }
    }

    // Append optional packing and/or block post-fixes.
    if (fmt.packing.m > 0) {
        std::string packing_format = to_string(fmt.packing, style);
        switch (style) {
            case Format::PrintStyles::OGFormat:
            case Format::PrintStyles::SnakeCase: {
                str.append("_" + packing_format);
            } break;
            case Format::PrintStyles::PPFormat:
            case Format::PrintStyles::CamelCase: {
                str.append(packing_format);
            } break;
        }
    }

    if (fmt.block) {
        switch (style) {
            case Format::PrintStyles::OGFormat:
            case Format::PrintStyles::SnakeCase: {
                str.append("_BLOCK");
            } break;
            case Format::PrintStyles::PPFormat:
            case Format::PrintStyles::CamelCase: {
                str.append("Block");
            } break;
        }
    }

    if (fmt.component_format.type == ComponentFormatClass::PVRTC) {
        switch (style) {
            case Format::PrintStyles::OGFormat:
            case Format::PrintStyles::SnakeCase: {
                str.append("_IMG");
            } break;
            case Format::PrintStyles::PPFormat:
            case Format::PrintStyles::CamelCase: {
                str.append("IMG");
            } break;
        }
    }

    if (style == Format::PrintStyles::OGFormat) {
        str = "VK_FORMAT_" + str;
    }
    if (style == Format::PrintStyles::PPFormat) {
        str = "e" + str;
    }

    return str;
}

int get_red_bits(ComponentFormat component_format) {
    switch (component_format.type) {
        case ComponentFormatClass::R:
        case ComponentFormatClass::RG:
        case ComponentFormatClass::RGB:
        case ComponentFormatClass::RGBA:
        case ComponentFormatClass::RX:
        case ComponentFormatClass::RXGX:
        case ComponentFormatClass::RXGXBXAX: {
            return component_format.params[0];
        }
        case ComponentFormatClass::ARGB: {
            return component_format.params[1];
        }
        case ComponentFormatClass::BGRA:
        case ComponentFormatClass::BGRG:
        case ComponentFormatClass::BGR:
        case ComponentFormatClass::GBR: {
            return component_format.params[2];
        }
        case ComponentFormatClass::EBGR:
        case ComponentFormatClass::GBGR:
        case ComponentFormatClass::ABGR: {
            return component_format.params[3];
        }

        case ComponentFormatClass::BXGXRXGX:
        case ComponentFormatClass::GXBXRX: {
            return component_format.params[4];
        }
        case ComponentFormatClass::GXBXGXRX: {
            return component_format.params[6];
        }

        case ComponentFormatClass::D:
        case ComponentFormatClass::S:
        case ComponentFormatClass::XD:
        case ComponentFormatClass::DS:
        case ComponentFormatClass::BC:
        case ComponentFormatClass::ETC2:
        case ComponentFormatClass::EAC:
        case ComponentFormatClass::ASTC:
        case ComponentFormatClass::PVRTC:
        case ComponentFormatClass::Invalid:
        default: {
            return 0;
        }
    }
}
int get_green_bits(ComponentFormat component_format) {
    switch (component_format.type) {
        case ComponentFormatClass::GBR:
        case ComponentFormatClass::GXBXRX:
        case ComponentFormatClass::GBGR:
        case ComponentFormatClass::GXBXGXRX: {
            return component_format.params[0];
        }
        case ComponentFormatClass::RG:
        case ComponentFormatClass::RGB:
        case ComponentFormatClass::BGR:
        case ComponentFormatClass::RGBA:
        case ComponentFormatClass::BGRA:
        case ComponentFormatClass::BGRG: {
            return component_format.params[1];
        }
        case ComponentFormatClass::RXGX:
        case ComponentFormatClass::ARGB:
        case ComponentFormatClass::ABGR:
        case ComponentFormatClass::EBGR:
        case ComponentFormatClass::RXGXBXAX:
        case ComponentFormatClass::BXGXRXGX: {
            return component_format.params[2];
        }

        case ComponentFormatClass::R:
        case ComponentFormatClass::RX:
        case ComponentFormatClass::D:
        case ComponentFormatClass::S:
        case ComponentFormatClass::XD:
        case ComponentFormatClass::DS:
        case ComponentFormatClass::BC:
        case ComponentFormatClass::ETC2:
        case ComponentFormatClass::EAC:
        case ComponentFormatClass::ASTC:
        case ComponentFormatClass::PVRTC:
        case ComponentFormatClass::Invalid:
        default: {
            return 0;
        }
    }
}
int get_blue_bits(ComponentFormat component_format) {
    switch (component_format.type) {
        case ComponentFormatClass::BGRA:
        case ComponentFormatClass::BGR:
        case ComponentFormatClass::BGRG:
        case ComponentFormatClass::BXGXRXGX: {
            return component_format.params[0];
        }
        case ComponentFormatClass::ABGR:
        case ComponentFormatClass::GBR:
        case ComponentFormatClass::EBGR:
        case ComponentFormatClass::GBGR: {
            return component_format.params[1];
        }
        case ComponentFormatClass::RGB:
        case ComponentFormatClass::RGBA:
        case ComponentFormatClass::GXBXRX:
        case ComponentFormatClass::GXBXGXRX: {
            return component_format.params[2];
        }
        case ComponentFormatClass::ARGB: {
            return component_format.params[3];
        }
        case ComponentFormatClass::RXGXBXAX: {
            return component_format.params[4];
        }

        case ComponentFormatClass::R:
        case ComponentFormatClass::RG:
        case ComponentFormatClass::RX:
        case ComponentFormatClass::RXGX:
        case ComponentFormatClass::D:
        case ComponentFormatClass::S:
        case ComponentFormatClass::XD:
        case ComponentFormatClass::DS:
        case ComponentFormatClass::BC:
        case ComponentFormatClass::ETC2:
        case ComponentFormatClass::EAC:
        case ComponentFormatClass::ASTC:
        case ComponentFormatClass::PVRTC:
        case ComponentFormatClass::Invalid:
        default: {
            return 0;
        }
    }
}
int get_alpha_bits(ComponentFormat component_format) {
    switch (component_format.type) {
        case ComponentFormatClass::ARGB:
        case ComponentFormatClass::ABGR: {
            return component_format.params[0];
        }
        case ComponentFormatClass::RGBA:
        case ComponentFormatClass::BGRA: {
            return component_format.params[3];
        }
        case ComponentFormatClass::RXGXBXAX: {
            return component_format.params[6];
        }

        case ComponentFormatClass::R:
        case ComponentFormatClass::RG:
        case ComponentFormatClass::RGB:
        case ComponentFormatClass::RX:
        case ComponentFormatClass::RXGX:
        case ComponentFormatClass::BGR:
        case ComponentFormatClass::GBR:
        case ComponentFormatClass::EBGR:
        case ComponentFormatClass::GBGR:
        case ComponentFormatClass::BGRG:
        case ComponentFormatClass::BXGXRXGX:
        case ComponentFormatClass::GXBXRX:
        case ComponentFormatClass::GXBXGXRX:
        case ComponentFormatClass::D:
        case ComponentFormatClass::S:
        case ComponentFormatClass::XD:
        case ComponentFormatClass::DS:
        case ComponentFormatClass::BC:
        case ComponentFormatClass::ETC2:
        case ComponentFormatClass::EAC:
        case ComponentFormatClass::ASTC:
        case ComponentFormatClass::PVRTC:
        case ComponentFormatClass::Invalid:
        default: {
            return 0;
        }
    }
}

size_t get_bits_per_pixel(ComponentFormat component_format) {
    switch (component_format.type) {
        case ComponentFormatClass::R:
        case ComponentFormatClass::D:
        case ComponentFormatClass::S: {
            return component_format.params[0];
        }
        case ComponentFormatClass::RG:
        case ComponentFormatClass::RX:
        case ComponentFormatClass::XD:
        case ComponentFormatClass::DS: {
            return component_format.params[0] + component_format.params[1];
        }
        case ComponentFormatClass::RGB:
        case ComponentFormatClass::BGR:
        case ComponentFormatClass::GBR: {
            return component_format.params[0] + component_format.params[1] +
                   component_format.params[2];
        }
        case ComponentFormatClass::RGBA:
        case ComponentFormatClass::RXGX:
        case ComponentFormatClass::ARGB:
        case ComponentFormatClass::ABGR:
        case ComponentFormatClass::BGRA:
        case ComponentFormatClass::GBGR:
        case ComponentFormatClass::BGRG:
        case ComponentFormatClass::EBGR: {
            return component_format.params[0] + component_format.params[1] +
                   component_format.params[2] + component_format.params[3];
        }
        case ComponentFormatClass::GXBXRX: {
            return component_format.params[0] + component_format.params[1] +
                   component_format.params[2] + component_format.params[3] +
                   component_format.params[4] + component_format.params[5];
        }
        case ComponentFormatClass::BXGXRXGX:
        case ComponentFormatClass::RXGXBXAX:
        case ComponentFormatClass::GXBXGXRX: {
            return component_format.params[0] + component_format.params[1] +
                   component_format.params[2] + component_format.params[3] +
                   component_format.params[4] + component_format.params[5] +
                   component_format.params[6] + component_format.params[7];
        }
        /* Compressed */
        case ComponentFormatClass::BC:
        case ComponentFormatClass::ETC2:
        case ComponentFormatClass::EAC:
        case ComponentFormatClass::ASTC:
        case ComponentFormatClass::PVRTC: {
            return 0;
        }
        /* Invalid */
        case ComponentFormatClass::Invalid:
        default: {
            return 0;
        }
    }
}

}  // namespace satr::vk
