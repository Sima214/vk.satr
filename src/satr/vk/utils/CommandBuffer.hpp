#ifndef SATR_VK_COMMANDBUFFER_HPP
#define SATR_VK_COMMANDBUFFER_HPP
/**
 * @file
 * @brief
 */

#include <Buffer.hpp>
#include <Context.hpp>
#include <Image.hpp>
#include <SharedQueue.hpp>
#include <Utils.hpp>
#include <vulkan/vulkan.hpp>

#include <forward_list>
#include <functional>

namespace satr::vk {

class CommandBufferContext : public spec::INonCopyable {
   protected:
    ::vk::CommandBuffer _cmd_buf;
    ::vk::Fence _cmd_complete;

    std::forward_list<Buffer> _res_lst_buffer;
    std::forward_list<Image1D> _res_lst_img1d;
    std::forward_list<Image2D> _res_lst_img2d;
    std::forward_list<Image3D> _res_lst_img3d;
    /**
     * The following objects will invalidate the command buffer they are used by when destroyed:
     *   - VkEvent
     *   - VkQueryPool
     *   - VkBufferView
     *   - VkImageView
     *   - VkPipeline
     *   - VkSampler
     *   - VkSamplerYcbcrConversion
     *   - VkDescriptorPool
     *   - VkFramebuffer
     *   - VkDescriptorSet
     *   - VkIndirectCommandsLayoutNV
     *   - VkAccelerationStructureNV
     *   - VkAccelerationStructureKHR
     *   - VkVideoSessionKHR
     *   - VkVideoSessionParametersKHR
     * Handled:
     *   - VkDeviceMemory
     *   - VkBuffer
     *   - VkImage
     * Handled implicitly:
     *   - VkCommandPool
     *   - VkCommandBuffer
     *   - VkRenderPass
     */

   public:
    /** Lifecycle */
    CommandBufferContext() noexcept : _cmd_buf(nullptr), _cmd_complete(nullptr) {}
    /**
     * NOTE: Start of lifecycle - explicitly resets command buffer and start command recording.
     */
    CommandBufferContext(::vk::CommandBuffer cmd_buf, ::vk::Fence cmd_complete,
                         ::vk::CommandBufferUsageFlags flags =
                              ::vk::CommandBufferUsageFlagBits::eOneTimeSubmit);

    CommandBufferContext(CommandBufferContext&&) noexcept;
    CommandBufferContext& operator=(CommandBufferContext&&);

    ~CommandBufferContext();

    /** Getters/Setters */
    operator bool() const {
        return static_cast<bool>(_cmd_buf);
    }
    operator ::vk::CommandBuffer() const {
        return get_cmd_buffer();
    }
    operator ::vk::Fence() const {
        return get_fence();
    }

    ::vk::CommandBuffer get_cmd_buffer() const {
        return _cmd_buf;
    }
    ::vk::Fence get_fence() const {
        return _cmd_complete;
    }

    /** Operations */
    void register_resource(Buffer& res);
    void register_resource(Image1D& res);
    void register_resource(Image2D& res);
    void register_resource(Image3D& res);

    bool submit(const SharedQueue& queue, ::vk::SubmitInfo& submit_info) const;
    bool submit(const SharedQueue& queue) const {
        ::vk::SubmitInfo submit_info;
        return submit(queue, submit_info);
    }
    /**
     * NOTE: Also the end of the lifecycle - the object is no longer usable.
     */
    bool wait(::vk::Device dev);

   protected:
    bool _start(::vk::CommandBufferUsageFlags flags);
};

/**
 * For one-of command submissions - the associated vulkan command pool,
 * buffer and fence objects are destroyed along with this.
 */
class SingleUseCommandBufferContext : public CommandBufferContext {
   protected:
    std::reference_wrapper<Context> _engine;
    QueueSlotRole _queue_role;
    size_t _queue_sub_index;
    ::vk::CommandPool _cmd_pool;

   public:
    SingleUseCommandBufferContext(Context& engine, QueueSlotRole role, size_t sub_index = 0);
    SingleUseCommandBufferContext(SingleUseCommandBufferContext&&);
    SingleUseCommandBufferContext& operator=(SingleUseCommandBufferContext&&);
    ~SingleUseCommandBufferContext();

    bool submit();
    bool wait();
};

}  // namespace satr::vk

#endif /*SATR_VK_COMMANDBUFFER_HPP*/