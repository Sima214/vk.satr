#include "DeviceCapabilities.hpp"

#include <Extensions.hpp>
#include <SharedQueue.hpp>
#include <logger/Logger.hpp>
#include <trace/Trace.hpp>
#include <vulkan/vulkan_to_string.hpp>

#include <algorithm>
#include <array>
#include <cstdint>
#include <memory>

namespace satr::vk {

bool DeviceCapabilities::Filter::is_device_type_accepted(
     ::vk::PhysicalDeviceType dev_type) const {
    switch (dev_type) {
        case ::vk::PhysicalDeviceType::eOther: {
            return MASK_TEST(device_type_mask, DEVICE_TYPE_MASK_OTHER);
        }
        case ::vk::PhysicalDeviceType::eIntegratedGpu: {
            return MASK_TEST(device_type_mask, DEVICE_TYPE_MASK_INTEGRATED_GPU);
        }
        case ::vk::PhysicalDeviceType::eDiscreteGpu: {
            return MASK_TEST(device_type_mask, DEVICE_TYPE_MASK_DISCRETE_GPU);
        }
        case ::vk::PhysicalDeviceType::eVirtualGpu: {
            return MASK_TEST(device_type_mask, DEVICE_TYPE_MASK_VIRTUAL_GPU);
        }
        case ::vk::PhysicalDeviceType::eCpu: {
            return MASK_TEST(device_type_mask, DEVICE_TYPE_MASK_CPU);
        }
    }
    spec::fatal("DeviceCapabilities::Filter::is_device_type_accepted unknown dev_type `",
                to_string(dev_type), "`!");
}

DeviceCapabilities::DeviceCapabilities(::vk::PhysicalDevice phys_dev) :
    _handle(phys_dev), _props(_handle.getProperties()), _features(_handle.getFeatures()),
    _mem_props(_handle.getMemoryProperties()){};

::vk::PhysicalDevice DeviceCapabilities::get_handle() const {
    return _handle;
}
uint32_t DeviceCapabilities::get_api_version() const {
    return _props.apiVersion;
}
uint32_t DeviceCapabilities::get_driver_version() const {
    return _props.driverVersion;
}
uint32_t DeviceCapabilities::get_vendor_id() const {
    return _props.vendorID;
}
uint32_t DeviceCapabilities::get_device_id() const {
    return _props.deviceID;
}
::vk::PhysicalDeviceType DeviceCapabilities::get_device_type() const {
    return _props.deviceType;
}
const char* DeviceCapabilities::get_device_name() const {
    return _props.deviceName;
}
::vk::DeviceSize DeviceCapabilities::measure_total_heap_for_flags(
     ::vk::MemoryPropertyFlags flags) const {
    ::vk::DeviceSize r = 0;
    // For each heap:
    for (uint32_t heap_index = 0; heap_index < _mem_props.memoryHeapCount; heap_index++) {
        ::vk::MemoryPropertyFlags accumulated_flags;
        // For each memory type:
        for (uint32_t type_index = 0; type_index < _mem_props.memoryTypeCount; type_index++) {
            if (_mem_props.memoryTypes[type_index].heapIndex == heap_index) {
                accumulated_flags |= _mem_props.memoryTypes[type_index].propertyFlags;
            }
        }
        // If all required flags are set, accumulate heap size:
        if ((accumulated_flags & flags) == flags) {
            r += _mem_props.memoryHeaps[heap_index].size;
        }
    }
    return r;
}
::vk::DeviceSize DeviceCapabilities::get_device_local_bytes() const {
    return measure_total_heap_for_flags(::vk::MemoryPropertyFlagBits::eDeviceLocal);
}
::vk::DeviceSize DeviceCapabilities::get_device_local_host_visible_bytes() const {
    return measure_total_heap_for_flags(::vk::MemoryPropertyFlagBits::eDeviceLocal |
                                        ::vk::MemoryPropertyFlagBits::eHostVisible);
}

const ::vk::PhysicalDeviceLimits& DeviceCapabilities::get_limits() const {
    return _props.limits;
}
const ::vk::PhysicalDeviceSparseProperties& DeviceCapabilities::get_sparse_properties() const {
    return _props.sparseProperties;
}
const ::vk::PhysicalDeviceFeatures& DeviceCapabilities::get_features() const {
    return _features;
}
const ::vk::PhysicalDeviceMemoryProperties& DeviceCapabilities::get_memory_properties() const {
    return _mem_props;
}

::vk::FormatProperties DeviceCapabilities::get_format_support(::vk::Format fmt) const {
    return _handle.getFormatProperties(fmt);
}

Extensions DeviceCapabilities::get_extensions(const char* layer_name) const {
    uint32_t count = 0;
    ::vk::Result r = _handle.enumerateDeviceExtensionProperties(layer_name, &count, nullptr);
    if (r == ::vk::Result::eErrorLayerNotPresent) {
        spec::trace("DeviceCapabilities::get_extensions -> LayerNotPresent!");
        return Extensions();
    }
    Extensions ext(count);
    std::vector<::vk::ExtensionProperties> props(count);
    r = _handle.enumerateDeviceExtensionProperties(layer_name, &count, props.data());
    if (r == ::vk::Result::eIncomplete) {
        spec::trace("DeviceCapabilities::get_extensions -> Incomplete.");
    }
    else if (r != ::vk::Result::eSuccess) {
        spec::trace("DeviceCapabilities::get_extensions -> ", to_string(r), ".");
        return Extensions();
    }
    for (uint32_t i = 0; i < count; i++) {
        ext.append(props[i].extensionName);
    }
    return ext;
}

bool DeviceCapabilities::match(const Filter& f1, const Requests& f2) const {
    if (get_api_version() < f1.api_version) {
        spec::trace("Device ", *this, " does not fulfill the minimum api version requirement!");
        return false;
    }
    if (!f1.is_device_type_accepted(get_device_type())) {
        spec::trace("Device ", *this, " does not match the allowed device types!");
        return false;
    }
    if (get_device_local_bytes() < f1.min_device_local) {
        spec::trace("Device ", *this, " does not have enough local memory!");
        return false;
    }
    if (get_device_local_host_visible_bytes() < f1.min_device_local_host_visible) {
        spec::trace("Device ", *this, " does not have enough local host-visible memory!");
        return false;
    }
    {
        Extensions supported = get_extensions();
        auto [required_len, required_data] = f1.required_extensions.get_data();
        for (uint32_t i = 0; i < required_len; i++) {
            const char* required_extension = required_data[i];
            if (!supported.contains(required_extension)) {
                spec::trace("Device ", *this, " does not support ", required_extension, "!");
                return false;
            }
        }
    }
    {
        const ::vk::PhysicalDeviceFeatures& required = f2.get_requested_features();
        const ::vk::PhysicalDeviceFeatures& supported = get_features();
        if (required.robustBufferAccess && !supported.robustBufferAccess) {
            spec::trace("Device ", *this, " does not support feature robustBufferAccess!");
            return false;
        }
        if (required.fullDrawIndexUint32 && !supported.fullDrawIndexUint32) {
            spec::trace("Device ", *this, " does not support feature fullDrawIndexUint32!");
            return false;
        }
        if (required.imageCubeArray && !supported.imageCubeArray) {
            spec::trace("Device ", *this, " does not support feature imageCubeArray!");
            return false;
        }
        if (required.independentBlend && !supported.independentBlend) {
            spec::trace("Device ", *this, " does not support feature independentBlend!");
            return false;
        }
        if (required.geometryShader && !supported.geometryShader) {
            spec::trace("Device ", *this, " does not support feature geometryShader!");
            return false;
        }
        if (required.tessellationShader && !supported.tessellationShader) {
            spec::trace("Device ", *this, " does not support feature tessellationShader!");
            return false;
        }
        if (required.sampleRateShading && !supported.sampleRateShading) {
            spec::trace("Device ", *this, " does not support feature sampleRateShading!");
            return false;
        }
        if (required.dualSrcBlend && !supported.dualSrcBlend) {
            spec::trace("Device ", *this, " does not support feature dualSrcBlend!");
            return false;
        }
        if (required.logicOp && !supported.logicOp) {
            spec::trace("Device ", *this, " does not support feature logicOp!");
            return false;
        }
        if (required.multiDrawIndirect && !supported.multiDrawIndirect) {
            spec::trace("Device ", *this, " does not support feature multiDrawIndirect!");
            return false;
        }
        if (required.drawIndirectFirstInstance && !supported.drawIndirectFirstInstance) {
            spec::trace("Device ", *this,
                        " does not support feature drawIndirectFirstInstance!");
            return false;
        }
        if (required.depthClamp && !supported.depthClamp) {
            spec::trace("Device ", *this, " does not support feature depthClamp!");
            return false;
        }
        if (required.depthBiasClamp && !supported.depthBiasClamp) {
            spec::trace("Device ", *this, " does not support feature depthBiasClamp!");
            return false;
        }
        if (required.fillModeNonSolid && !supported.fillModeNonSolid) {
            spec::trace("Device ", *this, " does not support feature fillModeNonSolid!");
            return false;
        }
        if (required.depthBounds && !supported.depthBounds) {
            spec::trace("Device ", *this, " does not support feature depthBounds!");
            return false;
        }
        if (required.wideLines && !supported.wideLines) {
            spec::trace("Device ", *this, " does not support feature wideLines!");
            return false;
        }
        if (required.largePoints && !supported.largePoints) {
            spec::trace("Device ", *this, " does not support feature largePoints!");
            return false;
        }
        if (required.alphaToOne && !supported.alphaToOne) {
            spec::trace("Device ", *this, " does not support feature alphaToOne!");
            return false;
        }
        if (required.multiViewport && !supported.multiViewport) {
            spec::trace("Device ", *this, " does not support feature multiViewport!");
            return false;
        }
        if (required.samplerAnisotropy && !supported.samplerAnisotropy) {
            spec::trace("Device ", *this, " does not support feature samplerAnisotropy!");
            return false;
        }
        if (required.textureCompressionETC2 && !supported.textureCompressionETC2) {
            spec::trace("Device ", *this, " does not support feature textureCompressionETC2!");
            return false;
        }
        if (required.textureCompressionASTC_LDR && !supported.textureCompressionASTC_LDR) {
            spec::trace("Device ", *this,
                        " does not support feature textureCompressionASTC_LDR!");
            return false;
        }
        if (required.textureCompressionBC && !supported.textureCompressionBC) {
            spec::trace("Device ", *this, " does not support feature textureCompressionBC!");
            return false;
        }
        if (required.occlusionQueryPrecise && !supported.occlusionQueryPrecise) {
            spec::trace("Device ", *this, " does not support feature occlusionQueryPrecise!");
            return false;
        }
        if (required.pipelineStatisticsQuery && !supported.pipelineStatisticsQuery) {
            spec::trace("Device ", *this, " does not support feature pipelineStatisticsQuery!");
            return false;
        }
        if (required.vertexPipelineStoresAndAtomics &&
            !supported.vertexPipelineStoresAndAtomics) {
            spec::trace("Device ", *this,
                        " does not support feature vertexPipelineStoresAndAtomics!");
            return false;
        }
        if (required.fragmentStoresAndAtomics && !supported.fragmentStoresAndAtomics) {
            spec::trace("Device ", *this,
                        " does not support feature fragmentStoresAndAtomics!");
            return false;
        }
        if (required.shaderTessellationAndGeometryPointSize &&
            !supported.shaderTessellationAndGeometryPointSize) {
            spec::trace("Device ", *this,
                        " does not support feature shaderTessellationAndGeometryPointSize!");
            return false;
        }
        if (required.shaderImageGatherExtended && !supported.shaderImageGatherExtended) {
            spec::trace("Device ", *this,
                        " does not support feature shaderImageGatherExtended!");
            return false;
        }
        if (required.shaderStorageImageExtendedFormats &&
            !supported.shaderStorageImageExtendedFormats) {
            spec::trace("Device ", *this,
                        " does not support feature shaderStorageImageExtendedFormats!");
            return false;
        }
        if (required.shaderStorageImageMultisample &&
            !supported.shaderStorageImageMultisample) {
            spec::trace("Device ", *this,
                        " does not support feature shaderStorageImageMultisample!");
            return false;
        }
        if (required.shaderStorageImageReadWithoutFormat &&
            !supported.shaderStorageImageReadWithoutFormat) {
            spec::trace("Device ", *this,
                        " does not support feature shaderStorageImageReadWithoutFormat!");
            return false;
        }
        if (required.shaderStorageImageWriteWithoutFormat &&
            !supported.shaderStorageImageWriteWithoutFormat) {
            spec::trace("Device ", *this,
                        " does not support feature shaderStorageImageWriteWithoutFormat!");
            return false;
        }
        if (required.shaderUniformBufferArrayDynamicIndexing &&
            !supported.shaderUniformBufferArrayDynamicIndexing) {
            spec::trace("Device ", *this,
                        " does not support feature shaderUniformBufferArrayDynamicIndexing!");
            return false;
        }
        if (required.shaderSampledImageArrayDynamicIndexing &&
            !supported.shaderSampledImageArrayDynamicIndexing) {
            spec::trace("Device ", *this,
                        " does not support feature shaderSampledImageArrayDynamicIndexing!");
            return false;
        }
        if (required.shaderStorageBufferArrayDynamicIndexing &&
            !supported.shaderStorageBufferArrayDynamicIndexing) {
            spec::trace("Device ", *this,
                        " does not support feature shaderStorageBufferArrayDynamicIndexing!");
            return false;
        }
        if (required.shaderStorageImageArrayDynamicIndexing &&
            !supported.shaderStorageImageArrayDynamicIndexing) {
            spec::trace("Device ", *this,
                        " does not support feature shaderStorageImageArrayDynamicIndexing!");
            return false;
        }
        if (required.shaderClipDistance && !supported.shaderClipDistance) {
            spec::trace("Device ", *this, " does not support feature shaderClipDistance!");
            return false;
        }
        if (required.shaderCullDistance && !supported.shaderCullDistance) {
            spec::trace("Device ", *this, " does not support feature shaderCullDistance!");
            return false;
        }
        if (required.shaderFloat64 && !supported.shaderFloat64) {
            spec::trace("Device ", *this, " does not support feature shaderFloat64!");
            return false;
        }
        if (required.shaderInt64 && !supported.shaderInt64) {
            spec::trace("Device ", *this, " does not support feature shaderInt64!");
            return false;
        }
        if (required.shaderInt16 && !supported.shaderInt16) {
            spec::trace("Device ", *this, " does not support feature shaderInt16!");
            return false;
        }
        if (required.shaderResourceResidency && !supported.shaderResourceResidency) {
            spec::trace("Device ", *this, " does not support feature shaderResourceResidency!");
            return false;
        }
        if (required.shaderResourceMinLod && !supported.shaderResourceMinLod) {
            spec::trace("Device ", *this, " does not support feature shaderResourceMinLod!");
            return false;
        }
        if (required.sparseBinding && !supported.sparseBinding) {
            spec::trace("Device ", *this, " does not support feature sparseBinding!");
            return false;
        }
        if (required.sparseResidencyBuffer && !supported.sparseResidencyBuffer) {
            spec::trace("Device ", *this, " does not support feature sparseResidencyBuffer!");
            return false;
        }
        if (required.sparseResidencyImage2D && !supported.sparseResidencyImage2D) {
            spec::trace("Device ", *this, " does not support feature sparseResidencyImage2D!");
            return false;
        }
        if (required.sparseResidencyImage3D && !supported.sparseResidencyImage3D) {
            spec::trace("Device ", *this, " does not support feature sparseResidencyImage3D!");
            return false;
        }
        if (required.sparseResidency2Samples && !supported.sparseResidency2Samples) {
            spec::trace("Device ", *this, " does not support feature sparseResidency2Samples!");
            return false;
        }
        if (required.sparseResidency4Samples && !supported.sparseResidency4Samples) {
            spec::trace("Device ", *this, " does not support feature sparseResidency4Samples!");
            return false;
        }
        if (required.sparseResidency8Samples && !supported.sparseResidency8Samples) {
            spec::trace("Device ", *this, " does not support feature sparseResidency8Samples!");
            return false;
        }
        if (required.sparseResidency16Samples && !supported.sparseResidency16Samples) {
            spec::trace("Device ", *this,
                        " does not support feature sparseResidency16Samples!");
            return false;
        }
        if (required.sparseResidencyAliased && !supported.sparseResidencyAliased) {
            spec::trace("Device ", *this, " does not support feature sparseResidencyAliased!");
            return false;
        }
        if (required.variableMultisampleRate && !supported.variableMultisampleRate) {
            spec::trace("Device ", *this, " does not support feature variableMultisampleRate!");
            return false;
        }
        if (required.inheritedQueries && !supported.inheritedQueries) {
            spec::trace("Device ", *this, " does not support feature inheritedQueries!");
            return false;
        }
    }
    return true;
}

std::optional<::vk::Device> DeviceCapabilities::create_logical(
     const Requests& req, const Extensions& extensions,
     std::pair<uint32_t, const char* const*> layers,
     std::tuple<size_t, const QueueSlotRole*, QueueSlotIndex*> queue_slots) const {
    // Prepare vulkan arg-struct with already available arguments.
    ::vk::DeviceCreateInfo dev_create_info;
    std::tie(dev_create_info.enabledLayerCount, dev_create_info.ppEnabledLayerNames) = layers;
    std::tie(dev_create_info.enabledExtensionCount, dev_create_info.ppEnabledExtensionNames) =
         extensions.get_data();
    dev_create_info.pEnabledFeatures = &req.get_requested_features();
    // Parse availability and select queues indexes.
    std::vector<uint32_t> queue_family_allocations;
    {
        uint32_t queue_families_count = 0;
        _handle.getQueueFamilyProperties(&queue_families_count, nullptr);
        ::vk::QueueFamilyProperties* queue_families =
             new ::vk::QueueFamilyProperties[queue_families_count];
        _handle.getQueueFamilyProperties(&queue_families_count, queue_families);
        // Discover queue family purposes.
        uint32_t dedicated_graphics_family = -1U;
        uint32_t dedicated_transfer_family = -1U;
        uint32_t dedicated_compute_family = -1U;
        for (size_t i = 0; i < queue_families_count; i++) {
            auto& family = queue_families[i];
            if (family.queueFlags & ::vk::QueueFlagBits::eGraphics) {
                // Presentation support is checked in higher level when required.
                if (dedicated_graphics_family == -1U) {
                    logger.logi("Found graphics queue family: ", i, '.');
                    dedicated_graphics_family = i;
                }
                else {
                    logger.logw("Found multiple graphics queue families. Found: ", i,
                                ", selected: ", dedicated_graphics_family, ".");
                }
            }
            else if (family.queueFlags & ::vk::QueueFlagBits::eCompute) {
                if (dedicated_compute_family == -1U) {
                    logger.logi("Found dedicated compute family: ", i, '.');
                    dedicated_compute_family = i;
                }
                else {
                    logger.logw("Found multiple dedicated compute queue families. Found: ", i,
                                ", selected: ", dedicated_compute_family, ".");
                }
            }
            else if (family.queueFlags & ::vk::QueueFlagBits::eVideoDecodeKHR) {
                logger.logi("Found dedicated video decode family: ", i, '.');
            }
            else if (family.queueFlags & ::vk::QueueFlagBits::eTransfer) {
                if (dedicated_transfer_family == -1U) {
                    logger.logi("Found dedicated transfer family: ", i, '.');
                    dedicated_transfer_family = i;
                }
                else {
                    logger.logw("Found multiple dedicated transfer queue families. Found: ", i,
                                ", selected: ", dedicated_transfer_family, ".");
                }
            }
        }
        queue_family_allocations.resize(queue_families_count, 0);
        // Select queue for each slot:
        auto [queue_slot_count, queue_slot_roles, queue_slot_indexes] = queue_slots;
        for (size_t slot_index = 0; slot_index < queue_slot_count; slot_index++) {
            queue_slot_indexes[slot_index] = select_queue_index(
                 slot_index, {queue_slot_count, queue_slot_roles, queue_slot_indexes},
                 queue_families, dedicated_graphics_family, dedicated_transfer_family,
                 dedicated_compute_family, queue_family_allocations.data());
            logger.logi("Selected queue ", queue_slot_indexes[slot_index], " for slot #",
                        slot_index, " with role `", queue_slot_roles[slot_index], "`.");
        }
        delete[] queue_families;
    }
    // Build vulkan structure describing queues to be allocated.
    std::vector<::vk::DeviceQueueCreateInfo> queue_requests;
    /**
     * NOTE: N is the maximum number of queues we might request from a family,
     * where N is the max count of queue slots.
     */
    std::vector<std::unique_ptr<float[]>> queue_requests_priorities;
    for (size_t family_index = 0; family_index < queue_family_allocations.size();
         family_index++) {
        uint32_t family_queue_count = queue_family_allocations[family_index];
        if (family_queue_count > 0) {
            ::vk::DeviceQueueCreateInfo create_info;
            create_info.queueFamilyIndex = family_index;
            create_info.queueCount = family_queue_count;
            std::unique_ptr<float[]> priorities(new float[family_queue_count]);
            create_info.pQueuePriorities = priorities.get();
            std::fill_n(priorities.get(), family_queue_count, 1.0f);
            queue_requests_priorities.push_back(std::move(priorities));
            queue_requests.push_back(create_info);
        }
    }
    // Actual logical device creation here!
    dev_create_info.queueCreateInfoCount = queue_requests.size();
    dev_create_info.pQueueCreateInfos = queue_requests.data();
    auto [device_result, device_handle] = _handle.createDevice(dev_create_info);
    if (device_result != ::vk::Result::eSuccess) {
        logger.loge("Could not allocate device: ", to_string(device_result), "!");
        return {};
    }
    return device_handle;
}

static std::optional<QueueSlotIndex> select_queue_with_reuse_fallback(
     const size_t slot_index,
     const std::tuple<size_t, const QueueSlotRole*, const QueueSlotIndex*> queue_slots,
     const uint32_t next_free_pool_slot_index, const uint32_t target_family_index,
     const ::vk::QueueFamilyProperties& target_family, uint32_t& target_family_allocation) {
    const auto [queue_slot_count, queue_slot_roles, queue_slot_indexes] = queue_slots;
    auto* queue_slot_indexes_end = queue_slot_indexes + slot_index;
    if (target_family.queueCount > target_family_allocation) {
        // Allocate new queue for slot.
        auto slot = QueueSlotIndex(target_family_index, target_family_allocation,
                                   next_free_pool_slot_index);
        target_family_allocation++;
        return slot;
    }
    else {
        // Reuse a queue for slot.
        auto* reused_slot = std::find_if(queue_slot_indexes, queue_slot_indexes_end,
                                         [=](const QueueSlotIndex& o) {
                                             return o.phys.family_index == target_family_index;
                                         });
        if (reused_slot != queue_slot_indexes_end) {
            return *reused_slot;
        }
        else {
            return {};
        }
    }
}

QueueSlotIndex DeviceCapabilities::select_queue_index(
     const size_t slot_index,
     const std::tuple<size_t, const QueueSlotRole*, const QueueSlotIndex*> queue_slots,
     const ::vk::QueueFamilyProperties* const phys_dev_queue_families,
     const uint32_t dedicated_graphics_family, const uint32_t dedicated_transfer_family,
     const uint32_t dedicated_compute_family, uint32_t* phys_dev_queue_family_allocations) {
    const auto [queue_slot_count, queue_slot_roles, queue_slot_indexes] = queue_slots;
    // auto* queue_slot_indexes_end = queue_slot_indexes + slot_index;
    // 1. Find next non-allocated (pool) slot.
    uint32_t next_free_pool_slot_index = 0;
    for (size_t i = 0; i < slot_index; i++) {
        auto c_i = queue_slot_indexes[i].slot_index;
        if (c_i >= next_free_pool_slot_index) {
            next_free_pool_slot_index = c_i + 1;
        }
    }
    // TODO: Lex grammar for tunable special format parsing.
    // 2. Select queues heuristically, based on role.
    switch (queue_slot_roles[slot_index]) {
        case QueueSlotRole::Graphics: {
            if (dedicated_graphics_family != -1U) {
                auto optslot = select_queue_with_reuse_fallback(
                     slot_index, queue_slots, next_free_pool_slot_index,
                     dedicated_graphics_family,
                     phys_dev_queue_families[dedicated_graphics_family],
                     phys_dev_queue_family_allocations[dedicated_graphics_family]);
                if (optslot) {
                    return optslot.value();
                }
            }
            logger.logf("Could not find graphics queue!");
        } break;
        case QueueSlotRole::Transfer: {
            if (dedicated_transfer_family != -1U) {
                auto optslot = select_queue_with_reuse_fallback(
                     slot_index, queue_slots, next_free_pool_slot_index,
                     dedicated_transfer_family,
                     phys_dev_queue_families[dedicated_transfer_family],
                     phys_dev_queue_family_allocations[dedicated_transfer_family]);
                if (optslot) {
                    return optslot.value();
                }
                else if (dedicated_graphics_family != -1U) {
                    // Not enough dedicated transfer queues.
                    // Fallback to a graphics queue family.
                    auto optslot = select_queue_with_reuse_fallback(
                         slot_index, queue_slots, next_free_pool_slot_index,
                         dedicated_graphics_family,
                         phys_dev_queue_families[dedicated_graphics_family],
                         phys_dev_queue_family_allocations[dedicated_graphics_family]);
                    if (optslot) {
                        return optslot.value();
                    }
                }
            }
            if (dedicated_graphics_family != -1U) {
                // Not dedicated transfer queue family.
                // Fallback to a graphics queue family.
                auto optslot = select_queue_with_reuse_fallback(
                     slot_index, queue_slots, next_free_pool_slot_index,
                     dedicated_graphics_family,
                     phys_dev_queue_families[dedicated_graphics_family],
                     phys_dev_queue_family_allocations[dedicated_graphics_family]);
                if (optslot) {
                    return optslot.value();
                }
            }
            logger.logf("Could not find transfer queue!");
        } break;
        case QueueSlotRole::Compute: {
            if (dedicated_compute_family != -1U) {
                auto optslot = select_queue_with_reuse_fallback(
                     slot_index, queue_slots, next_free_pool_slot_index,
                     dedicated_compute_family,
                     phys_dev_queue_families[dedicated_compute_family],
                     phys_dev_queue_family_allocations[dedicated_compute_family]);
                if (optslot) {
                    return optslot.value();
                }
                else if (dedicated_graphics_family != -1U &&
                         phys_dev_queue_families[dedicated_graphics_family].queueFlags &
                              ::vk::QueueFlagBits::eCompute) {
                    // Not enough dedicated compute queues.
                    // Fallback to a graphics queue family.
                    auto optslot = select_queue_with_reuse_fallback(
                         slot_index, queue_slots, next_free_pool_slot_index,
                         dedicated_graphics_family,
                         phys_dev_queue_families[dedicated_graphics_family],
                         phys_dev_queue_family_allocations[dedicated_graphics_family]);
                    if (optslot) {
                        return optslot.value();
                    }
                }
            }
            if (dedicated_graphics_family != -1U &&
                phys_dev_queue_families[dedicated_graphics_family].queueFlags &
                     ::vk::QueueFlagBits::eCompute) {
                // Not dedicated compute queue family.
                // Fallback to a graphics queue family.
                auto optslot = select_queue_with_reuse_fallback(
                     slot_index, queue_slots, next_free_pool_slot_index,
                     dedicated_graphics_family,
                     phys_dev_queue_families[dedicated_graphics_family],
                     phys_dev_queue_family_allocations[dedicated_graphics_family]);
                if (optslot) {
                    return optslot.value();
                }
            }
            logger.logf("Could not find transfer queue!");
        } break;
        case QueueSlotRole::Convert: {
            // If transfer queue has compute support, then use that.
            uint32_t transfer_family = dedicated_transfer_family;
            for (size_t i = 0; i < slot_index; i++) {
                if (queue_slot_roles[i] == QueueSlotRole::Transfer) {
                    transfer_family = queue_slot_indexes[i].phys.family_index;
                    break;
                }
            }
            if (transfer_family != -1U && phys_dev_queue_families[transfer_family].queueFlags &
                                               ::vk::QueueFlagBits::eCompute) {
                auto optslot = select_queue_with_reuse_fallback(
                     slot_index, queue_slots, next_free_pool_slot_index, transfer_family,
                     phys_dev_queue_families[transfer_family],
                     phys_dev_queue_family_allocations[transfer_family]);
                if (optslot) {
                    return optslot.value();
                }
            }
            if (dedicated_compute_family != -1U) {
                // Selected transfer family queue does not support compute.
                auto optslot = select_queue_with_reuse_fallback(
                     slot_index, queue_slots, next_free_pool_slot_index,
                     dedicated_compute_family,
                     phys_dev_queue_families[dedicated_compute_family],
                     phys_dev_queue_family_allocations[dedicated_compute_family]);
                if (optslot) {
                    return optslot.value();
                }
                else if (dedicated_graphics_family != -1U &&
                         phys_dev_queue_families[dedicated_graphics_family].queueFlags &
                              ::vk::QueueFlagBits::eCompute) {
                    // Not enough dedicated compute queues.
                    // Fallback to a graphics queue family.
                    auto optslot = select_queue_with_reuse_fallback(
                         slot_index, queue_slots, next_free_pool_slot_index,
                         dedicated_graphics_family,
                         phys_dev_queue_families[dedicated_graphics_family],
                         phys_dev_queue_family_allocations[dedicated_graphics_family]);
                    if (optslot) {
                        return optslot.value();
                    }
                }
            }
            if (dedicated_graphics_family != -1U &&
                phys_dev_queue_families[dedicated_graphics_family].queueFlags &
                     ::vk::QueueFlagBits::eCompute) {
                // Not dedicated compute queue family.
                // Fallback to a graphics queue family.
                auto optslot = select_queue_with_reuse_fallback(
                     slot_index, queue_slots, next_free_pool_slot_index,
                     dedicated_graphics_family,
                     phys_dev_queue_families[dedicated_graphics_family],
                     phys_dev_queue_family_allocations[dedicated_graphics_family]);
                if (optslot) {
                    return optslot.value();
                }
            }
            logger.logf("Could not find convert queue!");
        } break;
    }
    spec::fatal("Programming error: unhandled QueueSlotRole!");
}

}  // namespace satr::vk
