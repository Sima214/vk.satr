#include "Image.hpp"

#include <Context.hpp>
#include <Memory.hpp>
#include <logger/Logger.hpp>
#include <trace/Trace.hpp>
#include <vulkan/vulkan_to_string.hpp>

#include <string_view>
#include <type_traits>

namespace satr::vk {

template<::vk::ImageType type>
ImageImpl<type>::ImageImpl(ImageImpl&& o) noexcept :
    _handle(std::exchange(o._handle, nullptr)), _device(std::exchange(o._device, nullptr)),
    _memory(std::move(o._memory)), _format(o._format), _size(o._size) {
    spec::trace("Image::construct_move(", _handle, ")");
}
template<::vk::ImageType type>
ImageImpl<type>& ImageImpl<type>::operator=(ImageImpl&& o) noexcept {
    _handle = std::exchange(o._handle, nullptr);
    _device = std::exchange(o._device, nullptr);
    _memory = std::move(o._memory);
    _format = o._format;
    _size = o._size;
    spec::trace("Image::assign_move(", _handle, ")");
    return *this;
}
template<::vk::ImageType type> ImageImpl<type>::~ImageImpl() {
    if (_handle) {
        if (_memory) {
            // Heap-allocated ImageImpl.
            spec::trace("Image::delete(", _handle, ")");
            _device.destroy(_handle);
            // Heap deallocation handled by `_memory` destructor.
            _memory.reset();
        }
        else {
            // Externally allocated.
            spec::trace("Image::delete_ext(", _handle, ")");
            // Also cleaned up externally.
            /* NOP */
        }
        _handle = nullptr;
    }
}

template<::vk::ImageType type>
ImageImpl<type>::ImageImpl(::vk::Device device, ::vk::Image handle, ::vk::Format format,
                           size_type size) :
    _handle(handle), _device(device), _memory(), _format(format), _size(size) {
    spec::trace("Image::new_ext(", _handle, ")");
}

template<::vk::ImageType type>
ImageImpl<type>::ImageImpl(Context& context, const char* name, ::vk::Format format,
                           size_type size, const ImageOptions& options) :
    _handle(nullptr), _device(context.get_active_device()), _memory(), _format(format),
    _size(size) {
    Allocator& allocator = context.get_allocator();

    ::vk::ImageCreateInfo cinfo(
         options._flags, type, format, vec_to_extent<::vk::Extent3D>(size), options._mip_levels,
         options._array_layers, options._sample_count, options._tiling, options._usage,
         ::vk::SharingMode::eExclusive, 0, nullptr, options._initial_layout);

    std::tie(_memory, _handle) =
         allocator.allocate(cinfo, options._memory_mapped_ram, options._memory_mapped_sqw,
                            options._memory_dedicated, options._memory_priority);
    spec::trace("Image::new(", _handle, ")");
    if (_memory) {
        _memory.set_name(name);
    }
    if (_handle) {
        context.associate_handle_with_name(_handle, name);
    }
}

template<::vk::ImageType type> bool ImageImpl<type>::is_external() const {
    return _handle && !_memory;
}

template<::vk::ImageType type>::vk::Device ImageImpl<type>::get_device() const {
    if (_handle) {
        return _device;
    }
    else {
        spec::trace("ImageImpl::get_device null handle");
        return nullptr;
    }
}
template<::vk::ImageType type> MemoryHandle ImageImpl<type>::get_memory() const {
    if (_handle) {
        return _memory;
    }
    else {
        spec::trace("ImageImpl::get_memory null handle");
        return nullptr;
    }
}

template<::vk::ImageType type>::vk::Format ImageImpl<type>::get_format() const {
    if (_handle) {
        return _format;
    }
    else {
        spec::trace("ImageImpl::get_format null handle");
        return ::vk::Format::eUndefined;
    }
}
template<::vk::ImageType type>
typename ImageImpl<type>::size_type ImageImpl<type>::get_size() const {
    if (_handle) {
        return _size;
    }
    else {
        spec::trace("ImageImpl::get_size null handle");
        return {};  // Return zero size.
    }
}
template<::vk::ImageType type>
template<typename vulkan_extent>
vulkan_extent ImageImpl<type>::get_extent() const {
    if (_handle) {
        return vec_to_extent<vulkan_extent>(_size);
    }
    else {
        spec::trace("ImageImpl::get_extent null handle");
        return {};  // Return zero size.
    }
}

template<::vk::ImageType type> const char* ImageImpl<type>::get_name() const {
    if (_handle && _memory) {
        return _memory.get_name();
    }
    else {
        if (_handle) {
            spec::trace("ImageImpl::get_name_view external allocation");
        }
        else {
            spec::trace("ImageImpl::get_name_view null handle");
        }
        return nullptr;
    }
}
template<::vk::ImageType type> std::string_view ImageImpl<type>::get_name_view() const {
    if (_handle && _memory) {
        return _memory.get_name_view();
    }
    else {
        if (_handle) {
            spec::trace("ImageImpl::get_name_view external allocation");
        }
        else {
            spec::trace("ImageImpl::get_name_view null handle");
        }
        return {};
    }
}

template<::vk::ImageType type>
std::optional<ImageView<type>> Image<type>::get_view(
     ::vk::ImageViewType vtype, ::vk::Format format, ::vk::ImageAspectFlags aspect,
     std::pair<uint32_t, uint32_t> mipmap_range, std::pair<uint32_t, uint32_t> layers_range,
     ::vk::ComponentSwizzle r, ::vk::ComponentSwizzle g, ::vk::ComponentSwizzle b,
     ::vk::ComponentSwizzle a) const {
    if (*this) {
        ::vk::ComponentMapping cinfo_cm(r, g, b, a);
        ::vk::ImageSubresourceRange cinfo_isr(aspect, mipmap_range.first, mipmap_range.second,
                                              layers_range.first, layers_range.second);
        ::vk::ImageViewCreateInfo cinfo({}, (*this)->_handle, vtype, format, cinfo_cm,
                                        cinfo_isr);
        auto [result, handle] = (*this)->_device.createImageView(cinfo);
        if (result != ::vk::Result::eSuccess) {
            spec::trace("Image<", to_string(type), ">::get_ImageView::createImageView -> ",
                        to_string(result), ".");
            return {};
        }
        return ImageView<type>::make_shared(*this, handle);
    }
    else {
        spec::trace("Image::get_view null handle");
        return {};
    }
}

template<::vk::ImageType type>
ImageViewImpl<type>::ImageViewImpl(ImageViewImpl&& o) noexcept :
    _parent(std::exchange(o._parent, nullptr)), _handle(std::exchange(o._handle, nullptr)) {}

template<::vk::ImageType type>
ImageViewImpl<type>& ImageViewImpl<type>::operator=(ImageViewImpl&& o) noexcept {
    _parent = std::exchange(o._parent, nullptr);
    _handle = std::exchange(o._handle, nullptr);
    return *this;
}
template<::vk::ImageType type> ImageViewImpl<type>::~ImageViewImpl() {
    if (_handle) {
        spec::trace("ImageView::delete(", _handle, ")");
        _parent->_device.destroyImageView(_handle);
        _handle = nullptr;
        _parent.reset();
    }
}
template<::vk::ImageType type>
ImageViewImpl<type>::ImageViewImpl(Image<type> parent, ::vk::ImageView handle) :
    _parent(parent), _handle(handle) {
    spec::trace("ImageView::new(", _handle, ")");
}

template<::vk::ImageType type>
void ImageViewImpl<type>::update_descriptor(::vk::DescriptorSet set, uint32_t binding,
                                            ::vk::ImageLayout img_layout) const {
    ::vk::DescriptorImageInfo img_info({}, get_handle(), img_layout);
    ::vk::WriteDescriptorSet write_info(set, binding, 0, 1,
                                        ::vk::DescriptorType::eSampledImage);
    write_info.pImageInfo = &img_info;
    get_device().updateDescriptorSets(1, &write_info, 0, nullptr);
}

}  // namespace satr::vk

template class satr::vk::ImageImpl<::vk::ImageType::e1D>;
template class satr::vk::ImageImpl<::vk::ImageType::e2D>;
template class satr::vk::ImageImpl<::vk::ImageType::e3D>;

template class satr::vk::Image<::vk::ImageType::e1D>;
template class satr::vk::Image<::vk::ImageType::e2D>;
template class satr::vk::Image<::vk::ImageType::e3D>;

template class satr::vk::ImageViewImpl<::vk::ImageType::e1D>;
template class satr::vk::ImageViewImpl<::vk::ImageType::e2D>;
template class satr::vk::ImageViewImpl<::vk::ImageType::e3D>;
