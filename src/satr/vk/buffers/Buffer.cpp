#include "Buffer.hpp"

#include <Context.hpp>
#include <trace/Trace.hpp>
#include <vulkan/vulkan.hpp>

namespace satr::vk {

BufferImpl::BufferImpl(BufferImpl&& o) noexcept :
    _handle(std::exchange(o._handle, nullptr)), _memory(std::move(o._memory)) {
    spec::trace("Buffer::construct_move(", _handle, ")");
}
BufferImpl& BufferImpl::operator=(BufferImpl&& o) noexcept {
    _handle = std::exchange(o._handle, nullptr);
    _memory = std::move(o._memory);
    spec::trace("Buffer::assign_move(", _handle, ")");
    return *this;
}

BufferImpl::BufferImpl(Context& context, const char* name, ::vk::BufferUsageFlags usage,
                       ::vk::DeviceSize size_bytes, ::vk::BufferCreateFlags flags,
                       bool memory_mapped_ram, bool memory_mapped_sqw, bool memory_dedicated,
                       float memory_priority) :
    _handle(nullptr),
    _memory() {
    Allocator& allocator = context.get_allocator();
    ::vk::BufferCreateInfo cinfo(flags, size_bytes, usage);
    std::tie(_memory, _handle) = allocator.allocate(cinfo, memory_mapped_ram, memory_mapped_sqw,
                                                    memory_dedicated, memory_priority);
    spec::trace("Buffer::new(", _handle, ")");
    if (_memory) {
        _memory.set_name(name);
    }
    if (_handle) {
        context.associate_handle_with_name(_handle, name);
    }
}

BufferImpl::~BufferImpl() {
    if (_handle) {
        if (!_memory) {
            spec::fatal("Buffer::delete: "
                        "detected non-empty buffer with empty memory allocation!");
        }
        spec::trace("Buffer::delete(", _handle, ")");
        get_device().destroyBuffer(_handle);
        _handle = nullptr;
        _memory.reset();
    }
}

void BufferImpl::bind_vertex(::vk::CommandBuffer cmd_buf, uint32_t binding,
                             ::vk::DeviceSize offset) const {
    cmd_buf.bindVertexBuffers(binding, 1, &_handle, &offset);
}
void BufferImpl::bind_index(::vk::CommandBuffer cmd_buf, ::vk::IndexType index_type,
                            ::vk::DeviceSize offset) const {
    cmd_buf.bindIndexBuffer(_handle, offset, index_type);
}

void BufferImpl::update_uniform(::vk::DescriptorSet set, uint32_t binding,
                                ::vk::DeviceSize offset, ::vk::DeviceSize size) const {
    ::vk::DescriptorBufferInfo buffer_info(_handle, offset, size);
    ::vk::WriteDescriptorSet write_info(set, binding, 0, 1,
                                        ::vk::DescriptorType::eUniformBuffer);
    write_info.pBufferInfo = &buffer_info;
    get_device().updateDescriptorSets(1, &write_info, 0, nullptr);
}

void BufferImpl::copy(::vk::CommandBuffer cmd_buf, const BufferImpl& src, const BufferImpl& dst,
                      ::vk::DeviceSize n, ::vk::DeviceSize src_offset,
                      ::vk::DeviceSize dst_offset) {
    ::vk::BufferCopy cpy_info(src_offset, dst_offset, n);
    cmd_buf.copyBuffer(src._handle, dst._handle, 1, &cpy_info);
}

}  // namespace satr::vk
