#ifndef SATR_VK_IMAGE_HPP
#define SATR_VK_IMAGE_HPP
/**
 * @file
 * @brief Wrapper for vulkan Image and their views
 * and helpers for data uploading.
 */

#include <Attr.hpp>
#include <Memory.hpp>
#include <TypeConversions.hpp>
#include <Utils.hpp>
#include <glm/gtc/vec1.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <vulkan/vulkan.hpp>

#include <cstddef>
#include <string>
#include <string_view>
#include <utility>

namespace satr::vk {

/* Fwd decls */
class Context;

inline constexpr ::vk::ImageViewType image_to_view_type(::vk::ImageType type) {
    switch (type) {
        case ::vk::ImageType::e1D: return ::vk::ImageViewType::e1D;
        case ::vk::ImageType::e2D: return ::vk::ImageViewType::e2D;
        case ::vk::ImageType::e3D: return ::vk::ImageViewType::e3D;
    }
}

/** Extended/Optional arguments for allocating and constructing new Image(s). */
class ImageOptions {
   protected:
    ::vk::ImageUsageFlags _usage = {};
    int32_t _mip_levels = 1;
    uint32_t _array_layers = 1;
    ::vk::ImageLayout _initial_layout = ::vk::ImageLayout::eUndefined;
    ::vk::ImageCreateFlags _flags = {};
    ::vk::SampleCountFlagBits _sample_count = ::vk::SampleCountFlagBits::e1;
    ::vk::ImageTiling _tiling = ::vk::ImageTiling::eOptimal;
    bool _memory_mapped_ram = false;
    bool _memory_mapped_sqw = false;
    bool _memory_dedicated = false;
    float _memory_priority = 0.5;

    template<::vk::ImageType type> friend class ImageImpl;

   public:
    ImageOptions() = default;  // All set to default.
    explicit ImageOptions(::vk::ImageUsageFlags usage) : _usage(usage) {}
    explicit ImageOptions(::vk::ImageLayout initial_layout) : _initial_layout(initial_layout) {}
    explicit ImageOptions(::vk::ImageCreateFlags flags) : _flags(flags) {}
    explicit ImageOptions(::vk::SampleCountFlagBits sample_count) :
        _sample_count(sample_count) {}
    explicit ImageOptions(::vk::ImageTiling tiling) : _tiling(tiling) {}

    DEFINE_AUTO_PROPERTY(usage);
    DEFINE_AUTO_PROPERTY(mip_levels);
    DEFINE_AUTO_PROPERTY(array_layers);
    DEFINE_AUTO_PROPERTY(initial_layout);
    DEFINE_AUTO_PROPERTY(flags);
    DEFINE_AUTO_PROPERTY(sample_count);
    DEFINE_AUTO_PROPERTY(tiling);
    DEFINE_AUTO_PROPERTY(memory_mapped_ram);
    DEFINE_AUTO_PROPERTY(memory_mapped_sqw);
    DEFINE_AUTO_PROPERTY(memory_dedicated);
    DEFINE_AUTO_PROPERTY(memory_priority);
};

// Forward declarations for life-cycle management objects.
template<::vk::ImageType type> class ImageImpl;
template<::vk::ImageType type> class Image;
template<::vk::ImageType type> using ImageWeak = std::weak_ptr<ImageImpl<type>>;

template<::vk::ImageType type> class ImageViewImpl;
template<::vk::ImageType type> class ImageView;
template<::vk::ImageType type> using ImageViewWeak = std::weak_ptr<ImageViewImpl<type>>;

template<::vk::ImageType type> class ImageImpl : public spec::INonCopyable {
   public:
    typedef typename std::conditional<
         type == ::vk::ImageType::e1D, glm::uvec1,
         typename std::conditional<type == ::vk::ImageType::e2D, glm::uvec2,
                                   glm::uvec3>::type>::type size_type;

   protected:
    ::vk::Image _handle;
    ::vk::Device _device;
    MemoryHandle _memory;

    ::vk::Format _format;
    size_type _size;

    friend class Image<type>;
    friend class ImageViewImpl<type>;

   public:
    /** Constructors */
    constexpr ImageImpl() noexcept : _handle(nullptr), _device(nullptr), _memory() {}
    ImageImpl(ImageImpl&& o) noexcept;
    ImageImpl& operator=(ImageImpl&& o) noexcept;
    ~ImageImpl();

    /**
     * For externally allocated images(ex: swapchain)
     */
    ImageImpl(::vk::Device device, ::vk::Image handle, ::vk::Format format, size_type size);
    template<typename vulkan_extent>
    ImageImpl(::vk::Device device, ::vk::Image handle, ::vk::Format format,
              vulkan_extent size) :
        ImageImpl(device, handle, format, extent_to_vec<size_type>(size)) {}
    /**
     * For images to be allocated in the private heap.
     */
    ImageImpl(Context& allocator, const char* name, ::vk::Format format, size_type size,
              const ImageOptions& options = ImageOptions());
    ImageImpl(Context& allocator, const std::string& name, ::vk::Format format, size_type size,
              const ImageOptions& options = ImageOptions()) :
        ImageImpl(allocator, name.c_str(), format, size, options) {}

   public:
    operator bool() const {
        return static_cast<bool>(_handle);
    }

    /** Getters */
    ::vk::Image get_handle() const {
        return _handle;
    }
    ::vk::Device get_device() const;
    MemoryHandle get_memory() const;
    ::vk::Format get_format() const;
    size_type get_size() const;
    template<typename vulkan_extent> vulkan_extent get_extent() const;
    const char* get_name() const;
    std::string_view get_name_view() const;
    /**
     * Returns true if this Image object is backed by externally allocated memory.
     */
    bool is_external() const;

    /** Operations */
    // TODO: state/format transitions.
    // TODO: upload/transfer data.
    // TODO:
    // void clear(::vk::CommandBuffer& cmds);
};

/** Wrapper class for Impl */
template<::vk::ImageType type> class Image : public std::shared_ptr<ImageImpl<type>> {
   public:
    // For convenience:
    using Weak = ImageWeak<type>;
    using View = ImageView<type>;
    using size_type = typename ImageImpl<type>::size_type;

   protected:
    // Factory methods.
    Image(std::shared_ptr<ImageImpl<type>>&& o) noexcept :
        std::shared_ptr<ImageImpl<type>>(o) {}

    template<typename... Args> static Image make_shared(Args&&... args) {
        auto obj = std::make_shared<ImageImpl<type>>(std::forward<Args>(args)...);
        return obj;
    }

    friend class Context;
    friend class Presenter;

   public:
    // inherit shared_ptr's constructors
    constexpr Image() noexcept : std::shared_ptr<ImageImpl<type>>() {}
    constexpr Image(std::nullptr_t _) noexcept : std::shared_ptr<ImageImpl<type>>(_) {}
    Image(const Image& r) noexcept = default;
    Image(Image&& r) noexcept = default;

    Image& operator=(const Image& r) noexcept = default;
    Image& operator=(Image&& r) noexcept = default;

    // Forward Impl's methods.
    operator bool() const {
        return static_cast<bool>((std::shared_ptr<ImageImpl<type>>) *this) &&
               static_cast<bool>((ImageImpl<type>&) **this);
    }
    /** Getters */
    ::vk::Image get_handle() const {
        return (*this)->get_handle();
    }
    ::vk::Device get_device() const {
        return (*this)->get_device();
    }
    MemoryHandle get_memory() const {
        return (*this)->get_memory();
    }
    ::vk::Format get_format() const {
        return (*this)->get_format();
    }
    size_type get_size() const {
        return (*this)->get_size();
    }
    template<typename vulkan_extent> vulkan_extent get_extent() const {
        return (*this)->get_extent();
    }
    const char* get_name() const {
        return (*this)->get_name();
    }
    std::string_view get_name_view() const {
        return (*this)->get_name_view();
    }
    bool is_external() const {
        return (*this)->is_external();
    }

    // Views are only available for shared image objects.
    /**
     * Full view constructor.
     */
    std::optional<ImageView<type>> get_view(
         ::vk::ImageViewType vtype, ::vk::Format format,
         ::vk::ImageAspectFlags aspect = ::vk::ImageAspectFlagBits::eColor,
         std::pair<uint32_t, uint32_t> mipmap_range = {0, VK_REMAINING_MIP_LEVELS},
         std::pair<uint32_t, uint32_t> layers_range = {0, VK_REMAINING_ARRAY_LAYERS},
         ::vk::ComponentSwizzle r = ::vk::ComponentSwizzle::eIdentity,
         ::vk::ComponentSwizzle g = ::vk::ComponentSwizzle::eIdentity,
         ::vk::ComponentSwizzle b = ::vk::ComponentSwizzle::eIdentity,
         ::vk::ComponentSwizzle a = ::vk::ComponentSwizzle::eIdentity) const;
    /**
     * View constructor with inherited format.
     */
    std::optional<ImageView<type>> get_view(
         ::vk::ImageViewType vtype = image_to_view_type(type),
         ::vk::ImageAspectFlags aspect = ::vk::ImageAspectFlagBits::eColor,
         std::pair<uint32_t, uint32_t> mipmap_range = {0, VK_REMAINING_MIP_LEVELS},
         std::pair<uint32_t, uint32_t> layers_range = {0, VK_REMAINING_ARRAY_LAYERS},
         ::vk::ComponentSwizzle r = ::vk::ComponentSwizzle::eIdentity,
         ::vk::ComponentSwizzle g = ::vk::ComponentSwizzle::eIdentity,
         ::vk::ComponentSwizzle b = ::vk::ComponentSwizzle::eIdentity,
         ::vk::ComponentSwizzle a = ::vk::ComponentSwizzle::eIdentity) const {
        return get_view(vtype, (*this)->get_format(), aspect, mipmap_range, layers_range, r, g,
                        b, a);
    }
};

template<::vk::ImageType type> class ImageViewImpl : public spec::INonCopyable {
   protected:
    Image<type> _parent;
    ::vk::ImageView _handle;

   public:
    constexpr ImageViewImpl() noexcept : _parent(nullptr), _handle(nullptr) {}
    ImageViewImpl(ImageViewImpl&& o) noexcept;
    ImageViewImpl& operator=(ImageViewImpl&& o) noexcept;
    ~ImageViewImpl();
    /* Default constructor */
    ImageViewImpl(Image<type>, ::vk::ImageView);

   public:
    operator bool() const {
        return static_cast<bool>(_handle);
    }

    /** Getters */
    const ::vk::ImageView get_handle() const {
        return _handle;
    }
    Image<type> get_image() const {
        return _parent;
    }
    ::vk::Device get_device() const {
        return _parent.get_device();
    }

    ::vk::Format get_image_format() const {
        return _parent->get_format();
    }
    typename Image<type>::size_type get_image_size() const {
        return _parent->get_size();
    }

    /** Operations */
    void update_descriptor(::vk::DescriptorSet set, uint32_t binding,
                           ::vk::ImageLayout img_layout) const;
};

template<::vk::ImageType type> class ImageView : public std::shared_ptr<ImageViewImpl<type>> {
   public:
    // For convenience:
    using Weak = ImageViewWeak<type>;

   protected:
    // Factory methods.
    ImageView(std::shared_ptr<ImageViewImpl<type>>&& o) noexcept :
        std::shared_ptr<ImageViewImpl<type>>(o) {}

    template<typename... Args> static ImageView make_shared(Args&&... args) {
        auto obj = std::make_shared<ImageViewImpl<type>>(std::forward<Args>(args)...);
        return obj;
    }

    friend class Image<type>;

   public:
    // inherit shared_ptr's constructors
    constexpr ImageView() noexcept : std::shared_ptr<ImageViewImpl<type>>() {}
    constexpr ImageView(std::nullptr_t _) noexcept : std::shared_ptr<ImageViewImpl<type>>(_) {}
    ImageView(const ImageView& r) noexcept = default;
    ImageView(ImageView&& r) noexcept = default;

    ImageView& operator=(const ImageView& r) noexcept = default;
    ImageView& operator=(ImageView&& r) noexcept = default;

    // Forward Impl's methods.
    operator bool() const {
        return static_cast<bool>((std::shared_ptr<ImageViewImpl<type>>) *this) &&
               static_cast<bool>((ImageViewImpl<type>&) **this);
    }
    /** Getters */
    const ::vk::ImageView get_handle() const {
        return (*this)->get_handle();
    }
    Image<type> get_image() const {
        return (*this)->get_image();
    }

    ::vk::Format get_image_format() const {
        return (*this)->get_image_format();
    }
    typename Image<type>::size_type get_image_size() const {
        return (*this)->get_image_size();
    }

    void update_descriptor(
         ::vk::DescriptorSet set, uint32_t binding,
         ::vk::ImageLayout img_layout = ::vk::ImageLayout::eShaderReadOnlyOptimal) const {
        return (*this)->update_descriptor(set, binding, img_layout);
    }
};

using Image1D = Image<::vk::ImageType::e1D>;
using Image2D = Image<::vk::ImageType::e2D>;
using Image3D = Image<::vk::ImageType::e3D>;

}  // namespace satr::vk

#endif /*SATR_VK_IMAGE_HPP*/