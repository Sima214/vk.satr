#ifndef SATR_VK_BUFFER_HPP
#define SATR_VK_BUFFER_HPP
/**
 * @file
 * @brief Wrapper and helper for vk::Buffer
 */

#include <Memory.hpp>
#include <Utils.hpp>
#include <vulkan/vulkan.hpp>

#include <utility>

namespace satr::vk {

/* Fwd decls */
class Context;

class BufferImpl;
class Buffer;
using BufferWeak = std::weak_ptr<BufferImpl>;

// TODO: class BufferMapping;
// TODO: class BufferView;

class BufferImpl : public spec::INonCopyable {
   protected:
    ::vk::Buffer _handle;
    MemoryHandle _memory;

   public:
    /** Constructors */
    constexpr BufferImpl() noexcept : _handle(nullptr) {}
    BufferImpl(BufferImpl&& o) noexcept;
    BufferImpl& operator=(BufferImpl&& o) noexcept;
    ~BufferImpl();

   public:
    BufferImpl(Context& allocator, const char* name, ::vk::BufferUsageFlags usage,
               ::vk::DeviceSize size_bytes, ::vk::BufferCreateFlags flags = {},
               bool memory_mapped_ram = false, bool memory_mapped_sqw = false,
               bool memory_dedicated = false, float memory_priority = 0.5);
    BufferImpl(Context& allocator, const std::string& name, ::vk::BufferUsageFlags usage,
               ::vk::DeviceSize size_bytes, ::vk::BufferCreateFlags flags = {},
               bool memory_mapped_ram = false, bool memory_mapped_sqw = false,
               bool memory_dedicated = false, float memory_priority = 0.5) :
        BufferImpl(allocator, name.c_str(), usage, size_bytes, flags, memory_mapped_ram,
                   memory_mapped_sqw, memory_dedicated, memory_priority) {}

   public:
    operator bool() const {
        return static_cast<bool>(_handle);
    }

    /** Getters/Setters */
    ::vk::Buffer get_handle() const {
        return _handle;
    }
    ::vk::Device get_device() const {
        return _memory.get_device();
    }
    const MemoryHandle& get_memory() const {
        return _memory;
    }

    /** Operations */
    /**
     * Bind a single buffer to the provided command buffer's vertex bind point.
     */
    void bind_vertex(::vk::CommandBuffer cmd_buf, uint32_t binding,
                     ::vk::DeviceSize offset = 0) const;
    void bind_index(::vk::CommandBuffer cmd_buf, ::vk::IndexType index_type,
                    ::vk::DeviceSize offset = 0) const;

    /**
     * Updates descriptor sets.
     */
    void update_uniform(::vk::DescriptorSet set, uint32_t binding, ::vk::DeviceSize offset = 0,
                        ::vk::DeviceSize size = ::vk::WholeSize) const;

    static void copy(::vk::CommandBuffer cmd_buf, const BufferImpl& src, const BufferImpl& dst,
                     ::vk::DeviceSize n, ::vk::DeviceSize src_offset = 0,
                     ::vk::DeviceSize dst_offset = 0);
};

class Buffer : public std::shared_ptr<BufferImpl> {
   public:
    using Weak = BufferWeak;  // For convenience

   public:
    // Factory methods.
    Buffer(std::shared_ptr<BufferImpl>&& o) noexcept : std::shared_ptr<BufferImpl>(o) {}

    template<typename... Args> static Buffer make_shared(Args&&... args) {
        auto obj = std::make_shared<BufferImpl>(std::forward<Args>(args)...);
        return obj;
    }

    friend class Context;

   public:
    // inherit shared_ptr's constructors
    constexpr Buffer() noexcept : std::shared_ptr<BufferImpl>() {}
    constexpr Buffer(std::nullptr_t _) noexcept : std::shared_ptr<BufferImpl>(_) {}
    Buffer(const Buffer& r) noexcept = default;
    Buffer(Buffer&& r) noexcept = default;

    Buffer& operator=(const Buffer& r) noexcept = default;
    Buffer& operator=(Buffer&& r) noexcept = default;

    operator bool() const {
        return static_cast<bool>((std::shared_ptr<BufferImpl>) *this) &&
               static_cast<bool>((BufferImpl&) **this);
    }

    /** Getters/Setters */
    ::vk::Buffer get_handle() const {
        return (*this)->get_handle();
    }
    const MemoryHandle& get_memory() const {
        return (*this)->get_memory();
    }

    /** Operations */
    void bind_vertex(::vk::CommandBuffer cmd_buf, uint32_t binding, size_t offset = 0) const {
        return (*this)->bind_vertex(cmd_buf, binding, offset);
    }
    void bind_index(::vk::CommandBuffer cmd_buf, ::vk::IndexType index_type,
                    ::vk::DeviceSize offset = 0) const {
        return (*this)->bind_index(cmd_buf, index_type, offset);
    }
    void update_uniform(::vk::DescriptorSet set, uint32_t binding, ::vk::DeviceSize offset = 0,
                        ::vk::DeviceSize size = ::vk::WholeSize) const {
        return (*this)->update_uniform(set, binding, offset, size);
    }

    static void copy(::vk::CommandBuffer cmd_buf, const Buffer& src, const Buffer& dst,
                     size_t n, size_t src_offset = 0, size_t dst_offset = 0) {
        BufferImpl::copy(cmd_buf, (BufferImpl&) *src, (BufferImpl&) *dst, n, src_offset,
                         dst_offset);
    }
};

}  // namespace satr::vk

#endif /*SATR_VK_BUFFER_HPP*/
