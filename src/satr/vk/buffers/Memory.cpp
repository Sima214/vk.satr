#include "Memory.hpp"

#include <logger/Logger.hpp>
#include <trace/Trace.hpp>
#include <vulkan/vulkan_to_string.hpp>

#include <chrono>
#include <cstdint>
#include <cstring>
#include <filesystem>
#include <format>
#include <fstream>
#include <limits>
#include <memory>
#include <utility>

#include <vk_mem_alloc.h>

namespace satr::vk {

Allocator::Allocator(Allocator&& o) noexcept :
    _allocator(std::exchange(o._allocator, nullptr)) {
    if (_allocator != nullptr) {
        spec::trace("Allocator::construct_move");
    }
}
Allocator& Allocator::operator=(Allocator&& o) noexcept {
    _allocator = std::exchange(o._allocator, nullptr);
    if (_allocator != nullptr) {
        spec::trace("Allocator::assign_move");
    }
    return *this;
}
Allocator::~Allocator() {
    if (_allocator != nullptr) {
        spec::trace("Allocator: destructor called before deinit!");
    }
}

Extensions Allocator::get_optional_instance_extensions(uint32_t instance_api_version) {
    Extensions ext;
    if (instance_api_version < VK_API_VERSION_1_1) {
        ext += "VK_KHR_get_physical_device_properties2";
    }
    return ext;
}
Extensions Allocator::get_optional_device_extensions(uint32_t instance_api_version) {
    Extensions ext;
    if (instance_api_version == VK_API_VERSION_1_0) {
        ext += "VK_EXT_memory_budget";
        ext += "VK_KHR_bind_memory2";
        ext += "VK_KHR_dedicated_allocation";
        ext += "VK_KHR_get_memory_requirements2";
    }
    return ext;
}

bool Allocator::allocator_init(uint32_t instance_api_version, ::vk::Instance& instance,
                               const ::vk::PhysicalDevice& physical_device,
                               const ::vk::Device& logical_device,
                               const Extensions& logical_device_extensions) {
    VmaVulkanFunctions vma_vulkan_functions;
    std::memset(&vma_vulkan_functions, 0, sizeof(vma_vulkan_functions));
    vma_vulkan_functions.vkGetInstanceProcAddr = &vkGetInstanceProcAddr;
    vma_vulkan_functions.vkGetDeviceProcAddr = &vkGetDeviceProcAddr;
    VmaAllocatorCreateInfo create_info = {};
    std::memset(&create_info, 0, sizeof(create_info));
    create_info.vulkanApiVersion = instance_api_version;
    create_info.instance = instance;
    create_info.pVulkanFunctions = &vma_vulkan_functions;
    create_info.physicalDevice = physical_device;
    create_info.device = logical_device;
    // create_info.flags = 0;
    // Test for optional features.
    if (instance_api_version == VK_API_VERSION_1_0 &&
        logical_device_extensions.contains("VK_KHR_get_memory_requirements2") &&
        logical_device_extensions.contains("VK_KHR_dedicated_allocation")) {
        create_info.flags |= VMA_ALLOCATOR_CREATE_KHR_DEDICATED_ALLOCATION_BIT;
        spec::trace("Allocator::allocator_init: dedicated allocations");
    }
    if (instance_api_version == VK_API_VERSION_1_0 &&
        logical_device_extensions.contains("VK_KHR_bind_memory2")) {
        create_info.flags |= VMA_ALLOCATOR_CREATE_KHR_BIND_MEMORY2_BIT;
        spec::trace("Allocator::allocator_init: bind memory2");
    }
    if (logical_device_extensions.contains("VK_EXT_memory_budget")) {
        /**
         * Also requires instance extension VK_KHR_get_physical_device_properties2
         * when instance_api_version < 1.1.
         */
        create_info.flags |= VMA_ALLOCATOR_CREATE_EXT_MEMORY_BUDGET_BIT;
        spec::trace("Allocator::allocator_init: memory budget");
    }
    // TODO: VMA_ALLOCATOR_CREATE_AMD_DEVICE_COHERENT_MEMORY_BIT
    // TODO: VMA_ALLOCATOR_CREATE_BUFFER_DEVICE_ADDRESS_BIT
    // TODO: VMA_ALLOCATOR_CREATE_EXT_MEMORY_PRIORITY_BIT
    ::vk::Result r = static_cast<::vk::Result>(vmaCreateAllocator(&create_info, &_allocator));
    if (r != ::vk::Result::eSuccess) {
        logger.loge("Could not initialize vulkan allocator: ", to_string(r), "!");
        return false;
    }
    spec::trace("Allocator::init");
    return true;
}

void Allocator::allocator_deinit() {
    if (_allocator != nullptr) {
        vmaDestroyAllocator(_allocator);
        _allocator = nullptr;
        spec::trace("Allocator::deinit");
    }
    else {
        spec::fatal("Allocator::deinit called twice!");
    }
}

void Allocator::on_allocator_new_frame() {
    vmaSetCurrentFrameIndex(_allocator, _frame_counter++);
}

bool Allocator::allocator_dump_state(const std::string& base_name) {
    auto current_time =
         std::chrono::time_point_cast<std::chrono::seconds>(std::chrono::system_clock::now());
    std::filesystem::path fp(
         std::format("{0:}_{1:%Y%m%d_%H%M%S}.vma.json", base_name, current_time));

    if (std::filesystem::exists(fp)) {
        logger.logd("Failure to dump allocator state: ", fp, " already exists!");
        return false;
    }

    std::ofstream fs(fp);
    if (!fs.good()) {
        logger.logd("Failure to dump allocator state: ", fp, " couldn't be opened!");
        return false;
    }
    char* vma_state_str = nullptr;
    vmaBuildStatsString(_allocator, &vma_state_str, true);
    fs << vma_state_str;
    vmaFreeStatsString(_allocator, vma_state_str);
    // TODO: Verify that data was actually fully written.
    fs.close();

    logger.logi("Allocator state dumped at ", fp, ".");
    return true;
}

VmaAllocationCreateInfo Allocator::_allocate_get_create_info(bool mapped_ram, bool mapped_sqw,
                                                             bool dedicated, float priority) {
    VmaAllocationCreateInfo alloc_create_info = {};
    std::memset(&alloc_create_info, 0, sizeof(alloc_create_info));
    alloc_create_info.usage = VMA_MEMORY_USAGE_AUTO;
    alloc_create_info.flags = VMA_ALLOCATION_CREATE_WITHIN_BUDGET_BIT |
                              VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT;
    if (mapped_ram || mapped_sqw) {
        alloc_create_info.flags |= VMA_ALLOCATION_CREATE_MAPPED_BIT;
    }
    if (mapped_ram) {
        alloc_create_info.flags |= VMA_ALLOCATION_CREATE_HOST_ACCESS_RANDOM_BIT;
    }
    if (mapped_sqw) {
        alloc_create_info.flags |= VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT;
    }
    if (dedicated) {
        alloc_create_info.flags |= VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT;
    }
    alloc_create_info.priority = priority;
    return alloc_create_info;
}

std::pair<MemoryHandle, ::vk::Buffer> Allocator::allocate(const ::vk::BufferCreateInfo& info,
                                                          bool mapped_ram, bool mapped_sqw,
                                                          bool dedicated, float priority) {
    auto alloc_create_info =
         _allocate_get_create_info(mapped_ram, mapped_sqw, dedicated, priority);
    const VkBufferCreateInfo& cinfo = static_cast<const VkBufferCreateInfo&>(info);
    VkBuffer cbuffer;
    VmaAllocation alloc;
    VmaAllocationInfo alloc_info;
    ::vk::Result r = static_cast<::vk::Result>(vmaCreateBuffer(
         _allocator, &cinfo, &alloc_create_info, &cbuffer, &alloc, &alloc_info));
    if (r != ::vk::Result::eSuccess) {
        logger.loge("Allocator::allocate(buffer) -> ", to_string(r), '!');
        MemoryHandle invalid_mem_handle;
        ::vk::Buffer invalid_buffer;
        return {std::move(invalid_mem_handle), std::move(invalid_buffer)};
    }
    MemoryHandle mem_handle = MemoryHandle::make_shared(*this, alloc, alloc_info);
    ::vk::Buffer buffer = cbuffer;
    return {std::move(mem_handle), std::move(buffer)};
}

uint32_t Allocator::get_allocation_memory_type_index(const ::vk::BufferCreateInfo& info,
                                                     bool mapped_ram, bool mapped_sqw,
                                                     bool dedicated, float priority) {
    auto alloc_create_info =
         _allocate_get_create_info(mapped_ram, mapped_sqw, dedicated, priority);
    const VkBufferCreateInfo& cinfo = static_cast<const VkBufferCreateInfo&>(info);
    uint32_t mem_index;
    ::vk::Result r = static_cast<::vk::Result>(vmaFindMemoryTypeIndexForBufferInfo(
         _allocator, &cinfo, &alloc_create_info, &mem_index));
    if (r != ::vk::Result::eSuccess) {
        logger.loge("Allocator::get_allocation_memory_type_index(buffer) -> ", to_string(r),
                    '!');
        return std::numeric_limits<uint32_t>::max();
    }
    return mem_index;
}

std::pair<MemoryHandle, ::vk::Image> Allocator::allocate(const ::vk::ImageCreateInfo& info,
                                                         bool mapped_ram, bool mapped_sqw,
                                                         bool dedicated, float priority) {
    auto alloc_create_info =
         _allocate_get_create_info(mapped_ram, mapped_sqw, dedicated, priority);
    const VkImageCreateInfo& cinfo = static_cast<const VkImageCreateInfo&>(info);
    VkImage cimage;
    VmaAllocation alloc;
    VmaAllocationInfo alloc_info;
    ::vk::Result r = static_cast<::vk::Result>(
         vmaCreateImage(_allocator, &cinfo, &alloc_create_info, &cimage, &alloc, &alloc_info));
    if (r != ::vk::Result::eSuccess) {
        logger.loge("Allocator::allocate(image) -> ", to_string(r), '!');
        MemoryHandle invalid_mem_handle;
        ::vk::Image invalid_image;
        return {std::move(invalid_mem_handle), std::move(invalid_image)};
    }
    MemoryHandle mem_handle = MemoryHandle::make_shared(*this, alloc, alloc_info);
    ::vk::Image image = cimage;
    return {std::move(mem_handle), std::move(image)};
}

MemoryHandleImpl::MemoryHandleImpl(Allocator& allocator, VmaAllocation alloc,
                                   VmaAllocationInfo alloc_info) :
    _allocator(&allocator),
    _allocation(alloc), _allocation_info(alloc_info) {
    if (_allocation != nullptr) {
        spec::trace("MemoryHandle::new(", _allocation, ")");
    }
    else {
        spec::fatal("MemoryHandle::new null allocation");
    }
}

MemoryHandleImpl::MemoryHandleImpl(MemoryHandleImpl&& o) noexcept :
    _allocator(std::exchange(o._allocator, nullptr)),
    _allocation(std::exchange(o._allocation, nullptr)), _allocation_info(o._allocation_info) {
    if (_allocation != nullptr) {
        spec::trace("MemoryHandle::construct_move(", _allocation, ")");
    }
}
MemoryHandleImpl& MemoryHandleImpl::operator=(MemoryHandleImpl&& o) noexcept {
    _allocator = std::exchange(o._allocator, nullptr);
    _allocation = std::exchange(o._allocation, nullptr);
    _allocation_info = o._allocation_info;
    if (_allocation != nullptr) {
        spec::trace("MemoryHandle::assign_move(", _allocation, ")");
    }
    return *this;
}
MemoryHandleImpl::~MemoryHandleImpl() {
    if (*this) {
        vmaFreeMemory(_allocator->_allocator, _allocation);
        spec::trace("MemoryHandle::delete(", _allocation, ")");
        _allocation = nullptr;
        _allocator = nullptr;
    }
}

::vk::Instance MemoryHandleImpl::get_instance() const {
    if (_allocator == nullptr) {
        spec::fatal("MemoryHandle's Allocator is null!");
    }
    else {
        return _allocator->get_instance();
    }
}
::vk::PhysicalDevice MemoryHandleImpl::get_physical_device() const {
    if (_allocator == nullptr) {
        spec::fatal("MemoryHandle's Allocator is null!");
    }
    else {
        return _allocator->get_physical_device();
    }
}
::vk::Device MemoryHandleImpl::get_device() const {
    if (_allocator == nullptr) {
        spec::fatal("MemoryHandle's Allocator is null!");
    }
    else {
        return _allocator->get_device();
    }
}

void MemoryHandleImpl::set_name(const char* name) {
    if (*this) {
        vmaSetAllocationName(_allocator->_allocator, _allocation, name);
        _update_allocation_info();
    }
    else {
        spec::fatal("MemoryHandle::set_name null allocation");
    }
}
const char* MemoryHandleImpl::get_name() const {
    if (*this) {
        return _allocation_info.pName;
    }
    else {
        return nullptr;
    }
}
std::string_view MemoryHandleImpl::get_name_view() const {
    const char* name = get_name();
    if (name != nullptr) {
        return std::string_view(name);
    }
    else {
        return std::string_view();
    }
}

::vk::DeviceSize MemoryHandleImpl::get_size() const {
    if (*this) {
        return _allocation_info.size;
    }
    else {
        return 0;
    }
}
uint32_t MemoryHandleImpl::get_memory_type_index() const {
    if (*this) {
        return _allocation_info.memoryType;
    }
    else {
        return std::numeric_limits<uint32_t>::max();
    }
}

bool MemoryHandleImpl::is_mapped() const {
    return get_mapping() != nullptr;
}
void* MemoryHandleImpl::get_mapping() const {
    if (*this) {
        return _allocation_info.pMappedData;
    }
    else {
        return nullptr;
    }
}

bool MemoryHandleImpl::flush(::vk::DeviceSize offset, ::vk::DeviceSize size) const {
    if (*this) {
        ::vk::Result r = static_cast<::vk::Result>(
             vmaFlushAllocation(_allocator->_allocator, _allocation, offset, size));
        if (r != ::vk::Result::eSuccess) {
            logger.loge("MemoryHandleImpl::flush -> ", to_string(r), '!');
            return false;
        }
        else {
            return true;
        }
    }
    else {
        spec::trace("MemoryHandle::flush null allocation");
        return false;
    }
}
bool MemoryHandleImpl::invalidate(::vk::DeviceSize offset, ::vk::DeviceSize size) const {
    if (*this) {
        ::vk::Result r = static_cast<::vk::Result>(
             vmaInvalidateAllocation(_allocator->_allocator, _allocation, offset, size));
        if (r != ::vk::Result::eSuccess) {
            logger.loge("MemoryHandleImpl::invalidate -> ", to_string(r), '!');
            return false;
        }
        else {
            return true;
        }
    }
    else {
        spec::trace("MemoryHandle::invalidate null allocation");
        return false;
    }
}

void MemoryHandleImpl::_update_allocation_info() {
    vmaGetAllocationInfo(_allocator->_allocator, _allocation, &_allocation_info);
}

}  // namespace satr::vk
