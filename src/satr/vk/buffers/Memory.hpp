#ifndef SATR_VK_MEMORY_HPP
#define SATR_VK_MEMORY_HPP
/**
 * @file
 * @brief Reference to allocated vulkan memory handle.
 */

#include <Extensions.hpp>
#include <Utils.hpp>
#include <trace/Trace.hpp>
#include <vulkan/vulkan.hpp>

#include <memory>
#include <optional>
#include <string>
#include <utility>

#include <vk_mem_alloc.h>

namespace satr::vk {

class MemoryHandleImpl;
class MemoryHandle;
using MemoryHandleWeak = std::weak_ptr<MemoryHandleImpl>;

class Allocator : public spec::INonCopyable {
   protected:
    VmaAllocator _allocator;
    uint64_t _frame_counter = 0;

    friend class MemoryHandleImpl;

   protected:
    /** Constructor */
    constexpr Allocator() : _allocator(nullptr) {}
    Allocator(Allocator&& o) noexcept;
    Allocator& operator=(Allocator&& o) noexcept;
    ~Allocator();

   public:
    /**
     * Vulkan instance extensions for optional features of the allocator.
     */
    Extensions get_optional_instance_extensions(uint32_t instance_api_version);

    /**
     * Vulkan device extensions for optional features of the allocator.
     */
    Extensions get_optional_device_extensions(uint32_t instance_api_version);
    /**
     * Initialize allocator.
     */
    bool allocator_init(uint32_t instance_api_version, ::vk::Instance& instance,
                        const ::vk::PhysicalDevice& physical_device,
                        const ::vk::Device& logical_device,
                        const Extensions& logical_device_extensions);
    /**
     * Must be called before vulkan device destruction.
     */
    void allocator_deinit();
    /**
     * Update budget info (if available).
     */
    void on_allocator_new_frame();
    /**
     * Outputs VMA's internal state to file in the current working directory.
     */
    bool allocator_dump_state(const std::string& base_name);

    ::vk::Instance get_instance() const {
        VmaAllocatorInfo info;
        vmaGetAllocatorInfo(_allocator, &info);
        return info.instance;
    }
    ::vk::PhysicalDevice get_physical_device() const {
        VmaAllocatorInfo info;
        vmaGetAllocatorInfo(_allocator, &info);
        return info.physicalDevice;
    }
    ::vk::Device get_device() const {
        VmaAllocatorInfo info;
        vmaGetAllocatorInfo(_allocator, &info);
        return info.device;
    }

    /**
     * Simple buffer allocation.
     * \p mapped_ram Map for random access reads.
     * \p mapped_sqw Map for sequential writes.
     */
    std::pair<MemoryHandle, ::vk::Buffer> allocate(const ::vk::BufferCreateInfo& info,
                                                   bool mapped_ram = false,
                                                   bool mapped_sqw = false,
                                                   bool dedicated = false,
                                                   float priority = 0.5);

    /**
     * Dry/simulated buffer allocation.
     */
    uint32_t get_allocation_memory_type_index(const ::vk::BufferCreateInfo& info, bool mapped_ram = false,
                            bool mapped_sqw = false, bool dedicated = false,
                            float priority = 0.5);

    /**
     * Simple image allocation.
     * \p mapped_ram Map for random access reads.
     * \p mapped_sqw Map for sequential writes.
     */
    std::pair<MemoryHandle, ::vk::Image> allocate(const ::vk::ImageCreateInfo& info,
                                                  bool mapped_ram = false,
                                                  bool mapped_sqw = false,
                                                  bool dedicated = false, float priority = 0.5);

   private:
    VmaAllocationCreateInfo _allocate_get_create_info(bool mapped_ram, bool mapped_sqw,
                                                      bool dedicated, float priority);
};

class MemoryHandleImpl : public spec::INonCopyable {
   protected:
    Allocator* _allocator;
    VmaAllocation _allocation;
    VmaAllocationInfo _allocation_info;

   public:
    /** Constructors */
    constexpr MemoryHandleImpl() noexcept : _allocator(nullptr), _allocation(nullptr) {}
    /** Internal constructor. Main public interface are the methods \ref Allocator::allocate. */
    MemoryHandleImpl(Allocator& allocator, VmaAllocation alloc, VmaAllocationInfo alloc_info);
    MemoryHandleImpl(MemoryHandleImpl&& o) noexcept;
    MemoryHandleImpl& operator=(MemoryHandleImpl&& o) noexcept;
    ~MemoryHandleImpl();

   public:
    operator bool() const {
        return _allocation != nullptr;
    }
    /** Getters/Setters */
    ::vk::Instance get_instance() const;
    ::vk::PhysicalDevice get_physical_device() const;
    ::vk::Device get_device() const;

    void set_name(const std::string& name) {
        set_name(name.c_str());
    }
    void set_name(const char* name);
    const char* get_name() const;
    std::string_view get_name_view() const;

    ::vk::DeviceSize get_size() const;
    uint32_t get_memory_type_index() const;

    bool is_mapped() const;
    void* get_mapping() const;

    /** Operations */
    bool flush(::vk::DeviceSize offset = 0, ::vk::DeviceSize size = VK_WHOLE_SIZE) const;
    bool invalidate(::vk::DeviceSize offset = 0, ::vk::DeviceSize size = VK_WHOLE_SIZE) const;

   protected:
    void _update_allocation_info();
};

/** Wrapper class for Impl */
class MemoryHandle : public std::shared_ptr<MemoryHandleImpl> {
   public:
    using Weak = MemoryHandleWeak;  // For convenience

   protected:
    // Factory methods.
    MemoryHandle(std::shared_ptr<MemoryHandleImpl>&& o) noexcept :
        std::shared_ptr<MemoryHandleImpl>(o) {}  // main constructor

    template<typename... Args> static MemoryHandle make_shared(Args&&... args) {
        auto obj = std::make_shared<MemoryHandleImpl>(std::forward<Args>(args)...);
        return obj;
    }

    friend class Allocator;

   public:
    using std::shared_ptr<MemoryHandleImpl>::shared_ptr;  // inherit constructors
    using std::shared_ptr<MemoryHandleImpl>::shared_ptr::operator=;

    // Forward Impl's methods.
    operator bool() const {
        return static_cast<bool>((std::shared_ptr<MemoryHandleImpl>) *this) &&
               static_cast<bool>((MemoryHandleImpl&) **this);
    }
    /** Getters/Setters */
    ::vk::Instance get_instance() const {
        return (*this)->get_instance();
    }
    ::vk::PhysicalDevice get_physical_device() const {
        return (*this)->get_physical_device();
    }
    ::vk::Device get_device() const {
        return (*this)->get_device();
    }

    void set_name(const std::string& name) {
        (*this)->set_name(name);
    }
    void set_name(const char* name) {
        (*this)->set_name(name);
    }
    const char* get_name() const {
        return (*this)->get_name();
    }
    std::string_view get_name_view() const {
        return (*this)->get_name_view();
    }

    ::vk::DeviceSize get_size() const {
        return (*this)->get_size();
    }
    uint32_t get_memory_type_index() const {
        return (*this)->get_memory_type_index();
    }

    bool is_mapped() const {
        return (*this)->is_mapped();
    }
    void* get_mapping() const {
        return (*this)->get_mapping();
    }

    /** Operations */
    bool flush(::vk::DeviceSize offset = 0, ::vk::DeviceSize size = VK_WHOLE_SIZE) const {
        return (*this)->flush(offset, size);
    }
    bool invalidate(::vk::DeviceSize offset = 0, ::vk::DeviceSize size = VK_WHOLE_SIZE) const {
        return (*this)->invalidate(offset, size);
    }
};

}  // namespace satr::vk

#endif /*SATR_VK_MEMORY_HPP*/