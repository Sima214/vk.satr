#include "LoaderUtils.hpp"

#include <CommandBuffer.hpp>
#include <Formats.hpp>
#include <logger/Logger.hpp>
#include <vulkan/vulkan_enums.hpp>
#include <vulkan/vulkan_to_string.hpp>

#include <cstring>
#include <limits>
#include <span>

#include <vk_mem_alloc.h>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/component_wise.hpp>

namespace satr::vk {

Buffer load_buffer(Context& engine, CommandBufferContext& cmd_ctx, const char* name,
                   ::vk::BufferUsageFlags usage, ::vk::DeviceSize size_bytes,
                   const void* src_data, ::vk::BufferCreateFlags flags, bool memory_dedicated,
                   float memory_priority) {
    uint32_t mem_type_index;
    {
        ::vk::BufferCreateInfo cinfo(flags, size_bytes, usage);
        mem_type_index = engine.get_allocation_memory_type_index(
             cinfo, false, false, memory_dedicated, memory_priority);
    }
    const ::vk::PhysicalDeviceMemoryProperties& memory_properties =
         engine.get_active_device_capabilities().get_memory_properties();
    if (mem_type_index >= memory_properties.memoryTypeCount) {
        return {};
    }
    else if (memory_properties.memoryTypes[mem_type_index].propertyFlags &
             ::vk::MemoryPropertyFlagBits::eHostVisible) {
        // Requested buffer is host visible/mappable, so no staging buffer is required.
        Buffer buf = engine.new_buffer(name, usage, size_bytes, flags, false, true,
                                       memory_dedicated, memory_priority);
        if (!buf) [[unlikely]] {
            return buf;
        }
        // Check memory type index consistency.
        if (buf.get_memory().get_memory_type_index() != mem_type_index) {
            logger.logd("load_buffer: memory type mismatch.");
        }
        // Upload data.
        std::memcpy(buf.get_memory().get_mapping(), src_data, size_bytes);
        buf.get_memory().flush();

        return buf;
    }
    else {
        Buffer buf = engine.new_buffer(name, usage | ::vk::BufferUsageFlagBits::eTransferDst,
                                       size_bytes, flags, false, false, memory_dedicated,
                                       memory_priority);
        Buffer staging_buf = engine.new_buffer(std::string(name) + "_staging",
                                               usage | ::vk::BufferUsageFlagBits::eTransferSrc,
                                               size_bytes, flags, false, true);
        if (!buf || !staging_buf) [[unlikely]] {
            return {};
        }
        // Prepare data.
        std::memcpy(staging_buf.get_memory().get_mapping(), src_data, size_bytes);
        buf.get_memory().flush();
        // Record copy command.
        Buffer::copy(cmd_ctx.get_cmd_buffer(), staging_buf, buf, size_bytes);
        /**
         * Ensure that the staging buffer does not get destroyed
         * until the command buffer finishes execution.
         */
        cmd_ctx.register_resource(staging_buf);
        return buf;
    }
}

template<::vk::ImageType type>
Image<type> load_image(Context& engine, CommandBufferContext& cmd_ctx, const char* name,
                       ::vk::Format fmt, typename Image<type>::size_type size, const void* data,
                       ImageOptions options, std::span<const ::vk::BufferImageCopy> regions) {
    using namespace std::string_literals;

    // TODO: Compressed formats are not supported using this method.
    size_t bytes_per_pixel = get_bits_per_pixel(decompose_component_format(fmt)) / 8;
    logger.logd("For format ", to_string(fmt), " parsed ", bytes_per_pixel, " bytes/pixel.");
    size_t buf_size = static_cast<size_t>(glm::compMul(size)) * bytes_per_pixel;

    Buffer staging_buffer =
         engine.new_buffer(name + "_staging"s, ::vk::BufferUsageFlagBits::eTransferSrc,
                           buf_size, {}, false, true);
    if (!staging_buffer) {
        logger.logf("Couldn't allocate staging buffer for image loading!");
    }
    std::memcpy(staging_buffer.get_memory().get_mapping(), data, buf_size);
    staging_buffer.get_memory().flush();

    options.usage(options.usage() | ::vk::ImageUsageFlagBits::eTransferDst);
    Image<type> img = engine.new_image<type>(name, fmt, size, options);
    if (!img) {
        logger.logf("Couldn't allocate image!");
    }

    {
        ::vk::ImageSubresourceRange barrier_subrcr = {::vk::ImageAspectFlagBits::eColor, 0, 1,
                                                      0, 1};
        ::vk::ImageMemoryBarrier barrier = {{},
                                            ::vk::AccessFlagBits::eTransferWrite,
                                            ::vk::ImageLayout::eUndefined,
                                            ::vk::ImageLayout::eTransferDstOptimal,
                                            ::vk::QueueFamilyIgnored,
                                            ::vk::QueueFamilyIgnored,
                                            img.get_handle(),
                                            barrier_subrcr};
        cmd_ctx.get_cmd_buffer().pipelineBarrier(::vk::PipelineStageFlagBits::eTopOfPipe,
                                                 ::vk::PipelineStageFlagBits::eTransfer, {}, {},
                                                 {}, {barrier});
    }
    {
        ::vk::BufferImageCopy def_buf2img_info;
        if (regions.empty()) [[likely]] {
            ::vk::ImageSubresourceLayers buf2img_subrcl = {::vk::ImageAspectFlagBits::eColor, 0,
                                                           0, 1};
            def_buf2img_info = ::vk::BufferImageCopy{0,
                                                     0,
                                                     0,
                                                     buf2img_subrcl,
                                                     vec_to_offset<::vk::Offset3D>({0, 0, 0}),
                                                     vec_to_extent<::vk::Extent3D>(size)};
            regions = std::span<const ::vk::BufferImageCopy>(&def_buf2img_info, 1);
        }
        cmd_ctx.get_cmd_buffer().copyBufferToImage(
             staging_buffer.get_handle(), img.get_handle(),
             ::vk::ImageLayout::eTransferDstOptimal, regions);
    }
    {
        // TODO: Figure out the correct flags for using transfer-only queue.
        ::vk::ImageSubresourceRange barrier_subrcr = {::vk::ImageAspectFlagBits::eColor, 0, 1,
                                                      0, 1};
        ::vk::ImageMemoryBarrier barrier = {::vk::AccessFlagBits::eTransferWrite,
                                            ::vk::AccessFlagBits::eShaderRead,
                                            ::vk::ImageLayout::eTransferDstOptimal,
                                            ::vk::ImageLayout::eShaderReadOnlyOptimal,
                                            ::vk::QueueFamilyIgnored,
                                            ::vk::QueueFamilyIgnored,
                                            img.get_handle(),
                                            barrier_subrcr};
        cmd_ctx.get_cmd_buffer().pipelineBarrier(::vk::PipelineStageFlagBits::eTransfer,
                                                 ::vk::PipelineStageFlagBits::eFragmentShader,
                                                 {}, {}, {}, {barrier});
    }

    cmd_ctx.register_resource(staging_buffer);
    return img;
}

template Image<::vk::ImageType::e1D> load_image<::vk::ImageType::e1D>(
     Context&, CommandBufferContext&, const char*, ::vk::Format,
     typename Image<::vk::ImageType::e1D>::size_type, const void*, ImageOptions,
     std::span<const ::vk::BufferImageCopy>);
template Image<::vk::ImageType::e2D> load_image<::vk::ImageType::e2D>(
     Context&, CommandBufferContext&, const char*, ::vk::Format,
     typename Image<::vk::ImageType::e2D>::size_type, const void*, ImageOptions,
     std::span<const ::vk::BufferImageCopy>);
template Image<::vk::ImageType::e3D> load_image<::vk::ImageType::e3D>(
     Context&, CommandBufferContext&, const char*, ::vk::Format,
     typename Image<::vk::ImageType::e3D>::size_type, const void*, ImageOptions,
     std::span<const ::vk::BufferImageCopy>);

template<::vk::ImageType type, bool clattach>
Buffer retrieve_image(Context& engine, CommandBufferContext& cmd_ctx, Image<type> img) {
    using namespace std::string_literals;

    auto fmt = img.get_format();
    auto size = img.get_size();
    auto name = img.get_name_view();

    // TODO: Compressed formats are not supported using this method.
    size_t bytes_per_pixel = get_bits_per_pixel(decompose_component_format(fmt)) / 8;
    logger.logd("For format ", to_string(fmt), " parsed ", bytes_per_pixel, " bytes/pixel.");
    size_t buf_size = static_cast<size_t>(glm::compMul(size)) * bytes_per_pixel;

    Buffer buf =
         engine.new_buffer(std::string(name) + "_rb"s, ::vk::BufferUsageFlagBits::eTransferDst,
                           buf_size, {}, false, true);
    if (!buf) {
        logger.logf("Couldn't allocate readback buffer for image retrieving!");
    }

    /*cmd_ctx.register_resource(img);
    cmd_ctx.register_resource(buf);*/

    {
        ::vk::ImageSubresourceRange barrier_subrcr = {::vk::ImageAspectFlagBits::eColor, 0, 1,
                                                      0, 1};
        ::vk::ImageMemoryBarrier barrier = {
             clattach ? ::vk::AccessFlagBits::eColorAttachmentWrite :
                        ::vk::AccessFlagBits::eShaderRead,
             ::vk::AccessFlagBits::eTransferRead,
             clattach ? ::vk::ImageLayout::eColorAttachmentOptimal :
                        ::vk::ImageLayout::eShaderReadOnlyOptimal,
             ::vk::ImageLayout::eTransferSrcOptimal,
             ::vk::QueueFamilyIgnored,
             ::vk::QueueFamilyIgnored,
             img.get_handle(),
             barrier_subrcr};
        cmd_ctx.get_cmd_buffer().pipelineBarrier(
             clattach ? ::vk::PipelineStageFlagBits::eColorAttachmentOutput :
                        ::vk::PipelineStageFlagBits::eFragmentShader,
             ::vk::PipelineStageFlagBits::eTransfer, {}, {}, {}, {barrier});
    }
    {
        ::vk::ImageSubresourceLayers img2buf_subrcl = {::vk::ImageAspectFlagBits::eColor, 0, 0,
                                                       1};
        ::vk::BufferImageCopy img2buf_info =
             ::vk::BufferImageCopy{0,
                                   0,
                                   0,
                                   img2buf_subrcl,
                                   vec_to_offset<::vk::Offset3D>({0, 0, 0}),
                                   vec_to_extent<::vk::Extent3D>(size)};
        cmd_ctx.get_cmd_buffer().copyImageToBuffer(img.get_handle(),
                                                   ::vk::ImageLayout::eTransferSrcOptimal,
                                                   buf.get_handle(), {img2buf_info});
    }
    {
        ::vk::ImageSubresourceRange barrier_subrcr = {::vk::ImageAspectFlagBits::eColor, 0, 1,
                                                      0, 1};
        ::vk::ImageMemoryBarrier barrier = {
             ::vk::AccessFlagBits::eTransferRead,
             clattach ? ::vk::AccessFlags{} : ::vk::AccessFlagBits::eShaderRead,
             ::vk::ImageLayout::eTransferSrcOptimal,
             clattach ? ::vk::ImageLayout::eColorAttachmentOptimal :
                        ::vk::ImageLayout::eShaderReadOnlyOptimal,
             ::vk::QueueFamilyIgnored,
             ::vk::QueueFamilyIgnored,
             img.get_handle(),
             barrier_subrcr};
        cmd_ctx.get_cmd_buffer().pipelineBarrier(
             ::vk::PipelineStageFlagBits::eTransfer,
             clattach ? ::vk::PipelineStageFlagBits::eColorAttachmentOutput :
                        ::vk::PipelineStageFlagBits::eFragmentShader,
             {}, {}, {}, {barrier});
    }

    return buf;
}

template Buffer retrieve_image<::vk::ImageType::e1D, false>(Context&, CommandBufferContext&,
                                                            Image<::vk::ImageType::e1D>);
/*template Buffer retrieve_image<::vk::ImageType::e1D, true>(Context&, CommandBufferContext&,
                                                           Image<::vk::ImageType::e1D>);*/
template Buffer retrieve_image<::vk::ImageType::e2D, false>(Context&, CommandBufferContext&,
                                                            Image<::vk::ImageType::e2D>);
template Buffer retrieve_image<::vk::ImageType::e2D, true>(Context&, CommandBufferContext&,
                                                           Image<::vk::ImageType::e2D>);
template Buffer retrieve_image<::vk::ImageType::e3D, false>(Context&, CommandBufferContext&,
                                                            Image<::vk::ImageType::e3D>);
/*template Buffer retrieve_image<::vk::ImageType::e3D, true>(Context&, CommandBufferContext&,
                                                           Image<::vk::ImageType::e3D>);*/

}  // namespace satr::vk
