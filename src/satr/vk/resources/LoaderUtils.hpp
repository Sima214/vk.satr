#ifndef SATR_VK_LOADERUTILS_HPP
#define SATR_VK_LOADERUTILS_HPP
/**
 * @file
 * @brief
 */

#include "Trace.hpp"

#include <Buffer.hpp>
#include <CommandBuffer.hpp>
#include <Context.hpp>
#include <Framebuffer.hpp>
#include <Image.hpp>
#include <vulkan/vulkan.hpp>

namespace satr::vk {

/**
 * Create a new buffer and initialize its contents using the data stored in \p src_data.
 *
 * Intended for transfering data stored in virtual cpu address space to vulkan device local
 * memory.
 */
Buffer load_buffer(Context& engine, CommandBufferContext& cmd_ctx, const char* name,
                   ::vk::BufferUsageFlags usage, ::vk::DeviceSize size_bytes,
                   const void* src_data, ::vk::BufferCreateFlags flags = {},
                   bool memory_dedicated = false, float memory_priority = 0.5);

inline Buffer load_buffer(Context& engine, CommandBufferContext& cmd_ctx,
                          const std::string& name, ::vk::BufferUsageFlags usage,
                          ::vk::DeviceSize size_bytes, const void* src_data,
                          ::vk::BufferCreateFlags flags = {}, bool memory_dedicated = false,
                          float memory_priority = 0.5) {
    return load_buffer(engine, cmd_ctx, name.c_str(), usage, size_bytes, src_data, flags,
                       memory_dedicated, memory_priority);
}

inline Buffer load_vertex_buffer(Context& engine, CommandBufferContext& cmd_ctx,
                                 const char* name, ::vk::DeviceSize size_bytes,
                                 const void* src_data, ::vk::BufferUsageFlags extra_usage = {},
                                 ::vk::BufferCreateFlags flags = {},
                                 bool memory_dedicated = false, float memory_priority = 0.5) {
    return load_buffer(engine, cmd_ctx, name,
                       extra_usage | ::vk::BufferUsageFlagBits::eVertexBuffer, size_bytes,
                       src_data, flags, memory_dedicated, memory_priority);
}

inline Buffer load_vertex_buffer(Context& engine, CommandBufferContext& cmd_ctx,
                                 const std::string& name, ::vk::DeviceSize size_bytes,
                                 const void* src_data, ::vk::BufferUsageFlags extra_usage = {},
                                 ::vk::BufferCreateFlags flags = {},
                                 bool memory_dedicated = false, float memory_priority = 0.5) {
    return load_buffer(engine, cmd_ctx, name,
                       extra_usage | ::vk::BufferUsageFlagBits::eVertexBuffer, size_bytes,
                       src_data, flags, memory_dedicated, memory_priority);
}

inline Buffer load_index_buffer(Context& engine, CommandBufferContext& cmd_ctx,
                                const char* name, ::vk::DeviceSize size_bytes,
                                const void* src_data, ::vk::BufferUsageFlags extra_usage = {},
                                ::vk::BufferCreateFlags flags = {},
                                bool memory_dedicated = false, float memory_priority = 0.5) {
    return load_buffer(engine, cmd_ctx, name,
                       extra_usage | ::vk::BufferUsageFlagBits::eIndexBuffer, size_bytes,
                       src_data, flags, memory_dedicated, memory_priority);
}

inline Buffer load_index_buffer(Context& engine, CommandBufferContext& cmd_ctx,
                                const std::string& name, ::vk::DeviceSize size_bytes,
                                const void* src_data, ::vk::BufferUsageFlags extra_usage = {},
                                ::vk::BufferCreateFlags flags = {},
                                bool memory_dedicated = false, float memory_priority = 0.5) {
    return load_buffer(engine, cmd_ctx, name,
                       extra_usage | ::vk::BufferUsageFlagBits::eIndexBuffer, size_bytes,
                       src_data, flags, memory_dedicated, memory_priority);
}

inline Buffer load_uniform_buffer(Context& engine, CommandBufferContext& cmd_ctx,
                                  const char* name, ::vk::DeviceSize size_bytes,
                                  const void* src_data, ::vk::BufferUsageFlags extra_usage = {},
                                  ::vk::BufferCreateFlags flags = {},
                                  bool memory_dedicated = false, float memory_priority = 0.5) {
    return load_buffer(engine, cmd_ctx, name,
                       extra_usage | ::vk::BufferUsageFlagBits::eUniformBuffer, size_bytes,
                       src_data, flags, memory_dedicated, memory_priority);
}

inline Buffer load_uniform_buffer(Context& engine, CommandBufferContext& cmd_ctx,
                                  const std::string& name, ::vk::DeviceSize size_bytes,
                                  const void* src_data, ::vk::BufferUsageFlags extra_usage = {},
                                  ::vk::BufferCreateFlags flags = {},
                                  bool memory_dedicated = false, float memory_priority = 0.5) {
    return load_buffer(engine, cmd_ctx, name,
                       extra_usage | ::vk::BufferUsageFlagBits::eUniformBuffer, size_bytes,
                       src_data, flags, memory_dedicated, memory_priority);
}

template<::vk::ImageType type>
Image<type> load_image(Context& engine, CommandBufferContext& cmd_ctx, const char* name,
                       ::vk::Format format, typename Image<type>::size_type size,
                       const void* data, ImageOptions options,
                       std::span<const ::vk::BufferImageCopy> regions = {});
template<::vk::ImageType type>
inline Image<type> load_image(Context& engine, CommandBufferContext& cmd_ctx,
                              const std::string& name, ::vk::Format format,
                              typename Image<type>::size_type size, const void* data,
                              ImageOptions options,
                              std::span<const ::vk::BufferImageCopy> regions = {}) {
    return load_image<type>(engine, cmd_ctx, name.c_str(), format, size, data, options,
                            regions);
}

inline Image1D load_image1d(Context& engine, CommandBufferContext& cmd_ctx, const char* name,
                            ::vk::Format format, typename Image1D::size_type size,
                            const void* data, ImageOptions options,
                            std::span<const ::vk::BufferImageCopy> regions = {}) {
    return load_image<::vk::ImageType::e1D>(engine, cmd_ctx, name, format, size, data, options,
                                            regions);
}
inline Image1D load_image1d(Context& engine, CommandBufferContext& cmd_ctx,
                            const std::string& name, ::vk::Format format,
                            typename Image1D::size_type size, const void* data,
                            ImageOptions options,
                            std::span<const ::vk::BufferImageCopy> regions = {}) {
    return load_image<::vk::ImageType::e1D>(engine, cmd_ctx, name.c_str(), format, size, data,
                                            options, regions);
}

inline Image2D load_image2d(Context& engine, CommandBufferContext& cmd_ctx, const char* name,
                            ::vk::Format format, typename Image2D::size_type size,
                            const void* data, ImageOptions options,
                            std::span<const ::vk::BufferImageCopy> regions = {}) {
    return load_image<::vk::ImageType::e2D>(engine, cmd_ctx, name, format, size, data, options,
                                            regions);
}
inline Image2D load_image2d(Context& engine, CommandBufferContext& cmd_ctx,
                            const std::string& name, ::vk::Format format,
                            typename Image2D::size_type size, const void* data,
                            ImageOptions options,
                            std::span<const ::vk::BufferImageCopy> regions = {}) {
    return load_image<::vk::ImageType::e2D>(engine, cmd_ctx, name.c_str(), format, size, data,
                                            options, regions);
}

inline Image3D load_image3d(Context& engine, CommandBufferContext& cmd_ctx, const char* name,
                            ::vk::Format format, typename Image3D::size_type size,
                            const void* data, ImageOptions options,
                            std::span<const ::vk::BufferImageCopy> regions = {}) {
    return load_image<::vk::ImageType::e3D>(engine, cmd_ctx, name, format, size, data, options,
                                            regions);
}
inline Image3D load_image3d(Context& engine, CommandBufferContext& cmd_ctx,
                            const std::string& name, ::vk::Format format,
                            typename Image3D::size_type size, const void* data,
                            ImageOptions options,
                            std::span<const ::vk::BufferImageCopy> regions = {}) {
    return load_image<::vk::ImageType::e3D>(engine, cmd_ctx, name.c_str(), format, size, data,
                                            options, regions);
}

template<::vk::ImageType type, bool color_attachment>
Buffer retrieve_image(Context& engine, CommandBufferContext& cmd_ctx, Image<type> src);

inline Buffer retrieve_image1d(Context& engine, CommandBufferContext& cmd_ctx, Image1D src) {
    return retrieve_image<::vk::ImageType::e1D, false>(engine, cmd_ctx, src);
}
inline Buffer retrieve_image2d(Context& engine, CommandBufferContext& cmd_ctx, Image2D src) {
    return retrieve_image<::vk::ImageType::e2D, false>(engine, cmd_ctx, src);
}
inline Buffer retrieve_image3d(Context& engine, CommandBufferContext& cmd_ctx, Image3D src) {
    return retrieve_image<::vk::ImageType::e3D, false>(engine, cmd_ctx, src);
}

/**
 * As of writing this, the framebuffer class has support only for 2D attachments.
 */
inline std::optional<Buffer> framebuffer_readback(Context& engine,
                                                  CommandBufferContext& cmd_ctx,
                                                  FramebufferChain& fb, size_t chain_index,
                                                  size_t color_attachment_index) {
    auto views = fb.get_color_attachment_views(chain_index);
    if (color_attachment_index >= views.size()) [[unlikely]] {
        spec::trace("framebuffer_readback: invalid chain_index or "
                    "color_attachment_index out of bounds!");
        return {};
    }
    auto& view = views[color_attachment_index];
    // It's illegal to copy from renderpass image inside the renderpass.
    // return retrieve_image<::vk::ImageType::e2D, true>(engine, cmd_ctx, view.get_image());
    return retrieve_image<::vk::ImageType::e2D, false>(engine, cmd_ctx, view.get_image());
}

}  // namespace satr::vk

#endif /*SATR_VK_LOADERUTILS_HPP*/
