#ifndef SATR_VK_DEARIMGUISCENE_HPP
#define SATR_VK_DEARIMGUISCENE_HPP
/**
 * @file
 * @brief NOTE: Should only append one DearImguiScene per RenderLoop.
 */

#include <Buffer.hpp>
#include <DescriptorSetPack.hpp>
#include <Pipelines.hpp>
#include <RenderLoop.hpp>
#include <Samplers.hpp>
#include <Scene.hpp>

#include <memory>
#include <utility>

#include <imgui.h>

namespace satr::vk {

class DearImguiScene : public SceneImpl {
   protected:
    static const uint32_t VERTEX_SHADER_CODE[];
    static const uint32_t FRAGMENT_SHADER_CODE[];

    static constexpr size_t MAX_TEXTURE_COUNT = 1;

    static constexpr size_t VERTEX_BUFFER_INITIAL_SIZE = 5120 * sizeof(ImDrawVert);
    static constexpr size_t INDEX_BUFFER_INITIAL_SIZE = 10240 * sizeof(ImDrawIdx);

    static constexpr size_t vertex_buffer_apply_growth_factor(size_t prev_size,
                                                              size_t new_min_size) {
        size_t size = prev_size;
        do {
            size += size / 2;
            size = (size / sizeof(ImDrawVert)) * sizeof(ImDrawVert);
        } while (size <= new_min_size);
        return size;
    }
    static constexpr size_t index_buffer_apply_growth_factor(size_t prev_size,
                                                             size_t new_min_size) {
        size_t size = prev_size;
        do {
            size += size / 2;
            size = (size / sizeof(ImDrawIdx)) * sizeof(ImDrawIdx);
        } while (size <= new_min_size);
        return size;
    }

   protected:
    ImGuiContext* _imgui_context;
    double _last_time;

    Pipeline _pipeline;
    /* Contains MAX_TEXTURE_COUNT descriptors */
    DescriptorSetPack _texture_descs;
    uint32_t _texture_binding;

    Sampler _sampler;
    size_t _texture_count = 0;
    std::array<Image2D::View, MAX_TEXTURE_COUNT> _texture_views;
    std::unique_ptr<std::pair<Buffer /*vertex_buffer*/, Buffer /*index_buffer*/>[]> _buffers;

   public:
    DearImguiScene(RenderLoop& root);
    virtual ~DearImguiScene();

    virtual bool on_setup(size_t chain_size, glm::ivec2 scene_size,
                          [[maybe_unused]] glm::vec2& offset,
                          [[maybe_unused]] glm::vec2& extent) override;

    virtual std::tuple<SceneImpl::RenderData*, bool /*reused*/> on_render(
         size_t index) override;
    bool pre_render();
    virtual void render_ui(ImGuiContext* ctx) = 0;
    virtual bool render(size_t chain_index, ::vk::CommandBuffer cmd_buf) override;

    virtual void on_resize(glm::ivec2 size, glm::vec2& offset, glm::vec2& extent) override;
    virtual void on_focus(bool focused) override;
    virtual bool on_key_press(satr::nui::Window::Keys key, int scancode,
                              satr::nui::Window::ButtonState action,
                              satr::nui::Window::Modifiers mods) override;
    virtual bool on_text_input(const std::string& text) override;
    virtual bool on_mouse_press(satr::nui::Window::MouseButtons button,
                                satr::nui::Window::ButtonState action,
                                satr::nui::Window::Modifiers mods) override;
    virtual bool on_mouse_scroll(glm::dvec2 offset) override;
    virtual bool on_mouse_move(glm::dvec2 pos) override;

   private:
    static ImGuiKey __key_to_imgui(satr::nui::Window::Keys key);
};

}  // namespace satr::vk

#endif /*SATR_VK_DEARIMGUISCENE_HPP*/