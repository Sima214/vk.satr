#include "DearImguiScene.hpp"

#include <LoaderUtils.hpp>
#include <Scene.hpp>
#include <logger/Logger.hpp>
#include <vulkan/vulkan.hpp>

#include <cstdint>
#include <memory>
#include <string>
#include <utility>

#include <imgui.h>

namespace satr::vk {

const uint32_t DearImguiScene::VERTEX_SHADER_CODE[] =
#include "DearImgui.vert.spirv.h"
     ;

const uint32_t DearImguiScene::FRAGMENT_SHADER_CODE[] =
#include "DearImgui.frag.spirv.h"
     ;

DearImguiScene::DearImguiScene(RenderLoop& root) : SceneImpl("imgui", root) {
    IMGUI_CHECKVERSION();
    _imgui_context = ImGui::CreateContext();
    ImGuiIO& imgui_options = ImGui::GetIO();
    imgui_options.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
    // TODO: imgui_options.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;
    imgui_options.ConfigFlags |= ImGuiConfigFlags_NoMouseCursorChange;
    imgui_options.BackendRendererUserData = dynamic_cast<void*>(this);
    imgui_options.BackendRendererName = "vksatr";
    imgui_options.BackendFlags |= ImGuiBackendFlags_RendererHasVtxOffset;
}
DearImguiScene::~DearImguiScene() {
    if (_imgui_context != nullptr) {
        { /** Satisfy the assertion in ImGui::Shutdown */
            ImGuiIO& imgui_options = ImGui::GetIO();
            imgui_options.BackendRendererUserData = nullptr;
        }
        ImGui::DestroyContext(_imgui_context);
        _imgui_context = nullptr;
    }
}

bool DearImguiScene::on_setup(size_t chain_size, glm::ivec2 scene_size,
                              [[maybe_unused]] glm::vec2& offset,
                              [[maybe_unused]] glm::vec2& extent) {
    if (!SceneImpl::setup_framebuffer(chain_size, scene_size, ::vk::Format::eR8G8B8A8Srgb)) {
        return false;
    }
    get_framebuffer().set_color_clear_value(0, glm::vec4(0, 0, 0, 0));
    if (!SceneImpl::setup_render_data(chain_size)) {
        return false;
    }

    {
        _buffers = std::unique_ptr<std::pair<Buffer, Buffer>[]>(
             new std::pair<Buffer, Buffer>[chain_size]);

        for (size_t i = 0; i < chain_size; i++) {
            _buffers[i] = {
                 get_engine().new_vertex_buffer("imgui_vertex_buf" + std::to_string(i),
                                                VERTEX_BUFFER_INITIAL_SIZE, {}, {}, true),
                 get_engine().new_index_buffer("imgui_index_buf" + std::to_string(i),
                                               INDEX_BUFFER_INITIAL_SIZE, {}, {}, true)};
        }
    }

    {
        auto vertex_shader = get_engine().new_shader(VERTEX_SHADER_CODE);
        auto fragment_shader = get_engine().new_shader(FRAGMENT_SHADER_CODE);
        satr::vk::Module shaders[] = {vertex_shader, fragment_shader};

        satr::vk::GraphicsPipelineOptions pipeline_options;
        pipeline_options.dynamic_state_add(::vk::DynamicState::eViewport);
        pipeline_options.dynamic_state_add(::vk::DynamicState::eScissor);
        pipeline_options.multisample_rasterization_samples(::vk::SampleCountFlagBits::e1);

        pipeline_options.vertex_input_bindings_add(0, sizeof(ImDrawVert));
        pipeline_options.vertex_input_attributes_add("aPos", 0, offsetof(ImDrawVert, pos));
        pipeline_options.vertex_input_attributes_add("aUV", 0, offsetof(ImDrawVert, uv));
        pipeline_options.vertex_input_attributes_add("aColor", 0, offsetof(ImDrawVert, col),
                                                     ::vk::Format::eR8G8B8A8Unorm);

        pipeline_options.color_blend_enable(true);
        pipeline_options.color_blend_src_factor(::vk::BlendFactor::eSrcAlpha);
        pipeline_options.color_blend_dst_factor(::vk::BlendFactor::eOneMinusSrcAlpha);
        pipeline_options.color_blend_op(::vk::BlendOp::eAdd);
        pipeline_options.alpha_blend_src_factor(::vk::BlendFactor::eOne);
        pipeline_options.alpha_blend_dst_factor(::vk::BlendFactor::eOneMinusSrcAlpha);
        pipeline_options.alpha_blend_op(::vk::BlendOp::eAdd);

        _pipeline = get_engine().new_pipeline(get_framebuffer(), shaders, pipeline_options);
    }

    {
        auto texture_desc_info =
             _pipeline.get_descriptors().retrieve_by_name("sTexture").value();
        _texture_binding = texture_desc_info.get_binding();
        auto texture_set_layout =
             _pipeline.get_descriptors().get_set_layout(texture_desc_info.get_set());
        _texture_descs =
             get_engine().new_descriptor_set_pack(MAX_TEXTURE_COUNT, texture_set_layout);
        if (!_texture_descs) {
            logger.loge("DearImguiScene::on_setup: "
                        "Unable to allocate descriptor sets!");
            return false;
        }
    }

    _sampler =
         get_engine().new_sampler(::vk::Filter::eLinear, ::vk::SamplerMipmapMode::eLinear,
                                  ::vk::SamplerAddressMode::eRepeat, -1000.f, 1000.f, 1.0f);
    if (!_sampler) {
        logger.loge("DearImguiScene::on_setup: "
                    "Couldn't allocate sampler!");
        return false;
    }

    {
        if (_texture_count >= MAX_TEXTURE_COUNT) {
            logger.loge("DearImguiScene::on_setup: "
                        "Reached texture limit!");
            return false;
        }

        ImGuiIO& io = ImGui::GetIO();
        uint8_t* pixels;
        int width, height;
        io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);

        SingleUseCommandBufferContext trsfr_ctx(get_engine(), QueueSlotRole::Graphics);
        Image2D imgui_font_texture =
             load_image2d(get_engine(), trsfr_ctx, "imgui_font", ::vk::Format::eR8G8B8A8Unorm,
                          {width, height}, pixels,
                          ImageOptions(::vk::ImageUsageFlagBits::eSampled)
                               .tiling(::vk::ImageTiling::eOptimal));

        trsfr_ctx.submit();
        { /* In parallel to the transfer operation... */
            _texture_views[_texture_count] = imgui_font_texture.get_view().value();
            size_t texture_index = _texture_count;
            _texture_count++;
            io.Fonts->SetTexID(reinterpret_cast<void*>((uintptr_t) texture_index));
            _sampler.update_combined_descriptor<::vk::ImageType::e2D>(
                 _texture_descs[texture_index], _texture_binding,
                 _texture_views[texture_index]);
        }
        trsfr_ctx.wait();
    }

    {
        // Initialize size.
        ImGuiIO& io = ImGui::GetIO();
        io.DisplaySize = ImVec2(scene_size.x, scene_size.y);
        auto& window = get_root().get_window();
        auto window_size = window.get_size();
        auto framebuffer_size = window.get_framebuffer_size();
        auto scale = framebuffer_size / window_size;
        io.DisplayFramebufferScale = ImVec2(scale.x, scale.y);
    }

    _last_time = get_root().get_loop_time();
    return SceneImpl::on_setup_complete();
}  // namespace satr::vk

std::tuple<SceneImpl::RenderData*, bool /*reused*/> DearImguiScene::on_render(size_t index) {
    if (pre_render()) {
        mark_dirty();
    }
    return SceneImpl::on_render(index);
}

bool DearImguiScene::pre_render() {
    ImGuiIO& io = ImGui::GetIO();
    {  // Calculate delta time.
        double current_time = get_root().get_loop_time();
        float delta_time = current_time - _last_time;
        io.DeltaTime = delta_time > 0 ? delta_time : (1.f / 360.f);
        _last_time = current_time;
    }
    {  // TODO: Update cursor state/icon.
    }

    ImGui::NewFrame();
    render_ui(_imgui_context);

    return true;
}
bool DearImguiScene::render(size_t chain_index, ::vk::CommandBuffer cmd_buf) {
    ImGui::Render();
    ImDrawData* draw_data = ImGui::GetDrawData();
    int fb_width = (int) (draw_data->DisplaySize.x * draw_data->FramebufferScale.x);
    int fb_height = (int) (draw_data->DisplaySize.y * draw_data->FramebufferScale.y);
    bool is_minimized = fb_width <= 0.0f || fb_height <= 0.0f;

    if (!is_minimized) {
        size_t vertex_size = draw_data->TotalVtxCount * sizeof(ImDrawVert);
        size_t index_size = draw_data->TotalIdxCount * sizeof(ImDrawIdx);
        {  // Resize geometry buffers if required.
            auto& current_buffers = _buffers[chain_index];

            size_t current_vertex_size = current_buffers.first.get_memory().get_size();
            if (vertex_size > current_vertex_size) {
                size_t new_vertex_size =
                     vertex_buffer_apply_growth_factor(current_vertex_size, vertex_size);
                logger.logi("DearImguiScene::render: Resizing vertex buffer from ",
                            current_vertex_size / sizeof(ImDrawVert), " to ",
                            new_vertex_size / sizeof(ImDrawVert), ".");
                current_buffers.first.reset();
                current_buffers.first = get_engine().new_vertex_buffer(
                     "imgui_vertex_buf" + std::to_string(chain_index), new_vertex_size, {}, {},
                     true);
            }

            size_t current_index_size = current_buffers.second.get_memory().get_size();
            if (index_size > current_index_size) {
                size_t new_index_size =
                     index_buffer_apply_growth_factor(current_index_size, index_size);
                logger.logi("DearImguiScene::render: Resizing index buffer from ",
                            current_index_size / sizeof(ImDrawIdx), " to ",
                            new_index_size / sizeof(ImDrawIdx), ".");
                current_buffers.second.reset();
                current_buffers.second = get_engine().new_index_buffer(
                     "imgui_index_buf" + std::to_string(chain_index), new_index_size, {}, {},
                     true);
            }
        }

        // Copy geometry.
        auto [vertex_buffer, index_buffer] = _buffers[chain_index];
        {
            ImDrawVert* vertex_buffer_ptr =
                 reinterpret_cast<ImDrawVert*>(vertex_buffer.get_memory().get_mapping());
            ImDrawIdx* index_buffer_ptr =
                 reinterpret_cast<ImDrawIdx*>(index_buffer.get_memory().get_mapping());
            for (int n = 0; n < draw_data->CmdListsCount; n++) {
                const ImDrawList* cmd_list = draw_data->CmdLists[n];
                std::memcpy(vertex_buffer_ptr, cmd_list->VtxBuffer.Data,
                            cmd_list->VtxBuffer.Size * sizeof(ImDrawVert));
                std::memcpy(index_buffer_ptr, cmd_list->IdxBuffer.Data,
                            cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx));
                vertex_buffer_ptr += cmd_list->VtxBuffer.Size;
                index_buffer_ptr += cmd_list->IdxBuffer.Size;
            }
        }
        vertex_buffer.get_memory().flush();
        index_buffer.get_memory().flush();

        // Setup pipeline state.
        _pipeline.bind(cmd_buf);
        vertex_buffer.bind_vertex(cmd_buf, 0);
        index_buffer.bind_index(cmd_buf, sizeof(ImDrawIdx) == 2 ? ::vk::IndexType::eUint16 :
                                                                  ::vk::IndexType::eUint32);
        _pipeline.update_viewport(cmd_buf, get_framebuffer(), chain_index);

        {  // Setup scale and translation:
            /* Our visible imgui space lies from draw_data->DisplayPps (top left) to
             * draw_data->DisplayPos+data_data->DisplaySize (bottom right). DisplayPos is (0,0)
             * for single viewport apps. */
            glm::vec2 display_size(draw_data->DisplaySize.x, draw_data->DisplaySize.y);
            glm::vec2 display_pos(draw_data->DisplayPos.x, draw_data->DisplayPos.y);
            glm::vec2 scale = 2.0f / display_size;
            glm::vec2 translate = -1.0f - display_pos * scale;
            // TODO: Make use of my Pipeline interface.
            cmd_buf.pushConstants(_pipeline.get_layout(), ::vk::ShaderStageFlagBits::eVertex,
                                  sizeof(float) * 0, sizeof(float) * 2, &scale);
            cmd_buf.pushConstants(_pipeline.get_layout(), ::vk::ShaderStageFlagBits::eVertex,
                                  sizeof(float) * 2, sizeof(float) * 2, &translate);
        }

        // Will project scissor/clipping rectangles into framebuffer space

        ImVec2 clip_off = draw_data->DisplayPos;          // (0,0) unless using multi-viewports
        ImVec2 clip_scale = draw_data->FramebufferScale;  // (1,1) unless using retina display
                                                          // which are often (2,2)

        // Render command lists
        // (Because we merged all buffers into a single one, we maintain our own offset into
        // them)
        int global_vtx_offset = 0;
        int global_idx_offset = 0;
        for (int n = 0; n < draw_data->CmdListsCount; n++) {
            const ImDrawList* cmd_list = draw_data->CmdLists[n];
            for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++) {
                const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
                if (pcmd->UserCallback != nullptr) {
                    // User callback, registered via ImDrawList::AddCallback()
                    // (ImDrawCallback_ResetRenderState is a special callback value used by the
                    // user to request the renderer to reset render state.)
                    // if (pcmd->UserCallback == ImDrawCallback_ResetRenderState)
                    //     ImGui_ImplVulkan_SetupRenderState(draw_data, pipeline,
                    //     command_buffer,
                    //                                       rb, fb_width, fb_height);
                    // else
                    //     pcmd->UserCallback(cmd_list, pcmd);
                    logger.logf("DearImguiScene::render: Handling of "
                                "pcmd->UserCallback is not implemented!");
                }
                else {
                    // Project scissor/clipping rectangles into framebuffer space
                    ImVec2 clip_min((pcmd->ClipRect.x - clip_off.x) * clip_scale.x,
                                    (pcmd->ClipRect.y - clip_off.y) * clip_scale.y);
                    ImVec2 clip_max((pcmd->ClipRect.z - clip_off.x) * clip_scale.x,
                                    (pcmd->ClipRect.w - clip_off.y) * clip_scale.y);

                    // Clamp to viewport as vkCmdSetScissor() won't accept values that are off
                    // bounds
                    if (clip_min.x < 0.0f) {
                        clip_min.x = 0.0f;
                    }
                    if (clip_min.y < 0.0f) {
                        clip_min.y = 0.0f;
                    }
                    if (clip_max.x > fb_width) {
                        clip_max.x = (float) fb_width;
                    }
                    if (clip_max.y > fb_height) {
                        clip_max.y = (float) fb_height;
                    }
                    if (clip_max.x <= clip_min.x || clip_max.y <= clip_min.y)
                        continue;

                    // Apply scissor/clipping rectangle
                    ::vk::Rect2D scissor;
                    scissor.offset.x = (int32_t) (clip_min.x);
                    scissor.offset.y = (int32_t) (clip_min.y);
                    scissor.extent.width = (uint32_t) (clip_max.x - clip_min.x);
                    scissor.extent.height = (uint32_t) (clip_max.y - clip_min.y);
                    cmd_buf.setScissor(0, 1, &scissor);

                    // Bind DescriptorSet with font or user texture
                    size_t texture_index = reinterpret_cast<uintptr_t>(pcmd->TextureId);
                    ::vk::DescriptorSet texture_desc = _texture_descs.get_set(texture_index);
                    _pipeline.bind_sets(cmd_buf, std::span(&texture_desc, 1));

                    // Draw
                    cmd_buf.drawIndexed(pcmd->ElemCount, 1, pcmd->IdxOffset + global_idx_offset,
                                        pcmd->VtxOffset + global_vtx_offset, 0);
                }
            }
            global_idx_offset += cmd_list->IdxBuffer.Size;
            global_vtx_offset += cmd_list->VtxBuffer.Size;
        }
    }

    return false;
}

void DearImguiScene::on_resize(glm::ivec2 scene_size, glm::vec2& offset, glm::vec2& extent) {
    SceneImpl::on_resize(scene_size, offset, extent);
    ImGuiIO& io = ImGui::GetIO();
    io.DisplaySize = ImVec2(scene_size.x, scene_size.y);
    auto& window = get_root().get_window();
    auto window_size = window.get_size();
    auto framebuffer_size = window.get_framebuffer_size();
    auto scale = framebuffer_size / window_size;
    io.DisplayFramebufferScale = ImVec2(scale.x, scale.y);
}
void DearImguiScene::on_focus(bool focused) {
    ImGuiIO& io = ImGui::GetIO();
    io.AddFocusEvent(focused);
}
bool DearImguiScene::on_key_press(satr::nui::Window::Keys key, [[maybe_unused]] int scancode,
                                  satr::nui::Window::ButtonState action,
                                  [[maybe_unused]] satr::nui::Window::Modifiers mods) {
    if (action == satr::nui::Window::ButtonState::Repeat) {
        return false;
    }
    ImGuiIO& io = ImGui::GetIO();
    ImGuiKey imgui_key = __key_to_imgui(key);
    bool down = action == satr::nui::Window::ButtonState::Press;
    io.AddKeyEvent(imgui_key, down);
    return io.WantCaptureKeyboard;
}
bool DearImguiScene::on_text_input(const std::string& text) {
    ImGuiIO& io = ImGui::GetIO();
    if (io.WantTextInput) {
        io.AddInputCharactersUTF8(text.c_str());
        return true;
    }
    return false;
}

bool DearImguiScene::on_mouse_press(satr::nui::Window::MouseButtons button,
                                    satr::nui::Window::ButtonState action,
                                    [[maybe_unused]] satr::nui::Window::Modifiers mods) {
    if (action == satr::nui::Window::ButtonState::Repeat) {
        return false;
    }
    ImGuiIO& io = ImGui::GetIO();
    bool down = action == satr::nui::Window::ButtonState::Press;
    ImGuiMouseButton imgui_button = -1;
    switch (button) {
        case nui::Window::ButtonLeft: {
            imgui_button = 0;
        } break;
        case nui::Window::ButtonRight: {
            imgui_button = 1;
        } break;
        case nui::Window::ButtonMiddle: {
            imgui_button = 2;
        } break;
        default: { /*NOP*/
        } break;
    }
    if (imgui_button != -1) {
        io.AddMouseButtonEvent(imgui_button, down);
    }
    return false;
}
bool DearImguiScene::on_mouse_scroll(glm::dvec2 offset) {
    ImGuiIO& io = ImGui::GetIO();
    io.AddMouseWheelEvent((float) offset.x, (float) offset.y);
    return false;
}
bool DearImguiScene::on_mouse_move(glm::dvec2 pos) {
    ImGuiIO& io = ImGui::GetIO();
    io.AddMousePosEvent((float) pos.x, (float) pos.y);
    return io.WantCaptureMouse;
}

ImGuiKey DearImguiScene::__key_to_imgui(satr::nui::Window::Keys key) {
    switch (key) {
        case nui::Window::Space: {
            return ImGuiKey_Space;
        }
        case nui::Window::Apostrophe: {
            return ImGuiKey_Apostrophe;
        }
        case nui::Window::Comma: {
            return ImGuiKey_Comma;
        }
        case nui::Window::Minus: {
            return ImGuiKey_Minus;
        }
        case nui::Window::Period: {
            return ImGuiKey_Period;
        }
        case nui::Window::Slash: {
            return ImGuiKey_Slash;
        }
        case nui::Window::_0: {
            return ImGuiKey_0;
        }
        case nui::Window::_1: {
            return ImGuiKey_1;
        }
        case nui::Window::_2: {
            return ImGuiKey_2;
        }
        case nui::Window::_3: {
            return ImGuiKey_3;
        }
        case nui::Window::_4: {
            return ImGuiKey_4;
        }
        case nui::Window::_5: {
            return ImGuiKey_5;
        }
        case nui::Window::_6: {
            return ImGuiKey_6;
        }
        case nui::Window::_7: {
            return ImGuiKey_7;
        }
        case nui::Window::_8: {
            return ImGuiKey_8;
        }
        case nui::Window::_9: {
            return ImGuiKey_9;
        }
        case nui::Window::Semicolon: {
            return ImGuiKey_Semicolon;
        }
        case nui::Window::Equal: {
            return ImGuiKey_Equal;
        }
        case nui::Window::A: {
            return ImGuiKey_A;
        }
        case nui::Window::B: {
            return ImGuiKey_B;
        }
        case nui::Window::C: {
            return ImGuiKey_C;
        }
        case nui::Window::D: {
            return ImGuiKey_D;
        }
        case nui::Window::E: {
            return ImGuiKey_E;
        }
        case nui::Window::F: {
            return ImGuiKey_F;
        }
        case nui::Window::G: {
            return ImGuiKey_G;
        }
        case nui::Window::H: {
            return ImGuiKey_H;
        }
        case nui::Window::I: {
            return ImGuiKey_I;
        }
        case nui::Window::J: {
            return ImGuiKey_J;
        }
        case nui::Window::K: {
            return ImGuiKey_K;
        }
        case nui::Window::L: {
            return ImGuiKey_L;
        }
        case nui::Window::M: {
            return ImGuiKey_M;
        }
        case nui::Window::N: {
            return ImGuiKey_N;
        }
        case nui::Window::O: {
            return ImGuiKey_O;
        }
        case nui::Window::P: {
            return ImGuiKey_P;
        }
        case nui::Window::Q: {
            return ImGuiKey_Q;
        }
        case nui::Window::R: {
            return ImGuiKey_R;
        }
        case nui::Window::S: {
            return ImGuiKey_S;
        }
        case nui::Window::T: {
            return ImGuiKey_T;
        }
        case nui::Window::U: {
            return ImGuiKey_U;
        }
        case nui::Window::V: {
            return ImGuiKey_V;
        }
        case nui::Window::W: {
            return ImGuiKey_W;
        }
        case nui::Window::X: {
            return ImGuiKey_X;
        }
        case nui::Window::Y: {
            return ImGuiKey_Y;
        }
        case nui::Window::Z: {
            return ImGuiKey_Z;
        }
        case nui::Window::LeftBracket: {
            return ImGuiKey_LeftBracket;
        }
        case nui::Window::Backslash: {
            return ImGuiKey_Backslash;
        }
        case nui::Window::RightBracket: {
            return ImGuiKey_RightBracket;
        }
        case nui::Window::GraveAccent: {
            return ImGuiKey_GraveAccent;
        }
        case nui::Window::Escape: {
            return ImGuiKey_Escape;
        }
        case nui::Window::Enter: {
            return ImGuiKey_Enter;
        }
        case nui::Window::Tab: {
            return ImGuiKey_Tab;
        }
        case nui::Window::Backspace: {
            return ImGuiKey_Backspace;
        }
        case nui::Window::Insert: {
            return ImGuiKey_Insert;
        }
        case nui::Window::Delete: {
            return ImGuiKey_Delete;
        }
        case nui::Window::Right: {
            return ImGuiKey_RightArrow;
        }
        case nui::Window::Left: {
            return ImGuiKey_LeftArrow;
        }
        case nui::Window::Down: {
            return ImGuiKey_DownArrow;
        }
        case nui::Window::Up: {
            return ImGuiKey_UpArrow;
        }
        case nui::Window::PageUp: {
            return ImGuiKey_PageUp;
        }
        case nui::Window::PageDown: {
            return ImGuiKey_PageDown;
        }
        case nui::Window::Home: {
            return ImGuiKey_Home;
        }
        case nui::Window::End: {
            return ImGuiKey_End;
        }
        case nui::Window::CapsLock: {
            return ImGuiKey_CapsLock;
        }
        case nui::Window::ScrollLock: {
            return ImGuiKey_ScrollLock;
        }
        case nui::Window::NumLock: {
            return ImGuiKey_NumLock;
        }
        case nui::Window::PrintScreen: {
            return ImGuiKey_PrintScreen;
        }
        case nui::Window::Pause: {
            return ImGuiKey_Pause;
        }
        case nui::Window::F1: {
            return ImGuiKey_F1;
        }
        case nui::Window::F2: {
            return ImGuiKey_F2;
        }
        case nui::Window::F3: {
            return ImGuiKey_F3;
        }
        case nui::Window::F4: {
            return ImGuiKey_F4;
        }
        case nui::Window::F5: {
            return ImGuiKey_F5;
        }
        case nui::Window::F6: {
            return ImGuiKey_F6;
        }
        case nui::Window::F7: {
            return ImGuiKey_F7;
        }
        case nui::Window::F8: {
            return ImGuiKey_F8;
        }
        case nui::Window::F9: {
            return ImGuiKey_F9;
        }
        case nui::Window::F10: {
            return ImGuiKey_F10;
        }
        case nui::Window::F11: {
            return ImGuiKey_F11;
        }
        case nui::Window::F12: {
            return ImGuiKey_F12;
        }
        case nui::Window::F13: {
            return ImGuiKey_F13;
        }
        case nui::Window::F14: {
            return ImGuiKey_F14;
        }
        case nui::Window::F15: {
            return ImGuiKey_F15;
        }
        case nui::Window::F16: {
            return ImGuiKey_F16;
        }
        case nui::Window::F17: {
            return ImGuiKey_F17;
        }
        case nui::Window::F18: {
            return ImGuiKey_F18;
        }
        case nui::Window::F19: {
            return ImGuiKey_F19;
        }
        case nui::Window::F20: {
            return ImGuiKey_F20;
        }
        case nui::Window::F21: {
            return ImGuiKey_F21;
        }
        case nui::Window::F22: {
            return ImGuiKey_F22;
        }
        case nui::Window::F23: {
            return ImGuiKey_F23;
        }
        case nui::Window::F24: {
            return ImGuiKey_F24;
        }
        case nui::Window::KP0: {
            return ImGuiKey_Keypad0;
        }
        case nui::Window::KP1: {
            return ImGuiKey_Keypad1;
        }
        case nui::Window::KP2: {
            return ImGuiKey_Keypad2;
        }
        case nui::Window::KP3: {
            return ImGuiKey_Keypad3;
        }
        case nui::Window::KP4: {
            return ImGuiKey_Keypad4;
        }
        case nui::Window::KP5: {
            return ImGuiKey_Keypad5;
        }
        case nui::Window::KP6: {
            return ImGuiKey_Keypad6;
        }
        case nui::Window::KP7: {
            return ImGuiKey_Keypad7;
        }
        case nui::Window::KP8: {
            return ImGuiKey_Keypad8;
        }
        case nui::Window::KP9: {
            return ImGuiKey_Keypad9;
        }
        case nui::Window::KPDecimal: {
            return ImGuiKey_KeypadDecimal;
        }
        case nui::Window::KPDivide: {
            return ImGuiKey_KeypadDivide;
        }
        case nui::Window::KPMultiply: {
            return ImGuiKey_KeypadMultiply;
        }
        case nui::Window::KPSubtract: {
            return ImGuiKey_KeypadSubtract;
        }
        case nui::Window::KPAdd: {
            return ImGuiKey_KeypadAdd;
        }
        case nui::Window::KPEnter: {
            return ImGuiKey_KeypadEnter;
        }
        case nui::Window::KPEqual: {
            return ImGuiKey_KeypadEqual;
        }
        case nui::Window::LeftShift: {
            return ImGuiKey_LeftShift;
        }
        case nui::Window::RightShift: {
            return ImGuiKey_RightShift;
        }
        case nui::Window::LeftControl: {
            return ImGuiKey_LeftCtrl;
        }
        case nui::Window::RightControl: {
            return ImGuiKey_RightCtrl;
        }
        case nui::Window::LeftAlt: {
            return ImGuiKey_LeftAlt;
        }
        case nui::Window::RightAlt: {
            return ImGuiKey_RightAlt;
        }
        case nui::Window::LeftSuper: {
            return ImGuiKey_LeftSuper;
        }
        case nui::Window::RightSuper: {
            return ImGuiKey_RightSuper;
        }
        case nui::Window::Menu: {
            return ImGuiKey_Menu;
        }

        case nui::Window::Unknown:
        default: {
            return ImGuiKey_None;
        }
    }
}

}  // namespace satr::vk
