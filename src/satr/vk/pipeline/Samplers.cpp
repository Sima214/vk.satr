#include "Samplers.hpp"

#include <Context.hpp>
#include <logger/Logger.hpp>
#include <vulkan/vulkan_to_string.hpp>

#include <utility>

namespace satr::vk {

SamplerImpl::SamplerImpl(::vk::Device device, ::vk::Sampler handle) :
    _handle(handle), _device(device) {}
SamplerImpl::SamplerImpl(SamplerImpl&& o) noexcept :
    _handle(std::exchange(o._handle, nullptr)), _device(o._device) {}
SamplerImpl& SamplerImpl::operator=(SamplerImpl&& o) noexcept {
    _handle = std::exchange(o._handle, nullptr);
    _device = o._device;
    return *this;
}
SamplerImpl::~SamplerImpl() {
    if (_handle) {
        _device.destroy(_handle);
        _handle = nullptr;
    }
}

SamplerImpl SamplerImpl::create(Context& context, const VkSamplerCreateInfo& cinfo) {
    ::vk::Device dev = context.get_active_device();
    auto [r, h] = dev.createSampler(cinfo);
    if (r != ::vk::Result::eSuccess) {
        logger.loge("Sampler::create: error(", to_string(r), ")!");
        return SamplerImpl();
    }
    return SamplerImpl(dev, h);
}

void SamplerImpl::update_descriptor(::vk::DescriptorSet set, uint32_t binding) const {
    ::vk::DescriptorImageInfo sampler_info(get_handle());
    ::vk::WriteDescriptorSet write_info(set, binding, 0, 1, ::vk::DescriptorType::eSampler);
    write_info.pImageInfo = &sampler_info;
    _device.updateDescriptorSets(1, &write_info, 0, nullptr);
}
void SamplerImpl::update_combined_descriptor(::vk::DescriptorSet set, uint32_t binding,
                                             ::vk::ImageView img,
                                             ::vk::ImageLayout img_layout) const {
    ::vk::DescriptorImageInfo img_info(get_handle(), img, img_layout);
    ::vk::WriteDescriptorSet write_info(set, binding, 0, 1,
                                        ::vk::DescriptorType::eCombinedImageSampler);
    write_info.pImageInfo = &img_info;
    _device.updateDescriptorSets(1, &write_info, 0, nullptr);
}

}  // namespace satr::vk
