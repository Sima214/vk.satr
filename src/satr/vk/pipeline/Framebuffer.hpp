#ifndef SATR_VK_FRAMEBUFFER_HPP
#define SATR_VK_FRAMEBUFFER_HPP
/**
 * @file
 * @brief FramebufferChains with integrated renderpasses,
 * with the ability to be created from either
 * heap allocated or swapchain images.
 */

#include <Context.hpp>
#include <Formats.hpp>
#include <Image.hpp>
#include <Utils.hpp>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <vulkan/vulkan.hpp>
#include <vulkan/vulkan_handles.hpp>

#include <cstdint>
#include <limits>

namespace satr::vk {

class FramebufferChain : public spec::INonCopyable {
   protected:
    size_t _chain_size = -1UL;

    Context* _context = nullptr;
    std::string _name;

    ::vk::RenderPass _renderpass = nullptr;
    ::vk::Framebuffer* _framebuffers = nullptr;

    size_t _color_images_count = 0;
    /**
     * Indexed with [chain_index * color_attachment_count + color_attachment_index].
     */
    Image2D* _color_images = nullptr;
    Image2D::View* _color_image_views = nullptr;

    size_t _depth_images_count = 0;
    Image2D* _depth_images = nullptr;
    Image2D::View* _depth_image_views = nullptr;

    ::vk::ClearValue* _clear_values = nullptr;

   public:
    /** Constructors */
    constexpr FramebufferChain() noexcept : _renderpass(nullptr) {}
    FramebufferChain(FramebufferChain&&) noexcept;
    FramebufferChain& operator=(FramebufferChain&&) noexcept;
    ~FramebufferChain();

    // From swapchain images.
    FramebufferChain(Context& context, std::span<const Image2D> swapchain,
                     bool use_depth_attachment = false) :
        FramebufferChain(context, swapchain.size(), swapchain.data(), use_depth_attachment) {}
    FramebufferChain(Context& context, size_t chain_size, const Image2D* swapchain_images,
                     bool use_depth_attachment = false);

    // Heap allocated - single render subpass.
    FramebufferChain(const std::string& name, Context& context, glm::uvec2 size,
                     size_t chain_size, size_t color_attachment_count,
                     const Format* color_attachment_format, bool use_depth_attachment);
    FramebufferChain(const char* name, Context& context, glm::uvec2 size, size_t chain_size,
                     size_t color_attachment_count, const Format* color_attachment_format,
                     bool use_depth_attachment) :
        FramebufferChain(std::string(name), context, size, chain_size, color_attachment_count,
                         color_attachment_format, use_depth_attachment) {}

   protected:
    /** Initialization steps */
    bool _initialize_color_attachments(glm::uvec2 size, size_t color_attachment_count,
                                       const Format* color_attachment_format,
                                       size_t chain_offset = 0,
                                       size_t chain_count = std::numeric_limits<size_t>::max());
    bool _initialize_depth_attachments(glm::uvec2 size, size_t chain_offset = 0,
                                       size_t chain_count = std::numeric_limits<size_t>::max());
    bool _initialize_attachment_views(size_t color_images_count, const Image2D* color_images,
                                      size_t depth_images_count, const Image2D* depth_images);
    bool _initialize_attachment_views_partial(size_t chain_index);
    bool _initialize_renderpass(bool presentation_framebuffer);
    bool _initialize_clear_defaults();
    bool _initialize_framebuffers(glm::uvec2 size, size_t chain_offset = 0,
                                  size_t chain_count = std::numeric_limits<size_t>::max());

   public:
    operator bool() const {
        return static_cast<bool>(_renderpass);
    }

    /** Getters/Setters */
    ::vk::Device get_device() const;
    size_t get_chain_size() const;
    size_t get_colors_attachment_count() const;
    bool has_depth_attachment() const;
    ::vk::RenderPass get_renderpass() const;
    ::vk::Framebuffer get_handle(size_t chain_index) const;
    const std::string& get_name() const;

    glm::uvec2 get_size(size_t chain_index) const;
    ::vk::Format get_format(uint32_t attachment_index) const;
    ::vk::Format get_depth_format() const;

    std::span<Image2D> get_color_attachments(size_t chain_index) const;
    std::span<Image2D::View> get_color_attachment_views(size_t chain_index) const;
    Image2D::View get_depth_attachment(size_t chain_index) const;

    bool set_color_clear_value(uint32_t attachment_index, glm::vec4 rgba);
    bool set_depth_clear_value(float v);

    /** Operations */
    void start_pass(::vk::CommandBuffer cmds, size_t chain_index);
    void clear(::vk::CommandBuffer cmds);
    void end_pass(::vk::CommandBuffer cmds);

    /** Events - swapchain allocated */
    void on_swapchain_update(std::span<const Image2D> swapchain) {
        return on_swapchain_update(swapchain.size(), swapchain.data());
    }
    void on_swapchain_update(size_t chain_size, const Image2D* swapchain_images);
    /**
     * Releases any references held, which are associated with the swapchain.
     * Usefull also when recreating the swapchain, as it allows resource re-use by the driver.
     */
    void on_swapchain_destroy();

    /** Events - heap allocated */
    void on_resize(size_t chain_index, glm::uvec2 new_size);
};

/**
 * @tparam CS chain size
 * @tparam CN color attachment count
 * @tparam D depth attachment
 */
/*template<size_t CS, size_t CN, bool D> class StaticFramebufferChain : public FramebufferChain {
   protected:
   public:
};*/

}  // namespace satr::vk

#endif /*SATR_VK_FRAMEBUFFER_HPP*/