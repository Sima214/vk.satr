#ifndef SATR_VK_SAMPLERS_HPP
#define SATR_VK_SAMPLERS_HPP
/**
 * @file
 * @brief
 */

#include <Image.hpp>
#include <Utils.hpp>
#include <vulkan/vulkan.hpp>

#include <memory>

namespace satr::vk {

/* Fwd decls */
class Context;

class SamplerImpl;
class Sampler;
using SamplerWeak = std::weak_ptr<SamplerImpl>;

// TODO: Track & verify limits and global registry optimization.
class SamplerImpl : public spec::INonCopyable {
   protected:
    ::vk::Sampler _handle;
    ::vk::Device _device;

   public:
    constexpr SamplerImpl() noexcept : _handle(nullptr) {}
    SamplerImpl(SamplerImpl&&) noexcept;
    SamplerImpl& operator=(SamplerImpl&&) noexcept;
    ~SamplerImpl();

   protected:
    SamplerImpl(::vk::Device device, ::vk::Sampler handle);

    static SamplerImpl create(Context& context, const VkSamplerCreateInfo& cinfo);

    friend class Sampler;

   public:
    operator bool() const {
        return static_cast<bool>(_handle);
    }

    /* Getters */
    ::vk::Sampler get_handle() const {
        return _handle;
    }

    /* Operations */
    void update_descriptor(::vk::DescriptorSet set, uint32_t binding) const;
    void update_combined_descriptor(::vk::DescriptorSet set, uint32_t binding,
                                    ::vk::ImageView img, ::vk::ImageLayout img_layout) const;
};

class Sampler : public std::shared_ptr<SamplerImpl> {
   public:
    // For convenience:
    using Weak = SamplerWeak;

   protected:
    // Factory methods.
    Sampler(std::shared_ptr<SamplerImpl>&& o) noexcept : std::shared_ptr<SamplerImpl>(o) {}

    template<typename... Args> static Sampler make_shared(Args&&... args) {
        auto obj = std::make_shared_for_overwrite<SamplerImpl>();
        SamplerImpl impl_obj = SamplerImpl::create(std::forward<Args>(args)...);
        if (impl_obj) {
            *obj.get() = std::move(impl_obj);
        }
        return obj;
    }

    friend class Context;

   public:
    // inherit shared_ptr's constructors
    constexpr Sampler() noexcept : std::shared_ptr<SamplerImpl>() {}
    constexpr Sampler(std::nullptr_t _) noexcept : std::shared_ptr<SamplerImpl>(_) {}
    Sampler(const Sampler& r) noexcept = default;
    Sampler(Sampler&& r) noexcept = default;

    Sampler& operator=(const Sampler& r) noexcept = default;
    Sampler& operator=(Sampler&& r) noexcept = default;

    // Forward Impl's methods.
    operator bool() const {
        return static_cast<bool>((std::shared_ptr<SamplerImpl>) *this) &&
               static_cast<bool>((SamplerImpl&) **this);
    }
    /** Getters */
    const ::vk::Sampler get_handle() const {
        return (*this)->get_handle();
    }

    /* Operations */
    void update_descriptor(::vk::DescriptorSet set, uint32_t binding) const {
        return (*this)->update_descriptor(set, binding);
    }
    void update_combined_descriptor(::vk::DescriptorSet set, uint32_t binding,
                                    ::vk::ImageView img, ::vk::ImageLayout img_layout) const {
        return (*this)->update_combined_descriptor(set, binding, img, img_layout);
    }
    template<::vk::ImageType type>
    void update_combined_descriptor(
         ::vk::DescriptorSet set, uint32_t binding, Image<type>::View img,
         ::vk::ImageLayout img_layout = ::vk::ImageLayout::eShaderReadOnlyOptimal) const {
        return (*this)->update_combined_descriptor(set, binding, img.get_handle(), img_layout);
    }
};

}  // namespace satr::vk

#endif /*SATR_VK_SAMPLERS_HPP*/