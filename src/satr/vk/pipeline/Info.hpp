#ifndef SATR_VK_INFO_HPP
#define SATR_VK_INFO_HPP
/**
 * @file
 * @brief Info structures used for Module/Stage/Pipeline creation.
 */

#include <Formats.hpp>
#include <Utils.hpp>
#include <glm/vec4.hpp>
#include <meta/Attr.hpp>
#include <vulkan/vulkan.hpp>

#include <cstdint>
#include <cstdlib>
#include <filesystem>
#include <libconfig.h++>
#include <optional>
#include <span>
#include <string>

namespace satr::vk {

/** Fw decls */
class Module;
class FramebufferChain;
class Descriptors;
class PushConstants;
class VertexAttributes;

class StageConfig {
   protected:
    /** VK_EXT_subgroup_size_control or Core 1.3 */
    bool _allow_varying_subgroup_size = false;
    bool _require_full_subgroups = false;

    /**
     * spv_reflect doesn't officially support querying specialization entries,
     * so just store hardcoded numerical ids.
     */
    std::vector<::vk::SpecializationMapEntry> _specialization_entries;
    std::vector<uint8_t> _specialization_buffer;

    /**
     * Storage for get_partial_create_info
     */
    ::vk::SpecializationInfo _cinfo_specialization;

   public:
    StageConfig() = default;  // Set all to default.

    StageConfig(const StageConfig&) = default;
    StageConfig(StageConfig&&) = default;
    StageConfig& operator=(const StageConfig&) = default;
    StageConfig& operator=(StageConfig&&) = default;

    /**
     * Parse options from the provided string \p buffer.
     */
    bool load(const std::string& buffer);
    /**
     * Load options by parsing the file at \p path.
     */
    bool load(const std::filesystem::path& path);

    /** Getters & Setters */
    ::vk::PipelineShaderStageCreateInfo get_partial_create_info();

    DEFINE_AUTO_PROPERTY(allow_varying_subgroup_size);
    DEFINE_AUTO_PROPERTY(require_full_subgroups);

    bool has_specialization_entry(uint32_t constant_id) const;
    /**
     * If not found, returns as SpecializationMapEntry with
     * constant_id = UINT32_MAX and an empty span.
     */
    std::pair<::vk::SpecializationMapEntry, std::span<const uint8_t>> get_specialization_entry(
         uint32_t constant_id) const;
    template<typename T> std::optional<T> get_specialization_entry(uint32_t constant_id) const;

    /**
     * If there is already a map entry with the specified constant_id,
     * but the data sizes don't match, then a runtime_error is thrown.
     */
    void set_specialization_entry(uint32_t constant_id, std::span<const uint8_t> data);
    template<typename T> void set_specialization_entry(uint32_t constant_id, T data) {
        std::span<const uint8_t> raw_data((uint8_t*) &data, sizeof(T));
        set_specialization_entry(constant_id, raw_data);
    }

    void specialization_clear();

   protected:
    bool parse(const libconfig::Config& cfg);
};

/** Common pipeline creation options. */
class PipelineOptions {
   protected:
    /** Core 1.0 */
    bool _disable_optimization = false;
    bool _allow_derivatives = false;
    bool _derivative = false;
    /** Core 1.1 */
    bool _view_index_from_device_index = false;
    bool _dispatch_base = false;
    /** Core 1.3 */
    bool _fail_on_pipeline_compile_required = false;
    bool _early_return_on_failure = false;
    /** VK_KHR_pipeline_executable_properties */
    bool _capture_statistics = false;
    bool _capture_internal_representations = false;
    /** VK_KHR_pipeline_library */
    bool _library = false;
    /** VK_KHR_dynamic_rendering */
    bool _rendering_fragment_shading_rate_attachment = false;
    bool _rendering_fragment_density_map_attachment = false;
    /** VK_EXT_graphics_pipeline_library */
    bool _link_time_optimization = false;
    bool _retain_link_time_optimization_info = false;
    /** VK_EXT_descriptor_buffer */
    bool _descriptor_buffer = false;
    bool _color_attachment_feedback_loop = false;
    bool _depth_stencil_attachment_feedback_loop = false;
    /** VK_EXT_pipeline_protected_access */
    bool _no_protected_access = false;
    bool _protected_access_only = false;
    /** VK_KHR_ray_tracing_pipeline */
    bool _ray_tracing_no_null_any_hit_shaders = false;
    bool _ray_tracing_no_null_closest_hit_shaders = false;
    bool _ray_tracing_no_null_miss_shaders = false;
    bool _ray_tracing_no_null_intersection_shaders = false;
    bool _ray_tracing_skip_triangles = false;
    bool _ray_tracing_skip_aabbs = false;
    bool _ray_tracing_shader_group_handle_capture_replay = false;
    /** VK_EXT_opacity_micromap */
    bool _ray_tracing_opacity_micromap = false;
    /** VK_NV_displacement_micromap(TODO: Beta) */
    // bool _ray_tracing_displacement_micromap = false;

    bool _layout_create_independent_sets = false;

   public:
    constexpr PipelineOptions() = default;

    /** Getters & Setters */
    ::vk::PipelineCreateFlags get_create_info() const;

    ::vk::PipelineLayout make_pipeline_layout(::vk::Device device, Descriptors& descriptors,
                                              const PushConstants& push_consts) const;

    DEFINE_AUTO_PROPERTY(disable_optimization);
    DEFINE_AUTO_PROPERTY(allow_derivatives);
    DEFINE_AUTO_PROPERTY(derivative);
    DEFINE_AUTO_PROPERTY(view_index_from_device_index);
    DEFINE_AUTO_PROPERTY(dispatch_base);
    DEFINE_AUTO_PROPERTY(fail_on_pipeline_compile_required);
    DEFINE_AUTO_PROPERTY(early_return_on_failure);
    DEFINE_AUTO_PROPERTY(capture_statistics);
    DEFINE_AUTO_PROPERTY(capture_internal_representations);
    DEFINE_AUTO_PROPERTY(library);
    DEFINE_AUTO_PROPERTY(rendering_fragment_shading_rate_attachment);
    DEFINE_AUTO_PROPERTY(rendering_fragment_density_map_attachment);
    DEFINE_AUTO_PROPERTY(link_time_optimization);
    DEFINE_AUTO_PROPERTY(retain_link_time_optimization_info);
    DEFINE_AUTO_PROPERTY(descriptor_buffer);
    DEFINE_AUTO_PROPERTY(color_attachment_feedback_loop);
    DEFINE_AUTO_PROPERTY(depth_stencil_attachment_feedback_loop);
    DEFINE_AUTO_PROPERTY(no_protected_access);
    DEFINE_AUTO_PROPERTY(protected_access_only);
    DEFINE_AUTO_PROPERTY(ray_tracing_no_null_any_hit_shaders);
    DEFINE_AUTO_PROPERTY(ray_tracing_no_null_closest_hit_shaders);
    DEFINE_AUTO_PROPERTY(ray_tracing_no_null_miss_shaders);
    DEFINE_AUTO_PROPERTY(ray_tracing_no_null_intersection_shaders);
    DEFINE_AUTO_PROPERTY(ray_tracing_skip_triangles);
    DEFINE_AUTO_PROPERTY(ray_tracing_skip_aabbs);
    DEFINE_AUTO_PROPERTY(ray_tracing_shader_group_handle_capture_replay);
    DEFINE_AUTO_PROPERTY(ray_tracing_opacity_micromap);

    DEFINE_AUTO_PROPERTY(layout_create_independent_sets);

   protected:
    bool parse(const libconfig::Config& cfg);
};

/** Graphics pipeline creation options. */
class GraphicsPipelineOptions : public PipelineOptions {
   public:
    /** NOTE: Stored data gets invalidated when downcast! */
    class DynamicGraphicsPipelineCreateInfo : public ::vk::GraphicsPipelineCreateInfo,
                                              public spec::INonCopyable {
       protected:
        std::vector<::vk::PipelineShaderStageCreateInfo> _shader_stages_cinfo;
        std::vector<::vk::VertexInputAttributeDescription> _vertex_input_attributes;
        ::vk::PipelineVertexInputStateCreateInfo _vertex_input_state_cinfo;
        ::vk::PipelineInputAssemblyStateCreateInfo _input_assembly_state_cinfo;
        ::vk::PipelineTessellationStateCreateInfo _tessellation_state_cinfo;
        ::vk::PipelineViewportStateCreateInfo _viewport_state_cinfo;
        ::vk::PipelineRasterizationStateCreateInfo _rasterizer_state_cinfo;
        ::vk::PipelineMultisampleStateCreateInfo _multisample_state_cinfo;
        ::vk::PipelineDepthStencilStateCreateInfo _depth_stencil_state_cinfo;
        std::vector<::vk::PipelineColorBlendAttachmentState> _color_blend_states;
        ::vk::PipelineColorBlendStateCreateInfo _color_blend_state_cinfo;
        ::vk::PipelineDynamicStateCreateInfo _dynamic_state_cinfo;

       public:
        DynamicGraphicsPipelineCreateInfo() noexcept;
        DynamicGraphicsPipelineCreateInfo(DynamicGraphicsPipelineCreateInfo&&) noexcept;
        DynamicGraphicsPipelineCreateInfo& operator=(
             DynamicGraphicsPipelineCreateInfo&&) noexcept;
        ~DynamicGraphicsPipelineCreateInfo();

        std::vector<::vk::PipelineShaderStageCreateInfo>& shader_stages() {
            return _shader_stages_cinfo;
        }
        std::vector<::vk::VertexInputAttributeDescription>& vertex_input_attributes() {
            return _vertex_input_attributes;
        }
        ::vk::PipelineVertexInputStateCreateInfo& vertex_input_state() {
            return _vertex_input_state_cinfo;
        };
        ::vk::PipelineInputAssemblyStateCreateInfo& input_assembly_state() {
            return _input_assembly_state_cinfo;
        }
        ::vk::PipelineTessellationStateCreateInfo& tessellation_state() {
            return _tessellation_state_cinfo;
        }
        ::vk::PipelineViewportStateCreateInfo& viewport_state() {
            return _viewport_state_cinfo;
        }
        ::vk::PipelineRasterizationStateCreateInfo& rasterizer_state() {
            return _rasterizer_state_cinfo;
        }
        ::vk::PipelineMultisampleStateCreateInfo& multisample_state() {
            return _multisample_state_cinfo;
        }
        ::vk::PipelineDepthStencilStateCreateInfo& depth_stencil_state() {
            return _depth_stencil_state_cinfo;
        };
        std::vector<::vk::PipelineColorBlendAttachmentState>& color_blend_states() {
            return _color_blend_states;
        }
        ::vk::PipelineColorBlendStateCreateInfo& color_blend_state() {
            return _color_blend_state_cinfo;
        }
        ::vk::PipelineDynamicStateCreateInfo& dynamic_state() {
            return _dynamic_state_cinfo;
        }

       private:
        void __update_pointers();
    };

    struct VertexInputAttributeFinder {
        std::string name;
        uint32_t binding;
        uint32_t offset;
        ::vk::Format fmt_override = ::vk::Format::eUndefined;
    };

   protected:
    std::vector<::vk::DynamicState> _dynamic_states;

    std::vector<::vk::VertexInputBindingDescription> _vertex_input_bindings;
    std::vector<VertexInputAttributeFinder> _vertex_input_attributes;

    ::vk::PrimitiveTopology _primitive_topology = ::vk::PrimitiveTopology::eTriangleList;
    bool _primitive_restart_enable = false;

    uint32_t _tessellation_patch_control_points = 0;

    // TODO: multi-viewport for vr.
    /** Combined viewport, scissor state */
    std::pair<::vk::Viewport, ::vk::Rect2D> _viewport_state;

    bool _rasterizer_depth_clamp_enable = false;
    bool _rasterizer_discard_enable = false;
    ::vk::PolygonMode _rasterizer_polygon_mode = ::vk::PolygonMode::eFill;
    ::vk::CullModeFlags _rasterizer_cull_mode = ::vk::CullModeFlagBits::eNone;
    ::vk::FrontFace _rasterizer_front_face = ::vk::FrontFace::eCounterClockwise;
    bool _rasterizer_depth_bias_enable = false;
    float _rasterizer_depth_bias_constant_factor = 0.0f;
    float _rasterizer_depth_bias_clamp = 0.0f;
    float _rasterizer_depth_bias_slope_factor = 1.0f;
    float _rasterizer_line_width = 1.0f;

    ::vk::SampleCountFlagBits _multisample_rasterization_samples =
         ::vk::SampleCountFlagBits::e1;
    bool _multisample_sample_shading_enable = false;
    float _multisample_min_sample_shading = 1.0f;
    // TODO: std::vector<VkSampleMask> _multisample_sample_mask;
    bool _multisample_alpha_to_coverage_enable = false;
    bool _multisample_alpha_to_one_enable = false;

    // TODO: Stencil not supported.
    bool _depth_rasterization_order_attachment_access = false;
    // Depth implicitly enabled based on framebuffer configuration.
    bool _depth_test_disabled = false;
    bool _depth_write_disabled = false;
    ::vk::CompareOp _depth_compare_op = ::vk::CompareOp::eLess;
    bool _depth_bounds_test_enable = false;
    float _depth_min_bounds = 0.0f;
    float _depth_max_bounds = 1.0f;

    bool _color_blend_rasterization_order_attachment_access = false;
    bool _color_blend_logic_op_enable = false;
    ::vk::LogicOp _color_blend_logic_op = ::vk::LogicOp::eCopy;
    glm::vec4 _color_blend_constants = {0.0f, 0.0f, 0.0f, 0.0f};
    // TODO: attachment independent blend state.
    bool _color_blend_enable = false;
    ::vk::BlendFactor _color_blend_src_factor = ::vk::BlendFactor::eOne;
    ::vk::BlendFactor _color_blend_dst_factor = ::vk::BlendFactor::eZero;
    ::vk::BlendOp _color_blend_op = ::vk::BlendOp::eAdd;
    ::vk::BlendFactor _alpha_blend_src_factor = ::vk::BlendFactor::eOne;
    ::vk::BlendFactor _alpha_blend_dst_factor = ::vk::BlendFactor::eZero;
    ::vk::BlendOp _alpha_blend_op = ::vk::BlendOp::eAdd;
    ::vk::ColorComponentFlags _color_write_mask =
         ::vk::ColorComponentFlagBits::eR | ::vk::ColorComponentFlagBits::eG |
         ::vk::ColorComponentFlagBits::eB | ::vk::ColorComponentFlagBits::eA;

   public:
    constexpr GraphicsPipelineOptions() = default;
    /**
     * Parse options from the provided string \p buffer.
     */
    bool load(const std::string& buffer);
    /**
     * Load options by parsing the file at \p path.
     */
    bool load(const std::filesystem::path& path);

    /** Getters & Setters */
    void dynamic_state_add(::vk::DynamicState v);
    bool dynamic_state_has(::vk::DynamicState v) const;

    bool vertex_input_bindings_add(::vk::VertexInputBindingDescription v);
    bool vertex_input_bindings_add(
         uint32_t binding, uint32_t stride,
         ::vk::VertexInputRate input_rate = ::vk::VertexInputRate::eVertex) {
        ::vk::VertexInputBindingDescription desc(binding, stride, input_rate);
        return vertex_input_bindings_add(desc);
    }
    std::optional<::vk::VertexInputBindingDescription> vertex_input_bindings_find_from_binding(
         uint32_t binding) const;

    bool vertex_input_attributes_add(VertexInputAttributeFinder v);
    bool vertex_input_attributes_add(const std::string& name, uint32_t binding,
                                     uint32_t binding_offset,
                                     ::vk::Format format_override = ::vk::Format::eUndefined) {
        VertexInputAttributeFinder desc = {name, binding, binding_offset, format_override};
        return vertex_input_attributes_add(desc);
    }

    DEFINE_AUTO_PROPERTY(primitive_topology);
    DEFINE_AUTO_PROPERTY(primitive_restart_enable);

    DEFINE_AUTO_PROPERTY(tessellation_patch_control_points);

    bool set_viewport_state(::vk::Viewport v, ::vk::Rect2D s);
    // TODO: glm rect compatibility.
    std::optional<::vk::Viewport> get_viewport() const;
    std::optional<::vk::Rect2D> get_viewport_scissor() const;

    DEFINE_AUTO_PROPERTY(rasterizer_depth_clamp_enable);
    DEFINE_AUTO_PROPERTY(rasterizer_discard_enable);
    DEFINE_AUTO_PROPERTY(rasterizer_polygon_mode);
    DEFINE_AUTO_PROPERTY(rasterizer_cull_mode);
    DEFINE_AUTO_PROPERTY(rasterizer_front_face);
    DEFINE_AUTO_PROPERTY(rasterizer_depth_bias_enable);
    DEFINE_AUTO_PROPERTY(rasterizer_depth_bias_constant_factor);
    DEFINE_AUTO_PROPERTY(rasterizer_depth_bias_clamp);
    DEFINE_AUTO_PROPERTY(rasterizer_depth_bias_slope_factor);
    DEFINE_AUTO_PROPERTY(rasterizer_line_width);

    DEFINE_AUTO_PROPERTY(multisample_rasterization_samples);
    DEFINE_AUTO_PROPERTY(multisample_sample_shading_enable);
    DEFINE_AUTO_PROPERTY(multisample_min_sample_shading);
    DEFINE_AUTO_PROPERTY(multisample_alpha_to_coverage_enable);
    DEFINE_AUTO_PROPERTY(multisample_alpha_to_one_enable);

    DEFINE_AUTO_PROPERTY(depth_rasterization_order_attachment_access);
    DEFINE_AUTO_PROPERTY(depth_compare_op);
    void depth_test_disable() {
        _depth_test_disabled = true;
    }
    void depth_write_disable() {
        _depth_write_disabled = true;
    }
    DEFINE_AUTO_PROPERTY(depth_bounds_test_enable);
    DEFINE_AUTO_PROPERTY(depth_min_bounds);
    DEFINE_AUTO_PROPERTY(depth_max_bounds);

    DEFINE_AUTO_PROPERTY(color_blend_rasterization_order_attachment_access);
    DEFINE_AUTO_PROPERTY(color_blend_logic_op_enable);
    DEFINE_AUTO_PROPERTY(color_blend_logic_op);
    DEFINE_AUTO_PROPERTY(color_blend_constants);

    DEFINE_AUTO_PROPERTY(color_blend_enable);
    DEFINE_AUTO_PROPERTY(color_blend_src_factor);
    DEFINE_AUTO_PROPERTY(color_blend_dst_factor);
    DEFINE_AUTO_PROPERTY(color_blend_op);
    DEFINE_AUTO_PROPERTY(alpha_blend_src_factor);
    DEFINE_AUTO_PROPERTY(alpha_blend_dst_factor);
    DEFINE_AUTO_PROPERTY(alpha_blend_op);
    DEFINE_AUTO_PROPERTY(color_write_mask);

    DynamicGraphicsPipelineCreateInfo make_partial_create_info();
    DynamicGraphicsPipelineCreateInfo make_partial_create_info() const;

    void finalize_create_info(DynamicGraphicsPipelineCreateInfo& cinfo,
                              std::span<Module> shader_modules,
                              VertexAttributes& vertex_attributes,
                              const FramebufferChain& framebuffer,
                              ::vk::PipelineLayout layout) const;

   protected:
    bool parse(const libconfig::Config& cfg);
};

class ComputePipelineOptions : public PipelineOptions {
   protected:
   public:
    constexpr ComputePipelineOptions() = default;

    /** Getters & Setters */

    ::vk::ComputePipelineCreateInfo make_create_info(Module shader_module,
                                                     ::vk::PipelineLayout layout) const;
};

}  // namespace satr::vk

#endif /*SATR_VK_INFO_HPP*/
