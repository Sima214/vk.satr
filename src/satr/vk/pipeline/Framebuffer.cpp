#include "Framebuffer.hpp"

#include <Formats.hpp>
#include <TypeConversions.hpp>
#include <logger/Logger.hpp>
#include <trace/Trace.hpp>
#include <vulkan/vulkan_to_string.hpp>

#include <algorithm>
#include <limits>
#include <memory>
#include <utility>

namespace satr::vk {

FramebufferChain::~FramebufferChain() {
    if (_renderpass) {  // Check for valid object.
        spec::trace("Framebuffer::destroy(", _renderpass, ")");
        ::vk::Device device = get_device();
        // 0. Renderpass.
        device.destroy(_renderpass);
        if (_clear_values) {
            delete[] _clear_values;
        }
        // 1. Framebuffers.
        if (_framebuffers) {
            for (size_t i = 0; i < _chain_size; i++) {
                device.destroy(_framebuffers[i]);
            }
            delete[] _framebuffers;
        }
        // 2. Image views.
        if (_color_image_views) {
            delete[] _color_image_views;
        }
        if (_depth_image_views) {
            delete[] _depth_image_views;
        }
        // 3. Images.
        if (_color_images) {
            delete[] _color_images;
        }
        if (_depth_images) {
            delete[] _depth_images;
        }
        // Invalidate object.
        _renderpass = nullptr;
    }
}

FramebufferChain::FramebufferChain(FramebufferChain&& o) noexcept :
    _chain_size(o._chain_size), _context(o._context), _name(std::move(o._name)),
    _renderpass(std::exchange(o._renderpass, nullptr)), _framebuffers(o._framebuffers),
    _color_images_count(o._color_images_count), _color_images(o._color_images),
    _color_image_views(o._color_image_views), _depth_images_count(o._depth_images_count),
    _depth_images(o._depth_images), _depth_image_views(o._depth_image_views),
    _clear_values(o._clear_values) {
    spec::trace("Framebuffer::construct_move(", _renderpass, ")");
}
FramebufferChain& FramebufferChain::operator=(FramebufferChain&& o) noexcept {
    _chain_size = o._chain_size;
    _context = o._context;
    _name = std::move(o._name);
    _renderpass = std::exchange(o._renderpass, nullptr);
    _framebuffers = o._framebuffers;
    _color_images_count = o._color_images_count;
    _color_images = o._color_images;
    _color_image_views = o._color_image_views;
    _depth_images_count = o._depth_images_count;
    _depth_images = o._depth_images;
    _depth_image_views = o._depth_image_views;
    _clear_values = o._clear_values;
    spec::trace("Framebuffer::assign_move(", _renderpass, ")");
    return *this;
}

::vk::Device FramebufferChain::get_device() const {
    return _context->get_active_device();
}
size_t FramebufferChain::get_chain_size() const {
    return _chain_size;
}
size_t FramebufferChain::get_colors_attachment_count() const {
    return _color_images_count / _chain_size;
}
bool FramebufferChain::has_depth_attachment() const {
    return _depth_images_count != 0;
}
::vk::RenderPass FramebufferChain::get_renderpass() const {
    return _renderpass;
}
::vk::Framebuffer FramebufferChain::get_handle(size_t chain_index) const {
    return _framebuffers[chain_index];
}

bool FramebufferChain::_initialize_color_attachments(glm::uvec2 size,
                                                     size_t color_attachment_count,
                                                     const Format* color_attachment_formats,
                                                     size_t chain_offset, size_t chain_count) {
    size_t chain_end_count;
    if (chain_count == std::numeric_limits<size_t>::max()) {
        chain_end_count = _chain_size;
    }
    else {
        chain_end_count = std::min(chain_offset + chain_count, _chain_size);
    }
    std::string base_name(_name);
    for (size_t cli = chain_offset; cli < chain_end_count; cli++) {
        // Color.
        ImageOptions options(::vk::ImageUsageFlagBits::eColorAttachment |
                             ::vk::ImageUsageFlagBits::eSampled |
                             ::vk::ImageUsageFlagBits::eTransferSrc);
        options.memory_priority(1.0f);
        for (size_t i = 0; i < color_attachment_count; i++) {
            std::string img_name =
                 base_name + "@c" + std::to_string(cli) + '.' + std::to_string(i);
            ::vk::Format img_fmt;
            if (color_attachment_formats != nullptr) {
                img_fmt = recompose(color_attachment_formats[i]);
            }
            else if (_color_images[cli * color_attachment_count + i]) {
                img_fmt = _color_images[cli * color_attachment_count + i].get_format();
            }
            else {
                /* Neither color_attachment_formats was provided,
                   nor were there previous images for format reuse. */
                return false;
            }
            auto img = _context->new_image2d(img_name, img_fmt, size, options);
            _color_images[cli * color_attachment_count + i].reset();
            _color_images[cli * color_attachment_count + i] = std::move(img);
        }
    }
    return true;
}
bool FramebufferChain::_initialize_depth_attachments(glm::uvec2 size, size_t chain_offset,
                                                     size_t chain_count) {
    size_t chain_end_count;
    if (chain_count == std::numeric_limits<size_t>::max()) {
        chain_end_count = _chain_size;
    }
    else {
        chain_end_count = std::min(chain_offset + chain_count, _chain_size);
    }
    ImageOptions options(::vk::ImageUsageFlagBits::eDepthStencilAttachment);
    options.memory_priority(1.0f);
    for (size_t cli = chain_offset; cli < chain_end_count; cli++) {
        // Depth.
        std::string img_name = std::string(_name) + "@d" + std::to_string(cli);
        auto img = _context->new_image2d(img_name, ::vk::Format::eD32Sfloat, size, options);
        _depth_images[cli].reset();
        _depth_images[cli] = std::move(img);
    }
    return true;
}
bool FramebufferChain::_initialize_attachment_views(size_t color_images_count,
                                                    const Image2D* color_images,
                                                    size_t depth_images_count,
                                                    const Image2D* depth_images) {
    for (size_t i = 0; i < color_images_count; i++) {
        auto view = color_images[i].get_view();
        if (view) {
            if (_context != nullptr && _context->is_validation_layer_active() &&
                color_images[i].get_name() != nullptr) {
                _context->associate_handle_with_name(
                     view->get_handle(), std::string(color_images[i].get_name()) + "_fbview");
            }
            // Destroy old image views.
            _color_image_views[i].reset();
            _color_image_views[i] = std::move(view.value());
        }
        else {
            logger.loge("Couldn't get view for color attachment #", i, ".");
            return false;
        }
    }
    for (size_t i = 0; i < depth_images_count; i++) {
        auto view = depth_images[i].get_view();
        if (view) {
            if (_context != nullptr && _context->is_validation_layer_active()) {
                _context->associate_handle_with_name(
                     view->get_handle(), std::string(depth_images[i].get_name()) + "_fbview");
            }
            _depth_image_views[i].reset();
            _depth_image_views[i] = std::move(view.value());
        }
        else {
            logger.loge("Couldn't get view for depth attachment #", i, ".");
            return false;
        }
    }
    return true;
}
bool FramebufferChain::_initialize_attachment_views_partial(size_t chain_index) {
    size_t colors_attachment_count = get_colors_attachment_count();
    size_t colors_attachment_offset = chain_index * colors_attachment_count;
    for (size_t i = 0; i < colors_attachment_count; i++) {
        size_t full_index = colors_attachment_offset + i;
        auto view = _color_images[full_index].get_view();
        if (view) {
            if (_context != nullptr && _context->is_validation_layer_active()) {
                _context->associate_handle_with_name(
                     view->get_handle(),
                     std::string(_color_images[full_index].get_name()) + "_fbview");
            }
            // Destroy old image views.
            _color_image_views[full_index].reset();
            _color_image_views[full_index] = std::move(view.value());
        }
        else {
            logger.loge("Couldn't get view for color attachment #", full_index, ".");
            return false;
        }
    }
    if (has_depth_attachment()) {
        auto view = _depth_images[chain_index].get_view();
        if (view) {
            if (_context != nullptr && _context->is_validation_layer_active()) {
                _context->associate_handle_with_name(
                     view->get_handle(),
                     std::string(_depth_images[chain_index].get_name()) + "_fbview");
            }
            _depth_image_views[chain_index].reset();
            _depth_image_views[chain_index] = std::move(view.value());
        }
        else {
            logger.loge("Couldn't get view for depth attachment #", chain_index, ".");
            return false;
        }
    }
    return true;
}
bool FramebufferChain::_initialize_renderpass(bool presentation_framebuffer) {
    ::vk::Device device = get_device();
    size_t color_attachment_count = get_colors_attachment_count();
    bool use_depth_attachment = has_depth_attachment();
    size_t attachment_count = color_attachment_count + (use_depth_attachment ? 1 : 0);
    // Attachment info.
    std::unique_ptr<::vk::AttachmentDescription[]> renderpass_cinfo_attachments(
         new ::vk::AttachmentDescription[attachment_count]);
    std::unique_ptr<::vk::AttachmentReference[]> renderpass_cinfo_attachment_refs(
         new ::vk::AttachmentReference[attachment_count]);
    // Attachment info - color.
    for (size_t i = 0; i < color_attachment_count; i++) {
        auto& renderpass_cinfo_attachment_current = renderpass_cinfo_attachments[i];
        auto& renderpass_cinfo_attachment_ref_current = renderpass_cinfo_attachment_refs[i];
        renderpass_cinfo_attachment_current.format = _color_image_views[i].get_image_format();
        renderpass_cinfo_attachment_current.loadOp = ::vk::AttachmentLoadOp::eClear;
        renderpass_cinfo_attachment_current.storeOp = ::vk::AttachmentStoreOp::eStore;
        renderpass_cinfo_attachment_current.stencilLoadOp = ::vk::AttachmentLoadOp::eDontCare;
        renderpass_cinfo_attachment_current.stencilStoreOp = ::vk::AttachmentStoreOp::eDontCare;
        if (presentation_framebuffer) {
            renderpass_cinfo_attachment_current.finalLayout = ::vk::ImageLayout::ePresentSrcKHR;
        }
        else {
            renderpass_cinfo_attachment_current.finalLayout =
                 ::vk::ImageLayout::eShaderReadOnlyOptimal;
        }
        renderpass_cinfo_attachment_ref_current.attachment = i;
        renderpass_cinfo_attachment_ref_current.layout =
             ::vk::ImageLayout::eColorAttachmentOptimal;
    }
    // Attachment info - depth.
    if (use_depth_attachment) {
        auto& renderpass_cinfo_attachment_current =
             renderpass_cinfo_attachments[color_attachment_count];
        auto& renderpass_cinfo_attachment_ref_current =
             renderpass_cinfo_attachment_refs[color_attachment_count];
        renderpass_cinfo_attachment_current.format = _depth_image_views[0].get_image_format();
        renderpass_cinfo_attachment_current.loadOp = ::vk::AttachmentLoadOp::eClear;
        renderpass_cinfo_attachment_current.storeOp = ::vk::AttachmentStoreOp::eStore;
        renderpass_cinfo_attachment_current.stencilLoadOp = ::vk::AttachmentLoadOp::eDontCare;
        renderpass_cinfo_attachment_current.stencilStoreOp = ::vk::AttachmentStoreOp::eDontCare;
        renderpass_cinfo_attachment_current.finalLayout =
             ::vk::ImageLayout::eDepthAttachmentOptimal;
        renderpass_cinfo_attachment_ref_current.attachment = color_attachment_count;
        renderpass_cinfo_attachment_ref_current.layout =
             ::vk::ImageLayout::eDepthAttachmentOptimal;
    }
    // Subpass info.
    ::vk::SubpassDescription renderpass_cinfo_subpass;
    renderpass_cinfo_subpass.pipelineBindPoint = ::vk::PipelineBindPoint::eGraphics;
    renderpass_cinfo_subpass.colorAttachmentCount = color_attachment_count;
    renderpass_cinfo_subpass.pColorAttachments = &renderpass_cinfo_attachment_refs[0];
    if (use_depth_attachment) {
        renderpass_cinfo_subpass.pDepthStencilAttachment =
             &renderpass_cinfo_attachment_refs[color_attachment_count];
    }
    /**
     * Subpass dependency info.
     *
     * NOTE:(?) Not needed as long as all attachments are independent between chain links and
     * each chain link is synchronized.
     */
    ::vk::SubpassDependency attachment_deps;
    attachment_deps.srcSubpass = ::vk::SubpassExternal;
    attachment_deps.dstSubpass = 0;
    attachment_deps.srcStageMask = ::vk::PipelineStageFlagBits::eColorAttachmentOutput;
    attachment_deps.srcAccessMask = {};
    attachment_deps.dstStageMask = ::vk::PipelineStageFlagBits::eColorAttachmentOutput;
    attachment_deps.dstAccessMask = ::vk::AccessFlagBits::eColorAttachmentWrite;
    // Construct renderpass.
    ::vk::RenderPassCreateInfo renderpass_cinfo;
    renderpass_cinfo.attachmentCount = attachment_count;
    renderpass_cinfo.pAttachments = renderpass_cinfo_attachments.get();
    renderpass_cinfo.subpassCount = 1;
    renderpass_cinfo.pSubpasses = &renderpass_cinfo_subpass;
    renderpass_cinfo.dependencyCount = 1;
    renderpass_cinfo.pDependencies = &attachment_deps;
    ::vk::Result r;
    std::tie(r, _renderpass) = device.createRenderPass(renderpass_cinfo);
    if (r != ::vk::Result::eSuccess) {
        logger.loge("Couldn't create render pass(", to_string(r), ")!");
        return false;
    }
    if (_context != nullptr && _context->is_validation_layer_active()) {
        _context->associate_handle_with_name(_renderpass, _name + "@renderpass");
    }
    return true;
}
bool FramebufferChain::_initialize_clear_defaults() {
    size_t color_attachment_count = get_colors_attachment_count();
    bool use_depth_attachment = has_depth_attachment();
    for (size_t i = 0; i < color_attachment_count; i++) {
        if (!set_color_clear_value(i, glm::vec4(0, 0, 0, 1))) {
            return false;
        }
    }
    if (use_depth_attachment) {
        if (!set_depth_clear_value(0)) {
            return false;
        }
    }
    return true;
}
bool FramebufferChain::_initialize_framebuffers(glm::uvec2 size, size_t chain_offset,
                                                size_t chain_count) {
    size_t chain_end_count;
    if (chain_count == std::numeric_limits<size_t>::max()) {
        chain_end_count = _chain_size;
    }
    else {
        chain_end_count = std::min(chain_offset + chain_count, _chain_size);
    }

    ::vk::Device device = get_device();
    size_t color_attachment_count = get_colors_attachment_count();
    bool use_depth_attachment = has_depth_attachment();
    size_t attachment_count = color_attachment_count + (use_depth_attachment ? 1 : 0);
    // Construct framebuffers.
    std::unique_ptr<::vk::ImageView[]> framebuffer_cinfo_attachments(
         new ::vk::ImageView[attachment_count]);
    for (size_t i = chain_offset; i < chain_end_count; i++) {
        // Destroy old objects.
        if (_framebuffers[i]) {
            device.destroy(_framebuffers[i]);
            _framebuffers[i] = nullptr;
        }

        for (size_t j = 0; j < color_attachment_count; j++) {
            framebuffer_cinfo_attachments[j] =
                 _color_image_views[i * color_attachment_count + j].get_handle();
        }
        if (use_depth_attachment) {
            framebuffer_cinfo_attachments[color_attachment_count] =
                 _depth_image_views[i].get_handle();
        }
        ::vk::FramebufferCreateInfo framebuffer_cinfo;
        framebuffer_cinfo.renderPass = _renderpass;
        framebuffer_cinfo.attachmentCount = attachment_count;
        framebuffer_cinfo.pAttachments = framebuffer_cinfo_attachments.get();
        framebuffer_cinfo.width = size.x;
        framebuffer_cinfo.height = size.y;
        framebuffer_cinfo.layers = 1;
        ::vk::Result r;
        std::tie(r, _framebuffers[i]) = device.createFramebuffer(framebuffer_cinfo);
        if (r != ::vk::Result::eSuccess) {
            logger.loge("Couldn't create framebuffer #", i, "(", to_string(r), ")!");
            return false;
        }
        if (_context != nullptr && _context->is_validation_layer_active()) {
            _context->associate_handle_with_name(_framebuffers[i],
                                                 _name + "@fb" + std::to_string(i));
        }
    }
    return true;
}

FramebufferChain::FramebufferChain(Context& context, size_t chain_size,
                                   const Image2D* swapchain_images, bool use_depth_attachment) :
    /** Whole-object settings. */
    _chain_size(chain_size), _context(&context), _name("swapchain"),
    _framebuffers(new ::vk::Framebuffer[chain_size]),
    /** Color attachments state. */
    _color_images_count(chain_size), _color_images(nullptr),
    _color_image_views(new Image2D::View[_color_images_count]),
    /** Depth attachment state. */
    _depth_images_count(use_depth_attachment ? chain_size : 0),
    _depth_images(use_depth_attachment ? new Image2D[_depth_images_count] : nullptr),
    _depth_image_views(use_depth_attachment ? new Image2D::View[_depth_images_count] : nullptr),
    _clear_values(new ::vk::ClearValue[use_depth_attachment ? 2 : 1]) {
    glm::uvec2 size = swapchain_images[0].get_size();
    // Allocate images for attachments.
    if (use_depth_attachment) {
        if (!_initialize_depth_attachments(size)) {
            logger.logf("Couldn't allocate depth attachment images!");
        }
    }
    if (!_initialize_attachment_views(_color_images_count, swapchain_images,
                                      _depth_images_count, _depth_images)) {
        logger.logf("Couldn't get view from attachment image!");
    }
    // Renderpass.
    if (!_initialize_renderpass(true)) {
        logger.logf("Couldn't initialize renderpass!");
    }
    // Renderpass - clear values.
    if (!_initialize_clear_defaults()) {
        logger.logf("Couldn't set default clear values for renderpass!");
    }
    // Framebuffers.
    if (!_initialize_framebuffers(size)) {
        logger.logf("Couldn't initialize framebuffers!");
    }
    spec::trace("Framebuffer::new_from_swapchain(", _renderpass, ")");
}

FramebufferChain::FramebufferChain(const std::string& name, Context& context, glm::uvec2 size,
                                   size_t chain_size, size_t color_attachment_count,
                                   const Format* color_attachment_options,
                                   bool use_depth_attachment) :
    /** Whole-object settings. */
    _chain_size(chain_size), _context(&context), _name(name),
    _framebuffers(new ::vk::Framebuffer[chain_size]),
    /** Color attachments state. */
    _color_images_count(color_attachment_count * chain_size),
    _color_images(color_attachment_count != 0 ? new Image2D[_color_images_count] : nullptr),
    _color_image_views(color_attachment_count != 0 ? new Image2D::View[_color_images_count] :
                                                     nullptr),
    /** Depth attachment state. */
    _depth_images_count(use_depth_attachment ? chain_size : 0),
    _depth_images(use_depth_attachment ? new Image2D[_depth_images_count] : nullptr),
    _depth_image_views(use_depth_attachment ? new Image2D::View[_depth_images_count] : nullptr),
    _clear_values(
         new ::vk::ClearValue[color_attachment_count + (use_depth_attachment ? 1 : 0)]) {
    // Allocate images for attachments.
    if (!_initialize_color_attachments(size, color_attachment_count,
                                       color_attachment_options)) {
        logger.logf("Couldn't allocate color attachment images!");
    }
    if (use_depth_attachment) {
        if (!_initialize_depth_attachments(size)) {
            logger.logf("Couldn't allocate depth attachment images!");
        }
    }
    if (!_initialize_attachment_views(_color_images_count, _color_images, _depth_images_count,
                                      _depth_images)) {
        logger.logf("Couldn't get view from attachment image!");
    }
    // Renderpass.
    if (!_initialize_renderpass(false)) {
        logger.logf("Couldn't initialize renderpass!");
    }
    // Renderpass - clear values.
    if (!_initialize_clear_defaults()) {
        logger.logf("Couldn't set default clear values for renderpass!");
    }
    // Framebuffers.
    if (!_initialize_framebuffers(size)) {
        logger.logf("Couldn't initialize framebuffers!");
    }
    spec::trace("Framebuffer::new(", _renderpass, ")");
}

glm::uvec2 FramebufferChain::get_size(size_t chain_index) const {
    if (_color_images_count != 0) {
        auto cac = get_colors_attachment_count();
        // Get size information from attachment index 0.
        return _color_image_views[chain_index * cac].get_image_size();
    }
    else if (_depth_images_count != 0) {
        // Handle depth-only framebuffers.
        return _depth_image_views[chain_index].get_image_size();
    }
    else {
        // Invalid state.
        return {0, 0};
    }
}
::vk::Format FramebufferChain::get_format(uint32_t attachment_index) const {
    if (_color_image_views == nullptr) {
        spec::trace("FramebufferChain::get_format: color image views are uninitialized!");
        return ::vk::Format::eUndefined;
    }
    if (attachment_index >= get_colors_attachment_count()) {
        spec::trace("FramebufferChain::get_format: "
                    "attachment index parameter is out of bounds!");
        return ::vk::Format::eUndefined;
    }
    // Retrieve formats from chain index 0.
    return _color_image_views[attachment_index].get_image_format();
}
::vk::Format FramebufferChain::get_depth_format() const {
    if (_depth_images == nullptr) {
        spec::trace("FramebufferChain::get_depth_format: depth images are uninitialized!");
        return ::vk::Format::eUndefined;
    }
    // Retrieve formats from chain index 0.
    return _depth_images[0].get_format();
}

std::span<Image2D> FramebufferChain::get_color_attachments(size_t chain_index) const {
    auto color_attachment_count = get_colors_attachment_count();
    if (_color_images != nullptr && color_attachment_count != 0) {
        Image2D* chain_start = &_color_images[chain_index * color_attachment_count];
        return std::span(chain_start, color_attachment_count);
    }
    else {
        return std::span<Image2D>();
    }
}
std::span<Image2D::View> FramebufferChain::get_color_attachment_views(
     size_t chain_index) const {
    auto color_attachment_count = get_colors_attachment_count();
    if (color_attachment_count != 0) {
        Image2D::View* chain_start = &_color_image_views[chain_index * color_attachment_count];
        return std::span(chain_start, color_attachment_count);
    }
    else {
        return std::span<Image2D::View>();
    }
}
Image2D::View FramebufferChain::get_depth_attachment(size_t chain_index) const {
    if (has_depth_attachment()) {
        return _depth_image_views[chain_index];
    }
    return {};
}

bool FramebufferChain::set_color_clear_value(uint32_t attachment_index, glm::vec4 rgba) {
    // Bounds check.
    if (attachment_index >= get_colors_attachment_count()) {
        return false;
    }
    // Query format.
    ::vk::Format fmt = get_format(attachment_index);
    NumericFormat num_fmt = decompose_numeric_format(fmt);
    ComponentFormat comp_fmt = decompose_component_format(fmt);

    // Copy values.
    ::vk::ClearValue& target = _clear_values[attachment_index];
    switch (num_fmt) {
        case NumericFormat::Uint: {
            auto max_red = max_from_unsigned_bits<uint64_t>(get_red_bits(comp_fmt));
            auto max_green = max_from_unsigned_bits<uint64_t>(get_green_bits(comp_fmt));
            auto max_blue = max_from_unsigned_bits<uint64_t>(get_blue_bits(comp_fmt));
            auto max_alpha = max_from_unsigned_bits<uint64_t>(get_alpha_bits(comp_fmt));
            // Float [0, 1] -> unsigned integer [0, MAX].
            const double eps = 1e-3;
            target.color.uint32[0] = rgba.r * (((double) max_red) + 1.0 - eps);
            target.color.uint32[1] = rgba.g * (((double) max_green) + 1.0 - eps);
            target.color.uint32[2] = rgba.b * (((double) max_blue) + 1.0 - eps);
            target.color.uint32[3] = rgba.a * (((double) max_alpha) + 1.0 - eps);
            return false;
        } break;

        case NumericFormat::Sint: {
            const double eps = 1e-3;
            if (rgba.r >= 0) {
                auto max_red = max_from_signed_bits<int64_t>(get_red_bits(comp_fmt));
                target.color.int32[0] = rgba.r * (((double) max_red) + 1.0 - eps);
            }
            else {
                auto min_red = min_from_signed_bits<int64_t>(get_red_bits(comp_fmt));
                target.color.int32[0] = -rgba.r * (((double) min_red) + 1.0 - eps);
            }
            if (rgba.g >= 0) {
                auto max_green = max_from_signed_bits<int64_t>(get_green_bits(comp_fmt));
                target.color.int32[1] = rgba.g * (((double) max_green) + 1.0 - eps);
            }
            else {
                auto min_green = min_from_signed_bits<int64_t>(get_green_bits(comp_fmt));
                target.color.int32[1] = -rgba.g * (((double) min_green) + 1.0 - eps);
            }
            if (rgba.b >= 0) {
                auto max_blue = max_from_signed_bits<int64_t>(get_blue_bits(comp_fmt));
                target.color.int32[2] = rgba.b * (((double) max_blue) + 1.0 - eps);
            }
            else {
                auto min_blue = min_from_signed_bits<int64_t>(get_blue_bits(comp_fmt));
                target.color.int32[2] = -rgba.b * (((double) min_blue) + 1.0 - eps);
            }
            if (rgba.a >= 0) {
                auto max_alpha = max_from_signed_bits<int64_t>(get_alpha_bits(comp_fmt));
                target.color.int32[3] = rgba.a * (((double) max_alpha) + 1.0 - eps);
            }
            else {
                auto min_alpha = min_from_signed_bits<int64_t>(get_alpha_bits(comp_fmt));
                target.color.int32[3] = -rgba.a * (((double) min_alpha) + 1.0 - eps);
            }
            return false;
        } break;

        case NumericFormat::Ufloat:
        case NumericFormat::Sfloat:
        case NumericFormat::Unorm:
        case NumericFormat::Snorm:
        case NumericFormat::Uscaled:
        case NumericFormat::Sscaled:
        case NumericFormat::Srgb: {
            target.color.float32 = vec2array(rgba);
            return true;
        } break;

        case NumericFormat::Invalid:
        case NumericFormat::NvI10F5:
        default: {
            spec::trace("Encountered invalid numerical format(", to_string(fmt),
                        ") while setting clear value for color attachment.");
            return false;
        } break;
    }
}
bool FramebufferChain::set_depth_clear_value(float v) {
    if (has_depth_attachment()) {
        size_t depth_index = get_colors_attachment_count();
        _clear_values[depth_index].depthStencil.depth = v;
        _clear_values[depth_index].depthStencil.stencil = 0;
        return true;
    }
    return false;
}

void FramebufferChain::start_pass(::vk::CommandBuffer cmd_buf, size_t chain_index) {
    ::vk::Rect2D render_area = vec_to_rect(get_size(chain_index));
    uint32_t clear_value_count =
         get_colors_attachment_count() + (has_depth_attachment() ? 1 : 0);
    ::vk::RenderPassBeginInfo binfo(get_renderpass(), get_handle(chain_index), render_area,
                                    clear_value_count, _clear_values);
    cmd_buf.beginRenderPass(binfo, ::vk::SubpassContents::eInline);
}
void FramebufferChain::end_pass(::vk::CommandBuffer cmd_buf) {
    cmd_buf.endRenderPass();
}
// void FramebufferChain::apply(uint32_t attachment_index);

void FramebufferChain::on_swapchain_update(size_t chain_size, const Image2D* swapchain_images) {
    if (get_chain_size() != chain_size) {
        logger.logf("Tried to update framebuffer with a swapchain of different size!");
    }
    glm::uvec2 size = swapchain_images[0].get_size();
    if (has_depth_attachment()) {
        if (!_initialize_depth_attachments(size)) {
            logger.logf("Couldn't allocate depth attachment images!");
        }
    }
    if (!_initialize_attachment_views(_color_images_count, swapchain_images,
                                      _depth_images_count, _depth_images)) {
        logger.logf("Couldn't get view from attachment image!");
    }
    if (!_initialize_framebuffers(size)) {
        logger.logf("Couldn't initialize framebuffers!");
    }
}
void FramebufferChain::on_swapchain_destroy() {
    ::vk::Device device = get_device();

    for (size_t i = 0; i < _color_images_count; i++) {
        _color_image_views[i].reset();
    }

    for (size_t i = 0; i < _chain_size; i++) {
        if (_framebuffers[i]) {
            device.destroy(_framebuffers[i]);
            _framebuffers[i] = nullptr;
        }
    }
}

void FramebufferChain::on_resize(size_t chain_index, glm::uvec2 new_size) {
    if (chain_index >= get_chain_size()) {
        spec::fatal("FramebufferChain::on_resize: chain_index is out of bounds!");
    }
    if (has_depth_attachment()) {
        if (!_initialize_depth_attachments(new_size, chain_index, 1)) {
            logger.logf("FramebufferChain::on_resize: "
                        "Couldn't allocate depth attachment image!");
        }
    }
    if (get_colors_attachment_count() != 0) {
        if (!_initialize_color_attachments(new_size, get_colors_attachment_count(), nullptr,
                                           chain_index, 1)) {
            logger.logf("FramebufferChain::on_resize: "
                        "Couldn't allocate color attachment images!");
        }
    }
    if (!_initialize_attachment_views_partial(chain_index)) {
        logger.logf("FramebufferChain::on_resize: "
                    "Couldn't get view from attachment image!");
    }
    if (!_initialize_framebuffers(new_size, chain_index, 1)) {
        logger.logf("FramebufferChain::on_resize: "
                    "Couldn't initialize framebuffers!");
    }
}

}  // namespace satr::vk
