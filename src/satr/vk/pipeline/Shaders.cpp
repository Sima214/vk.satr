#include "Shaders.hpp"

#include <Context.hpp>
#include <logger/Logger.hpp>
#include <trace/Trace.hpp>
#include <vulkan/vulkan_to_string.hpp>

#include <algorithm>
#include <cstring>
#include <filesystem>
#include <fstream>
#include <limits>
#include <set>
#include <utility>
#include <vector>

#include <spirv_reflect.h>
#include <stdint.h>

namespace satr::vk {

ModuleImpl::ModuleImpl(ModuleImpl&& o) noexcept :
    _handle(std::exchange(o._handle, nullptr)), _device(std::exchange(o._device, nullptr)),
    _stage_config(o._stage_config), _filedata(std::move(o._filedata)),
    _reflection(std::move(o._reflection)) {
    spec::trace("Module::construct_move(", _handle, ")");
}
ModuleImpl& ModuleImpl::operator=(ModuleImpl&& o) noexcept {
    _handle = std::exchange(o._handle, nullptr);
    _device = std::exchange(o._device, nullptr);
    _stage_config = o._stage_config;
    _filedata = std::move(o._filedata);
    _reflection = std::move(o._reflection);
    spec::trace("Module::assign_move(", _handle, ")");
    return *this;
}
ModuleImpl::ModuleImpl(::vk::ShaderModule handle, ::vk::Device device,
                       const StageConfig& config, std::span<const uint32_t> data) :
    _handle(handle),
    _device(device), _stage_config(config), _filedata(),
    _reflection(data.size_bytes(), data.data(), SPV_REFLECT_MODULE_FLAG_NO_COPY) {
    if (_reflection.GetResult() != SPV_REFLECT_RESULT_SUCCESS) {
        spec::fatal("Failed to parse shader module code for reflection!");
    }
    if (_reflection.GetEntryPointCount() != 1) {
        spec::fatal("Shader modules require exactly one entry point.");
    }
    spec::trace("Module::new_from_static(", _handle, ")");
}
ModuleImpl::ModuleImpl(::vk::ShaderModule handle, ::vk::Device device,
                       const StageConfig& config, spec::FileDescriptor&& file_object,
                       spec::MappedFile&& file_memory) :
    _handle(handle),
    _device(device), _stage_config(config),
    _filedata{std::move(file_object), std::move(file_memory)},
    _reflection(_filedata.second.get_size(), _filedata.second.get_ptr(),
                SPV_REFLECT_MODULE_FLAG_NO_COPY) {
    if (_reflection.GetResult() != SPV_REFLECT_RESULT_SUCCESS) {
        spec::fatal("Failed to parse shader module code for reflection!");
    }
    if (_reflection.GetEntryPointCount() != 1) {
        spec::fatal("Shader modules require exactly one entry point.");
    }
    spec::trace("Module::new_from_file(", _handle, ")");
}
ModuleImpl::~ModuleImpl() {
    if (_handle) {
        spec::trace("Module::delete(", _handle, ")");
        _device.destroy(_handle);
        _handle = nullptr;
    }
}

std::optional<ModuleImpl> ModuleImpl::load(const Context& ctx, const uint32_t* buffer,
                                           size_t buffer_size, const StageConfig& config) {
    if (buffer == nullptr || buffer_size == 0) {
        spec::trace(
             "Module::load(ctx, buffer, size, config): either buffer is null or size is 0.");
        return {};
    }
    ::vk::ShaderModuleCreateInfo cinfo;
    cinfo.codeSize = buffer_size * 4;
    cinfo.pCode = buffer;
    ::vk::Device dev = ctx.get_active_device();
    auto [r, hdl] = dev.createShaderModule(cinfo);
    if (r != ::vk::Result::eSuccess) {
        spec::trace("Module::load::createShaderModule -> ", to_string(r), ".");
        return {};
    }
    return ModuleImpl(hdl, dev, config, std::span(buffer, buffer_size));
}

std::optional<ModuleImpl> ModuleImpl::load(const Context& ctx,
                                           const std::filesystem::path& path,
                                           bool load_stage_config) {
    StageConfig config;
    if (load_stage_config) {
        // Try to load stage config file.
        auto config_path = path;
        config_path += ".stage.config";
        if (!config.load(config_path)) {
            // Failed to load stage config file.
            logger.loge("Couldn't load stage config file for shader module `", path, "`!");
            return {};
        }
    }
    return load(ctx, path, config);
}
std::optional<ModuleImpl> ModuleImpl::load(const Context& ctx,
                                           const std::filesystem::path& path,
                                           const StageConfig& config) {
    if (!std::filesystem::is_regular_file(path)) {
        logger.loge("Shader module `", path, "` could not be found!");
        return {};
    }
    size_t est_byte_count = std::filesystem::file_size(path);
    if ((est_byte_count % 4) != 0) {
        est_byte_count += 4 - (est_byte_count % 4);
        logger.logw("Shader module `", path, "` is not properly aligned!");
    }
    if (est_byte_count == 0) {
        logger.logw("Encountered empty(size 0) shader code file `", path, "`!");
    }

    spec::FileDescriptor file_object(path, spec::FileDescriptor::OpenFlagBits::Read);
    if (!file_object || !file_object.is_valid()) {
        logger.loge("Couldn't load shader code file `", path, "`!");
        return {};
    }

    spec::MappedFile file_memory(file_object, spec::MappedFile::CreateFlagBits::ReadAccess,
                                 spec::MappedFile::Advice::NormalAccessPattern);
    if (!file_memory || !file_memory.is_valid()) {
        logger.loge("Couldn't map shader code file `", path, "` to memory!");
        return {};
    }

    const uint32_t* file_buffer = (const uint32_t*) file_memory.get_ptr();
    size_t file_buffer_size_bytes = file_memory.get_size();
    if (file_buffer_size_bytes != est_byte_count) {
        logger.loge("Mapped byte count(", file_buffer_size_bytes, ") and estimated byte count(",
                    est_byte_count, ") mismatch while loading shader code file `", path, "`!");
        return {};
    }

    ::vk::ShaderModuleCreateInfo cinfo;
    cinfo.codeSize = file_buffer_size_bytes;
    cinfo.pCode = file_buffer;
    ::vk::Device dev = ctx.get_active_device();
    auto [r, hdl] = dev.createShaderModule(cinfo);
    if (r != ::vk::Result::eSuccess) {
        spec::trace("Module::load::createShaderModule -> ", to_string(r), ".");
        return {};
    }

    return ModuleImpl(hdl, dev, config, std::move(file_object), std::move(file_memory));
}

::vk::ShaderStageFlagBits ModuleImpl::get_stage_type() const {
    auto type = _reflection.GetShaderStage();
    switch (type) {
        case SPV_REFLECT_SHADER_STAGE_VERTEX_BIT: return ::vk::ShaderStageFlagBits::eVertex;
        case SPV_REFLECT_SHADER_STAGE_TESSELLATION_CONTROL_BIT:
            return ::vk::ShaderStageFlagBits::eTessellationControl;
        case SPV_REFLECT_SHADER_STAGE_TESSELLATION_EVALUATION_BIT:
            return ::vk::ShaderStageFlagBits::eTessellationEvaluation;
        case SPV_REFLECT_SHADER_STAGE_GEOMETRY_BIT: return ::vk::ShaderStageFlagBits::eGeometry;
        case SPV_REFLECT_SHADER_STAGE_FRAGMENT_BIT: return ::vk::ShaderStageFlagBits::eFragment;
        case SPV_REFLECT_SHADER_STAGE_COMPUTE_BIT: return ::vk::ShaderStageFlagBits::eCompute;
        case SPV_REFLECT_SHADER_STAGE_TASK_BIT_EXT: return ::vk::ShaderStageFlagBits::eTaskEXT;
        case SPV_REFLECT_SHADER_STAGE_MESH_BIT_EXT: return ::vk::ShaderStageFlagBits::eMeshEXT;
        case SPV_REFLECT_SHADER_STAGE_RAYGEN_BIT_KHR:
            return ::vk::ShaderStageFlagBits::eRaygenKHR;
        case SPV_REFLECT_SHADER_STAGE_ANY_HIT_BIT_KHR:
            return ::vk::ShaderStageFlagBits::eAnyHitKHR;
        case SPV_REFLECT_SHADER_STAGE_CLOSEST_HIT_BIT_KHR:
            return ::vk::ShaderStageFlagBits::eClosestHitKHR;
        case SPV_REFLECT_SHADER_STAGE_MISS_BIT_KHR: return ::vk::ShaderStageFlagBits::eMissKHR;
        case SPV_REFLECT_SHADER_STAGE_INTERSECTION_BIT_KHR:
            return ::vk::ShaderStageFlagBits::eIntersectionKHR;
        case SPV_REFLECT_SHADER_STAGE_CALLABLE_BIT_KHR:
            return ::vk::ShaderStageFlagBits::eCallableKHR;
        default: {
            spec::fatal("ModuleImpl::get_stage_type: Unknown reflection enum value (", type,
                        ")!");
        }
    }
}

std::string_view ModuleImpl::get_entry_point() const {
    auto str = _reflection.GetEntryPointName();
    return std::string_view(str);
}
bool ModuleImpl::has_vertex_input_attributes() const {
    ::vk::ShaderStageFlagBits stage = get_stage_type();
    constexpr ::vk::ShaderStageFlags VERTEX_INPUT_STAGES =
         ::vk::ShaderStageFlagBits::eVertex | ::vk::ShaderStageFlagBits::eGeometry |
         ::vk::ShaderStageFlagBits::eTessellationControl |
         ::vk::ShaderStageFlagBits::eTessellationEvaluation;
    // bitwise test-any.
    return !!(stage & VERTEX_INPUT_STAGES);
}

::vk::PipelineShaderStageCreateInfo ModuleImpl::make_stage_create_info() {
    ::vk::PipelineShaderStageCreateInfo cinfo = _stage_config.get_partial_create_info();
    cinfo.stage = get_stage_type();
    cinfo.module = _handle;
    cinfo.pName = get_entry_point().data();
    return cinfo;
}

Descriptor::Descriptor(const SpvReflectDescriptorBinding& reflection,
                       ::vk::ShaderStageFlags stages) :
    _reflection(reflection),
    _stages(stages) {}
Descriptor::Descriptor(const Descriptor& o) : _reflection(o._reflection), _stages(o._stages) {}
Descriptor& Descriptor::operator=(const Descriptor& o) {
    _reflection = o._reflection;
    _stages = o._stages;
    return *this;
}
std::string_view Descriptor::get_name() const {
    return _reflection.get().name;
}
uint32_t Descriptor::get_binding() const {
    return _reflection.get().binding;
}
::vk::ShaderStageFlags Descriptor::get_stages() const {
    return _stages;
}
uint32_t Descriptor::get_set() const {
    return _reflection.get().set;
}
::vk::DescriptorType Descriptor::get_type() const {
    return static_cast<::vk::DescriptorType>(_reflection.get().descriptor_type);
}
bool Descriptor::is_block() const {
    return !!(_reflection.get().decoration_flags & SPV_REFLECT_DECORATION_BLOCK);
}
bool Descriptor::is_array() const {
    return _reflection.get().array.dims_count != 0;
}
std::span<const uint32_t> Descriptor::get_array_dims() const {
    const uint32_t* dims = _reflection.get().array.dims;
    uint32_t dims_count = _reflection.get().array.dims_count;
    return std::span(dims, dims_count);
}
::vk::DescriptorSetLayoutBinding Descriptor::get_layout() const {
    ::vk::DescriptorSetLayoutBinding layout(get_binding(), get_type());
    if (is_array()) {
        auto array_dims = get_array_dims();
        uint32_t element_count = array_dims[0];
        for (size_t dim = 1; dim < array_dims.size(); dim++) {
            element_count *= array_dims[dim];
        }
        layout.descriptorCount = element_count;
    }
    else {
        layout.descriptorCount = 1;
    }
    layout.stageFlags = _stages;
    return layout;
}

Descriptors::Descriptors(std::span<Module> shader_modules) : _shader_modules(shader_modules) {
    for (size_t i = 0; i < shader_modules.size(); i++) {
        Module shi = _shader_modules[i];
        const SpvReflectShaderModule& modi = shi->get_reflection().GetShaderModule();
        // For each descriptor binding/uniform.
        for (size_t x = 0; x < modi.descriptor_binding_count; x++) {
            const SpvReflectDescriptorBinding& cur_desc = modi.descriptor_bindings[x];
            // Require reflection information.
            size_t name_len = (cur_desc.name == nullptr) ? 0 : std::strlen(cur_desc.name);
            if (name_len == 0) {
                spec::fatal("Descriptors: couldn't find reflection info!");
            }
            for (size_t j = i + 1; j < shader_modules.size(); j++) {
                Module shj = _shader_modules[j];
                /**
                 * If any other interface has a conflicting uniform
                 * variable, their interfaces must match.
                 */
                const SpvReflectDescriptorBinding* name_match =
                     _retrieve_by_name(shj, cur_desc.name, name_len);
                const SpvReflectDescriptorBinding* bind_match =
                     _retrieve_by_binding(shj, cur_desc.set, cur_desc.binding);
                if (name_match == bind_match) {
                    if (name_match != nullptr) {
                        // TODO: test decorations and type.
                    }
                }
                else {
                    spec::fatal("Descriptors: name or set:binding mismatch!");
                }
            }
        }
    }
}

std::optional<Descriptor> Descriptors::retrieve_by_name(const char* name_key,
                                                        size_t name_key_len) const {
    const SpvReflectDescriptorBinding* reflection = nullptr;
    ::vk::ShaderStageFlags stages;
    for (auto& mod : _shader_modules) {
        const SpvReflectDescriptorBinding* found =
             _retrieve_by_name(mod, name_key, name_key_len);
        if (found != nullptr) {
            if (reflection == nullptr) {
                reflection = found;
            }
            stages |= mod.get_stage_type();
        }
    }
    if (reflection != nullptr) {
        return Descriptor(*reflection, stages);
    }
    else {
        return {};
    }
}
std::optional<Descriptor> Descriptors::retrieve_by_binding(uint32_t set,
                                                           uint32_t binding) const {
    const SpvReflectDescriptorBinding* reflection = nullptr;
    ::vk::ShaderStageFlags stages;
    for (auto& mod : _shader_modules) {
        const SpvReflectDescriptorBinding* found = _retrieve_by_binding(mod, set, binding);
        if (found != nullptr) {
            if (reflection == nullptr) {
                reflection = found;
            }
            stages |= mod.get_stage_type();
        }
    }
    if (reflection != nullptr) {
        return Descriptor(*reflection, stages);
    }
    else {
        return {};
    }
}
std::vector<Descriptor> Descriptors::retrieve_by_set(uint32_t set) const {
    std::vector<std::pair<const SpvReflectDescriptorBinding*, ::vk::ShaderStageFlags>>
         desc_stages_list;
    for (auto& mod : _shader_modules) {
        const SpvReflectDescriptorSet* mod_set = _retrieve_set(mod, set);
        if (mod_set != nullptr) {
            // Append to desc_stages_list based refl binding field and concat stages.
            for (size_t i = 0; i < mod_set->binding_count; i++) {
                const SpvReflectDescriptorBinding* cur_desc = mod_set->bindings[i];
                auto desc_stages_pair =
                     std::find_if(desc_stages_list.begin(), desc_stages_list.end(),
                                  [cur_desc](const std::pair<const SpvReflectDescriptorBinding*,
                                                             ::vk::ShaderStageFlags>& p) {
                                      return p.first->binding == cur_desc->binding;
                                  });
                if (desc_stages_pair == desc_stages_list.end()) {
                    // If not found, append new.
                    desc_stages_list.emplace_back(cur_desc, mod.get_stage_type());
                }
                else {
                    // If found, append only stage.
                    desc_stages_pair->second |= mod.get_stage_type();
                }
            }
        }
    }
    std::vector<Descriptor> ret_list;
    ret_list.reserve(desc_stages_list.size());
    for (const auto& desc_stages_pair : desc_stages_list) {
        ret_list.emplace_back(Descriptor(*desc_stages_pair.first, desc_stages_pair.second));
    }
    // Sort by ascending binding.
    std::sort(ret_list.begin(), ret_list.end(), [](const Descriptor& a, const Descriptor& b) {
        return a.get_binding() < b.get_binding();
    });
    return ret_list;
}
void Descriptors::create_set_layouts(::vk::Device device) {
    if (!_set_layouts.empty()) {
        spec::fatal("Descriptors::create_set_layouts: Set layouts have already been created!");
    }
    // Scan to populate a list of used set indexes.
    std::set<uint32_t> sets;
    for (auto& mod : _shader_modules) {
        auto set_count = mod.get_reflection().GetShaderModule().descriptor_set_count;
        const auto* set_refls = mod->get_reflection().GetShaderModule().descriptor_sets;
        for (size_t i = 0; i < set_count; i++) {
            sets.insert(set_refls[i].set);
        }
    }
    _set_layouts.reserve(sets.size());
    // For each set:
    for (uint32_t set : sets) {
        // Collect bindings and their associated layouts.
        auto set_uniforms = retrieve_by_set(set);
        std::vector<::vk::DescriptorSetLayoutBinding> binding_layouts;
        binding_layouts.reserve(set_uniforms.size());
        for (auto& set_uniform : set_uniforms) {
            binding_layouts.push_back(set_uniform.get_layout());
        }
        // Create set layout.
        ::vk::DescriptorSetLayoutCreateInfo cinfo;
        cinfo.bindingCount = binding_layouts.size();
        cinfo.pBindings = binding_layouts.data();
        auto [r, h] = device.createDescriptorSetLayout(cinfo);
        if (r != ::vk::Result::eSuccess) {
            spec::fatal("Descriptors::create_set_layouts -> ", ::vk::to_string(r));
        }
        else {
            _set_layouts.emplace_back(set, h);
        }
    }
}
void Descriptors::free_set_layouts(::vk::Device device) {
    for (auto [set, layout] : _set_layouts) {
        device.destroy(layout);
    }
    _set_layouts.clear();
}
const std::vector<std::pair<uint32_t, ::vk::DescriptorSetLayout>>&
Descriptors::get_set_layouts() const {
    return _set_layouts;
}

::vk::DescriptorSetLayout Descriptors::get_set_layout(uint32_t set_req) const {
    for (auto [set, layout] : _set_layouts) {
        if (set == set_req) {
            return layout;
        }
    }
    return {};
}

const SpvReflectDescriptorBinding* Descriptors::_retrieve_by_name(Module shader,
                                                                  const char* name_key,
                                                                  size_t name_key_len) {
    const SpvReflectShaderModule& mod = shader->get_reflection().GetShaderModule();
    size_t list_size = mod.descriptor_binding_count;
    const SpvReflectDescriptorBinding* list = mod.descriptor_bindings;
    for (size_t i = 0; i < list_size; i++) {
        const SpvReflectDescriptorBinding* cur = &list[i];
        if (cur->name != nullptr && std::strncmp(cur->name, name_key, name_key_len) == 0) {
            return cur;
        }
    }
    return nullptr;
}
const SpvReflectDescriptorBinding* Descriptors::_retrieve_by_binding(Module shader,
                                                                     uint32_t set,
                                                                     uint32_t binding) {
    const SpvReflectDescriptorSet* set_obj = _retrieve_set(shader, set);
    if (set_obj == nullptr) {
        return nullptr;
    }
    size_t list_size = set_obj->binding_count;
    SpvReflectDescriptorBinding** const list = set_obj->bindings;
    for (size_t i = 0; i < list_size; i++) {
        const SpvReflectDescriptorBinding* cur = list[i];
        if (cur->binding == binding) {
            return cur;
        }
    }
    return nullptr;
}
const SpvReflectDescriptorSet* Descriptors::_retrieve_set(Module shader, uint32_t set) {
    const SpvReflectShaderModule& mod = shader->get_reflection().GetShaderModule();
    size_t list_size = mod.descriptor_set_count;
    const SpvReflectDescriptorSet* list = mod.descriptor_sets;
    for (size_t i = 0; i < list_size; i++) {
        const SpvReflectDescriptorSet* cur = &list[i];
        if (cur->set == set) {
            return cur;
        }
    }
    return nullptr;
}

PushConstant::PushConstant(::vk::ShaderStageFlags stage_flags,
                           const SpvReflectBlockVariable& reflection) :
    _stage_flags(stage_flags),
    _reflection(reflection) {}
PushConstant::PushConstant(const PushConstant& o) :
    _stage_flags(o._stage_flags), _reflection(o._reflection) {}
PushConstant& PushConstant::operator=(const PushConstant& o) {
    _stage_flags = o._stage_flags;
    _reflection = o._reflection;
    return *this;
}
::vk::ShaderStageFlags PushConstant::get_stages() const {
    return _stage_flags;
}
size_t PushConstant::get_offset() const {
    return _reflection.get().offset;
}
size_t PushConstant::get_size() const {
    return _reflection.get().size;
}
::vk::PushConstantRange PushConstant::get_range() const {
    return ::vk::PushConstantRange(get_stages(), get_offset(), get_size());
}

PushConstants::PushConstants(std::span<Module> shader_modules) {
    // Mask of modules registered in shader tree.
    std::vector<bool> shader_modules_mask(shader_modules.size(), false);
    // 1st pass: Verify.
    for (size_t i = 0; i < shader_modules.size(); i++) {
        Module& shi = shader_modules[i];
        const SpvReflectShaderModule& modi = shi->get_reflection().GetShaderModule();
        // Invalid to have more than 1 push constant block.
        if (modi.push_constant_block_count > 1) {
            spec::fatal("PushConstants: more than 1 push constant block.");
        }
        // If module has push constants:
        if (modi.push_constant_block_count != 0) {
            // Require reflection information.
            const char* namei = modi.push_constant_blocks[0].name;
            if (namei == nullptr || std::strlen(namei) == 0) {
                spec::fatal("PushConstants: reflection info not found!");
            }
        }
    }
    // 2nd pass: Group shader stages together based on push constants block names.
    for (size_t i = 0; i < shader_modules.size(); i++) {
        Module& shi = shader_modules[i];
        const SpvReflectShaderModule& modi = shi->get_reflection().GetShaderModule();
        // If not already registered and if it contains the push constant block:
        if (!shader_modules_mask[i] && modi.push_constant_block_count != 0) {
            const char* namei = modi.push_constant_blocks[0].name;
            // Create a new group.
            std::vector<Module> cur_reg = {shi};
            shader_modules_mask[i] = true;
            // Also append any other non-registered stages with matching push constant blocks.
            for (size_t j = i + 1; j < shader_modules.size(); j++) {
                Module& shj = shader_modules[j];
                const SpvReflectShaderModule& modj = shj->get_reflection().GetShaderModule();
                if (!shader_modules_mask[j] && modj.push_constant_block_count != 0) {
                    const char* namej = modj.push_constant_blocks[0].name;
                    if (std::strcmp(namei, namej) == 0) {
                        // Verify that block structures match.
                        if (modi.push_constant_blocks[0].offset !=
                                 modj.push_constant_blocks[0].offset ||
                            modi.push_constant_blocks[0].size !=
                                 modj.push_constant_blocks[0].size ||
                            modi.push_constant_blocks[0].member_count !=
                                 modj.push_constant_blocks[0].member_count) {
                            spec::fatal("PushConstants: structure mismatch between stages with "
                                        "blocks of the same name!");
                        }
                        cur_reg.push_back(shj);
                        shader_modules_mask[j] = true;
                    }
                }
            }
            // Finalize group.
            cur_reg.shrink_to_fit();
            _shader_groups.push_back(std::move(cur_reg));
        }
    }
    _shader_groups.shrink_to_fit();
}
std::optional<PushConstant> PushConstants::retrieve_by_name(const char* name_key,
                                                            size_t name_key_len) const {
    for (const std::vector<Module>& group : _shader_groups) {
        // Shaders in the same group have the same (push constant block) name.
        const SpvReflectBlockVariable& refl =
             group[0]->get_reflection().GetShaderModule().push_constant_blocks[0];
        const char* group_name = refl.name;
        if (std::strncmp(group_name, name_key, name_key_len) == 0) {
            return PushConstant(_combine_stage_flag_bits(group), refl);
        }
    }
    return {};
}
std::optional<PushConstant> PushConstants::retrieve_by_stage(
     ::vk::ShaderStageFlags target_stages) const {
    for (const std::vector<Module>& group : _shader_groups) {
        ::vk::ShaderStageFlags group_flags = _combine_stage_flag_bits(group);
        // Match only if all the stages set in `target_stages` are present in `group_flags`.
        if ((static_cast<uint32_t>(target_stages & (~group_flags))) == 0) {
            const SpvReflectBlockVariable& refl =
                 group[0]->get_reflection().GetShaderModule().push_constant_blocks[0];
            return PushConstant(group_flags, refl);
        }
    }
    return {};
}
std::vector<::vk::PushConstantRange> PushConstants::get_ranges() const {
    std::vector<::vk::PushConstantRange> ranges;
    ranges.reserve(_shader_groups.size());
    for (const std::vector<Module>& group : _shader_groups) {
        const SpvReflectBlockVariable& refl =
             group[0]->get_reflection().GetShaderModule().push_constant_blocks[0];
        auto push_const = PushConstant(_combine_stage_flag_bits(group), refl);
        ranges.push_back(push_const.get_range());
    }
    return ranges;
}
::vk::ShaderStageFlags PushConstants::_combine_stage_flag_bits(
     const std::vector<Module>& stages) {
    ::vk::ShaderStageFlags flags;  // default initialized to 0.
    for (const auto& stage : stages) {
        flags |= stage->get_stage_type();
    }
    return flags;
}

VertexAttribute::VertexAttribute(const SpvReflectInterfaceVariable& reflection) :
    _reflection(reflection) {}
VertexAttribute::VertexAttribute(const VertexAttribute& o) : _reflection(o._reflection) {}
VertexAttribute& VertexAttribute::operator=(const VertexAttribute& o) {
    _reflection = o._reflection;
    return *this;
}
std::string_view VertexAttribute::get_name() const {
    return std::string_view(_reflection.get().name);
}
uint32_t VertexAttribute::get_location() const {
    return _reflection.get().location;
}
::vk::Format VertexAttribute::get_format() const {
    return static_cast<::vk::Format>(_reflection.get().format);
}
bool VertexAttribute::is_array() const {
    return _reflection.get().array.dims_count != 0;
}
std::span<const uint32_t> VertexAttribute::get_array_dims() const {
    const uint32_t* dims = _reflection.get().array.dims;
    uint32_t dims_count = _reflection.get().array.dims_count;
    return std::span(dims, dims_count);
}

VertexAttributes::VertexAttributes(std::span<Module> shader_modules) :
    _shader_modules(shader_modules) {
    for (size_t i = 0; i < _shader_modules.size(); i++) {
        if (_shader_modules[i].has_vertex_input_attributes()) {
            const SpvReflectShaderModule& x_mod =
                 _shader_modules[i].get_reflection().GetShaderModule();
            for (size_t x = 0; x < x_mod.input_variable_count; x++) {
                // Only if variable is not a built-in.
                if (!(x_mod.input_variables[x]->decoration_flags &
                      SPV_REFLECT_DECORATION_BUILT_IN)) {

                    if (x_mod.input_variables[x]->component !=
                        std::numeric_limits<uint32_t>::max()) {
                        spec::fatal("VertexAttributes: non-default component!");
                    }

                    // Check if reflection data is available.
                    const char* name = x_mod.input_variables[x]->name;
                    size_t name_len = (name == nullptr) ? 0 : std::strlen(name);
                    uint32_t location = x_mod.input_variables[x]->location;

                    if (name_len != 0) {
                        for (size_t j = i + 1; j < _shader_modules.size(); j++) {
                            if (_shader_modules[j].has_vertex_input_attributes()) {
                                // Check interface variable consistency across modules.
                                const SpvReflectInterfaceVariable* name_match =
                                     _retrieve_by_name(shader_modules[j], name, name_len);
                                const SpvReflectInterfaceVariable* loc_match =
                                     _retrieve_by_location(shader_modules[j], location);

                                if (name_match == loc_match) {
                                    if (name_match != nullptr) {
                                        // name and location match, test the rest.
                                        if (name_match->format !=
                                            x_mod.input_variables[x]->format) {
                                            spec::fatal("VertexAttributes: format mismatch!");
                                        }
                                        if (name_match->array.dims_count ==
                                            x_mod.input_variables[x]->array.dims_count) {
                                            for (uint32_t dim = 0;
                                                 dim < name_match->array.dims_count; dim++) {
                                                if (name_match->array.dims[dim] !=
                                                    x_mod.input_variables[x]->array.dims[dim]) {
                                                    spec::fatal("VertexAttributes: array dims "
                                                                "mismatch!");
                                                }
                                            }
                                        }
                                        else {
                                            spec::fatal("VertexAttributes: array dims count "
                                                        "mismatch!");
                                        }
                                        // blocks are not supported for vertex inputs, skip.
                                    }
                                }
                                else {
                                    spec::fatal("VertexAttributes: name or location mismatch!");
                                }
                            }
                        }
                    }
                    else {
                        spec::fatal("Shader module reflection data for vertex input attribute "
                                    "is not available!");
                    }
                }
            }
        }
    }
}

std::optional<VertexAttribute> VertexAttributes::retrieve_by_name(const char* name_key,
                                                                  size_t name_key_len) {
    for (auto& shader : _shader_modules) {
        if (shader.has_vertex_input_attributes()) {
            auto inter_var = _retrieve_by_name(shader, name_key, name_key_len);
            if (inter_var != nullptr) {
                return VertexAttribute(*inter_var);
            }
        }
    }
    return {};
}
std::optional<VertexAttribute> VertexAttributes::retrieve_by_location(uint32_t location_key) {
    for (auto& shader : _shader_modules) {
        if (shader.has_vertex_input_attributes()) {
            auto inter_var = _retrieve_by_location(shader, location_key);
            if (inter_var != nullptr) {
                return VertexAttribute(*inter_var);
            }
        }
    }
    return {};
}

const SpvReflectInterfaceVariable* VertexAttributes::_retrieve_by_name(Module shader,
                                                                       const char* name_key,
                                                                       size_t name_key_len) {
    const SpvReflectShaderModule& mod = shader->get_reflection().GetShaderModule();
    size_t list_size = mod.input_variable_count;
    SpvReflectInterfaceVariable** list = mod.input_variables;
    for (size_t i = 0; i < list_size; i++) {
        const SpvReflectInterfaceVariable* cur = list[i];
        if (cur->name != nullptr && std::strncmp(cur->name, name_key, name_key_len) == 0) {
            return cur;
        }
    }
    return nullptr;
}
const SpvReflectInterfaceVariable* VertexAttributes::_retrieve_by_location(
     Module shader, uint32_t location_key) {
    const SpvReflectShaderModule& mod = shader->get_reflection().GetShaderModule();
    size_t list_size = mod.input_variable_count;
    SpvReflectInterfaceVariable** list = mod.input_variables;
    for (size_t i = 0; i < list_size; i++) {
        const SpvReflectInterfaceVariable* cur = list[i];
        if (cur->location == location_key) {
            return cur;
        }
    }
    return nullptr;
}

}  // namespace satr::vk
