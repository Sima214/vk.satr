#include "Info.hpp"

#include <Framebuffer.hpp>
#include <Shaders.hpp>
#include <config/Tunables.hpp>
#include <logger/Logger.hpp>
#include <trace/Trace.hpp>
#include <vulkan/vulkan_to_string.hpp>

#include <algorithm>
#include <libconfig.h++>
#include <limits>
#include <stdexcept>
#include <string>
#include <type_traits>

namespace satr::vk {

bool StageConfig::load(const std::string& buffer) {
    libconfig::Config cfg;
    try {
        cfg.readString(buffer);
    }
    catch (const libconfig::ParseException& pex) {
        logger.loge("Error parsing module's extra info from string at line ", pex.getLine(),
                    "(error: ", pex.getError(), ").");
        return false;
    }
    return parse(cfg);
}

bool StageConfig::load(const std::filesystem::path& path) {
    libconfig::Config cfg;
    try {
        cfg.readFile(path);
    }
    catch (const libconfig::FileIOException& fioex) {
        logger.loge("Error loading file ", path, " with module's extra info(", fioex.what(),
                    ").");
        return false;
    }
    catch (const libconfig::ParseException& pex) {
        logger.loge("Error parsing module's extra info from ", pex.getFile(), "@",
                    pex.getLine(), "(error: ", pex.getError(), ").");
        return false;
    }
    return parse(cfg);
}

bool StageConfig::parse(const libconfig::Config& cfg) {
    cfg.lookupValue("allow_varying_subgroup_size", _allow_varying_subgroup_size);
    cfg.lookupValue("require_full_subgroups", _require_full_subgroups);
    return true;
}

bool StageConfig::has_specialization_entry(uint32_t constant_id) const {
    const size_t n = _specialization_entries.size();
    for (size_t i = 0; i < n; i++) {
        if (_specialization_entries[i].constantID == constant_id) {
            return true;
        }
    }
    return false;
}
std::pair<::vk::SpecializationMapEntry, std::span<const uint8_t>>
StageConfig::get_specialization_entry(uint32_t constant_id) const {
    const size_t n = _specialization_entries.size();
    for (size_t i = 0; i < n; i++) {
        auto& entry = _specialization_entries[i];
        if (entry.constantID == constant_id) {
            auto specialization_buffer_view =
                 std::span(_specialization_buffer.begin(), _specialization_buffer.end());
            auto specialization_buffer_subview =
                 specialization_buffer_view.subspan(entry.offset, entry.size);
            return {entry, specialization_buffer_subview};
        }
    }
    return {{std::numeric_limits<uint32_t>::max()}, {}};
}
void StageConfig::set_specialization_entry(uint32_t constant_id,
                                           std::span<const uint8_t> data) {
    {  // Use already existing map entry, if available.
        const size_t n = _specialization_entries.size();
        for (size_t i = 0; i < n; i++) {
            auto& entry = _specialization_entries[i];
            if (entry.constantID == constant_id) {
                if (entry.size != data.size()) [[unlikely]] {
                    throw std::runtime_error("StageConfig::set_specialization_entry: "
                                             "entry.size != data.size()");
                }
                std::memcpy(&_specialization_buffer.at(entry.offset), data.data(), data.size());
                return;
            }
        }
    }
    // Add new mapping entry when one was not available.
    const size_t offset = _specialization_buffer.size();
    const size_t size = data.size();
    ::vk::SpecializationMapEntry entry(constant_id, offset, size);
    _specialization_entries.push_back(entry);
    // Pre-allocate buffer space and copy data into it.
    _specialization_buffer.resize(offset + size);
    std::memcpy(&_specialization_buffer.at(offset), data.data(), size);
}
void StageConfig::specialization_clear() {
    _specialization_entries.clear();
    _specialization_buffer.clear();
}

::vk::PipelineShaderStageCreateInfo StageConfig::get_partial_create_info() {
    ::vk::PipelineShaderStageCreateInfo cinfo;
    if (_allow_varying_subgroup_size) {
        cinfo.flags |= ::vk::PipelineShaderStageCreateFlagBits::eAllowVaryingSubgroupSize;
    }
    if (_require_full_subgroups) {
        cinfo.flags |= ::vk::PipelineShaderStageCreateFlagBits::eRequireFullSubgroups;
    }
    if (!_specialization_entries.empty()) {
        _cinfo_specialization.mapEntryCount = _specialization_entries.size();
        _cinfo_specialization.pMapEntries = _specialization_entries.data();
        _cinfo_specialization.dataSize = _specialization_buffer.size();
        _cinfo_specialization.pData = _specialization_buffer.data();
        cinfo.pSpecializationInfo = &_cinfo_specialization;
    }
    return cinfo;
}

bool PipelineOptions::parse(const libconfig::Config& cfg) {
    cfg.lookupValue("disable_optimization", _disable_optimization);
    cfg.lookupValue("allow_derivatives", _allow_derivatives);
    cfg.lookupValue("derivative", _derivative);
    cfg.lookupValue("view_index_from_device_index", _view_index_from_device_index);
    cfg.lookupValue("dispatch_base", _dispatch_base);
    cfg.lookupValue("fail_on_pipeline_compile_required", _fail_on_pipeline_compile_required);
    cfg.lookupValue("early_return_on_failure", _early_return_on_failure);
    cfg.lookupValue("capture_statistics", _capture_statistics);
    cfg.lookupValue("capture_internal_representations", _capture_internal_representations);
    cfg.lookupValue("library", _library);
    cfg.lookupValue("rendering_fragment_shading_rate_attachment",
                    _rendering_fragment_shading_rate_attachment);
    cfg.lookupValue("rendering_fragment_density_map_attachment",
                    _rendering_fragment_density_map_attachment);
    cfg.lookupValue("link_time_optimization", _link_time_optimization);
    cfg.lookupValue("retain_link_time_optimization_info", _retain_link_time_optimization_info);
    cfg.lookupValue("descriptor_buffer", _descriptor_buffer);
    cfg.lookupValue("color_attachment_feedback_loop", _color_attachment_feedback_loop);
    cfg.lookupValue("depth_stencil_attachment_feedback_loop",
                    _depth_stencil_attachment_feedback_loop);
    cfg.lookupValue("no_protected_access", _no_protected_access);
    cfg.lookupValue("protected_access_only", _protected_access_only);
    cfg.lookupValue("ray_tracing_no_null_any_hit_shaders",
                    _ray_tracing_no_null_any_hit_shaders);
    cfg.lookupValue("ray_tracing_no_null_closest_hit_shaders",
                    _ray_tracing_no_null_closest_hit_shaders);
    cfg.lookupValue("ray_tracing_no_null_miss_shaders", _ray_tracing_no_null_miss_shaders);
    cfg.lookupValue("ray_tracing_no_null_intersection_shaders",
                    _ray_tracing_no_null_intersection_shaders);
    cfg.lookupValue("ray_tracing_skip_triangles", _ray_tracing_skip_triangles);
    cfg.lookupValue("ray_tracing_skip_aabbs", _ray_tracing_skip_aabbs);
    cfg.lookupValue("ray_tracing_shader_group_handle_capture_replay",
                    _ray_tracing_shader_group_handle_capture_replay);
    cfg.lookupValue("ray_tracing_opacity_micromap", _ray_tracing_opacity_micromap);

    return true;
}

::vk::PipelineCreateFlags PipelineOptions::get_create_info() const {
    ::vk::PipelineCreateFlags flags;
    if (_disable_optimization) {
        flags |= ::vk::PipelineCreateFlagBits::eDisableOptimization;
    }
    if (_allow_derivatives) {
        flags |= ::vk::PipelineCreateFlagBits::eAllowDerivatives;
    }
    if (_derivative) {
        flags |= ::vk::PipelineCreateFlagBits::eDerivative;
    }
    if (_view_index_from_device_index) {
        flags |= ::vk::PipelineCreateFlagBits::eViewIndexFromDeviceIndex;
    }
    if (_dispatch_base) {
        flags |= ::vk::PipelineCreateFlagBits::eDispatchBase;
    }
    if (_fail_on_pipeline_compile_required) {
        flags |= ::vk::PipelineCreateFlagBits::eFailOnPipelineCompileRequired;
    }
    if (_early_return_on_failure) {
        flags |= ::vk::PipelineCreateFlagBits::eEarlyReturnOnFailure;
    }
    if (_capture_statistics) {
        flags |= ::vk::PipelineCreateFlagBits::eCaptureStatisticsKHR;
    }
    if (_capture_internal_representations) {
        flags |= ::vk::PipelineCreateFlagBits::eCaptureInternalRepresentationsKHR;
    }
    if (_library) {
        flags |= ::vk::PipelineCreateFlagBits::eLibraryKHR;
    }
    if (_rendering_fragment_shading_rate_attachment) {
        flags |= ::vk::PipelineCreateFlagBits::eRenderingFragmentShadingRateAttachmentKHR;
    }
    if (_rendering_fragment_density_map_attachment) {
        flags |= ::vk::PipelineCreateFlagBits::eRenderingFragmentDensityMapAttachmentEXT;
    }
    if (_link_time_optimization) {
        flags |= ::vk::PipelineCreateFlagBits::eLinkTimeOptimizationEXT;
    }
    if (_retain_link_time_optimization_info) {
        flags |= ::vk::PipelineCreateFlagBits::eRetainLinkTimeOptimizationInfoEXT;
    }
    if (_descriptor_buffer) {
        flags |= ::vk::PipelineCreateFlagBits::eDescriptorBufferEXT;
    }
    if (_color_attachment_feedback_loop) {
        flags |= ::vk::PipelineCreateFlagBits::eColorAttachmentFeedbackLoopEXT;
    }
    if (_depth_stencil_attachment_feedback_loop) {
        flags |= ::vk::PipelineCreateFlagBits::eDepthStencilAttachmentFeedbackLoopEXT;
    }
    if (_no_protected_access) {
        flags |= ::vk::PipelineCreateFlagBits::eNoProtectedAccessEXT;
    }
    if (_protected_access_only) {
        flags |= ::vk::PipelineCreateFlagBits::eProtectedAccessOnlyEXT;
    }
    if (_ray_tracing_no_null_any_hit_shaders) {
        flags |= ::vk::PipelineCreateFlagBits::eRayTracingNoNullAnyHitShadersKHR;
    }
    if (_ray_tracing_no_null_closest_hit_shaders) {
        flags |= ::vk::PipelineCreateFlagBits::eRayTracingNoNullClosestHitShadersKHR;
    }
    if (_ray_tracing_no_null_miss_shaders) {
        flags |= ::vk::PipelineCreateFlagBits::eRayTracingNoNullMissShadersKHR;
    }
    if (_ray_tracing_no_null_intersection_shaders) {
        flags |= ::vk::PipelineCreateFlagBits::eRayTracingNoNullIntersectionShadersKHR;
    }
    if (_ray_tracing_skip_triangles) {
        flags |= ::vk::PipelineCreateFlagBits::eRayTracingSkipTrianglesKHR;
    }
    if (_ray_tracing_skip_aabbs) {
        flags |= ::vk::PipelineCreateFlagBits::eRayTracingSkipAabbsKHR;
    }
    if (_ray_tracing_shader_group_handle_capture_replay) {
        flags |= ::vk::PipelineCreateFlagBits::eRayTracingShaderGroupHandleCaptureReplayKHR;
    }
    if (_ray_tracing_opacity_micromap) {
        flags |= ::vk::PipelineCreateFlagBits::eRayTracingOpacityMicromapEXT;
    }

    return flags;
}

::vk::PipelineLayout PipelineOptions::make_pipeline_layout(
     ::vk::Device device, Descriptors& descriptors, const PushConstants& push_consts) const {
    // We are responsible for triggering the allocation of VkDescriptorSetLayout(s).
    descriptors.create_set_layouts(device);

    // Build and collect the lists of objects required for create info.
    const auto& set_layouts_table = descriptors.get_set_layouts();
    std::vector<::vk::DescriptorSetLayout> set_layouts(set_layouts_table.size());
    if (std::transform(set_layouts_table.begin(), set_layouts_table.end(), set_layouts.begin(),
                       [](std::pair<uint32_t, ::vk::DescriptorSetLayout> set_layout) {
                           return set_layout.second;
                       }) != set_layouts.end()) {
        spec::fatal("PipelineOptions::make_pipeline_layout -> "
                    "cannot build set layout list!");
    }
    std::vector<::vk::PushConstantRange> push_const_ranges = push_consts.get_ranges();

    ::vk::PipelineLayoutCreateInfo cinfo;
    if (_layout_create_independent_sets) {
        cinfo.flags |= ::vk::PipelineLayoutCreateFlagBits::eIndependentSetsEXT;
    }
    cinfo.setLayoutCount = set_layouts.size();
    cinfo.pSetLayouts = set_layouts.data();
    cinfo.pushConstantRangeCount = push_const_ranges.size();
    cinfo.pPushConstantRanges = push_const_ranges.data();

    auto [r, h] = device.createPipelineLayout(cinfo);
    if (r != ::vk::Result::eSuccess) {
        spec::fatal("PipelineOptions::make_pipeline_layout -> ", ::vk::to_string(r));
    }
    return h;
}

void GraphicsPipelineOptions::dynamic_state_add(::vk::DynamicState v) {
    _dynamic_states.push_back(v);
}
bool GraphicsPipelineOptions::dynamic_state_has(::vk::DynamicState v) const {
    return std::find(_dynamic_states.begin(), _dynamic_states.end(), v) !=
           _dynamic_states.end();
}

bool GraphicsPipelineOptions::vertex_input_bindings_add(::vk::VertexInputBindingDescription v) {
    // VUID-VkPipelineVertexInputStateCreateInfo-pVertexBindingDescriptions-00616
    auto it = std::find_if(
         _vertex_input_bindings.begin(), _vertex_input_bindings.end(),
         [&v](::vk::VertexInputBindingDescription e) { return e.binding == v.binding; });
    if (it != _vertex_input_bindings.end()) {
        // Duplicate descriptor found.
        return false;
    }
    _vertex_input_bindings.push_back(v);
    return true;
}
std::optional<::vk::VertexInputBindingDescription>
GraphicsPipelineOptions::vertex_input_bindings_find_from_binding(uint32_t binding) const {
    auto it = std::find_if(
         _vertex_input_bindings.begin(), _vertex_input_bindings.end(),
         [binding](::vk::VertexInputBindingDescription v) { return v.binding == binding; });
    if (it == _vertex_input_bindings.end()) {
        return {};
    }
    else {
        return *it;
    }
}

bool GraphicsPipelineOptions::vertex_input_attributes_add(VertexInputAttributeFinder v) {
    // VUID-VkPipelineVertexInputStateCreateInfo-pVertexAttributeDescriptions-00617
    auto it = std::find_if(_vertex_input_attributes.begin(), _vertex_input_attributes.end(),
                           [&v](VertexInputAttributeFinder e) { return e.name == v.name; });
    if (it != _vertex_input_attributes.end()) {
        // Duplicate descriptor found.
        return false;
    }
    _vertex_input_attributes.push_back(v);
    return true;
}

bool GraphicsPipelineOptions::set_viewport_state(::vk::Viewport v, ::vk::Rect2D s) {
    bool dynamic_viewport = dynamic_state_has(::vk::DynamicState::eViewport);
    bool dynamic_scissor = dynamic_state_has(::vk::DynamicState::eScissor);
    if (!dynamic_viewport) {
        _viewport_state.first = v;
    }
    if (!dynamic_scissor) {
        _viewport_state.second = s;
    }
    return !dynamic_viewport && !dynamic_scissor;
}
// TODO: glm rect compatibility.
std::optional<::vk::Viewport> GraphicsPipelineOptions::get_viewport() const {
    if (dynamic_state_has(::vk::DynamicState::eViewport)) {
        return {};
    }
    return _viewport_state.first;
}
std::optional<::vk::Rect2D> GraphicsPipelineOptions::get_viewport_scissor() const {
    if (dynamic_state_has(::vk::DynamicState::eScissor)) {
        return {};
    }
    return _viewport_state.second;
}

GraphicsPipelineOptions::DynamicGraphicsPipelineCreateInfo
GraphicsPipelineOptions::make_partial_create_info() {
    // Opportunity to optimize storage.
    _dynamic_states.shrink_to_fit();
    _vertex_input_bindings.shrink_to_fit();
    _vertex_input_attributes.shrink_to_fit();
    return std::as_const(*this).make_partial_create_info();
}

GraphicsPipelineOptions::DynamicGraphicsPipelineCreateInfo
GraphicsPipelineOptions::make_partial_create_info() const {
    DynamicGraphicsPipelineCreateInfo cinfo;
    cinfo.flags = get_create_info();

    cinfo.vertex_input_state().vertexBindingDescriptionCount = _vertex_input_bindings.size();
    cinfo.vertex_input_state().pVertexBindingDescriptions = _vertex_input_bindings.data();
    cinfo.vertex_input_state().vertexAttributeDescriptionCount =
         _vertex_input_attributes.size();

    cinfo.input_assembly_state().topology = _primitive_topology;
    cinfo.input_assembly_state().primitiveRestartEnable = _primitive_restart_enable;

    cinfo.tessellation_state().patchControlPoints = _tessellation_patch_control_points;

    if (dynamic_state_has(::vk::DynamicState::eViewport)) {
        cinfo.viewport_state().viewportCount = 1;
    }
    else {
        spec::fatal("Unimplemented: non-dynamic viewport.");
    }
    if (dynamic_state_has(::vk::DynamicState::eScissor)) {
        cinfo.viewport_state().scissorCount = 1;
    }
    else {
        spec::fatal("Unimplemented: non-dynamic viewport scissor.");
    }

    cinfo.rasterizer_state().depthClampEnable = _rasterizer_depth_clamp_enable;
    cinfo.rasterizer_state().rasterizerDiscardEnable = _rasterizer_discard_enable;
    cinfo.rasterizer_state().polygonMode = _rasterizer_polygon_mode;
    cinfo.rasterizer_state().cullMode = _rasterizer_cull_mode;
    cinfo.rasterizer_state().frontFace = _rasterizer_front_face;
    cinfo.rasterizer_state().depthBiasEnable = _rasterizer_depth_clamp_enable;
    cinfo.rasterizer_state().depthBiasConstantFactor = _rasterizer_depth_bias_constant_factor;
    cinfo.rasterizer_state().depthBiasClamp = _rasterizer_depth_bias_clamp;
    cinfo.rasterizer_state().depthBiasSlopeFactor = _rasterizer_depth_bias_slope_factor;
    cinfo.rasterizer_state().lineWidth = _rasterizer_line_width;

    cinfo.multisample_state().rasterizationSamples = _multisample_rasterization_samples;
    cinfo.multisample_state().sampleShadingEnable = _multisample_sample_shading_enable;
    cinfo.multisample_state().minSampleShading = _multisample_min_sample_shading;
    // cinfo.multisample_state().pSampleMask
    cinfo.multisample_state().alphaToCoverageEnable = _multisample_alpha_to_coverage_enable;
    cinfo.multisample_state().alphaToOneEnable = _multisample_alpha_to_one_enable;

    cinfo.depth_stencil_state().depthTestEnable = !_depth_test_disabled;
    cinfo.depth_stencil_state().depthWriteEnable = !_depth_write_disabled;
    cinfo.depth_stencil_state().depthCompareOp = _depth_compare_op;
    cinfo.depth_stencil_state().depthBoundsTestEnable = _depth_bounds_test_enable;
    cinfo.depth_stencil_state().stencilTestEnable = false;
    // cinfo.depth_stencil_state().front;
    // cinfo.depth_stencil_state().back;
    cinfo.depth_stencil_state().minDepthBounds = _depth_min_bounds;
    cinfo.depth_stencil_state().maxDepthBounds = _depth_max_bounds;

    cinfo.color_blend_state().logicOpEnable = _color_blend_logic_op_enable;
    cinfo.color_blend_state().logicOp = _color_blend_logic_op;
    cinfo.color_blend_state().blendConstants[0] = _color_blend_constants.r;
    cinfo.color_blend_state().blendConstants[1] = _color_blend_constants.g;
    cinfo.color_blend_state().blendConstants[2] = _color_blend_constants.b;
    cinfo.color_blend_state().blendConstants[3] = _color_blend_constants.a;

    cinfo.dynamic_state().dynamicStateCount = _dynamic_states.size();
    cinfo.dynamic_state().pDynamicStates = _dynamic_states.data();

    return cinfo;
}

void GraphicsPipelineOptions::finalize_create_info(DynamicGraphicsPipelineCreateInfo& cinfo,
                                                   std::span<Module> shader_modules,
                                                   VertexAttributes& vertex_attributes,
                                                   const FramebufferChain& framebuffer,
                                                   ::vk::PipelineLayout layout) const {
    // Validate arguements.
    if (!framebuffer) {
        spec::fatal("GraphicsPipelineOptions::finalize_create_info: "
                    "invalid framebuffer chain!");
    }

    cinfo.shader_stages().clear();
    cinfo.shader_stages().reserve(shader_modules.size());
    for (size_t i = 0; i < shader_modules.size(); i++) {
        cinfo.shader_stages().push_back(shader_modules[i].make_stage_create_info());
    }
    cinfo.stageCount = cinfo.shader_stages().size();
    cinfo.pStages = cinfo.shader_stages().data();

    // Querry input attribute locations.
    cinfo.vertex_input_attributes().clear();
    cinfo.vertex_input_attributes().reserve(_vertex_input_attributes.size());
    for (auto& find_info : _vertex_input_attributes) {
        auto vert_attr = vertex_attributes.retrieve_by_name(find_info.name);
        if (vert_attr) {
            ::vk::VertexInputAttributeDescription vert_attr_desc(
                 vert_attr->get_location(), find_info.binding,
                 find_info.fmt_override == ::vk::Format::eUndefined ? vert_attr->get_format() :
                                                                      find_info.fmt_override,
                 find_info.offset);
            cinfo.vertex_input_attributes().push_back(vert_attr_desc);
        }
        else {
            logger.logw("GraphicsPipelineOptions::finalize_create_info: "
                        "Couldn't find vertex input attribute with name `",
                        find_info.name, "`.");
        }
    }
    cinfo.vertex_input_state().vertexAttributeDescriptionCount =
         cinfo.vertex_input_attributes().size();
    cinfo.vertex_input_state().pVertexAttributeDescriptions =
         cinfo.vertex_input_attributes().data();

    // Confirm final depth test state based on passed framebuffer.
    if (cinfo.depth_stencil_state().depthTestEnable && !framebuffer.has_depth_attachment()) {
        logger.logw("GraphicsPipelineOptions::finalize_create_info: "
                    "framebuffer chain is missing depth attachment, disabling depth test.");
        cinfo.depth_stencil_state().depthTestEnable = false;
    }

    // Confirm final values based on passed framebuffer.
    ::vk::PipelineColorBlendAttachmentState color_blend_state;
    color_blend_state.blendEnable = _color_blend_enable;
    color_blend_state.srcColorBlendFactor = _color_blend_src_factor;
    color_blend_state.dstColorBlendFactor = _color_blend_dst_factor;
    color_blend_state.colorBlendOp = _color_blend_op;
    color_blend_state.srcAlphaBlendFactor = _alpha_blend_src_factor;
    color_blend_state.dstAlphaBlendFactor = _alpha_blend_dst_factor;
    color_blend_state.alphaBlendOp = _alpha_blend_op;
    color_blend_state.colorWriteMask = _color_write_mask;

    auto color_blend_states =
         std::vector(framebuffer.get_colors_attachment_count(), color_blend_state);
    cinfo.color_blend_states() = std::move(color_blend_states);
    cinfo.color_blend_state().attachmentCount = framebuffer.get_colors_attachment_count();
    cinfo.color_blend_state().pAttachments = cinfo.color_blend_states().data();

    // TODO: Match and verify fragment shader outputs with passed framebuffer's color
    // attachments.

    cinfo.layout = layout;
    cinfo.renderPass = framebuffer.get_renderpass();
}

GraphicsPipelineOptions::DynamicGraphicsPipelineCreateInfo::
     DynamicGraphicsPipelineCreateInfo() noexcept {
    spec::trace("GraphicsPipelineOptions::DynamicGraphicsPipelineCreateInfo::new");
    __update_pointers();
}
GraphicsPipelineOptions::DynamicGraphicsPipelineCreateInfo::DynamicGraphicsPipelineCreateInfo(
     DynamicGraphicsPipelineCreateInfo&& o) noexcept :
    _shader_stages_cinfo(std::move(o._shader_stages_cinfo)),
    _vertex_input_attributes(std::move(o._vertex_input_attributes)),
    _vertex_input_state_cinfo(std::move(o._vertex_input_state_cinfo)),
    _input_assembly_state_cinfo(std::move(o._input_assembly_state_cinfo)),
    _tessellation_state_cinfo(std::move(o._tessellation_state_cinfo)),
    _viewport_state_cinfo(std::move(o._viewport_state_cinfo)),
    _rasterizer_state_cinfo(std::move(o._rasterizer_state_cinfo)),
    _multisample_state_cinfo(std::move(o._multisample_state_cinfo)),
    _depth_stencil_state_cinfo(std::move(o._depth_stencil_state_cinfo)),
    _color_blend_states(std::move(o._color_blend_states)),
    _color_blend_state_cinfo(std::move(o._color_blend_state_cinfo)),
    _dynamic_state_cinfo(std::move(o._dynamic_state_cinfo)) {
    __update_pointers();
}
GraphicsPipelineOptions::DynamicGraphicsPipelineCreateInfo&
GraphicsPipelineOptions::DynamicGraphicsPipelineCreateInfo::operator=(
     DynamicGraphicsPipelineCreateInfo&& o) noexcept {
    _shader_stages_cinfo = std::move(o._shader_stages_cinfo);
    _vertex_input_attributes = std::move(o._vertex_input_attributes);
    _vertex_input_state_cinfo = std::move(o._vertex_input_state_cinfo);
    _input_assembly_state_cinfo = std::move(o._input_assembly_state_cinfo);
    _tessellation_state_cinfo = std::move(o._tessellation_state_cinfo);
    _viewport_state_cinfo = std::move(o._viewport_state_cinfo);
    _rasterizer_state_cinfo = std::move(o._rasterizer_state_cinfo);
    _multisample_state_cinfo = std::move(o._multisample_state_cinfo);
    _depth_stencil_state_cinfo = std::move(o._depth_stencil_state_cinfo);
    _color_blend_states = std::move(o._color_blend_states);
    _color_blend_state_cinfo = std::move(o._color_blend_state_cinfo);
    _dynamic_state_cinfo = std::move(o._dynamic_state_cinfo);
    __update_pointers();
    return *this;
}
GraphicsPipelineOptions::DynamicGraphicsPipelineCreateInfo::
     ~DynamicGraphicsPipelineCreateInfo() = default;
void GraphicsPipelineOptions::DynamicGraphicsPipelineCreateInfo::__update_pointers() {
    stageCount = _shader_stages_cinfo.size();
    pStages = _shader_stages_cinfo.data();
    _vertex_input_state_cinfo.vertexAttributeDescriptionCount = _vertex_input_attributes.size();
    _vertex_input_state_cinfo.pVertexAttributeDescriptions = _vertex_input_attributes.data();
    pVertexInputState = &_vertex_input_state_cinfo;
    pInputAssemblyState = &_input_assembly_state_cinfo;
    pTessellationState = &_tessellation_state_cinfo;
    pViewportState = &_viewport_state_cinfo;
    pRasterizationState = &_rasterizer_state_cinfo;
    pMultisampleState = &_multisample_state_cinfo;
    pDepthStencilState = &_depth_stencil_state_cinfo;
    _color_blend_state_cinfo.attachmentCount = _color_blend_states.size();
    _color_blend_state_cinfo.pAttachments = _color_blend_states.data();
    pColorBlendState = &_color_blend_state_cinfo;
    pDynamicState = &_dynamic_state_cinfo;
}

bool GraphicsPipelineOptions::load(const std::string& buffer) {
    libconfig::Config cfg;
    try {
        cfg.readString(buffer);
    }
    catch (const libconfig::ParseException& pex) {
        logger.loge("Error parsing pipeline info from string at line ", pex.getLine(),
                    "(error: ", pex.getError(), ").");
        return false;
    }
    return parse(cfg);
}

bool GraphicsPipelineOptions::load(const std::filesystem::path& path) {
    libconfig::Config cfg;
    try {
        cfg.readFile(path);
    }
    catch (const libconfig::FileIOException& fioex) {
        logger.loge("Error loading file ", path, " with pipeline info(", fioex.what(), ").");
        return false;
    }
    catch (const libconfig::ParseException& pex) {
        logger.loge("Error parsing pipeline info from ", pex.getFile(), "@", pex.getLine(),
                    "(error: ", pex.getError(), ").");
        return false;
    }
    return parse(cfg);
}

static bool __lookup_value(const libconfig::Setting& cfg, const char* key,
                           ::vk::BlendFactor& value) {
    std::string str;
    if (cfg.lookupValue(key, str)) {
        if (str == "Zero") {
            value = ::vk::BlendFactor::eZero;
        }
        else if (str == "One") {
            value = ::vk::BlendFactor::eOne;
        }
        else if (str == "SrcColor") {
            value = ::vk::BlendFactor::eSrcColor;
        }
        else if (str == "OneMinusSrcColor") {
            value = ::vk::BlendFactor::eOneMinusSrcColor;
        }
        else if (str == "DstColor") {
            value = ::vk::BlendFactor::eDstColor;
        }
        else if (str == "OneMinusDstColor") {
            value = ::vk::BlendFactor::eOneMinusDstColor;
        }
        else if (str == "SrcAlpha") {
            value = ::vk::BlendFactor::eSrcAlpha;
        }
        else if (str == "OneMinusSrcAlpha") {
            value = ::vk::BlendFactor::eOneMinusSrcAlpha;
        }
        else if (str == "DstAlpha") {
            value = ::vk::BlendFactor::eDstAlpha;
        }
        else if (str == "OneMinusDstAlpha") {
            value = ::vk::BlendFactor::eOneMinusDstAlpha;
        }
        else if (str == "ConstantColor") {
            value = ::vk::BlendFactor::eConstantColor;
        }
        else if (str == "OneMinusConstantColor") {
            value = ::vk::BlendFactor::eOneMinusConstantColor;
        }
        else if (str == "ConstantAlpha") {
            value = ::vk::BlendFactor::eConstantAlpha;
        }
        else if (str == "OneMinusConstantAlpha") {
            value = ::vk::BlendFactor::eOneMinusConstantAlpha;
        }
        else if (str == "SrcAlphaSaturate") {
            value = ::vk::BlendFactor::eSrcAlphaSaturate;
        }
        else if (str == "Src1Color") {
            value = ::vk::BlendFactor::eSrc1Color;
        }
        else if (str == "OneMinusSrc1Color") {
            value = ::vk::BlendFactor::eOneMinusSrc1Color;
        }
        else if (str == "Src1Alpha") {
            value = ::vk::BlendFactor::eSrc1Alpha;
        }
        else if (str == "OneMinusSrc1Alpha") {
            value = ::vk::BlendFactor::eOneMinusSrc1Alpha;
        }
        else {
            logger.logw("Encountered invalid enum value for `", cfg.getName(), '_', key,
                        "` while parsing graphics pipeline configuration file!");
            return false;
        }
        return true;
    }
    return false;
}

static bool __lookup_value(const libconfig::Setting& cfg, const char* key,
                           ::vk::BlendOp& value) {
    std::string str;
    if (cfg.lookupValue(key, str)) {
        if (str == "Add") {
            value = ::vk::BlendOp::eAdd;
        }
        else if (str == "Subtract") {
            value = ::vk::BlendOp::eSubtract;
        }
        else if (str == "ReverseSubtract") {
            value = ::vk::BlendOp::eReverseSubtract;
        }
        else if (str == "Min") {
            value = ::vk::BlendOp::eMin;
        }
        else if (str == "Max") {
            value = ::vk::BlendOp::eMax;
        }
        else if (str == "Zero") {
            value = ::vk::BlendOp::eZeroEXT;
        }
        else if (str == "Src") {
            value = ::vk::BlendOp::eSrcEXT;
        }
        else if (str == "Dst") {
            value = ::vk::BlendOp::eDstEXT;
        }
        else if (str == "SrcOver") {
            value = ::vk::BlendOp::eSrcOverEXT;
        }
        else if (str == "DstOver") {
            value = ::vk::BlendOp::eDstOverEXT;
        }
        else if (str == "SrcIn") {
            value = ::vk::BlendOp::eSrcInEXT;
        }
        else if (str == "DstIn") {
            value = ::vk::BlendOp::eDstInEXT;
        }
        else if (str == "SrcOut") {
            value = ::vk::BlendOp::eSrcOutEXT;
        }
        else if (str == "DstOut") {
            value = ::vk::BlendOp::eDstOutEXT;
        }
        else if (str == "SrcAtop") {
            value = ::vk::BlendOp::eSrcAtopEXT;
        }
        else if (str == "DstAtop") {
            value = ::vk::BlendOp::eDstAtopEXT;
        }
        else if (str == "Xor") {
            value = ::vk::BlendOp::eXorEXT;
        }
        else if (str == "Multiply") {
            value = ::vk::BlendOp::eMultiplyEXT;
        }
        else if (str == "Screen") {
            value = ::vk::BlendOp::eScreenEXT;
        }
        else if (str == "Overlay") {
            value = ::vk::BlendOp::eOverlayEXT;
        }
        else if (str == "Darken") {
            value = ::vk::BlendOp::eDarkenEXT;
        }
        else if (str == "Lighten") {
            value = ::vk::BlendOp::eLightenEXT;
        }
        else if (str == "Colordodge") {
            value = ::vk::BlendOp::eColordodgeEXT;
        }
        else if (str == "Colorburn") {
            value = ::vk::BlendOp::eColorburnEXT;
        }
        else if (str == "Hardlight") {
            value = ::vk::BlendOp::eHardlightEXT;
        }
        else if (str == "Softlight") {
            value = ::vk::BlendOp::eSoftlightEXT;
        }
        else if (str == "Difference") {
            value = ::vk::BlendOp::eDifferenceEXT;
        }
        else if (str == "Exclusion") {
            value = ::vk::BlendOp::eExclusionEXT;
        }
        else if (str == "Invert") {
            value = ::vk::BlendOp::eInvertEXT;
        }
        else if (str == "InvertRgb") {
            value = ::vk::BlendOp::eInvertRgbEXT;
        }
        else if (str == "Lineardodge") {
            value = ::vk::BlendOp::eLineardodgeEXT;
        }
        else if (str == "Linearburn") {
            value = ::vk::BlendOp::eLinearburnEXT;
        }
        else if (str == "Vividlight") {
            value = ::vk::BlendOp::eVividlightEXT;
        }
        else if (str == "Linearlight") {
            value = ::vk::BlendOp::eLinearlightEXT;
        }
        else if (str == "Pinlight") {
            value = ::vk::BlendOp::ePinlightEXT;
        }
        else if (str == "Hardmix") {
            value = ::vk::BlendOp::eHardmixEXT;
        }
        else if (str == "HslHue") {
            value = ::vk::BlendOp::eHslHueEXT;
        }
        else if (str == "HslSaturation") {
            value = ::vk::BlendOp::eHslSaturationEXT;
        }
        else if (str == "HslColor") {
            value = ::vk::BlendOp::eHslColorEXT;
        }
        else if (str == "HslLuminosity") {
            value = ::vk::BlendOp::eHslLuminosityEXT;
        }
        else if (str == "Plus") {
            value = ::vk::BlendOp::ePlusEXT;
        }
        else if (str == "PlusClamped") {
            value = ::vk::BlendOp::ePlusClampedEXT;
        }
        else if (str == "PlusClampedAlpha") {
            value = ::vk::BlendOp::ePlusClampedAlphaEXT;
        }
        else if (str == "PlusDarker") {
            value = ::vk::BlendOp::ePlusDarkerEXT;
        }
        else if (str == "Minus") {
            value = ::vk::BlendOp::eMinusEXT;
        }
        else if (str == "MinusClamped") {
            value = ::vk::BlendOp::eMinusClampedEXT;
        }
        else if (str == "Contrast") {
            value = ::vk::BlendOp::eContrastEXT;
        }
        else if (str == "InvertOvg") {
            value = ::vk::BlendOp::eInvertOvgEXT;
        }
        else if (str == "Red") {
            value = ::vk::BlendOp::eRedEXT;
        }
        else if (str == "Green") {
            value = ::vk::BlendOp::eGreenEXT;
        }
        else if (str == "Blue") {
            value = ::vk::BlendOp::eBlueEXT;
        }
        else {
            logger.logw("Encountered invalid enum value for `", cfg.getName(), '_', key,
                        "` while parsing graphics pipeline configuration file!");
            return false;
        }
        return true;
    }
    return false;
}

bool GraphicsPipelineOptions::parse(const libconfig::Config& cfg) {
    if (!PipelineOptions::parse(cfg)) {
        return false;
    }

    if (cfg.exists("dynamic_states")) {
        const libconfig::Setting& cfg_dynamic_states = cfg.lookup("dynamic_states");
        if (!cfg_dynamic_states.isArray()) {
            logger.logw(
                 "Encountered invalid state while parsing graphics pipeline configuration file:"
                 " `dynamic_states` must be an array!");
            return false;
        }
        for (const auto& cfg_dynstate : cfg_dynamic_states) {
            std::string dynstate = static_cast<std::string>(cfg_dynstate);
            ::vk::DynamicState dynstate_vk;
            if (dynstate == "AlphaToCoverageEnable") {
                dynstate_vk = ::vk::DynamicState::eAlphaToCoverageEnableEXT;
            }
            else if (dynstate == "AlphaToOneEnable") {
                dynstate_vk = ::vk::DynamicState::eAlphaToOneEnableEXT;
            }
            else if (dynstate == "AttachmentFeedbackLoopEnable") {
                dynstate_vk = ::vk::DynamicState::eAttachmentFeedbackLoopEnableEXT;
            }
            else if (dynstate == "BlendConstants") {
                dynstate_vk = ::vk::DynamicState::eBlendConstants;
            }
            else if (dynstate == "ColorBlendAdvanced") {
                dynstate_vk = ::vk::DynamicState::eColorBlendAdvancedEXT;
            }
            else if (dynstate == "ColorBlendEnable") {
                dynstate_vk = ::vk::DynamicState::eColorBlendEnableEXT;
            }
            else if (dynstate == "ColorBlendEquation") {
                dynstate_vk = ::vk::DynamicState::eColorBlendEquationEXT;
            }
            else if (dynstate == "ColorWriteEnable") {
                dynstate_vk = ::vk::DynamicState::eColorWriteEnableEXT;
            }
            else if (dynstate == "ColorWriteMask") {
                dynstate_vk = ::vk::DynamicState::eColorWriteMaskEXT;
            }
            else if (dynstate == "ConservativeRasterizationMode") {
                dynstate_vk = ::vk::DynamicState::eConservativeRasterizationModeEXT;
            }
            else if (dynstate == "CoverageModulationMode") {
                dynstate_vk = ::vk::DynamicState::eCoverageModulationModeNV;
            }
            else if (dynstate == "CoverageModulationTable") {
                dynstate_vk = ::vk::DynamicState::eCoverageModulationTableNV;
            }
            else if (dynstate == "CoverageModulationTableEnable") {
                dynstate_vk = ::vk::DynamicState::eCoverageModulationTableEnableNV;
            }
            else if (dynstate == "CoverageReductionMode") {
                dynstate_vk = ::vk::DynamicState::eCoverageReductionModeNV;
            }
            else if (dynstate == "CoverageToColorEnable") {
                dynstate_vk = ::vk::DynamicState::eCoverageToColorEnableNV;
            }
            else if (dynstate == "CoverageToColorLocation") {
                dynstate_vk = ::vk::DynamicState::eCoverageToColorLocationNV;
            }
            else if (dynstate == "CullMode") {
                dynstate_vk = ::vk::DynamicState::eCullMode;
            }
            else if (dynstate == "DepthBias") {
                dynstate_vk = ::vk::DynamicState::eDepthBias;
            }
            else if (dynstate == "DepthBiasEnable") {
                dynstate_vk = ::vk::DynamicState::eDepthBiasEnable;
            }
            else if (dynstate == "DepthBounds") {
                dynstate_vk = ::vk::DynamicState::eDepthBounds;
            }
            else if (dynstate == "DepthBoundsTestEnable") {
                dynstate_vk = ::vk::DynamicState::eDepthBoundsTestEnable;
            }
            else if (dynstate == "DepthClampEnable") {
                dynstate_vk = ::vk::DynamicState::eDepthClampEnableEXT;
            }
            else if (dynstate == "DepthClipEnable") {
                dynstate_vk = ::vk::DynamicState::eDepthClipEnableEXT;
            }
            else if (dynstate == "DepthClipNegativeOneToOne") {
                dynstate_vk = ::vk::DynamicState::eDepthClipNegativeOneToOneEXT;
            }
            else if (dynstate == "DepthCompareOp") {
                dynstate_vk = ::vk::DynamicState::eDepthCompareOp;
            }
            else if (dynstate == "DepthTestEnable") {
                dynstate_vk = ::vk::DynamicState::eDepthTestEnable;
            }
            else if (dynstate == "DepthWriteEnable") {
                dynstate_vk = ::vk::DynamicState::eDepthWriteEnable;
            }
            else if (dynstate == "DiscardRectangle") {
                dynstate_vk = ::vk::DynamicState::eDiscardRectangleEXT;
            }
            else if (dynstate == "DiscardRectangleEnable") {
                dynstate_vk = ::vk::DynamicState::eDiscardRectangleEnableEXT;
            }
            else if (dynstate == "DiscardRectangleMode") {
                dynstate_vk = ::vk::DynamicState::eDiscardRectangleModeEXT;
            }
            else if (dynstate == "ExclusiveScissor") {
                dynstate_vk = ::vk::DynamicState::eExclusiveScissorNV;
            }
            else if (dynstate == "ExclusiveScissorEnable") {
                dynstate_vk = ::vk::DynamicState::eExclusiveScissorEnableNV;
            }
            else if (dynstate == "ExtraPrimitiveOverestimationSize") {
                dynstate_vk = ::vk::DynamicState::eExtraPrimitiveOverestimationSizeEXT;
            }
            else if (dynstate == "FragmentShadingRate") {
                dynstate_vk = ::vk::DynamicState::eFragmentShadingRateKHR;
            }
            else if (dynstate == "FrontFace") {
                dynstate_vk = ::vk::DynamicState::eFrontFace;
            }
            else if (dynstate == "LineRasterizationMode") {
                dynstate_vk = ::vk::DynamicState::eLineRasterizationModeEXT;
            }
            else if (dynstate == "LineStipple") {
                dynstate_vk = ::vk::DynamicState::eLineStippleEXT;
            }
            else if (dynstate == "LineStippleEnable") {
                dynstate_vk = ::vk::DynamicState::eLineStippleEnableEXT;
            }
            else if (dynstate == "LineWidth") {
                dynstate_vk = ::vk::DynamicState::eLineWidth;
            }
            else if (dynstate == "LogicOp") {
                dynstate_vk = ::vk::DynamicState::eLogicOpEXT;
            }
            else if (dynstate == "LogicOpEnable") {
                dynstate_vk = ::vk::DynamicState::eLogicOpEnableEXT;
            }
            else if (dynstate == "PatchControlPoints") {
                dynstate_vk = ::vk::DynamicState::ePatchControlPointsEXT;
            }
            else if (dynstate == "PolygonMode") {
                dynstate_vk = ::vk::DynamicState::ePolygonModeEXT;
            }
            else if (dynstate == "PrimitiveRestartEnable") {
                dynstate_vk = ::vk::DynamicState::ePrimitiveRestartEnable;
            }
            else if (dynstate == "PrimitiveTopology") {
                dynstate_vk = ::vk::DynamicState::ePrimitiveTopology;
            }
            else if (dynstate == "ProvokingVertexMode") {
                dynstate_vk = ::vk::DynamicState::eProvokingVertexModeEXT;
            }
            else if (dynstate == "RasterizationSamples") {
                dynstate_vk = ::vk::DynamicState::eRasterizationSamplesEXT;
            }
            else if (dynstate == "RasterizationStream") {
                dynstate_vk = ::vk::DynamicState::eRasterizationStreamEXT;
            }
            else if (dynstate == "RasterizerDiscardEnable") {
                dynstate_vk = ::vk::DynamicState::eRasterizerDiscardEnable;
            }
            else if (dynstate == "RayTracingPipelineStackSize") {
                dynstate_vk = ::vk::DynamicState::eRayTracingPipelineStackSizeKHR;
            }
            else if (dynstate == "RepresentativeFragmentTestEnable") {
                dynstate_vk = ::vk::DynamicState::eRepresentativeFragmentTestEnableNV;
            }
            else if (dynstate == "SampleLocations") {
                dynstate_vk = ::vk::DynamicState::eSampleLocationsEXT;
            }
            else if (dynstate == "SampleLocationsEnable") {
                dynstate_vk = ::vk::DynamicState::eSampleLocationsEnableEXT;
            }
            else if (dynstate == "SampleMask") {
                dynstate_vk = ::vk::DynamicState::eSampleMaskEXT;
            }
            else if (dynstate == "Scissor") {
                dynstate_vk = ::vk::DynamicState::eScissor;
            }
            else if (dynstate == "ScissorWithCount") {
                dynstate_vk = ::vk::DynamicState::eScissorWithCount;
            }
            else if (dynstate == "ShadingRateImageEnable") {
                dynstate_vk = ::vk::DynamicState::eShadingRateImageEnableNV;
            }
            else if (dynstate == "StencilCompareMask") {
                dynstate_vk = ::vk::DynamicState::eStencilCompareMask;
            }
            else if (dynstate == "StencilOp") {
                dynstate_vk = ::vk::DynamicState::eStencilOp;
            }
            else if (dynstate == "StencilReference") {
                dynstate_vk = ::vk::DynamicState::eStencilReference;
            }
            else if (dynstate == "StencilTestEnable") {
                dynstate_vk = ::vk::DynamicState::eStencilTestEnable;
            }
            else if (dynstate == "StencilWriteMask") {
                dynstate_vk = ::vk::DynamicState::eStencilWriteMask;
            }
            else if (dynstate == "TessellationDomainOrigin") {
                dynstate_vk = ::vk::DynamicState::eTessellationDomainOriginEXT;
            }
            else if (dynstate == "VertexInput") {
                dynstate_vk = ::vk::DynamicState::eVertexInputEXT;
            }
            else if (dynstate == "VertexInputBindingStride") {
                dynstate_vk = ::vk::DynamicState::eVertexInputBindingStride;
            }
            else if (dynstate == "Viewport") {
                dynstate_vk = ::vk::DynamicState::eViewport;
            }
            else if (dynstate == "ViewportCoarseSampleOrder") {
                dynstate_vk = ::vk::DynamicState::eViewportCoarseSampleOrderNV;
            }
            else if (dynstate == "ViewportShadingRatePalette") {
                dynstate_vk = ::vk::DynamicState::eViewportShadingRatePaletteNV;
            }
            else if (dynstate == "ViewportSwizzle") {
                dynstate_vk = ::vk::DynamicState::eViewportSwizzleNV;
            }
            else if (dynstate == "ViewportWithCount") {
                dynstate_vk = ::vk::DynamicState::eViewportWithCount;
            }
            else if (dynstate == "ViewportWScaling") {
                dynstate_vk = ::vk::DynamicState::eViewportWScalingNV;
            }
            else if (dynstate == "ViewportWScalingEnable") {
                dynstate_vk = ::vk::DynamicState::eViewportWScalingEnableNV;
            }
            else {
                logger.logw("Encountered invalid enum value for `dynamic_state` while parsing "
                            "graphics pipeline configuration file!");
                return false;
            }
            if (std::find(_dynamic_states.begin(), _dynamic_states.end(), dynstate_vk) ==
                _dynamic_states.end()) {
                _dynamic_states.push_back(dynstate_vk);
            }
        }
        _dynamic_states.shrink_to_fit();
    }

    /**
     * Vertex input config file structure/format.
     * {
     *   binding
     *   stride
     *   input_rate_per_instance
     *   attributes: (
     *     {
     *       name
     *       offset
     *     }
     *   )
     * }
     */
    if (cfg.exists("vertex_inputs")) {
        const auto& cfg_vertex_inputs = cfg.lookup("vertex_inputs");
        if (!cfg_vertex_inputs.isList()) {
            logger.logw(
                 "Encountered invalid state while parsing graphics pipeline configuration file:"
                 " `vertex_inputs` must be a list!");
            return false;
        }
        for (const auto& cfg_vertex_input : cfg_vertex_inputs) {
            if (!cfg_vertex_input.isGroup()) {
                logger.logw("Encountered invalid state while parsing graphics pipeline "
                            "configuration file: `vertex_input` must be a group!");
                return false;
            }

            ::vk::VertexInputBindingDescription binding_desc_vk;
            if (!cfg_vertex_input.lookupValue("binding", binding_desc_vk.binding)) {
                logger.logw(
                     "Encountered invalid state while parsing graphics pipeline "
                     "configuration file: couldn't find setting `vertex_input.binding`!");
                return false;
            }
            if (!cfg_vertex_input.lookupValue("stride", binding_desc_vk.stride)) {
                logger.logw("Encountered invalid state while parsing graphics pipeline "
                            "configuration file: couldn't find setting `vertex_input.stride`!");
                return false;
            }
            bool input_rate_per_instance = false;
            binding_desc_vk.inputRate = ::vk::VertexInputRate::eVertex;
            if (cfg_vertex_input.lookupValue("input_rate_per_instance",
                                             input_rate_per_instance) &&
                input_rate_per_instance) {
                binding_desc_vk.inputRate = ::vk::VertexInputRate::eInstance;
            }
            // Register vertex descriptor bindings.
            if (!vertex_input_bindings_add(binding_desc_vk)) {
                logger.logw("Couldn't register vertex input binding parsed from graphics "
                            "pipeline configuration file!");
                return false;
            }

            if (cfg_vertex_input.exists("attributes")) {
                const auto& cfg_vertex_attributes = cfg.lookup("attributes");
                if (!cfg_vertex_attributes.isList()) {
                    logger.logw(
                         "Encountered invalid state while parsing graphics pipeline "
                         "configuration file: `vertex_input.attributes` must be a list!");
                    return false;
                }
                for (const auto& cfg_vertex_attribute : cfg_vertex_attributes) {
                    VertexInputAttributeFinder vert_attr;
                    vert_attr.binding = binding_desc_vk.binding;
                    if (!cfg_vertex_attribute.lookupValue("name", vert_attr.name)) {
                        logger.logw("Encountered invalid state while parsing graphics pipeline "
                                    "configuration file: couldn't find setting "
                                    "`vertex_input.attribute.name`!");
                        return false;
                    }
                    if (!cfg_vertex_attribute.lookupValue("offset", vert_attr.offset)) {
                        logger.logw("Encountered invalid state while parsing graphics pipeline "
                                    "configuration file: couldn't find setting "
                                    "`vertex_input.attribute.offset`!");
                        return false;
                    }
                    if (!vertex_input_attributes_add(vert_attr)) {
                        logger.logw("Couldn't register vertex input attribute parsed from "
                                    "graphics pipeline configuration file!");
                        return false;
                    }
                }
            }
            _vertex_input_bindings.shrink_to_fit();
            _vertex_input_attributes.shrink_to_fit();
        }
    }

    std::string primitive_topology_str;
    if (cfg.lookupValue("primitive_topology", primitive_topology_str)) {
        if (primitive_topology_str == "PointList") {
            _primitive_topology = ::vk::PrimitiveTopology::ePointList;
        }
        else if (primitive_topology_str == "LineList") {
            _primitive_topology = ::vk::PrimitiveTopology::eLineList;
        }
        else if (primitive_topology_str == "LineStrip") {
            _primitive_topology = ::vk::PrimitiveTopology::eLineStrip;
        }
        else if (primitive_topology_str == "TriangleList") {
            _primitive_topology = ::vk::PrimitiveTopology::eTriangleList;
        }
        else if (primitive_topology_str == "TriangleStrip") {
            _primitive_topology = ::vk::PrimitiveTopology::eTriangleStrip;
        }
        else if (primitive_topology_str == "TriangleFan") {
            _primitive_topology = ::vk::PrimitiveTopology::eTriangleFan;
        }
        else if (primitive_topology_str == "LineListWithAdjacency") {
            _primitive_topology = ::vk::PrimitiveTopology::eLineListWithAdjacency;
        }
        else if (primitive_topology_str == "LineStripWithAdjacency") {
            _primitive_topology = ::vk::PrimitiveTopology::eLineStripWithAdjacency;
        }
        else if (primitive_topology_str == "TriangleListWithAdjacency") {
            _primitive_topology = ::vk::PrimitiveTopology::eTriangleListWithAdjacency;
        }
        else if (primitive_topology_str == "TriangleStripWithAdjacency") {
            _primitive_topology = ::vk::PrimitiveTopology::eTriangleStripWithAdjacency;
        }
        else if (primitive_topology_str == "PatchList") {
            _primitive_topology = ::vk::PrimitiveTopology::ePatchList;
        }
        else {
            logger.logw("Encountered invalid enum value for `primitive_topology` while parsing "
                        "graphics pipeline configuration file!");
            return false;
        }
    }
    cfg.lookupValue("primitive_restart_enable", _primitive_restart_enable);

    cfg.lookupValue("tessellation_patch_control_points", _tessellation_patch_control_points);

    if (cfg.exists("rasterizer")) {
        const auto& cfg_rasterizer = cfg.lookup("rasterizer");
        if (!cfg_rasterizer.isGroup()) {
            logger.logw("Encountered invalid state while parsing graphics pipeline "
                        "configuration file: `rasterizer` must be a group!");
            return false;
        }

        cfg_rasterizer.lookupValue("depth_clamp_enable", _rasterizer_depth_clamp_enable);
        cfg_rasterizer.lookupValue("discard_enable", _rasterizer_discard_enable);
        std::string polygon_mode_str;
        if (cfg_rasterizer.lookupValue("polygon_mode", polygon_mode_str)) {
            if (polygon_mode_str == "Fill") {
                _rasterizer_polygon_mode = ::vk::PolygonMode::eFill;
            }
            else if (polygon_mode_str == "Line") {
                _rasterizer_polygon_mode = ::vk::PolygonMode::eLine;
            }
            else if (polygon_mode_str == "Point") {
                _rasterizer_polygon_mode = ::vk::PolygonMode::ePoint;
            }
            else if (polygon_mode_str == "FillRectangle") {
                _rasterizer_polygon_mode = ::vk::PolygonMode::eFillRectangleNV;
            }
            else {
                logger.logw("Encountered invalid enum value for `rasterizer.polygon_mode` "
                            "while parsing graphics pipeline configuration file!");
                return false;
            }
        }
        std::string cull_mode_str;
        if (cfg_rasterizer.lookupValue("cull_mode", cull_mode_str)) {
            if (cull_mode_str == "None") {
                _rasterizer_cull_mode = ::vk::CullModeFlagBits::eNone;
            }
            else if (cull_mode_str == "Front") {
                _rasterizer_cull_mode = ::vk::CullModeFlagBits::eFront;
            }
            else if (cull_mode_str == "Back") {
                _rasterizer_cull_mode = ::vk::CullModeFlagBits::eBack;
            }
            else if (cull_mode_str == "FrontAndBack") {
                _rasterizer_cull_mode = ::vk::CullModeFlagBits::eFrontAndBack;
            }
            else {
                logger.logw("Encountered invalid enum value for `rasterizer.cull_mode` "
                            "while parsing graphics pipeline configuration file!");
                return false;
            }
        }
        bool front_face_counter_clockwise;
        if (cfg_rasterizer.lookupValue("front_face_counter_clockwise",
                                       front_face_counter_clockwise)) {
            _rasterizer_front_face = front_face_counter_clockwise ?
                                          ::vk::FrontFace::eCounterClockwise :
                                          ::vk::FrontFace::eClockwise;
        }
        cfg_rasterizer.lookupValue("depth_bias_enable", _rasterizer_depth_bias_enable);
        cfg_rasterizer.lookupValue("depth_bias_constant_factor",
                                   _rasterizer_depth_bias_constant_factor);
        cfg_rasterizer.lookupValue("depth_bias_clamp", _rasterizer_depth_bias_clamp);
        cfg_rasterizer.lookupValue("depth_bias_slope_factor",
                                   _rasterizer_depth_bias_slope_factor);
        cfg_rasterizer.lookupValue("line_width", _rasterizer_line_width);
    }

    if (cfg.exists("multisample")) {
        const auto& cfg_multisample = cfg.lookup("multisample");
        if (!cfg_multisample.isGroup()) {
            logger.logw("Encountered invalid state while parsing graphics pipeline "
                        "configuration file: `multisample` must be a group!");
            return false;
        }

        int rasterization_samples;
        if (cfg_multisample.lookupValue("rasterization_samples", rasterization_samples)) {
            if (rasterization_samples == 1) {
                _multisample_rasterization_samples = ::vk::SampleCountFlagBits::e1;
            }
            else if (rasterization_samples == 2) {
                _multisample_rasterization_samples = ::vk::SampleCountFlagBits::e2;
            }
            else if (rasterization_samples == 4) {
                _multisample_rasterization_samples = ::vk::SampleCountFlagBits::e4;
            }
            else if (rasterization_samples == 8) {
                _multisample_rasterization_samples = ::vk::SampleCountFlagBits::e8;
            }
            else if (rasterization_samples == 16) {
                _multisample_rasterization_samples = ::vk::SampleCountFlagBits::e16;
            }
            else if (rasterization_samples == 32) {
                _multisample_rasterization_samples = ::vk::SampleCountFlagBits::e32;
            }
            else if (rasterization_samples == 64) {
                _multisample_rasterization_samples = ::vk::SampleCountFlagBits::e64;
            }
            else {
                logger.logw(
                     "Encountered invalid enum value for `multisample.rasterization_samples` "
                     "while parsing graphics pipeline configuration file!");
                return false;
            }
        }
        cfg_multisample.lookupValue("sample_shading_enable",
                                    _multisample_sample_shading_enable);
        cfg_multisample.lookupValue("min_sample_shading", _multisample_min_sample_shading);
        cfg_multisample.lookupValue("alpha_to_coverage_enable",
                                    _multisample_alpha_to_coverage_enable);
        cfg_multisample.lookupValue("alpha_to_one_enable", _multisample_alpha_to_one_enable);
    }

    if (cfg.exists("depth")) {
        const auto& cfg_depth = cfg.lookup("depth");
        if (!cfg_depth.isGroup()) {
            logger.logw("Encountered invalid state while parsing graphics pipeline "
                        "configuration file: `depth` must be a group!");
            return false;
        }

        cfg_depth.lookupValue("rasterization_order_attachment_access",
                              _depth_rasterization_order_attachment_access);
        cfg_depth.lookupValue("test_disabled", _depth_test_disabled);
        cfg_depth.lookupValue("write_disabled", _depth_write_disabled);

        std::string compare_op_str;
        if (cfg_depth.lookupValue("compare_op", compare_op_str)) {
            if (compare_op_str == "Never") {
                _depth_compare_op = ::vk::CompareOp::eNever;
            }
            else if (compare_op_str == "Less") {
                _depth_compare_op = ::vk::CompareOp::eLess;
            }
            else if (compare_op_str == "Equal") {
                _depth_compare_op = ::vk::CompareOp::eEqual;
            }
            else if (compare_op_str == "LessOrEqual") {
                _depth_compare_op = ::vk::CompareOp::eLessOrEqual;
            }
            else if (compare_op_str == "Greater") {
                _depth_compare_op = ::vk::CompareOp::eGreater;
            }
            else if (compare_op_str == "NotEqual") {
                _depth_compare_op = ::vk::CompareOp::eNotEqual;
            }
            else if (compare_op_str == "GreaterOrEqual") {
                _depth_compare_op = ::vk::CompareOp::eGreaterOrEqual;
            }
            else if (compare_op_str == "Always") {
                _depth_compare_op = ::vk::CompareOp::eAlways;
            }
            else {
                logger.logw("Encountered invalid enum value for `depth.compare_op` "
                            "while parsing graphics pipeline configuration file!");
                return false;
            }
        }

        cfg_depth.lookupValue("bounds_test_enable", _depth_bounds_test_enable);
        cfg_depth.lookupValue("min_bounds", _depth_min_bounds);
        cfg_depth.lookupValue("max_bounds", _depth_max_bounds);
    }

    if (cfg.exists("color_blend")) {
        const auto& cfg_color = cfg.lookup("color_blend");
        if (!cfg_color.isGroup()) {
            logger.logw("Encountered invalid state while parsing graphics pipeline "
                        "configuration file: `color_blend` must be a group!");
            return false;
        }

        cfg_color.lookupValue("rasterization_order_attachment_access",
                              _color_blend_rasterization_order_attachment_access);
        cfg_color.lookupValue("logic_op_enable", _color_blend_logic_op_enable);

        std::string logic_op_str;
        if (cfg_color.lookupValue("logic_op", logic_op_str)) {
            if (logic_op_str == "Clear") {
                _color_blend_logic_op = ::vk::LogicOp::eClear;
            }
            else if (logic_op_str == "And") {
                _color_blend_logic_op = ::vk::LogicOp::eAnd;
            }
            else if (logic_op_str == "AndReverse") {
                _color_blend_logic_op = ::vk::LogicOp::eAndReverse;
            }
            else if (logic_op_str == "Copy") {
                _color_blend_logic_op = ::vk::LogicOp::eCopy;
            }
            else if (logic_op_str == "AndInverted") {
                _color_blend_logic_op = ::vk::LogicOp::eAndInverted;
            }
            else if (logic_op_str == "NoOp") {
                _color_blend_logic_op = ::vk::LogicOp::eNoOp;
            }
            else if (logic_op_str == "Xor") {
                _color_blend_logic_op = ::vk::LogicOp::eXor;
            }
            else if (logic_op_str == "Or") {
                _color_blend_logic_op = ::vk::LogicOp::eOr;
            }
            else if (logic_op_str == "Nor") {
                _color_blend_logic_op = ::vk::LogicOp::eNor;
            }
            else if (logic_op_str == "Equivalent") {
                _color_blend_logic_op = ::vk::LogicOp::eEquivalent;
            }
            else if (logic_op_str == "Invert") {
                _color_blend_logic_op = ::vk::LogicOp::eInvert;
            }
            else if (logic_op_str == "OrReverse") {
                _color_blend_logic_op = ::vk::LogicOp::eOrReverse;
            }
            else if (logic_op_str == "CopyInverted") {
                _color_blend_logic_op = ::vk::LogicOp::eCopyInverted;
            }
            else if (logic_op_str == "OrInverted") {
                _color_blend_logic_op = ::vk::LogicOp::eOrInverted;
            }
            else if (logic_op_str == "Nand") {
                _color_blend_logic_op = ::vk::LogicOp::eNand;
            }
            else if (logic_op_str == "Set") {
                _color_blend_logic_op = ::vk::LogicOp::eSet;
            }
            else {
                logger.logw("Encountered invalid enum value for `color_blend.logic_op` "
                            "while parsing graphics pipeline configuration file!");
                return false;
            }

            cfg_color.lookupValue("enable", _color_blend_enable);
            __lookup_value(cfg_color, "src_factor", _color_blend_src_factor);
            __lookup_value(cfg_color, "dst_factor", _color_blend_dst_factor);
            __lookup_value(cfg_color, "op", _color_blend_op);
            if (cfg_color.exists("alpha")) {
                const auto& cfg_alpha = cfg_color.lookup("alpha");
                if (!cfg_alpha.isGroup()) {
                    logger.logw("Encountered invalid state while parsing graphics pipeline "
                                "configuration file: `color_blend.alpha` must be a group!");
                    return false;
                }
                __lookup_value(cfg_alpha, "src_factor", _alpha_blend_src_factor);
                __lookup_value(cfg_alpha, "dst_factor", _alpha_blend_dst_factor);
                __lookup_value(cfg_alpha, "op", _alpha_blend_op);
            }

            if (cfg_color.exists("write_mask")) {
                const auto& cfg_write_mask = cfg_color.lookup("write_mask");
                if (!cfg_write_mask.isArray()) {
                    logger.logw(
                         "Encountered invalid state while parsing graphics pipeline "
                         "configuration file: `color_blend.write_mask` must be an array!");
                    return false;
                }
                ::vk::ColorComponentFlags color_write_mask;
                for (const auto& color_component : cfg_write_mask) {
                    auto color_component_str = static_cast<std::string>(color_component);
                    if (color_component_str.size() == 1) {
                        auto color_component_char = std::tolower(color_component_str[0]);
                        switch (color_component_char) {
                            case 'R': {
                                color_write_mask |= ::vk::ColorComponentFlagBits::eR;
                            } break;
                            case 'G': {
                                color_write_mask |= ::vk::ColorComponentFlagBits::eG;
                            } break;
                            case 'B': {
                                color_write_mask |= ::vk::ColorComponentFlagBits::eB;
                            } break;
                            case 'A': {
                                color_write_mask |= ::vk::ColorComponentFlagBits::eA;
                            } break;
                            default: {
                                logger.logw("Encountered invalid enum value for "
                                            "`color_blend.write_mask` while parsing graphics "
                                            "pipeline configuration file!");
                                return false;
                            }
                        }
                    }
                    else {
                        logger.logw("Encountered invalid enum value's length for "
                                    "`color_blend.write_mask` "
                                    "while parsing graphics pipeline configuration file!");
                        return false;
                    }
                }
            }

            if (cfg_color.exists("constants")) {
                const auto& cfg_constants = cfg_color.lookup("constants");
                if (!cfg_constants.isArray() || cfg_constants.getLength() != 4 ||
                    cfg_constants[0].getType() != libconfig::Setting::TypeFloat) {
                    logger.logw("Encountered invalid state while parsing graphics pipeline "
                                "configuration file: `color_blend.constants` must be "
                                "an array of 4 floating point elements!");
                    return false;
                }
                for (size_t i = 0; i < 4; i++) {
                    _color_blend_constants[i] = static_cast<double>(cfg_constants[i]);
                }
            }
        }
    }

    cfg.lookupValue("layout_create_independent_sets", _layout_create_independent_sets);

    return true;
}

::vk::ComputePipelineCreateInfo ComputePipelineOptions::make_create_info(
     Module shader_module, ::vk::PipelineLayout layout) const {
    ::vk::ComputePipelineCreateInfo cinfo;

    cinfo.flags = get_create_info();
    cinfo.stage = shader_module.make_stage_create_info();
    cinfo.layout = layout;

    return cinfo;
}

}  // namespace satr::vk
