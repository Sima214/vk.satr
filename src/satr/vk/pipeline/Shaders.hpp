#ifndef SATR_VK_SHADERS_HPP
#define SATR_VK_SHADERS_HPP
/**
 * @file
 * @brief Loading and introspecting shader code.
 */

#include <Info.hpp>
#include <Utils.hpp>
#include <io/FileDescriptor.hpp>
#include <io/MappedFile.hpp>
#include <trace/Trace.hpp>
#include <vulkan/vulkan.hpp>

#include <cstdint>
#include <cstring>
#include <filesystem>
#include <functional>
#include <memory>
#include <optional>
#include <span>
#include <string>
#include <string_view>
#include <utility>

#include <spirv_reflect.h>

namespace satr::vk {

/* Fwd decls */
class Context;
class ModuleImpl;
class Module;
using ModuleWeak = std::weak_ptr<ModuleImpl>;

template<::vk::PipelineBindPoint bind_point> class PipelineImpl;

/**
 * Container for vulkan shader modules.
 * The fundamental building block of pipeline stages.
 * Also handles the associated resource loading.
 */
class ModuleImpl : public spec::INonCopyable {
   protected:
    ::vk::ShaderModule _handle;
    ::vk::Device _device;
    StageConfig _stage_config;
    /* Member used only for automatic cleanup. */
    std::pair<spec::FileDescriptor, spec::MappedFile> _filedata;
    spv_reflect::ShaderModule _reflection;

   protected:
    /**
     * Constructor when data is loaded from static storage.
     */
    ModuleImpl(::vk::ShaderModule handle, ::vk::Device device, const StageConfig& info,
               std::span<const uint32_t> data);
    /**
     * Constructor when data is loaded from file.
     */
    ModuleImpl(::vk::ShaderModule handle, ::vk::Device device, const StageConfig& info,
               spec::FileDescriptor&& file_object, spec::MappedFile&& file_memory);

   public:
    constexpr ModuleImpl() : _handle(nullptr), _device(nullptr), _stage_config() {}
    ModuleImpl(ModuleImpl&& o) noexcept;
    ModuleImpl& operator=(ModuleImpl&& o) noexcept;
    ~ModuleImpl();

    operator bool() const {
        return static_cast<bool>(_handle);
    }

    /**
     * Load from SPIR-V contained in a memory buffer.
     * NOTE: \p buffer_size here refers to element count, not size in bytes!
     * Uses the provided extra config.
     */
    static std::optional<ModuleImpl> load(const Context& ctx, const uint32_t* buffer,
                                          size_t buffer_size,
                                          const StageConfig& config = StageConfig());
    /**
     * Load from SPIR-V bytecode contained in a C++ container.
     * Uses the provided extra config.
     */
    static std::optional<ModuleImpl> load(const Context& ctx, std::span<uint32_t> buffer,
                                          const StageConfig& config = StageConfig()) {
        return load(ctx, buffer.data(), buffer.size(), config);
    }
    /**
     * Load from file based on \p path.
     * Optionally loads the stage info from the config
     * file at path appended with '.stage.info'.
     */
    static std::optional<ModuleImpl> load(const Context& ctx, const std::filesystem::path& path,
                                          bool load_stage_config = true);
    /**
     * Load from file based on \p path.
     * Uses the provided extra info.
     */
    static std::optional<ModuleImpl> load(const Context& ctx, const std::filesystem::path& path,
                                          const StageConfig& info = StageConfig());

    /** Getters */
    const StageConfig& get_stage_config() const {
        return _stage_config;
    }

    /** Reflection */
    const spv_reflect::ShaderModule& get_reflection() const {
        return _reflection;
    }
    ::vk::ShaderStageFlagBits get_stage_type() const;
    std::string_view get_entry_point() const;

    /** Reflection extras */
    bool has_vertex_input_attributes() const;

    /** Operations */
    ::vk::PipelineShaderStageCreateInfo make_stage_create_info();
};

class Module : public std::shared_ptr<ModuleImpl> {
   public:
    // For convenience:
    using Weak = ModuleWeak;

   protected:
    // Factory methods.
    Module(std::shared_ptr<ModuleImpl>&& o) noexcept :
        std::shared_ptr<ModuleImpl>(o) {}  // main constructor

    template<typename... Args> static Module load_shader(Args&&... args) {
        auto obj = std::make_shared_for_overwrite<ModuleImpl>();
        std::optional<ModuleImpl> loaded_obj = ModuleImpl::load(std::forward<Args>(args)...);
        if (loaded_obj) {
            *obj.get() = std::move(loaded_obj.value());
        }
        return obj;
    }

    friend class Context;

   public:
    // inherit shared_ptr's constructors
    constexpr Module() noexcept : std::shared_ptr<ModuleImpl>() {}
    constexpr Module(std::nullptr_t _) noexcept : std::shared_ptr<ModuleImpl>(_) {}
    Module(const Module& r) noexcept = default;
    Module(Module&& r) noexcept = default;

    Module& operator=(const Module& r) noexcept = default;
    Module& operator=(Module&& r) noexcept = default;

    // Forward Impl's methods.
    operator bool() const {
        return static_cast<bool>((std::shared_ptr<ModuleImpl>) *this) &&
               static_cast<bool>((ModuleImpl&) **this);
    }
    /** Getters */
    const StageConfig& get_stage_config() const {
        return (*this)->get_stage_config();
    }

    const spv_reflect::ShaderModule& get_reflection() {
        return (*this)->get_reflection();
    }
    ::vk::ShaderStageFlagBits get_stage_type() const {
        return (*this)->get_stage_type();
    }
    std::string_view get_entry_point() const {
        return (*this)->get_entry_point();
    }

    bool has_vertex_input_attributes() const {
        return (*this)->has_vertex_input_attributes();
    }

    /** Operations */
    ::vk::PipelineShaderStageCreateInfo make_stage_create_info() const {
        return (*this)->make_stage_create_info();
    }
};

/** Accessed through pipeline objects. */
class Descriptors;
class PushConstants;
class VertexAttributes;

class Descriptor {
   protected:
    std::reference_wrapper<const SpvReflectDescriptorBinding> _reflection;
    ::vk::ShaderStageFlags _stages;

   protected:
    Descriptor(const SpvReflectDescriptorBinding& reflection, ::vk::ShaderStageFlags stages);

    friend class Descriptors;

   public:
    Descriptor(const Descriptor&);
    Descriptor& operator=(const Descriptor&);

    std::string_view get_name() const;
    uint32_t get_binding() const;
    ::vk::ShaderStageFlags get_stages() const;
    uint32_t get_set() const;
    ::vk::DescriptorType get_type() const;
    bool is_block() const;
    bool is_array() const;
    std::span<const uint32_t> get_array_dims() const;

    ::vk::DescriptorSetLayoutBinding get_layout() const;
};
class Descriptors : public spec::INonCopyable {
   protected:
    std::span<Module> _shader_modules;
    /** mapping of set, layout */
    std::vector<std::pair<uint32_t, ::vk::DescriptorSetLayout>> _set_layouts;

   protected:
    Descriptors(std::span<Module> shader_modules);

    template<::vk::PipelineBindPoint> friend class PipelineImpl;

   public:
    Descriptors() noexcept = default;

    std::optional<Descriptor> operator[](const char* name) const {
        return retrieve_by_name(name);
    }
    std::optional<Descriptor> operator[](std::pair<uint32_t, uint32_t> set_binding) const {
        return retrieve_by_binding(set_binding.first, set_binding.second);
    }

    std::optional<Descriptor> retrieve_by_name(const std::string& name_key) const {
        return retrieve_by_name(name_key.c_str(), name_key.size());
    }
    std::optional<Descriptor> retrieve_by_name(std::string_view name_key) const {
        return retrieve_by_name(name_key.data(), name_key.size());
    }
    std::optional<Descriptor> retrieve_by_name(const char* name_key) const {
        return retrieve_by_name(name_key, strlen(name_key));
    }

    std::optional<Descriptor> retrieve_by_name(const char* name_key, size_t name_key_len) const;
    std::optional<Descriptor> retrieve_by_binding(uint32_t set, uint32_t binding) const;
    std::vector<Descriptor> retrieve_by_set(uint32_t set) const;

    /** Called by GraphicsPipelineOptions::make_pipeline_layout once. */
    void create_set_layouts(::vk::Device device);
    void free_set_layouts(::vk::Device device);
    const std::vector<std::pair<uint32_t, ::vk::DescriptorSetLayout>>& get_set_layouts() const;
    ::vk::DescriptorSetLayout get_set_layout(uint32_t set) const;

   protected:
    static const SpvReflectDescriptorBinding* _retrieve_by_name(Module shader,
                                                                const char* name_key,
                                                                size_t name_key_len);
    static const SpvReflectDescriptorBinding* _retrieve_by_binding(Module shader, uint32_t set,
                                                                   uint32_t binding);
    static const SpvReflectDescriptorSet* _retrieve_set(Module shader, uint32_t set);
};

class PushConstant {
   protected:
    ::vk::ShaderStageFlags _stage_flags;
    std::reference_wrapper<const SpvReflectBlockVariable> _reflection;

   protected:
    PushConstant(::vk::ShaderStageFlags stage_flags, const SpvReflectBlockVariable& reflection);

    friend class PushConstants;

   public:
    PushConstant(const PushConstant&);
    PushConstant& operator=(const PushConstant&);

    ::vk::ShaderStageFlags get_stages() const;
    size_t get_offset() const;
    size_t get_size() const;

    ::vk::PushConstantRange get_range() const;
};
class PushConstants : public spec::INonCopyable {
   protected:
    std::vector<std::vector<Module>> _shader_groups;

   protected:
    PushConstants(std::span<Module> shader_modules);

    template<::vk::PipelineBindPoint> friend class PipelineImpl;

   public:
    PushConstants() noexcept = default; /* Invalid */

    std::optional<PushConstant> operator[](const char* name) const {
        return retrieve_by_name(name);
    }

    std::optional<PushConstant> retrieve_by_name(const std::string& name_key) const {
        return retrieve_by_name(name_key.c_str(), name_key.size());
    }
    std::optional<PushConstant> retrieve_by_name(std::string_view name_key) const {
        return retrieve_by_name(name_key.data(), name_key.size());
    }
    std::optional<PushConstant> retrieve_by_name(const char* name_key) const {
        return retrieve_by_name(name_key, strlen(name_key));
    }

    std::optional<PushConstant> retrieve_by_name(const char* name_key,
                                                 size_t name_key_len) const;
    std::optional<PushConstant> retrieve_by_stage(::vk::ShaderStageFlagBits stage) const {
        return retrieve_by_stage(::vk::ShaderStageFlags(stage));
    }
    std::optional<PushConstant> retrieve_by_stage(::vk::ShaderStageFlags stages) const;

    std::vector<::vk::PushConstantRange> get_ranges() const;

   protected:
    static ::vk::ShaderStageFlags _combine_stage_flag_bits(const std::vector<Module>& stages);
};

class VertexAttribute {
   protected:
    std::reference_wrapper<const SpvReflectInterfaceVariable> _reflection;

   protected:
    VertexAttribute(const SpvReflectInterfaceVariable& reflection);

    friend class VertexAttributes;

   public:
    VertexAttribute(const VertexAttribute&);
    VertexAttribute& operator=(const VertexAttribute&);

    std::string_view get_name() const;
    uint32_t get_location() const;
    ::vk::Format get_format() const;

    bool is_array() const;
    std::span<const uint32_t> get_array_dims() const;
};
class VertexAttributes : public spec::INonCopyable {
   protected:
    std::span<Module> _shader_modules;

   protected:
    VertexAttributes(std::span<Module> shader_modules);

    template<::vk::PipelineBindPoint> friend class PipelineImpl;

   public:
    VertexAttributes() noexcept = default; /* Invalid */

    std::optional<VertexAttribute> operator[](const char* name) {
        return retrieve_by_name(name);
    }
    std::optional<VertexAttribute> operator[](uint32_t location_key) {
        return retrieve_by_location(location_key);
    }

    std::optional<VertexAttribute> retrieve_by_name(const std::string& name_key) {
        return retrieve_by_name(name_key.c_str(), name_key.size());
    }
    std::optional<VertexAttribute> retrieve_by_name(std::string_view name_key) {
        return retrieve_by_name(name_key.data(), name_key.size());
    }
    std::optional<VertexAttribute> retrieve_by_name(const char* name_key) {
        return retrieve_by_name(name_key, strlen(name_key));
    }
    std::optional<VertexAttribute> retrieve_by_name(const char* name_key, size_t name_key_len);
    std::optional<VertexAttribute> retrieve_by_location(uint32_t location_key);
    std::vector<VertexAttribute> retrieve_builtin();

   protected:
    static const SpvReflectInterfaceVariable* _retrieve_by_name(Module mod,
                                                                const char* name_key,
                                                                size_t name_key_len);
    static const SpvReflectInterfaceVariable* _retrieve_by_location(Module mod,
                                                                    uint32_t location_key);
};

}  // namespace satr::vk

#endif /*SATR_VK_SHADERS_HPP*/