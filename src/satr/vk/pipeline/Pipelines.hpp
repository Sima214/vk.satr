#ifndef SATR_VK_PIPELINES_HPP
#define SATR_VK_PIPELINES_HPP
/**
 * @file
 * @brief
 */

#include <Info.hpp>
#include <Shaders.hpp>
#include <Utils.hpp>
#include <vulkan/vulkan.hpp>

#include <memory>
#include <optional>

namespace satr::vk {

/* Fwd decls */
class Context;
class FramebufferChain;
template<::vk::PipelineBindPoint bind_point> class PipelineImpl;
template<::vk::PipelineBindPoint bind_point>
using PipelineWeak = std::weak_ptr<PipelineImpl<bind_point>>;

/**
 * Built from a set of shader modules configured as stages
 * and also a set of configuration options for describing
 * the behaviour of fixed functionality.
 */
template<::vk::PipelineBindPoint bind_point> class PipelineImpl : public spec::INonCopyable {
   protected:
    ::vk::Pipeline _handle;
    ::vk::Device _device;
    ::vk::PipelineLayout _layout;

    // Hold strong reference to the shaders.
    std::vector<Module> _shaders;
    VertexAttributes _vertex_attributes;
    Descriptors _descriptors;
    PushConstants _push_constants;

   protected:
    PipelineImpl(::vk::Pipeline handle, ::vk::Device device, ::vk::PipelineLayout layout,
                 std::span<Module> shaders, VertexAttributes&& vertex_attributes,
                 Descriptors&& descriptors, PushConstants&& push_consts);
    PipelineImpl(::vk::Pipeline handle, ::vk::Device device, ::vk::PipelineLayout layout,
                 Module shaders, Descriptors&& descriptors, PushConstants&& push_consts);

   public:
    constexpr PipelineImpl() noexcept : _handle(nullptr) {}
    PipelineImpl(PipelineImpl&&) noexcept;
    PipelineImpl& operator=(PipelineImpl&&) noexcept;
    ~PipelineImpl();

    /** Graphics pipeline creation. */
    static std::optional<PipelineImpl> compile(const Context& context,
                                               const FramebufferChain& framebuffer,
                                               std::span<Module> shader_modules,
                                               const GraphicsPipelineOptions& options);

    /** Compute pipeline creation. */
    static std::optional<PipelineImpl> compile(const Context& context, Module shader,
                                               const ComputePipelineOptions& options);

    /** Operators */
    operator bool() const {
        return static_cast<bool>(_handle);
    }

    /** Getters */
    ::vk::PipelineLayout get_layout() const {
        return _layout;
    }
    const VertexAttributes& get_vertex_attributes() const {
        return _vertex_attributes;
    }
    const Descriptors& get_descriptors() const {
        return _descriptors;
    }
    const PushConstants& get_push_constants() const {
        return _push_constants;
    }

    /** Operations */
    void bind(::vk::CommandBuffer cmd_buf);
    void bind_sets(::vk::CommandBuffer cmd_buf, std::span<const ::vk::DescriptorSet> sets,
                   uint32_t first_set_offset = 0);
    /**
     * Updates the current viewport such that the whole framebuffer is the render target.
     */
    void update_viewport(::vk::CommandBuffer cmd_buf, const FramebufferChain& framebuffer,
                         size_t chain_index);
};

template<::vk::PipelineBindPoint bind_point>
class GenericPipeline : public std::shared_ptr<PipelineImpl<bind_point>> {
   public:
    // For convenience:
    using Weak = PipelineWeak<bind_point>;

   protected:
    // Factory methods.
    GenericPipeline(std::shared_ptr<PipelineImpl<bind_point>>&& o) noexcept :
        std::shared_ptr<PipelineImpl<bind_point>>(o) {}

    template<typename... Args> static GenericPipeline compile(Args&&... args) {
        auto obj = std::make_shared_for_overwrite<PipelineImpl<bind_point>>();
        std::optional<PipelineImpl<bind_point>> compiled_obj =
             PipelineImpl<bind_point>::compile(std::forward<Args>(args)...);
        if (compiled_obj) {
            *obj.get() = std::move(compiled_obj.value());
        }
        return obj;
    }

    friend class Context;

   public:
    // inherit shared_ptr's constructors
    constexpr GenericPipeline() noexcept : std::shared_ptr<PipelineImpl<bind_point>>() {}
    constexpr GenericPipeline(std::nullptr_t _) noexcept :
        std::shared_ptr<PipelineImpl<bind_point>>(_) {}
    GenericPipeline(const GenericPipeline& r) noexcept = default;
    GenericPipeline(GenericPipeline&& r) noexcept = default;

    GenericPipeline& operator=(const GenericPipeline& r) noexcept = default;
    GenericPipeline& operator=(GenericPipeline&& r) noexcept = default;

    // Forward Impl's methods.
    operator bool() const {
        return static_cast<bool>((std::shared_ptr<PipelineImpl<bind_point>>) *this) &&
               static_cast<bool>((PipelineImpl<bind_point>&) **this);
    }

    /** Getters */
    ::vk::PipelineLayout get_layout() const {
        return (*this)->get_layout();
    }
    const VertexAttributes& get_vertex_attributes() const {
        return (*this)->get_vertex_attributes();
    }
    const Descriptors& get_descriptors() const {
        return (*this)->get_descriptors();
    }
    const PushConstants& get_push_constants() const {
        return (*this)->get_push_constants();
    }

    /** Operations */
    void bind(::vk::CommandBuffer cmd_buf) {
        (*this)->bind(cmd_buf);
    }
    void bind_sets(::vk::CommandBuffer cmd_buf, std::span<const ::vk::DescriptorSet> sets,
                   uint32_t first_set_offset = 0) {
        (*this)->bind_sets(cmd_buf, sets, first_set_offset);
    }
    void update_viewport(::vk::CommandBuffer cmd_buf, const FramebufferChain& framebuffer,
                         size_t chain_index) {
        (*this)->update_viewport(cmd_buf, framebuffer, chain_index);
    }
};

using Pipeline = GenericPipeline<::vk::PipelineBindPoint::eGraphics>;
using ComputePipeline = GenericPipeline<::vk::PipelineBindPoint::eCompute>;

}  // namespace satr::vk

#endif /*SATR_VK_PIPELINES_HPP*/
