#include "Pipelines.hpp"

#include <Context.hpp>
#include <Framebuffer.hpp>
#include <TypeConversions.hpp>
#include <logger/Logger.hpp>
#include <trace/Trace.hpp>
#include <vulkan/vulkan_to_string.hpp>

namespace satr::vk {

template<::vk::PipelineBindPoint bind_point>
PipelineImpl<bind_point>::PipelineImpl(::vk::Pipeline handle, ::vk::Device device,
                                       ::vk::PipelineLayout layout, std::span<Module> shaders,
                                       VertexAttributes&& vertex_attributes,
                                       Descriptors&& descriptors, PushConstants&& push_consts) :
    _handle(handle), _device(device), _layout(layout), _shaders(shaders.begin(), shaders.end()),
    _vertex_attributes(std::move(vertex_attributes)), _descriptors(std::move(descriptors)),
    _push_constants(std::move(push_consts)) {
    // Update reference to shaders' storage.
    _vertex_attributes._shader_modules = _shaders;
    _descriptors._shader_modules = _shaders;
    spec::trace("Pipeline::new(", _handle, ")");
}
template<::vk::PipelineBindPoint bind_point>
PipelineImpl<bind_point>::PipelineImpl(::vk::Pipeline handle, ::vk::Device device,
                                       ::vk::PipelineLayout layout, Module shader,
                                       Descriptors&& descriptors, PushConstants&& push_consts) :
    _handle(handle), _device(device), _layout(layout), _shaders(1, shader),
    _vertex_attributes(), _descriptors(std::move(descriptors)),
    _push_constants(std::move(push_consts)) {
    // Update reference to shaders' storage.
    _vertex_attributes._shader_modules = _shaders;
    _descriptors._shader_modules = _shaders;
    spec::trace("Pipeline::new(", _handle, ")");
}
template<::vk::PipelineBindPoint bind_point>
PipelineImpl<bind_point>::PipelineImpl(PipelineImpl<bind_point>&& o) noexcept :
    _handle(std::exchange(o._handle, nullptr)), _device(std::exchange(o._device, nullptr)),
    _layout(std::exchange(o._layout, nullptr)), _shaders(std::move(o._shaders)),
    _vertex_attributes(std::move(o._vertex_attributes)),
    _descriptors(std::move(o._descriptors)), _push_constants(std::move(o._push_constants)) {
    // Update reference to shaders' storage.
    _vertex_attributes._shader_modules = _shaders;
    _descriptors._shader_modules = _shaders;
    spec::trace("Pipeline::construct_move(", _handle, ")");
}
template<::vk::PipelineBindPoint bind_point>
PipelineImpl<bind_point>& PipelineImpl<bind_point>::operator=(
     PipelineImpl<bind_point>&& o) noexcept {
    _handle = std::exchange(o._handle, nullptr);
    _device = std::exchange(o._device, nullptr);
    _layout = std::exchange(o._layout, nullptr);
    _shaders = std::move(o._shaders);
    _vertex_attributes = std::move(o._vertex_attributes);
    _descriptors = std::move(o._descriptors);
    _push_constants = std::move(o._push_constants);
    // Update reference to shaders' storage.
    _vertex_attributes._shader_modules = _shaders;
    _descriptors._shader_modules = _shaders;
    spec::trace("Pipeline::assign_move(", _handle, ")");
    return *this;
}
template<::vk::PipelineBindPoint bind_point> PipelineImpl<bind_point>::~PipelineImpl() {
    if (_handle) {
        spec::trace("Pipeline::delete(", _handle, ")");
        _device.destroyPipeline(_handle);
        _handle = nullptr;
        _device.destroyPipelineLayout(_layout);
        _layout = nullptr;
        _descriptors.free_set_layouts(_device);
        // NOTE: There might be problems with calling field destructors while _handle is null.
    }
}

template<::vk::PipelineBindPoint bind_point>
std::optional<PipelineImpl<bind_point>> PipelineImpl<bind_point>::compile(
     const Context& context, const FramebufferChain& framebuffer,
     std::span<Module> shader_modules, const GraphicsPipelineOptions& options) {
    ::vk::Device dev = context.get_active_device();
    VertexAttributes vertex_attributes(shader_modules);
    Descriptors descriptors(shader_modules);
    PushConstants push_consts(shader_modules);
    ::vk::PipelineLayout layout = options.make_pipeline_layout(dev, descriptors, push_consts);
    GraphicsPipelineOptions::DynamicGraphicsPipelineCreateInfo cinfo =
         options.make_partial_create_info();
    options.finalize_create_info(cinfo, shader_modules, vertex_attributes, framebuffer, layout);

    auto [r, h] = dev.createGraphicsPipeline(nullptr, cinfo);
    if (r != ::vk::Result::eSuccess) {
        if (r == ::vk::Result::ePipelineCompileRequired) {
            logger.logw("Created a non-compiled pipeline.");
        }
        else {
            logger.loge("Couldn't create pipeline(", to_string(r), ")!");
            return {};
        }
    }

    return PipelineImpl(h, dev, layout, shader_modules, std::move(vertex_attributes),
                        std::move(descriptors), std::move(push_consts));
}
template<::vk::PipelineBindPoint bind_point>
std::optional<PipelineImpl<bind_point>> PipelineImpl<bind_point>::compile(
     const Context& context, Module shader, const ComputePipelineOptions& options) {
    ::vk::Device dev = context.get_active_device();
    Descriptors descriptors(std::span(&shader, 1));
    PushConstants push_consts(std::span(&shader, 1));
    ::vk::PipelineLayout layout = options.make_pipeline_layout(dev, descriptors, push_consts);
    auto cinfo = options.make_create_info(shader, layout);

    auto [r, h] = dev.createComputePipeline(nullptr, cinfo);
    if (r != ::vk::Result::eSuccess) {
        if (r == ::vk::Result::ePipelineCompileRequired) {
            logger.logw("Created a non-compiled pipeline.");
        }
        else {
            logger.loge("Couldn't create pipeline(", to_string(r), ")!");
            return {};
        }
    }

    return PipelineImpl(h, dev, layout, shader, std::move(descriptors), std::move(push_consts));
}

template<::vk::PipelineBindPoint bind_point>
void PipelineImpl<bind_point>::bind(::vk::CommandBuffer cmd_buf) {
    cmd_buf.bindPipeline(bind_point, _handle);
}
template<::vk::PipelineBindPoint bind_point>
void PipelineImpl<bind_point>::bind_sets(::vk::CommandBuffer cmd_buf,
                                         std::span<const ::vk::DescriptorSet> sets,
                                         uint32_t first_set) {
    cmd_buf.bindDescriptorSets(bind_point, _layout, first_set, sets, {});
}

template<::vk::PipelineBindPoint bind_point>
void PipelineImpl<bind_point>::update_viewport(::vk::CommandBuffer cmd_buf,
                                               const FramebufferChain& framebuffer,
                                               size_t chain_index) {
    auto size = framebuffer.get_size(chain_index);
    ::vk::Viewport viewport{0.0f, 0.0f, static_cast<float>(size.x), static_cast<float>(size.y),
                            0.0f, 1.0f};
    cmd_buf.setViewport(0, 1, &viewport);

    auto scissor = vec_to_rect(size);
    cmd_buf.setScissor(0, 1, &scissor);
}

}  // namespace satr::vk

template class satr::vk::PipelineImpl<::vk::PipelineBindPoint::eGraphics>;
template class satr::vk::PipelineImpl<::vk::PipelineBindPoint::eCompute>;
