#ifndef SATR_VK_DESCRIPTORSETPACK_HPP
#define SATR_VK_DESCRIPTORSETPACK_HPP
/**
 * @file
 * @brief
 */

#include <Utils.hpp>
#include <vulkan/vulkan.hpp>

namespace satr::vk {

/* Fwd decls */
class Context;

class DescriptorSetPack : public spec::INonCopyable {
   protected:
    Context* _context;
    ::vk::DescriptorPool _pool;
    std::vector<::vk::DescriptorSet> _sets;

   public:
    constexpr DescriptorSetPack() noexcept : _context(nullptr), _pool(nullptr) {}
    DescriptorSetPack(DescriptorSetPack&&) noexcept;
    DescriptorSetPack& operator=(DescriptorSetPack&&) noexcept;

    ~DescriptorSetPack();

   protected:
    DescriptorSetPack(Context& context, ::vk::DescriptorPool pool,
                      std::vector<::vk::DescriptorSet>&& sets);

    friend class Context;

   public:
    operator bool() const {
        return _context != nullptr;
    }
    ::vk::DescriptorSet operator[](size_t i) const {
        return get_set(i);
    }

    const std::vector<::vk::DescriptorSet>& get_sets() const {
        return _sets;
    }
    ::vk::DescriptorSet get_set(size_t i) const {
        return _sets.at(i);
    }
};

}  // namespace satr::vk

#endif /*SATR_VK_DESCRIPTORSETPACK_HPP*/