#include "DescriptorSetPack.hpp"

#include <Context.hpp>

#include <utility>

namespace satr::vk {

DescriptorSetPack::DescriptorSetPack(Context& context, ::vk::DescriptorPool pool,
                                     std::vector<::vk::DescriptorSet>&& sets) :
    _context(&context),
    _pool(pool), _sets(std::move(sets)) {}

DescriptorSetPack::DescriptorSetPack(DescriptorSetPack&& o) noexcept :
    _context(std::exchange(o._context, nullptr)), _pool(std::exchange(o._pool, nullptr)),
    _sets(std::move(o._sets)) {}
DescriptorSetPack& DescriptorSetPack::operator=(DescriptorSetPack&& o) noexcept {
    // Free old pools.
    if (_context) {
        _context->free_descriptor_set_pack(_pool, _sets);
        // _sets.clear();
    }

    _context = std::exchange(o._context, nullptr);
    _pool = std::exchange(o._pool, nullptr);
    _sets = std::move(o._sets);

    return *this;
}

DescriptorSetPack::~DescriptorSetPack() {
    if (_context) {
        _context->free_descriptor_set_pack(_pool, _sets);
        _context = nullptr;
        _pool = nullptr;
        _sets.clear();
    }
}

}  // namespace satr::vk
