#version 450

struct SceneProperties {
    vec2 offset;
    vec2 extent;
} scene_properties;

#define MAX_SCENE_COUNT 8

layout(set = 0, binding = 0) uniform Scenes {
    SceneProperties properties[MAX_SCENE_COUNT];
    vec4 visibilities[((MAX_SCENE_COUNT-1)/4)+1];
} scenes;

layout(set = 0, binding = 1) uniform sampler scene_sampler;
layout(set = 0, binding = 2) uniform texture2D scenes_attachments[MAX_SCENE_COUNT];

layout(location = 0) in vec2 frag_pos;
layout(location = 1) in flat int instance_index;

layout(location = 0) out vec4 color_output;

void main() {
    color_output = texture(sampler2D(scenes_attachments[instance_index], scene_sampler), frag_pos);
    color_output.a *= scenes.visibilities[instance_index / 4][instance_index % 4];
}
