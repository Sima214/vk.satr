#ifndef SATR_VK_PRESENTER_HPP
#define SATR_VK_PRESENTER_HPP
/**
 * @file
 * @brief Helper for acquiring Image(s) from window surface.
 */

#include <Context.hpp>
#include <Framebuffer.hpp>
#include <GlfwWindow.hpp>
#include <Image.hpp>
#include <Utils.hpp>
#include <vulkan/vulkan.hpp>

namespace satr::vk {

class Presenter : public spec::INonCopyable {
   protected:
    ::vk::SurfaceKHR _surface;
    ::vk::SwapchainKHR _swapchain;

    ::vk::SurfaceFormatKHR _surface_format;
    ::vk::PresentModeKHR _present_immediate_mode = ::vk::PresentModeKHR::eFifo;
    ::vk::PresentModeKHR _present_vsync_mode = ::vk::PresentModeKHR::eFifo;
    uint32_t _preferred_image_count;

    std::vector<Image2D> _swapchain_images;

    FramebufferChain _framebuffer;
    /**
     * Framebuffer needs to be recreated.
     */
    bool _out_of_date;

    /** Frame-rate control */
    bool _vsync_enabled;
    // TODO: int _skip_frames;

   public:
    /** Constructors */
    Presenter();  // Empty/Invalid

    /** Lifecycle management */
    bool init(Context& instance, satr::nui::Window& window, bool use_depth_buffer = false);
    /**
     * @param force Skip out_of_date flag check and device wait idle.
     */
    bool recreate(Context& instance, satr::nui::Window& window, bool force = false);
    void deinit(Context& instance);

    /** Getters/Setters */
    bool is_out_of_date() const {
        return _out_of_date;
    }

    ::vk::SurfaceKHR get_surface() const {
        return _surface;
    }
    ::vk::SurfaceFormatKHR get_surface_format() const {
        return _surface_format;
    }

    ::vk::SwapchainKHR get_swapchain() const {
        return _swapchain;
    }

    FramebufferChain& get_framebuffer() {
        return _framebuffer;
    }
    uint32_t get_chain_size() const {
        return _swapchain_images.size();
    }

    /**
     * Configures the frame limit.
     *
     * Negative values enable Vsync.
     */
    bool set_frame_limit(int limit);

    /** Operations */
    uint32_t acquire(::vk::Device device, ::vk::Semaphore window_surface_ready_signal);
    bool submit(const SharedQueue& queue, ::vk::Semaphore rendering_complete_signal,
                uint32_t index);

   protected:
    /** Events */

    /** Internal lifecycle */
    bool _init_swapchain(::vk::PhysicalDevice, ::vk::Device, satr::nui::Window&);
    bool _init_swapchain_framebuffer(Context& context, bool use_depth_attachment);
    void _deinit_swapchain(::vk::Device);
};

}  // namespace satr::vk

namespace vk {

std::ostream& operator<<(std::ostream& o, const SurfaceFormatKHR& v);

}  // namespace vk

#endif /*SATR_VK_PRESENTER_HPP*/
