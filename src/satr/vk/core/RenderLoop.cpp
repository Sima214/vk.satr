
#include "RenderLoop.hpp"

#include <CommandBuffer.hpp>
#include <Context.hpp>
#include <Image.hpp>
#include <LoaderUtils.hpp>
#include <Presenter.hpp>
#include <Scene.hpp>
#include <SharedQueue.hpp>
#include <TypeConversions.hpp>
#include <config/Tunables.hpp>
#include <logger/Logger.hpp>
#include <trace/Trace.hpp>
#include <vulkan/vulkan_to_string.hpp>

#include <algorithm>
#include <cstdint>
#include <cstring>
#include <memory>
#include <mutex>
#include <new>
#include <string>

using namespace std::string_literals;

namespace satr::vk {

const uint32_t RenderLoop::VERTEX_SHADER_CODE[] =
#include "RenderLoopComposite.vert.spirv.h"
     ;

const uint32_t RenderLoop::FRAGMENT_SHADER_CODE[] =
#include "RenderLoopComposite.frag.spirv.h"
     ;

void RenderLoop::__framebuffer_size_callback(satr::nui::Window& window, glm::ivec2 size) {
    RenderLoop* obj = reinterpret_cast<RenderLoop*>(window.get_user_data());
    obj->_framebuffer_size_callback(size);
}
void RenderLoop::__close_callback(satr::nui::Window& window) {
    RenderLoop* obj = reinterpret_cast<RenderLoop*>(window.get_user_data());
    obj->_close_callback();
}
void RenderLoop::__focus_callback(satr::nui::Window& window, bool focused) {
    RenderLoop* obj = reinterpret_cast<RenderLoop*>(window.get_user_data());
    obj->_focus_callback(focused);
}
void RenderLoop::__on_keyboard_button_press(satr::nui::Window& window,
                                            satr::nui::Window::Keys key, int scancode,
                                            satr::nui::Window::ButtonState action,
                                            satr::nui::Window::Modifiers mods) {
    RenderLoop* obj = reinterpret_cast<RenderLoop*>(window.get_user_data());
    obj->_on_keyboard_button_press(key, scancode, action, mods);
}
void RenderLoop::__on_char_input(satr::nui::Window& window, spec::utf8_codepoint c) {
    RenderLoop* obj = reinterpret_cast<RenderLoop*>(window.get_user_data());
    obj->_on_char_input(c);
}
void RenderLoop::__on_mouse_button_press(satr::nui::Window& window,
                                         satr::nui::Window::MouseButtons button,
                                         satr::nui::Window::ButtonState action,
                                         satr::nui::Window::Modifiers mods) {
    RenderLoop* obj = reinterpret_cast<RenderLoop*>(window.get_user_data());
    obj->_on_mouse_button_press(button, action, mods);
}
void RenderLoop::__on_mouse_scroll(satr::nui::Window& window, glm::dvec2 offset) {
    RenderLoop* obj = reinterpret_cast<RenderLoop*>(window.get_user_data());
    obj->_on_mouse_scroll(offset);
}
void RenderLoop::__on_mouse_movement(satr::nui::Window& window, glm::dvec2 pos) {
    RenderLoop* obj = reinterpret_cast<RenderLoop*>(window.get_user_data());
    obj->_on_mouse_movement(pos);
}

RenderLoop::RenderLoop(Context& engine, satr::nui::Window& window) :
    _engine(engine), _window(window) {
    // Register callbacks.
    _window.set_user_data(this);
    _cb_uid_fbsize = _window.register_framebuffer_size_callback(__framebuffer_size_callback);
    _cb_uid_close = _window.register_close_callback(__close_callback);
    _cb_uid_focus = _window.register_focus_callback(__focus_callback);
    _cb_uid_keys = _window.register_key_callback(__on_keyboard_button_press);
    _cb_uid_chars = _window.register_char_callback(__on_char_input);
    _cb_uid_buttons = _window.register_mouse_button_callback(__on_mouse_button_press);
    _cb_uid_scrolls = _window.register_scroll_callback(__on_mouse_scroll);
    _cb_uid_move = _window.register_cursor_pos_callback(__on_mouse_movement);

    // Init swapchain.
    if (!spec::tunables::get_bool("VKSATR_DISABLE_VSYNC").second) {
        Presenter::set_frame_limit(-1); /* vsync */
    }
    if (!Presenter::init(engine, window, false)) {
        std::abort();
    }
}

RenderLoop::~RenderLoop() {
    // Sanity check.
    if (_scenes_count != 0) {
        spec::fatal("RenderLoop destructor called while scenes are still registered!");
    }
    // Cleanup resources allocated from setup.
    for (RenderData& rd : _render_data) {
        rd.destroy(get_device());
    }
    get_device().destroy(_render_data_cmd_buf_pool);
    // Destroy swapchain.
    Presenter::deinit(_engine);
    // Revert callback registration.
    _window.unregister_cursor_pos_callback(_cb_uid_move);
    _window.unregister_scroll_callback(_cb_uid_scrolls);
    _window.unregister_mouse_button_callback(_cb_uid_buttons);
    _window.unregister_char_callback(_cb_uid_chars);
    _window.unregister_key_callback(_cb_uid_keys);
    _window.unregister_focus_callback(_cb_uid_focus);
    _window.unregister_close_callback(_cb_uid_close);
    _window.unregister_framebuffer_size_callback(_cb_uid_fbsize);
}
void RenderLoop::RenderData::destroy(::vk::Device dev) {
    // Sanity check.
    if (in_flight) {
        logger.logw("Destroying RenderLoop::RenderData object while frame is in flight!");
    }
    dev.destroy(frame_complete);
    frame_complete = nullptr;
    dev.destroy(window_surface_ready);
    window_surface_ready = nullptr;
    dev.destroy(rendering_complete);
    rendering_complete = nullptr;
    /** Command buffers get freed along with their CommandPools. */
}

void RenderLoop::setup() {
    // Sanity check.
    if (!_render_data.empty()) {
        spec::fatal("RenderLoop::setup: must be called only once!");
    }
    const uint32_t max_frames_in_flight = Presenter::get_chain_size();

    // Pipeline creation.
    {
        auto vertex_shader = _engine.new_shader(VERTEX_SHADER_CODE);
        auto fragment_shader = _engine.new_shader(FRAGMENT_SHADER_CODE);
        satr::vk::Module shaders[] = {vertex_shader, fragment_shader};
        satr::vk::GraphicsPipelineOptions pipeline_options;
        pipeline_options.dynamic_state_add(::vk::DynamicState::eViewport);
        pipeline_options.dynamic_state_add(::vk::DynamicState::eScissor);
        pipeline_options.primitive_topology(::vk::PrimitiveTopology::eTriangleStrip);
        pipeline_options.rasterizer_cull_mode(::vk::CullModeFlagBits::eFront);
        pipeline_options.rasterizer_front_face(::vk::FrontFace::eCounterClockwise);
        pipeline_options.depth_test_disable();
        pipeline_options.color_blend_enable(true);
        pipeline_options.color_blend_src_factor(::vk::BlendFactor::eSrcAlpha);
        pipeline_options.color_blend_dst_factor(::vk::BlendFactor::eOneMinusSrcAlpha);
        pipeline_options.color_blend_op(::vk::BlendOp::eAdd);
        pipeline_options.alpha_blend_src_factor(::vk::BlendFactor::eOne);
        pipeline_options.alpha_blend_dst_factor(::vk::BlendFactor::eZero);
        pipeline_options.alpha_blend_op(::vk::BlendOp::eAdd);
        _pipeline = _engine.new_pipeline(get_framebuffer(), shaders, pipeline_options);
    }
    if (!_pipeline) {
        logger.logf("Couldn't setup RenderLoop's composition pipeline!");
    }

    // Samplers/descriptor sets.
    auto opt_scene_props_desc = _pipeline.get_descriptors().retrieve_by_name("scenes");
    if (!opt_scene_props_desc) {
        logger.logf("Couldn't find `scenes` uniform block!");
    }
    satr::vk::Descriptor scene_props_desc = opt_scene_props_desc.value();
    auto scene_state_layout =
         _pipeline.get_descriptors().get_set_layout(scene_props_desc.get_set());
    _scene_state_set_pack =
         _engine.new_descriptor_set_pack(max_frames_in_flight, scene_state_layout);
    if (!_scene_state_set_pack) {
        std::abort();
    }

    _scene_state_sampler =
         _engine.new_sampler(::vk::Filter::eLinear, ::vk::SamplerAddressMode::eRepeat);
    auto opt_scene_sampler_desc = _pipeline.get_descriptors().retrieve_by_name("scene_sampler");
    if (!opt_scene_sampler_desc) {
        logger.logf("Couldn't find `scene_sampler` sampler!");
    }
    satr::vk::Descriptor scene_sampler_desc = opt_scene_sampler_desc.value();
    if (scene_sampler_desc.get_set() != scene_props_desc.get_set()) {
        logger.logf("Mismatched sets between `scenes` and `scene_sampler` descriptors!");
    }
    for (size_t i = 0; i < max_frames_in_flight; i++) {
        _scene_state_sampler.update_descriptor(_scene_state_set_pack[i],
                                               scene_sampler_desc.get_binding());
    }

    {
        SingleUseCommandBufferContext trsfr_ctx(get_engine(), QueueSlotRole::Graphics);
        static const uint8_t INVALID_ATTACHMENT_INPUT_DATA[4 * 4] = {
             0xcb, 0x7f, 0x7f, 0xff, 0, 0, 0, 0xff, 0, 0, 0, 0xff, 0xcb, 0x7f, 0x7f, 0xff};
        Image2D invalid_attachment_input =
             load_image2d(_engine, trsfr_ctx, "render_loop_invalid",
                          ::vk::Format::eR8G8B8A8Unorm, {2, 2}, INVALID_ATTACHMENT_INPUT_DATA,
                          ImageOptions(::vk::ImageUsageFlagBits::eSampled)
                               .tiling(::vk::ImageTiling::eOptimal));
        trsfr_ctx.submit();
        _scene_attachment_invalid = invalid_attachment_input.get_view().value();
        trsfr_ctx.wait();
    }

    auto opt_scene_attachments_desc =
         _pipeline.get_descriptors().retrieve_by_name("scenes_attachments");
    if (!opt_scene_attachments_desc) {
        logger.logf("Couldn't find `scenes_attachments` texture array!");
    }
    satr::vk::Descriptor scene_attachments_desc = opt_scene_attachments_desc.value();
    if (scene_attachments_desc.get_set() != scene_props_desc.get_set()) {
        logger.logf("Mismatched sets between `scenes` and `scenes_attachments` descriptors!");
    }
    if (scene_attachments_desc.get_array_dims().size() != 1) {
        logger.logf("Descriptor `scenes_attachments` must be an 1D array!");
    }
    _max_scene_count = scene_attachments_desc.get_array_dims()[0];
    _scene_attachments_binding = scene_attachments_desc.get_binding();

    size_t max_scene_count = get_max_scene_count();
    _scenes.reset(new SceneData[max_scene_count]);
    _scenes_staging_data.reset(
         new std::tuple<Scene::RenderData*, bool /*reused*/>[max_scene_count]);

    // Create render data for each available frame.
    _render_data_cmd_buf_pool = _engine.create_command_pool(QueueSlotRole::Graphics);
    _render_data.reserve(max_frames_in_flight);
    for (size_t i = 0; i < max_frames_in_flight; i++) {
        _render_data.emplace_back(i, _engine, _render_data_cmd_buf_pool,
                                  _scene_state_set_pack[i], scene_props_desc.get_binding(),
                                  max_scene_count);
    }
    _render_data_index = 0;

    get_framebuffer().set_color_clear_value(0, satr::vk::rgba_hex_to_vec(0xcb7f7fff));
}
size_t RenderLoop::get_max_scene_count() const {
    return _max_scene_count;
}

RenderLoop::RenderData::RenderData([[maybe_unused]] size_t i, satr::vk::Context& engine,
                                   ::vk::CommandPool cmd_pool,
                                   ::vk::DescriptorSet scene_state_set_,
                                   uint32_t scene_props_binding, size_t max_scene_count) :
    scenes_in_flight(new Scene[max_scene_count]), in_flight(false) {
    cmd_buf = engine.create_command_buffer(cmd_pool);
    frame_complete = engine.create_fence(false);
    std::tie(window_surface_ready, rendering_complete) = engine.create_semaphores<2>();
    {
        scene_state_set = scene_state_set_;
        scene_props_buffer = engine.new_uniform_buffer(
             "scene_props_"s + std::to_string(i),
             sizeof_pipeline_scene_props_state(max_scene_count), {}, {}, true, false);
        if (!scene_props_buffer) {
            spec::fatal("Couldn't allocate uniform buffer!");
        }
        scene_props_buffer.update_uniform(scene_state_set, scene_props_binding);
    }
}

void RenderLoop::loop() {
    setup();
    _loop_timer_offset = nui::GLFW::get_instance().get_time();
    while (_scenes_count != 0 || !_window.should_close()) {
        render();
    }
    // Wait for all outstanding frames.
    const uint32_t chain_size = Presenter::get_chain_size();
    for (size_t i = 0; i < chain_size; i++) {
        size_t rel_i = (_render_data_index + i) % chain_size;
        _render_data[rel_i].wait(get_device(), get_max_scene_count());
    }
}

void RenderLoop::RenderData::wait(::vk::Device dev, size_t max_scene_count) {
    // Nothing to wait for.
    if (!in_flight) {
        return;
    }

    // Wait.
    ::vk::Result fence_result;
    do {
        fence_result =
             dev.waitForFences({frame_complete}, true, std::numeric_limits<uint64_t>::max());
    } while (fence_result == ::vk::Result::eTimeout);
    if (fence_result != ::vk::Result::eSuccess) {
        logger.logf("Error while waiting on fence(", to_string(fence_result), ")!");
    }
    // Reset.
    dev.resetFences({frame_complete});
    for (size_t i = 0; i < max_scene_count; i++) {
        scenes_in_flight[i].reset();
    }
    in_flight = false;
}

void RenderLoop::render() {
    const uint32_t chain_size = Presenter::get_chain_size();
    RenderData& render_data = _render_data[_render_data_index];

    // Wait for previous frame completion.
    render_data.wait(get_device(), get_max_scene_count());
    /**
     * NOTE: VUID-vkAcquireNextImageKHR-semaphore-01779
     * The wait for fence operation must precede due to the reuse of the semaphore by
     * acquireNextImage.
     */
    uint32_t current_chain_index =
         Presenter::acquire(get_device(), render_data.window_surface_ready);

    if (current_chain_index < chain_size) [[likely]] {
        // Update some state.
        get_engine().get_allocator().on_allocator_new_frame();

        // Rendering.
        render_scenes(
             _render_data_index); /* 1:1 mapping between RenderLoop and Scene RenderData */
        _render(render_data, current_chain_index);

        // Submission.
        const SharedQueue& queue = _engine.get_queue(QueueSlotRole::Graphics);
        {
            const size_t active_scene_count = _scenes_count.load();
            // TODO: allocate on stack.
            std::unique_ptr<::vk::Semaphore[]> wait_semaph(
                 new ::vk::Semaphore[active_scene_count + 1]);
            std::unique_ptr<::vk::PipelineStageFlags[]> wait_stages(
                 new ::vk::PipelineStageFlags[active_scene_count + 1]);
            size_t wait_scene_count = 0;
            for (size_t i = 0; i < active_scene_count; i++) {
                auto [scene_render_data, reused] = _scenes_staging_data[i];
                if (!reused) {
                    wait_semaph[wait_scene_count] = scene_render_data->rdy_sig;
                    wait_stages[wait_scene_count] =
                         ::vk::PipelineStageFlagBits::eFragmentShader;
                    wait_scene_count++;
                }
                else {
                    logger.logd("RenderLoop::render: scene repeat");
                }
            }
            wait_semaph[wait_scene_count] = render_data.window_surface_ready;
            wait_stages[wait_scene_count] = ::vk::PipelineStageFlagBits::eColorAttachmentOutput;
            ::vk::SubmitInfo info;
            info.waitSemaphoreCount = wait_scene_count + 1;
            info.pWaitSemaphores = wait_semaph.get();
            info.pWaitDstStageMask = wait_stages.get();
            info.commandBufferCount = 1;
            info.pCommandBuffers = &render_data.cmd_buf;
            info.signalSemaphoreCount = 1;
            info.pSignalSemaphores = &render_data.rendering_complete;
            if (!queue.submit(info, render_data.frame_complete)) {
                std::abort();
            }
            for (size_t i = 0; i < active_scene_count; i++) {
                render_data.scenes_in_flight[i] = _scenes[i].scene;
            }
            render_data.in_flight = true;
        }
        if (!Presenter::submit(queue, render_data.rendering_complete, current_chain_index)) {
            std::abort();
        }
        if (!Presenter::is_out_of_date()) {
            /**
             * If presenter submit failed,
             * then event handling happens
             * after swapchain recreation.
             */
            on_frame_submitted();
        }
        // Select next render state index.
        _render_data_index = (_render_data_index + 1) % chain_size;
    }
    if (Presenter::is_out_of_date()) {
        // Wait for all in-flight frames.
        for (size_t i = 0; i < chain_size; i++) {
            size_t rel_i = (_render_data_index + i) % chain_size;
            _render_data[rel_i].wait(get_device(), get_max_scene_count());
        }
        // Full swapchain & framebuffer recreation.
        Presenter::recreate(_engine, _window, true);
        // Perform event handling that was skipped above.
        on_frame_submitted();
    }
}

void RenderLoop::_render(RenderData& render_data, uint32_t current_chain_index) {
    render_data.cmd_buf.reset();
    // Rendering start.
    {
        ::vk::CommandBufferBeginInfo binfo(::vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
        ::vk::Result r = render_data.cmd_buf.begin(binfo);
        if (r != ::vk::Result::eSuccess) {
            logger.logf("Couldn't begin command recording(", to_string(r), ")!");
        }
    }
    get_framebuffer().start_pass(render_data.cmd_buf, current_chain_index);

    // Rendering state update.
    const size_t max_scenes_count = get_max_scene_count();
    const size_t scenes_count = _scenes_count.load();
    {
        // Scene properties uniform buffer.
        uint8_t* buf_dat = (uint8_t*) render_data.scene_props_buffer.get_memory().get_mapping();
        for (size_t i = 0; i < scenes_count; i++) {
            size_t s_i = scenes_count - i - 1;
            uintptr_t props_offset = i * (sizeof(glm::vec2) * 2);
            std::memcpy(buf_dat + props_offset, &_scenes[s_i].properties.offset,
                        sizeof(glm::vec2) * 2);
            uintptr_t vis_offset =
                 offsetof_pipeline_scene_props_visibility_state(max_scenes_count) +
                 i * sizeof(float);
            std::memcpy(buf_dat + vis_offset, &_scenes[s_i].properties.visibility,
                        sizeof(float));
        }
        render_data.scene_props_buffer.get_memory().flush();

        // Scene attachments.
        std::unique_ptr<::vk::DescriptorImageInfo[]> attachment_info(
             new ::vk::DescriptorImageInfo[max_scenes_count]);
        for (size_t i = 0; i < max_scenes_count; i++) {
            attachment_info[i].sampler = nullptr;
            attachment_info[i].imageLayout = ::vk::ImageLayout::eShaderReadOnlyOptimal;
            if (i < scenes_count) {
                size_t s_i = scenes_count - i - 1;
                // auto [scene_data, reused] = _scenes_staging_data[s_i];
                // if (scene_data == nullptr) {
                //     spec::fatal("RenderLoop::_render: Cannot upload null scene staging
                //     data!");
                // }
                // logger.logd("RenderLoop::render scene #", s_i, " uses ",
                //             scene_data->chain_index);
                // TODO: Take reference to scene's image view in case it resizes.
                attachment_info[i].imageView =
                     _scenes[s_i]
                          .scene.get_framebuffer()
                          .get_color_attachment_views(_render_data_index)[0]
                          .get_handle();
            }
            else {
                attachment_info[i].imageView = _scene_attachment_invalid.get_handle();
            }
        }
        ::vk::WriteDescriptorSet write_info(render_data.scene_state_set,
                                            _scene_attachments_binding, 0, max_scenes_count,
                                            ::vk::DescriptorType::eSampledImage);
        write_info.pImageInfo = attachment_info.get();
        get_device().updateDescriptorSets(1, &write_info, 0, nullptr);
    }

    // Rendering draw.
    if (scenes_count > 0) {
        _pipeline.bind(render_data.cmd_buf);
        _pipeline.update_viewport(render_data.cmd_buf, get_framebuffer(), current_chain_index);
        _pipeline.bind_sets(render_data.cmd_buf, std::span(&render_data.scene_state_set, 1));
        render_data.cmd_buf.draw(4, scenes_count, 0, 0);
    }

    // Rendering end.
    get_framebuffer().end_pass(render_data.cmd_buf);
    {
        ::vk::Result r = render_data.cmd_buf.end();
        if (r != ::vk::Result::eSuccess) {
            logger.logf("Couldn't end command recording(", to_string(r), ")!");
        }
    }
}

void RenderLoop::on_frame_submitted() {
    _update_scenes();
    _window.poll_events();
    _dispatch_window_events();
}
void RenderLoop::render_scenes(size_t index) {
    const size_t scenes_count = _scenes_count.load();
    for (size_t i = 0; i < scenes_count; i++) {
        _scenes_staging_data[i] = _scenes[i].scene.on_render(index);
    }
}

bool RenderLoop::append_scene(Scene scene, SceneProperties props) {
    if (scene.get() == nullptr) {
        logger.loge("RenderLoop::append_scene: tried to append an empty object!");
        return false;
    }
    std::lock_guard _l(_scenes_changes_mutex);
    const size_t scenes_count = _scenes_count.load();
    // Fail if scene already exists in either containers.
    if (_scenes) {
        for (size_t i = 0; i < scenes_count; i++) {
            if (_scenes[i].scene == scene) {
                return false;
            }
        }
    }
    for (auto& [scene_data, to_delete] : _scenes_changes_queue) {
        if (scene_data.scene == scene) {
            return false;
        }
    }
    // TODO: Scene count limit check happens asynchronously. Detect that here.
    glm::ivec2 size = get_window().get_framebuffer_size();
    glm::ivec2 scene_size(size.x * props.extent.x, size.y * props.extent.y);
    _scenes_changes_queue.emplace_back(SceneData(scene, props), false);
    return true;
}
bool RenderLoop::update_scene(Scene scene, SceneProperties props) {
    std::lock_guard _l(_scenes_changes_mutex);
    // Update queued data if that exists.
    for (auto& it : _scenes_changes_queue) {
        auto& [scene_data, to_delete] = it;
        if (scene_data.scene == scene) {
            if (to_delete) {
                logger.logw("RenderLoop::update_scene: trying to update scene "
                            "that is scheduled to be removed!");
            }
            it.first.properties = props;
            return true;
        }
    }
    // Check if the scene exists in active container.
    const size_t scenes_count = _scenes_count.load();
    if (_scenes) {
        for (size_t i = 0; i < scenes_count; i++) {
            // If found, then queue the update.
            if (_scenes[scenes_count].scene == scene) {
                _scenes_changes_queue.emplace_back(SceneData(scene, props), false);
                return true;
            }
        }
    }
    return false;
}
bool RenderLoop::remove_scene(const Scene scene) {
    std::lock_guard _l(_scenes_changes_mutex);
    const size_t scenes_count = _scenes_count.load();
    // Check if the scene exists in active container.
    bool found = false;
    if (_scenes) {
        for (size_t i = 0; i < scenes_count; i++) {
            if (_scenes[i].scene == scene) {
                found = true;
                break;
            }
        }
    }
    if (!found) [[unlikely]] {
        // Second chance, scene is so new, that it hasn't moved from the queue.
        for (auto it = _scenes_changes_queue.begin(); it != _scenes_changes_queue.end(); ++it) {
            auto& [scene_data, to_delete] = *it;
            if (scene_data.scene == scene) {
                if (to_delete) {
                    spec::fatal("RenderLoop::remove_scene: detected "
                                "pending removal of non-active scene!");
                }
                // Remove pending data.
                scene.on_remove();
                _scenes_changes_queue.erase(it);
                return true;
            }
        }
        logger.logw("RenderLoop::remove_scene: scene to be removed couldn't be found!");
        return false;
    }

    // Update if in queued data.
    scene.on_remove();
    for (auto& it : _scenes_changes_queue) {
        auto& [scene_data, to_delete] = it;
        if (scene_data.scene == scene) {
            it.second = true;
            return true;
        }
    }
    // Else push to front of queue.
    _scenes_changes_queue.emplace_front(SceneData(scene), true);
    return true;
}
std::optional<RenderLoop::SceneData> RenderLoop::retrieve_scene(SceneImpl* impl) {
    // Could be called from not the event handling thread.
    std::lock_guard _l(_scenes_changes_mutex);

    // Search in active list first.
    const size_t scenes_count = _scenes_count.load();
    if (_scenes) {
        for (size_t i = 0; i < scenes_count; i++) {
            if (_scenes[i].scene.get() == impl) {
                return _scenes[i];
            }
        }
    }

    // Search in pending list as a fallback.
    for (auto& [scene_data, to_delete] : _scenes_changes_queue) {
        if (scene_data.scene.get() == impl) {
            return scene_data;
        }
    }

    // Not found!
    return {};
}

double RenderLoop::get_loop_time() const {
    return nui::GLFW::get_instance().get_time() - _loop_timer_offset;
}
bool RenderLoop::is_mouse_captured() const {
    return _window.get_input_cursor_mode() == nui::Window::CursorMode::Disabled;
}
void RenderLoop::capture_mouse() {
    if (!is_mouse_captured()) {
        bool user_no_raw = spec::tunables::get_bool("VKSATR_DISABLE_RAW_INPUT").second;
        if (!user_no_raw && _window.raw_mouse_motion_supported()) {
            _window.set_input_raw_mouse_motion_mode(true);
        }
        _window.set_input_cursor_mode(nui::Window::CursorMode::Disabled);
        // (Re)-set starting point.
        _mouse_last_relative = _window.get_cursor_pos();
    }
}
void RenderLoop::release_mouse() {
    if (is_mouse_captured()) {
        _window.set_input_raw_mouse_motion_mode(false);
        _window.set_input_cursor_mode(nui::Window::CursorMode::Normal);
    }
}
void RenderLoop::close() {
    const size_t scenes_count = _scenes_count.load();
    for (size_t i = 0; i < scenes_count; i++) {
        _scenes[i].scene.on_close();
    }
    _window.set_should_close(true);
}

void RenderLoop::_dispatch_window_events() {
    if (is_mouse_captured()) {
        glm::dvec2 mouse_new_relative = _window.get_cursor_pos();
        glm::dvec2 mouse_diff_relative = mouse_new_relative - _mouse_last_relative;
        if (mouse_diff_relative != glm::dvec2(0)) {
            for (size_t i = 0; i < _scenes_count; i++) {
                if (_scenes[i].scene.on_mouse_motion(mouse_diff_relative)) {
                    break;
                }
            }
        }
        _mouse_last_relative = mouse_new_relative;
    }

    if (!_character_buffer.empty()) {
        for (size_t i = 0; i < _scenes_count; i++) {
            if (_scenes[i].scene.on_text_input(_character_buffer)) {
                break;
            }
        }
        _character_buffer.clear();
    }
}

void RenderLoop::_update_scenes() {
    std::lock_guard _l(_scenes_changes_mutex);
    const size_t scenes_max_count = get_max_scene_count();
    for (auto& [scene_data, to_delete] : _scenes_changes_queue) {
        // Check if scene already exists.
        const size_t scenes_count = _scenes_count.load();
        auto* scenes_end = _scenes.get() + scenes_count;
        auto* old_scene_data = std::find(_scenes.get(), scenes_end, scene_data);
        if (to_delete) {
            if (old_scene_data == scenes_end) {
                logger.logf("RenderLoop::_update_scenes: Scene to be deleted not found!");
            }
            for (auto* it = old_scene_data; it < scenes_end - 1; it++) {
                /* Copy operation, shouldn't be expensive,
                   as _scenes_changes_queue still holds a reference! */
                *it = *(it + 1);
            }
            // Clear past-the-new-end element.
            _scenes[--_scenes_count] = SceneData();
            // TODO: invalidate command buffers if reusing.
        }
        else {
            if (old_scene_data == scenes_end) {
                if (_scenes_count == scenes_max_count) {
                    logger.logf("RenderLoop::_update_scenes: Tried to append scenes past "
                                "the limit!");
                }
                // Prepare scene.
                auto size = _window.get_framebuffer_size();
                glm::ivec2 scene_size(size.x * scene_data.properties.extent.x,
                                      size.y * scene_data.properties.extent.y);
                bool scene_ok = scene_data.scene.on_setup(get_chain_size(), scene_size,
                                                          scene_data.properties.offset,
                                                          scene_data.properties.extent);
                if (!scene_ok) {
                    logger.logf("Cannot setup scene `", scene_data.scene.get_name(), "`!");
                }
                // Add scene.
                auto* insertion_pos = std::upper_bound(_scenes.get(), scenes_end, scene_data);
                std::copy(insertion_pos, scenes_end, insertion_pos + 1);
                *insertion_pos = scene_data;
                _scenes_count++;
                // TODO: invalidate command buffers if reusing.
            }
            else {
                // Update scene (implicit: scenes_count >= 1).
                bool requires_reordering = false;
                if (scenes_count != 1) {
                    // Check if new priority disrupts sorted order.
                    if (old_scene_data == _scenes.get()) {
                        // old_scene_data is at index 0.
                        requires_reordering =
                             scene_data.properties.priority < _scenes[1].properties.priority;
                    }
                    else if (old_scene_data == (scenes_end - 1)) {
                        // old_scene_data is at last index.
                        requires_reordering = scene_data.properties.priority >
                                              (scenes_end - 2)->properties.priority;
                    }
                    else {
                        // old_scene_data is somewhere in the middle.
                        requires_reordering = !((old_scene_data - 1)->properties.priority >=
                                                     scene_data.properties.priority &&
                                                scene_data.properties.priority >=
                                                     (old_scene_data + 1)->properties.priority);
                    }
                } /* else if scenes_count == 1 -> requires_reordering = false */

                if (!requires_reordering) {
                    old_scene_data->properties = scene_data.properties;
                }
                else {
                    if (scene_data.properties.priority < old_scene_data->properties.priority) {
                        // Goes to right side.
                        auto* updated_scene_data =
                             std::upper_bound(old_scene_data, scenes_end, scene_data);
                        std::copy(old_scene_data + 1, updated_scene_data, old_scene_data);
                        *(updated_scene_data - 1) = scene_data;
                    }
                    else {
                        // Goes to left side.
                        auto* updated_scene_data =
                             std::upper_bound(_scenes.get(), old_scene_data, scene_data);
                        std::copy(updated_scene_data, scenes_end - 1, updated_scene_data + 1);
                        *updated_scene_data = scene_data;
                    }
                }
            }
        }
    }
    _scenes_changes_queue.clear();
}

void RenderLoop::_framebuffer_size_callback(glm::ivec2 size) {
    for (size_t i = 0; i < _scenes_count; i++) {
        glm::ivec2 scene_size(size.x * _scenes[i].properties.extent.x,
                              size.y * _scenes[i].properties.extent.y);
        _scenes[i].scene.on_resize(scene_size, _scenes[i].properties.offset,
                                   _scenes[i].properties.extent);
    }
}
void RenderLoop::_close_callback() {
    for (size_t i = 0; i < _scenes_count; i++) {
        _scenes[i].scene.on_close();
    }
}
void RenderLoop::_focus_callback(bool focused) {
    for (size_t i = 0; i < _scenes_count; i++) {
        _scenes[i].scene.on_focus(focused);
    }
}

void RenderLoop::_on_keyboard_button_press(satr::nui::Window::Keys key, int scancode,
                                           satr::nui::Window::ButtonState action,
                                           satr::nui::Window::Modifiers mods) {
    if (key == satr::nui::Window::Keys::Escape && !mods.control && !mods.shift && mods.alt &&
        action == satr::nui::Window::ButtonState::Press) {
        close();
    }
    else if (key == satr::nui::Window::Keys::Pause && mods.control && !mods.shift && mods.alt &&
             action == satr::nui::Window::ButtonState::Press) {
        logger.logd("Dumping allocator state...");
        get_engine().get_allocator().allocator_dump_state(get_engine().get_app_name());
    }
    else {
        for (size_t i = 0; i < _scenes_count; i++) {
            if (_scenes[i].scene.on_key_press(key, scancode, action, mods)) {
                break;
            }
        }
    }
}
void RenderLoop::_on_char_input(spec::utf8_codepoint c) {
    _character_buffer.append(c.to_string());
}

void RenderLoop::_on_mouse_button_press(satr::nui::Window::MouseButtons button,
                                        satr::nui::Window::ButtonState action,
                                        satr::nui::Window::Modifiers mods) {
    for (size_t i = 0; i < _scenes_count; i++) {
        if (_scenes[i].scene.on_mouse_press(button, action, mods)) {
            break;
        }
    }
}
void RenderLoop::_on_mouse_scroll(glm::dvec2 offset) {
    for (size_t i = 0; i < _scenes_count; i++) {
        if (_scenes[i].scene.on_mouse_scroll(offset)) {
            break;
        }
    }
}
void RenderLoop::_on_mouse_movement(glm::dvec2 pos) {
    // TODO: Filter based on Scene boundaries.
    if (!is_mouse_captured()) {
        for (size_t i = 0; i < _scenes_count; i++) {
            if (_scenes[i].scene.on_mouse_move(pos)) {
                break;
            }
        }
    }
}

}  // namespace satr::vk
