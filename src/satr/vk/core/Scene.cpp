#include "Scene.hpp"

#include <RenderLoop.hpp>
#include <logger/Logger.hpp>
#include <vulkan/vulkan_to_string.hpp>

#include <cstdlib>
#include <memory>
#include <mutex>

#include <pthread.h>

namespace satr::vk {

SceneImpl::SceneImpl(const char* name, RenderLoop& root) : _root(root), _name(name) {}
SceneImpl::~SceneImpl() {
    // Clean-up vulkan resources.
    destroy_render_data();
}

Context& SceneImpl::get_engine() const {
    return _root.get_engine();
}
::vk::Device SceneImpl::get_device() const {
    return get_engine().get_active_device();
}
size_t SceneImpl::get_chain_size() const {
    return get_framebuffer().get_chain_size();
}

bool SceneImpl::setup_framebuffer(size_t chain_size, glm::ivec2 fb_size,
                                  size_t color_attachment_count,
                                  const Format* color_attachment_format,
                                  bool use_depth_attachment) {
    _framebuffer_target_size.store(fb_size);
    _framebuffer =
         FramebufferChain(_name, get_engine(), fb_size, chain_size, color_attachment_count,
                          color_attachment_format, use_depth_attachment);
    return static_cast<bool>(_framebuffer);
}
bool SceneImpl::setup_render_data(size_t chain_size) {
    _render_data = std::unique_ptr<RenderData[]>(new RenderData[chain_size]);
    _cmd_pool = get_engine().create_command_pool(QueueSlotRole::Graphics);
    if (!_cmd_pool) {
        return false;
    }
    for (size_t i = 0; i < get_chain_size(); i++) {
        _render_data[i].init(get_engine(), _cmd_pool);
    }
    return true;
}

void SceneImpl::RenderData::init(Context& engine, ::vk::CommandPool cmd_pool) {
    cmd_buf = engine.create_command_buffer(cmd_pool);
    rdy_sig = engine.create_semaphore();
}
void SceneImpl::RenderData::reset() {
    cmd_buf.reset();
}
void SceneImpl::RenderData::do_resize(FramebufferChain& framebuffer, size_t chain_index,
                                      glm::ivec2 target_size) {
    if (static_cast<glm::ivec2>(framebuffer.get_size(chain_index)) != target_size) {
        framebuffer.on_resize(chain_index, target_size);
    }
}
bool SceneImpl::RenderData::start(FramebufferChain& framebuffer, size_t chain_index) {
    {
        ::vk::CommandBufferBeginInfo binfo(::vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
        ::vk::Result r = cmd_buf.begin(binfo);
        if (r != ::vk::Result::eSuccess) {
            logger.loge("Couldn't begin command recording(", to_string(r), ")!");
            return false;
        }
    }
    framebuffer.start_pass(cmd_buf, chain_index);
    return true;
}
bool SceneImpl::RenderData::end(FramebufferChain& framebuffer) {
    framebuffer.end_pass(cmd_buf);
    {
        ::vk::Result r = cmd_buf.end();
        if (r != ::vk::Result::eSuccess) {
            logger.loge("Couldn't end command recording(", to_string(r), ")!");
            return false;
        }
    }
    return true;
}
void SceneImpl::RenderData::submit(const SharedQueue& queue) {
    ::vk::SubmitInfo submit_info;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &cmd_buf;
    submit_info.signalSemaphoreCount = 1;
    submit_info.pSignalSemaphores = &rdy_sig;
    queue.submit(submit_info);
}
void SceneImpl::RenderData::destroy(::vk::Device dev) {
    dev.destroy(rdy_sig);
}

bool SceneImpl::on_setup_complete() {
    mark_dirty();
    return true;
}

void SceneImpl::mark_dirty() {
    _timeline_counter += 1;
}
std::tuple<SceneImpl::RenderData*, bool /*reused*/> SceneImpl::on_render(size_t index) {
    RenderData& rnd_dat = _render_data[index];
    // logger.logd("Scene::on_render(#", index, "): timeline_counter=", _timeline_counter,
    //             ", rnd_dat_timeline=", rnd_dat.last_timeline);
    if (_timeline_counter == rnd_dat.last_timeline) {
        return {&rnd_dat, true};
    }
    else if (_timeline_counter > rnd_dat.last_timeline) {
        bool continue_rendering = render(index, rnd_dat);
        rnd_dat.last_timeline = _timeline_counter;
        if (continue_rendering) {
            mark_dirty();
        }
        return {&rnd_dat, false};
    }
    else {
        logger.logf("Scene::on_render: current timeline value is "
                    "less than RenderData's last timeline value!");
    }
}

bool SceneImpl::render(size_t chain_index, RenderData& rnd_dat) {
    // Render pass start.
    rnd_dat.reset();
    rnd_dat.do_resize(_framebuffer, chain_index, _framebuffer_target_size);
    if (!rnd_dat.start(_framebuffer, chain_index)) {
        std::abort();
    }
    // Render/user code.
    bool continue_rendering = render(chain_index, rnd_dat.cmd_buf);
    // Render pass end.
    if (!rnd_dat.end(_framebuffer)) {
        std::abort();
    }
    rnd_dat.submit(get_engine().get_queue(QueueSlotRole::Graphics));
    return continue_rendering;
}

void SceneImpl::on_resize(glm::ivec2 size, glm::vec2&, glm::vec2&) {
    // Framebuffer resizing happens asynchronously.
    _framebuffer_target_size.store(size);
}

void SceneImpl::destroy_render_data() {
    ::vk::Device dev = get_device();
    if (_render_data) {
        for (size_t i = 0; i < get_chain_size(); i++) {
            _render_data[i].destroy(dev);
        }
        dev.destroy(_cmd_pool);
        std::destroy_at(&_framebuffer);
    }
}
void SceneImpl::on_remove() {
    /* No-Op */
}
void SceneImpl::on_close() {
    // Remove self from render loop.
    auto opt_reg = _root.retrieve_scene(this);
    if (!opt_reg) {
        logger.logf("Scene::on_close: Cannot find self!");
    }
    _root.remove_scene(opt_reg->scene);
}
void SceneImpl::on_focus([[maybe_unused]] bool focused) {}

bool SceneImpl::on_key_press(satr::nui::Window::Keys, int, satr::nui::Window::ButtonState,
                             satr::nui::Window::Modifiers) {
    return false;
}
bool SceneImpl::on_text_input(const std::string&) {
    return false;
}
bool SceneImpl::on_mouse_press(satr::nui::Window::MouseButtons, satr::nui::Window::ButtonState,
                               satr::nui::Window::Modifiers) {
    return false;
}
bool SceneImpl::on_mouse_scroll(glm::dvec2) {
    return false;
}
bool SceneImpl::on_mouse_move(glm::dvec2) {
    return false;
}
bool SceneImpl::on_mouse_motion(glm::dvec2) {
    return false;
}

}  // namespace satr::vk
