#include "Presenter.hpp"

#include <Context.hpp>
#include <config/Tunables.hpp>
#include <logger/Logger.hpp>
#include <trace/Trace.hpp>
#include <vulkan/vulkan_to_string.hpp>

#include <algorithm>
#include <limits>
#include <memory>
#include <tuple>

namespace satr::vk {

Presenter::Presenter() :
    _surface(nullptr), _out_of_date(true), _vsync_enabled(false) /*, _skip_frames(1)*/ {}

bool Presenter::init(Context& context, satr::nui::Window& window, bool use_depth_attachment) {
    ::vk::Instance instance = context.get_instance();
    ::vk::PhysicalDevice phy = context.get_active_device_capabilities().get_handle();
    ::vk::Device dev = context.get_active_device();

    ::vk::Result r;
    // Acquire surface from window system.
    std::tie(r, _surface) = window.create_window_surface(instance);
    if (r != ::vk::Result::eSuccess) {
        spec::trace("Presenter::init::create_window_surface -> ", to_string(r), ".");
        return false;
    }
    // Figure out optimal present modes for our use cases.
    {
        uint32_t present_modes_count;
        r = phy.getSurfacePresentModesKHR(_surface, &present_modes_count, nullptr);
        if (r != ::vk::Result::eSuccess) {
            spec::trace("Presenter::init::getSurfacePresentModesKHR(&, nullptr) -> ",
                        to_string(r), ".");
            return false;
        }
        std::unique_ptr<::vk::PresentModeKHR[]> present_modes(
             new ::vk::PresentModeKHR[present_modes_count]);
        r = phy.getSurfacePresentModesKHR(_surface, &present_modes_count, present_modes.get());
        if (r != ::vk::Result::eSuccess) {
            spec::trace("Presenter::init::getSurfacePresentModesKHR(&, &) -> ", to_string(r),
                        ".");
            return false;
        }
        ::vk::PresentModeKHR* present_modes_end = present_modes.get() + present_modes_count;
        if (std::find(present_modes.get(), present_modes_end,
                      ::vk::PresentModeKHR::eImmediate) != present_modes_end) {
            _present_immediate_mode = ::vk::PresentModeKHR::eImmediate;
        }
        if (std::find(present_modes.get(), present_modes_end,
                      ::vk::PresentModeKHR::eFifoRelaxed) != present_modes_end) {
            _present_vsync_mode = ::vk::PresentModeKHR::eFifoRelaxed;
        }
        if (spec::tunables::get_bool("VKSATR_PREFER_MAILBOX").second &&
            std::find(present_modes.get(), present_modes_end, ::vk::PresentModeKHR::eMailbox) !=
                 present_modes_end) {
            _present_vsync_mode = ::vk::PresentModeKHR::eMailbox;
        }
        logger.logi("Presentation non-vsync mode: ", to_string(_present_immediate_mode));
        logger.logi("Presentation vsync mode: ", to_string(_present_vsync_mode));
    }
    // Select a format that converts from linear RGB to sRGB at presentation.
    {
        uint32_t surface_formats_count;
        r = phy.getSurfaceFormatsKHR(_surface, &surface_formats_count, nullptr);
        if (r != ::vk::Result::eSuccess) {
            spec::trace("Presenter::init::getSurfaceFormatsKHR(&, nullptr) -> ", to_string(r),
                        ".");
            return false;
        }
        std::unique_ptr<::vk::SurfaceFormatKHR[]> surface_formats(
             new ::vk::SurfaceFormatKHR[surface_formats_count]);
        r = phy.getSurfaceFormatsKHR(_surface, &surface_formats_count, surface_formats.get());
        if (r != ::vk::Result::eSuccess) {
            spec::trace("Presenter::init::getSurfaceFormatsKHR(&, &) -> ", to_string(r), ".");
            return false;
        }
        // Debug.
        for (uint32_t i = 0; i < surface_formats_count; i++) {
            logger.logd("Surface format #", i, ": ", surface_formats[i], '.');
        }
        // Select first format as fallback.
        _surface_format = surface_formats[0];
        bool prefer_unorm = spec::tunables::get_bool("VKSATR_UNORM_OUT").second;
        for (uint32_t i = 0; i < surface_formats_count; i++) {
            ::vk::Format format = surface_formats[i].format;
            // If format name contains with 'Srgb' or 'Unorm':
            if ((prefer_unorm && to_string(format).rfind("Unorm") != std::string::npos) ||
                (!prefer_unorm && to_string(format).rfind("Srgb") != std::string::npos)) {
                _surface_format = surface_formats[i];
                logger.logi("Selected #", i, " surface format.");
                break;
            }
        }
    }
    // Select swapchain image count.
    {
        auto [r, surface_caps] = phy.getSurfaceCapabilitiesKHR(_surface);
        if (r != ::vk::Result::eSuccess) {
            spec::trace("Presenter::init::getSurfaceCapabilitiesKHR -> ", to_string(r), ".");
            return false;
        }
        uint32_t user_preferred_image_count =
             spec::tunables::get_uint32("VKSATR_IMG_COUNT", 3, 1).second;
        _preferred_image_count =
             std::clamp(user_preferred_image_count, surface_caps.minImageCount,
                        surface_caps.maxImageCount != 0 ? surface_caps.maxImageCount : 64);
        logger.logi("Selected ", _preferred_image_count, " for swapchain image count.");
    }
    return _init_swapchain(phy, dev, window) &&
           _init_swapchain_framebuffer(context, use_depth_attachment);
}
bool Presenter::recreate(Context& context, satr::nui::Window& window, bool force) {
    ::vk::PhysicalDevice phy = context.get_active_device_capabilities().get_handle();
    ::vk::Device dev = context.get_active_device();

    if (!force && !_out_of_date) {
        return true;
    }

    logger.logi("Recreating swapchain...");

    if (!force) {
        if (dev.waitIdle() != ::vk::Result::eSuccess) {
            logger.loge("recreate: Couldn't wait for device idle!");
            return false;
        }
    }

    _framebuffer.on_swapchain_destroy();
    if (_init_swapchain(phy, dev, window)) {
        _framebuffer.on_swapchain_update(_swapchain_images);
        return true;
    }
    else {
        return false;
    }
}
void Presenter::deinit(Context& context) {
    ::vk::Instance instance = context.get_instance();
    ::vk::Device dev = context.get_active_device();

    _deinit_swapchain(dev);
    if (_surface) {
        instance.destroy(_surface);
        _surface = nullptr;
    }
}

uint32_t Presenter::acquire(::vk::Device device, ::vk::Semaphore window_surface_ready_signal) {
    ::vk::Result r;
    uint32_t i;
    do {
        std::tie(r, i) = device.acquireNextImageKHR(
             _swapchain, std::numeric_limits<uint64_t>::max(), window_surface_ready_signal);
    } while (r == ::vk::Result::eTimeout);
    if (r == ::vk::Result::eSuccess) {
        return i;
    }
    else if (r == ::vk::Result::eSuboptimalKHR) {
        logger.logw("acquire: suboptimal");
        _out_of_date = true;
        return i;
    }
    else if (r == ::vk::Result::eErrorOutOfDateKHR) {
        logger.logw("acquire: out of date");
        _out_of_date = true;
        return std::numeric_limits<uint32_t>::max();
    }
    else {
        logger.loge("acquire failure(", to_string(r), ")!");
        return std::numeric_limits<uint32_t>::max();
    }
}
bool Presenter::submit(const SharedQueue& queue, ::vk::Semaphore rendering_complete_signal,
                       uint32_t index) {
    ::vk::PresentInfoKHR info;
    info.waitSemaphoreCount = 1;
    info.pWaitSemaphores = &rendering_complete_signal;
    info.swapchainCount = 1;
    info.pSwapchains = &_swapchain;
    info.pImageIndices = &index;
    info.pResults = nullptr;
    ::vk::Result r = queue.submit(info);
    if (r == ::vk::Result::eSuccess) {
        return true;
    }
    else if (r == ::vk::Result::eSuboptimalKHR) {
        logger.logw("submit: suboptimal");
        _out_of_date = true;
        return true;
    }
    else if (r == ::vk::Result::eErrorOutOfDateKHR) {
        logger.logw("submit: out of date");
        _out_of_date = true;
        return true;
    }
    else {
        logger.loge("Couldn't submit present command buffer to queue(", to_string(r), ")!");
        return false;
    }
}

bool Presenter::set_frame_limit(int limit) {
    if (limit == -1) {
        _vsync_enabled = true;
        _out_of_date = true;
        return true;
    }
    else if (limit == 0) {
        _vsync_enabled = false;
        _out_of_date = true;
        return true;
    }
    else {
        logger.loge("set_frame_limit: Unsupported limit: ", limit, "!");
        return false;
    }
}

bool Presenter::_init_swapchain(::vk::PhysicalDevice phy, ::vk::Device dev,
                                satr::nui::Window& window) {
    // Start filling create info structure.
    ::vk::SwapchainCreateInfoKHR swapchain_create_info;
    swapchain_create_info.surface = _surface;
    swapchain_create_info.minImageCount = _preferred_image_count;
    swapchain_create_info.imageFormat = _surface_format.format;
    swapchain_create_info.imageColorSpace = _surface_format.colorSpace;
    swapchain_create_info.imageArrayLayers = 1;
    swapchain_create_info.imageUsage = ::vk::ImageUsageFlagBits::eColorAttachment;
    swapchain_create_info.imageSharingMode = ::vk::SharingMode::eExclusive;
    swapchain_create_info.compositeAlpha = ::vk::CompositeAlphaFlagBitsKHR::eOpaque;
    swapchain_create_info.presentMode =
         _vsync_enabled ? _present_vsync_mode : _present_immediate_mode;
    swapchain_create_info.clipped = true;
    swapchain_create_info.oldSwapchain = _swapchain;
    // Retrieve capabilities.
    auto [r, surface_caps] = phy.getSurfaceCapabilitiesKHR(_surface);
    if (r != ::vk::Result::eSuccess) {
        spec::trace("Presenter::_init_swapchain::getSurfaceCapabilitiesKHR -> ", to_string(r),
                    ".");
        return false;
    }
    // Fields based on capabilities.
    auto fb_size = window.get_framebuffer_size();
    if (surface_caps.currentExtent.width == std::numeric_limits<uint32_t>::max()) {
        swapchain_create_info.imageExtent.width = fb_size.x;
    }
    else {
        if (surface_caps.currentExtent.width != (uint32_t) fb_size.x) {
            logger.logw("Vulkan surface extent and window width mismatch(",
                        surface_caps.currentExtent.width, ", ", fb_size.x, ")");
        }
        swapchain_create_info.imageExtent.width = surface_caps.currentExtent.width;
    }
    if (surface_caps.currentExtent.height == std::numeric_limits<uint32_t>::max()) {
        swapchain_create_info.imageExtent.height = fb_size.y;
    }
    else {
        if (surface_caps.currentExtent.height != (uint32_t) fb_size.y) {
            logger.logw("Vulkan surface extent and window height mismatch(",
                        surface_caps.currentExtent.height, ", ", fb_size.y, ")");
        }
        swapchain_create_info.imageExtent.height = surface_caps.currentExtent.height;
    }
    swapchain_create_info.preTransform = surface_caps.currentTransform;

    // Destroy old objects.
    _swapchain_images.clear();
    // Collected all parameters, (re)create swapchain...
    std::tie(r, _swapchain) = dev.createSwapchainKHR(swapchain_create_info);
    _out_of_date = false;
    if (r != ::vk::Result::eSuccess) {
        spec::trace("Presenter::_init_swapchain::createSwapchainKHR -> ", to_string(r), ".");
        return false;
    }
    // Destroy old swapchain after the handle has been used to create the new one.
    if (swapchain_create_info.oldSwapchain) {
        dev.destroySwapchainKHR(swapchain_create_info.oldSwapchain);
    }

    // Retrieve the swapchain images and create get their views.
    {
        uint32_t swapchain_image_count;
        r = dev.getSwapchainImagesKHR(_swapchain, &swapchain_image_count, nullptr);
        if (r != ::vk::Result::eSuccess) {
            spec::trace("Presenter::_init_swapchain::getSwapchainImagesKHR(&, nullptr) -> ",
                        to_string(r), ".");
            return false;
        }
        std::unique_ptr<::vk::Image[]> swapchain_images(new ::vk::Image[swapchain_image_count]);
        r = dev.getSwapchainImagesKHR(_swapchain, &swapchain_image_count,
                                      swapchain_images.get());
        if (r != ::vk::Result::eSuccess) {
            spec::trace("Presenter::_init_swapchain::getSwapchainImagesKHR(&, &) -> ",
                        to_string(r), ".");
            return false;
        }
        _swapchain_images.reserve(swapchain_image_count);
        for (size_t i = 0; i < swapchain_image_count; i++) {
            Image2D img = Image2D::make_shared(dev, swapchain_images[i], _surface_format.format,
                                               swapchain_create_info.imageExtent);
            _swapchain_images.push_back(std::move(img));
        }
        logger.logi("Retreived #", swapchain_image_count, " swapchain images.");
    }
    return true;
}

bool Presenter::_init_swapchain_framebuffer(Context& context, bool use_depth_attachment) {
    _framebuffer = FramebufferChain(context, _swapchain_images, use_depth_attachment);
    return static_cast<bool>(_framebuffer);
}

void Presenter::_deinit_swapchain(::vk::Device dev) {
    if (_swapchain) {
        // Release references held by framebuffer.
        if (_framebuffer) {
            _framebuffer.on_swapchain_destroy();
        }
        // For normal operation, no operations should be 'in-flight' when this point is reached.
        _swapchain_images.clear();
        dev.destroySwapchainKHR(_swapchain);
        _swapchain = nullptr;
    }
}

}  // namespace satr::vk

namespace vk {

std::ostream& operator<<(std::ostream& o, const SurfaceFormatKHR& v) {
    o << to_string(v.format) << ':' << to_string(v.colorSpace);
    return o;
}

}
