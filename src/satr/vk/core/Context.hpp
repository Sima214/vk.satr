#ifndef SATR_VK_CONTEXT_HPP
#define SATR_VK_CONTEXT_HPP
/**
 * @file
 * @brief Core component. Handles:
 * - vk::Instance lifecycle,
 * - vk::Device filtering and creation,
 * - vk::Queue creation and synchronization.
 */

#include <Buffer.hpp>
#include <DescriptorSetPack.hpp>
#include <DeviceCapabilities.hpp>
#include <Extensions.hpp>
#include <Glfw.hpp>
#include <Image.hpp>
#include <Memory.hpp>
#include <Pipelines.hpp>
#include <Samplers.hpp>
#include <Shaders.hpp>
#include <SharedQueue.hpp>
#include <Utils.hpp>
#include <vulkan/vulkan.hpp>

#include <array>
#include <cstdint>
#include <memory>
#include <mutex>
#include <span>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

namespace satr::vk {

class Context : protected Allocator {
   protected:
    /** Private compile-time configuration. */
    static constexpr size_t QUEUE_SLOT_COUNT = 4;
    static constexpr QueueSlotRole QUEUE_SLOT_ROLES[QUEUE_SLOT_COUNT] = {
         /** Ordered in descending priority. */
         QueueSlotRole::Graphics,
         QueueSlotRole::Transfer,
         QueueSlotRole::Compute,
         QueueSlotRole::Convert,
    };

    static constexpr size_t DESC_POOL_MAX_SETS = 32;
    static constexpr auto DESC_POOL_SUPPORTED_TYPES = std::to_array(
         {::vk::DescriptorPoolSize(::vk::DescriptorType::eUniformBuffer, DESC_POOL_MAX_SETS),
          ::vk::DescriptorPoolSize(::vk::DescriptorType::eSampler, DESC_POOL_MAX_SETS),
          ::vk::DescriptorPoolSize(::vk::DescriptorType::eSampledImage, DESC_POOL_MAX_SETS),
          ::vk::DescriptorPoolSize(::vk::DescriptorType::eCombinedImageSampler,
                                   DESC_POOL_MAX_SETS)});

   protected:
    /**
     * Engine's vulkan instance.
     */
    ::vk::Instance _instance = nullptr;
    const uint32_t _instance_version;
    std::string _app_name;
    /**
     * Available devices.
     */
    std::unique_ptr<DeviceCapabilities[]> _devices_caps = nullptr;
    uint32_t _devices_caps_len = 0;
    /**
     * Selected/active device index in _devices_caps.
     */
    uint32_t _device_index = -1U;
    /**
     * Selected/active device.
     */
    ::vk::Device _device = nullptr;

    /**
     * Queue states.
     */
    SharedQueue _queues_pool[QUEUE_SLOT_COUNT];
    QueueSlotIndex _queue_indexes[QUEUE_SLOT_COUNT];

    /**
     * Descriptor pool states.
     */
    std::mutex _desc_pool_mutex;
    std::vector<::vk::DescriptorPool> _desc_pools;

    /**
     * State/object management for validation layer.
     */
    bool _validation_layer_initialized = false;
    bool _validation_layer_enabled = false;
    ::vk::DebugUtilsMessengerEXT _validation_logger{};

    /**
     * Space for extensions function pointers.
     */
    ::vk::detail::DispatchLoaderDynamic _dispatcher{vkGetInstanceProcAddr};

   private:
    static constexpr std::array<const char*, 1> VALIDATION_LAYERS = {
         "VK_LAYER_KHRONOS_validation"};
    static const Extensions DEFAULT_EXTENSIONS;

   public:
    /** Object lifecycle. */
    Context(const char* app_name, uint32_t vulkan_version = VK_API_VERSION_1_0,
            const Extensions& extensions = DEFAULT_EXTENSIONS);
    Context(const std::string& app_name, const Extensions& extensions = DEFAULT_EXTENSIONS,
            uint32_t vulkan_version = VK_API_VERSION_1_0) :
        Context(app_name.c_str(), vulkan_version, extensions) {}
    ~Context();

    /** Setters/Getters */
    Allocator& get_allocator() {
        return std::forward<Allocator&>(*this);
    }
    const std::string& get_app_name() const {
        return _app_name;
    }

    ::vk::Instance get_instance() const;
    std::span<const DeviceCapabilities> get_devices() const;

    bool is_active() const;
    bool is_validation_layer_active() const;

    const DeviceCapabilities& get_active_device_capabilities() const;
    size_t get_active_device_index() const;
    ::vk::Device get_active_device() const;

    /** Operators for some syntactic sugar for some getters. */
    operator bool() const {
        return is_active();
    }
    operator ::vk::Instance() const {
        return get_instance();
    }
    operator ::vk::Device() const {
        return get_active_device();
    }

    std::pair<const QueueSlotIndex&, const SharedQueue&> get_queue_slot(
         QueueSlotRole role, size_t sub_index = 0) const {
        size_t slot = get_queue_slot_index(role, sub_index);
        if (slot == -1U) {
            spec::fatal("Invalid subindex ", sub_index, " for queue for ", role, "!");
        }
        return {_queue_indexes[slot], _queues_pool[_queue_indexes[slot].slot_index]};
    }
    const SharedQueue& get_queue(QueueSlotRole role, size_t sub_index = 0) const {
        return get_queue_slot(role, sub_index).second;
    }

    /** Supported operations. */
    bool select_device(const DeviceCapabilities::Filter& requirements,
                       const DeviceCapabilities::Requests& features);
    bool select_device(const DeviceCapabilities::Filter& requirements,
                       const DeviceCapabilities::Requests& features, nui::GLFW& glfw_instance);

    ::vk::CommandPool create_command_pool(QueueSlotRole role, size_t sub_index = 0) const;
    ::vk::CommandBuffer create_command_buffer(::vk::CommandPool) const;
    ::vk::Fence create_fence(bool signaled = false) const;
    ::vk::Semaphore create_semaphore() const;
    template<size_t N> auto create_semaphores() const {
        std::array<::vk::Semaphore, N> a;
        for (size_t i = 0; i < N; i++) {
            a[i] = create_semaphore();
        }
        return std::tuple_cat(a);
    }

    using Allocator::allocate;
    using Allocator::get_allocation_memory_type_index;

    void associate_handle_with_name(uint64_t handle, ::vk::ObjectType type, const char* name);

    template<typename H> void associate_handle_with_name(H handle, const char* name) {
        associate_handle_with_name((uint64_t) static_cast<H::CType>(handle), H::objectType,
                                   name);
    }
    template<typename H> void associate_handle_with_name(H handle, const std::string& name) {
        associate_handle_with_name((uint64_t) static_cast<H::CType>(handle), H::objectType,
                                   name.c_str());
    }

    template<::vk::ImageType type>
    Image<type> new_image(const char* name, ::vk::Format format,
                          typename Image<type>::size_type size,
                          const ImageOptions& options = {}) {
        return Image<type>::make_shared(*this, name, format, size, options);
    }
    template<::vk::ImageType type>
    Image<type> new_image(const std::string& name, ::vk::Format format,
                          typename Image<type>::size_type size,
                          const ImageOptions& options = {}) {
        return Image<type>::make_shared(*this, name, format, size, options);
    }
    Image1D new_image1d(const char* name, ::vk::Format format, typename Image1D::size_type size,
                        const ImageOptions& options = {}) {
        return Image1D::make_shared(*this, name, format, size, options);
    }
    Image1D new_image1d(const std::string& name, ::vk::Format format,
                        typename Image1D::size_type size, const ImageOptions& options = {}) {
        return Image1D::make_shared(*this, name, format, size, options);
    }
    Image2D new_image2d(const char* name, ::vk::Format format, typename Image2D::size_type size,
                        const ImageOptions& options = {}) {
        return Image2D::make_shared(*this, name, format, size, options);
    }
    Image2D new_image2d(const std::string& name, ::vk::Format format,
                        typename Image2D::size_type size, const ImageOptions& options = {}) {
        return Image2D::make_shared(*this, name, format, size, options);
    }
    Image3D new_image3d(const char* name, ::vk::Format format, typename Image3D::size_type size,
                        const ImageOptions& options = {}) {
        return Image3D::make_shared(*this, name, format, size, options);
    }
    Image3D new_image3d(const std::string& name, ::vk::Format format,
                        typename Image3D::size_type size, const ImageOptions& options = {}) {
        return Image3D::make_shared(*this, name, format, size, options);
    }

    Buffer new_buffer(const char* name, ::vk::BufferUsageFlags usage,
                      ::vk::DeviceSize size_bytes, ::vk::BufferCreateFlags flags = {},
                      bool memory_mapped_ram = false, bool memory_mapped_sqw = false,
                      bool memory_dedicated = false, float memory_priority = 0.5) {
        return Buffer::make_shared(*this, name, usage, size_bytes, flags, memory_mapped_ram,
                                   memory_mapped_sqw, memory_dedicated, memory_priority);
    }
    Buffer new_buffer(const std::string& name, ::vk::BufferUsageFlags usage,
                      ::vk::DeviceSize size_bytes, ::vk::BufferCreateFlags flags = {},
                      bool memory_mapped_ram = false, bool memory_mapped_sqw = false,
                      bool memory_dedicated = false, float memory_priority = 0.5) {
        return Buffer::make_shared(*this, name, usage, size_bytes, flags, memory_mapped_ram,
                                   memory_mapped_sqw, memory_dedicated, memory_priority);
    }
    Buffer new_vertex_buffer(const char* name, ::vk::DeviceSize size_bytes,
                             ::vk::BufferUsageFlags extra_usage = {},
                             ::vk::BufferCreateFlags flags = {}, bool memory_mapped_ram = false,
                             bool memory_mapped_sqw = false, bool memory_dedicated = false,
                             float memory_priority = 0.5) {
        return Buffer::make_shared(
             *this, name, extra_usage | ::vk::BufferUsageFlagBits::eVertexBuffer, size_bytes,
             flags, memory_mapped_ram, memory_mapped_sqw, memory_dedicated, memory_priority);
    }
    Buffer new_vertex_buffer(const std::string& name, ::vk::DeviceSize size_bytes,
                             ::vk::BufferUsageFlags extra_usage = {},
                             ::vk::BufferCreateFlags flags = {}, bool memory_mapped_ram = false,
                             bool memory_mapped_sqw = false, bool memory_dedicated = false,
                             float memory_priority = 0.5) {
        return Buffer::make_shared(
             *this, name, extra_usage | ::vk::BufferUsageFlagBits::eVertexBuffer, size_bytes,
             flags, memory_mapped_ram, memory_mapped_sqw, memory_dedicated, memory_priority);
    }
    Buffer new_index_buffer(const char* name, ::vk::DeviceSize size_bytes,
                            ::vk::BufferUsageFlags extra_usage = {},
                            ::vk::BufferCreateFlags flags = {}, bool memory_mapped_ram = false,
                            bool memory_mapped_sqw = false, bool memory_dedicated = false,
                            float memory_priority = 0.5) {
        return Buffer::make_shared(
             *this, name, extra_usage | ::vk::BufferUsageFlagBits::eIndexBuffer, size_bytes,
             flags, memory_mapped_ram, memory_mapped_sqw, memory_dedicated, memory_priority);
    }
    Buffer new_index_buffer(const std::string& name, ::vk::DeviceSize size_bytes,
                            ::vk::BufferUsageFlags extra_usage = {},
                            ::vk::BufferCreateFlags flags = {}, bool memory_mapped_ram = false,
                            bool memory_mapped_sqw = false, bool memory_dedicated = false,
                            float memory_priority = 0.5) {
        return Buffer::make_shared(
             *this, name, extra_usage | ::vk::BufferUsageFlagBits::eIndexBuffer, size_bytes,
             flags, memory_mapped_ram, memory_mapped_sqw, memory_dedicated, memory_priority);
    }
    Buffer new_uniform_buffer(const char* name, ::vk::DeviceSize size_bytes,
                              ::vk::BufferUsageFlags extra_usage = {},
                              ::vk::BufferCreateFlags flags = {},
                              bool memory_mapped_ram = false, bool memory_mapped_sqw = false,
                              bool memory_dedicated = false, float memory_priority = 0.5) {
        return Buffer::make_shared(
             *this, name, extra_usage | ::vk::BufferUsageFlagBits::eUniformBuffer, size_bytes,
             flags, memory_mapped_ram, memory_mapped_sqw, memory_dedicated, memory_priority);
    }
    Buffer new_uniform_buffer(const std::string& name, ::vk::DeviceSize size_bytes,
                              ::vk::BufferUsageFlags extra_usage = {},
                              ::vk::BufferCreateFlags flags = {},
                              bool memory_mapped_ram = false, bool memory_mapped_sqw = false,
                              bool memory_dedicated = false, float memory_priority = 0.5) {
        return Buffer::make_shared(
             *this, name, extra_usage | ::vk::BufferUsageFlagBits::eUniformBuffer, size_bytes,
             flags, memory_mapped_ram, memory_mapped_sqw, memory_dedicated, memory_priority);
    }

    template<typename T, size_t N> requires(sizeof(T) == 4)
    Module new_shader(const T (&buffer)[N], const StageConfig& config = StageConfig()) const {
        return Module::load_shader(*this, buffer, N, config);
    }
    Module new_shader(const uint32_t* buffer, size_t buffer_size,
                      const StageConfig& config = StageConfig()) const {
        return Module::load_shader(*this, buffer, buffer_size, config);
    }
    Module new_shader(const std::filesystem::path& path,
                      const StageConfig& config = StageConfig()) const {
        return Module::load_shader(*this, path, config);
    }

    template<typename T, size_t N> requires(std::is_base_of_v<Module, T>)
    Pipeline new_pipeline(
         const FramebufferChain& framebuffer, T (&shaders)[N],
         const GraphicsPipelineOptions& options = GraphicsPipelineOptions()) const {
        return Pipeline::compile(*this, framebuffer, std::span<T>(shaders, N), options);
    }

    template<typename T, size_t N> requires(std::is_base_of_v<Module, T>)
    ComputePipeline new_compute_pipeline(
         T shader, const ComputePipelineOptions& options = ComputePipelineOptions()) const {
        return ComputePipeline::compile(*this, shader, options);
    }

    Sampler new_sampler(::vk::Filter filter, ::vk::SamplerAddressMode border_mode) {
        ::vk::SamplerCreateInfo cinfo;
        cinfo.magFilter = filter;
        cinfo.minFilter = filter;
        cinfo.addressModeU = border_mode;
        cinfo.addressModeV = border_mode;
        cinfo.addressModeW = border_mode;
        return Sampler::make_shared(*this, cinfo);
    }

    Sampler new_sampler(::vk::Filter filter, ::vk::SamplerMipmapMode mipmap_mode,
                        ::vk::SamplerAddressMode border_mode, float min_lod, float max_lod,
                        float max_anisotropy) {
        ::vk::SamplerCreateInfo cinfo;
        cinfo.magFilter = filter;
        cinfo.minFilter = filter;
        cinfo.mipmapMode = mipmap_mode;
        cinfo.addressModeU = border_mode;
        cinfo.addressModeV = border_mode;
        cinfo.addressModeW = border_mode;
        cinfo.minLod = min_lod;
        cinfo.maxLod = max_lod;
        cinfo.maxAnisotropy = max_anisotropy;
        return Sampler::make_shared(*this, cinfo);
    }

    /**
     * Recommended for long-lived descriptor sets.
     */
    DescriptorSetPack new_descriptor_set_pack(std::span<::vk::DescriptorSetLayout> set_layouts);
    DescriptorSetPack new_descriptor_set_pack(size_t count,
                                              ::vk::DescriptorSetLayout set_layout) {
        std::vector<::vk::DescriptorSetLayout> set_layouts(count, set_layout);
        return new_descriptor_set_pack(set_layouts);
    }
    void free_descriptor_set_pack(::vk::DescriptorPool pool,
                                  const std::vector<::vk::DescriptorSet>& sets);

    // TODO: On new frame.

   protected:
    /**
     * Optionally(or forcibly on debug builds)
     * add validation layers.
     */
    bool enable_validation_layers(uint32_t& layer_count, const char* const*& layer_names);

    /** Compile time search. */
    static constexpr size_t get_queue_slot_index(const QueueSlotRole role,
                                                 const size_t sub_index = 0) {
        size_t counter = 0;
        for (size_t queue_slot_index = 0; queue_slot_index < QUEUE_SLOT_COUNT;
             queue_slot_index++) {
            if (QUEUE_SLOT_ROLES[queue_slot_index] == role) {
                if (counter == sub_index) {
                    return queue_slot_index;
                }
                else {
                    counter++;
                }
            }
        }
        return -1U;
    }

   private:
    static VKAPI_ATTR ::vk::Bool32 VKAPI_CALL __on_validation_layer_log(
        ::vk::DebugUtilsMessageSeverityFlagBitsEXT severity, ::vk::DebugUtilsMessageTypeFlagsEXT types,
         const ::vk::DebugUtilsMessengerCallbackDataEXT* data, void*);

    static constexpr ::vk::DebugUtilsMessengerCreateInfoEXT validation_logger_creator =
         ::vk::DebugUtilsMessengerCreateInfoEXT(
              {},
              ::vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo |
                   ::vk::DebugUtilsMessageSeverityFlagBitsEXT::eError |
                   ::vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose |
                   ::vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning,
              ::vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral |
                   ::vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance |
                   ::vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation,
              __on_validation_layer_log);
};

}  // namespace satr::vk

#endif /*SATR_VK_CONTEXT_HPP*/