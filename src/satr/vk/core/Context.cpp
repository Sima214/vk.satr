#include "Context.hpp"

#include <DescriptorSetPack.hpp>
#include <Extensions.hpp>
#include <SharedQueue.hpp>
#include <config/Tunables.hpp>
#include <logger/Logger.hpp>
#include <trace/Trace.hpp>
#include <vulkan/vulkan_enums.hpp>
#include <vulkan/vulkan_to_string.hpp>

#include <array>
#include <bitset>
#include <cstdint>
#include <memory>
#include <mutex>
#include <tuple>
#include <utility>
#include <vector>

#ifdef NDEBUG
    #define FORCE_ENABLE_VALIDATION_LAYERS false
#else
    #define FORCE_ENABLE_VALIDATION_LAYERS true
#endif

namespace vk {

template<typename O> O& operator<<(O& os, ::vk::ToolPurposeFlagsEXT flags) {
    os << '{';
    bool previous = false;
    if (flags & ::vk::ToolPurposeFlagBits::eValidation) {
        os << "Validation";
        previous = true;
    }
    if (flags & ::vk::ToolPurposeFlagBits::eProfiling) {
        if (previous) {
            os << ", Profiling";
        }
        else {
            os << "Profiling";
            previous = true;
        }
    }
    if (flags & ::vk::ToolPurposeFlagBits::eTracing) {
        if (previous) {
            os << ", Tracing";
        }
        else {
            os << "Tracing";
            previous = true;
        }
    }
    if (flags & ::vk::ToolPurposeFlagBits::eAdditionalFeatures) {
        if (previous) {
            os << ", AdditionalFeatures";
        }
        else {
            os << "AdditionalFeatures";
            previous = true;
        }
    }
    if (flags & ::vk::ToolPurposeFlagBits::eModifyingFeatures) {
        if (previous) {
            os << ", ModifyingFeatures";
        }
        else {
            os << "ModifyingFeatures";
            previous = true;
        }
    }
    if (flags & ::vk::ToolPurposeFlagBits::eDebugReportingEXT) {
        if (previous) {
            os << ", DebugReporting";
        }
        else {
            os << "DebugReporting";
            previous = true;
        }
    }
    if (flags & ::vk::ToolPurposeFlagBits::eDebugMarkersEXT) {
        if (previous) {
            os << ", DebugMarkers";
        }
        else {
            os << "DebugMarkers";
        }
    }
    os << '}';
    return os;
}

}  // namespace vk

namespace satr::vk {

const Extensions Context::DEFAULT_EXTENSIONS;

Context::Context(const char* app_name, uint32_t vulkan_version,
                 const Extensions& user_extensions) :
    Allocator(), _instance_version(vulkan_version), _app_name(app_name),
    _dispatcher(vkGetInstanceProcAddr) {
    // Create instance - arguments.
    ::vk::ApplicationInfo app_info;
    app_info.pApplicationName = app_name;
    app_info.pEngineName = "vk.satr";
    app_info.apiVersion = vulkan_version;
    ::vk::InstanceCreateInfo instance_create_info;
    instance_create_info.pApplicationInfo = &app_info;
    Extensions extensions;
    {
        // Filter optional, automatically enabled extensions.
        Extensions optional_extensions = get_optional_instance_extensions(vulkan_version);
        Extensions supported_extensions;
        supported_extensions.append_supported();
        optional_extensions &= supported_extensions;
        extensions += optional_extensions;
    }
    if (enable_validation_layers(instance_create_info.enabledLayerCount,
                                 instance_create_info.ppEnabledLayerNames)) {
        extensions += VK_EXT_DEBUG_UTILS_EXTENSION_NAME;
    }
    extensions += user_extensions;
    std::tie(instance_create_info.enabledExtensionCount,
             instance_create_info.ppEnabledExtensionNames) = extensions.get_data();
    // Create instance.
    ::vk::Result instance_result;
    std::tie(instance_result, _instance) = ::vk::createInstance(instance_create_info);
    if (instance_result == ::vk::Result::eSuccess) {
        spec::trace("Context::instance::create");
    }
    else {
        logger.logf("Could not create vulkan instance(", to_string(instance_result), ")!");
    }
    // Upgrade dispatcher.
    _dispatcher.init(_instance, vkGetInstanceProcAddr);
    // Post-work for validation layers.
    if (_validation_layer_enabled) {
        ::vk::Result validation_logger_result;
        std::tie(validation_logger_result, _validation_logger) =
             _instance.createDebugUtilsMessengerEXT(validation_logger_creator, nullptr,
                                                    _dispatcher);
        if (validation_logger_result == ::vk::Result::eSuccess) {
            spec::trace("Allocated validation logger!");
        }
        else {
            logger.loge("Could not create validation logger(",
                        to_string(validation_logger_result), ")!");
        }
    }
    // Parse & allocate device list.
    auto vkr = _instance.enumeratePhysicalDevices(&_devices_caps_len, nullptr);
    if (vkr != ::vk::Result::eSuccess) {
        logger.logf("Could retrieve device count!");
    }
    if (_devices_caps_len == 0) {
        logger.logf("No vulkan capable devices are available!");
    }
    _devices_caps =
         std::unique_ptr<DeviceCapabilities[]>(new DeviceCapabilities[_devices_caps_len]);
    auto* physical_devices = new ::vk::PhysicalDevice[_devices_caps_len];
    vkr = _instance.enumeratePhysicalDevices(&_devices_caps_len, physical_devices);
    if (vkr != ::vk::Result::eSuccess) {
        logger.logf("Could retrieve device list!");
    }
    for (uint32_t i = 0; i < _devices_caps_len; i++) {
        _devices_caps[i] = DeviceCapabilities(physical_devices[i]);
    }
    delete[] physical_devices;
}
Context::~Context() {
    if (_instance) {
        // Release descriptor pools.
        {
            std::scoped_lock _g(_desc_pool_mutex);
            for (auto desc_pool : _desc_pools) {
                _device.destroy(desc_pool);
            }
        }
        // Release queues.
        for (size_t qi = 0; qi < QUEUE_SLOT_COUNT; qi++) {
            // Invalidate all queues.
            _queues_pool[qi] = SharedQueue();
        }
        spec::trace("Context::queues::invalidated");
        // Release device handles.
        if (_device) {
            allocator_deinit();
            _device.destroy();
            spec::trace("Context::device::destroyed");
        }
        _devices_caps.reset();
        // Release instance(with optional validation).
        if (_validation_logger) {
            _instance.destroyDebugUtilsMessengerEXT(_validation_logger, nullptr, _dispatcher);
            spec::trace("Context::logger::destroyed");
        }
        _instance.destroy();
        spec::trace("Context::instance::destroyed");
    }
}

::vk::Instance Context::get_instance() const {
    return _instance;
}
std::span<const DeviceCapabilities> Context::get_devices() const {
    return std::span(_devices_caps.get(), _devices_caps_len);
}
bool Context::is_active() const {
    return _device_index != -1U;
}
bool Context::is_validation_layer_active() const {
    return _validation_layer_initialized && _validation_layer_enabled;
}
const DeviceCapabilities& Context::get_active_device_capabilities() const {
    if (!is_active()) [[unlikely]] {
        spec::fatal("Context::get_active_device_capabilities: "
                    "context not active!");
    }
    return _devices_caps[_device_index];
}
size_t Context::get_active_device_index() const {
    return _device_index;
}
::vk::Device Context::get_active_device() const {
    if (!is_active()) [[unlikely]] {
        spec::fatal("Context::get_active_device: context not active!");
    }
    return _device;
}

bool Context::select_device(const DeviceCapabilities::Filter& requirements,
                            const DeviceCapabilities::Requests& features) {
    auto device_index = _device_index;
    if (device_index != -1U) {
        logger.loge("Cannot select device for an already active vulkan context!");
        return false;
    }
    // Check for optional manual user override.
    auto [user_error, user_index] =
         spec::tunables::get_uint32("VKSATR_DEVICE", -1U, 0, _devices_caps_len - 1);
    switch (user_error) {
        case spec::tunables::Errors::Ok: {
            device_index = user_index;
        } break;
        case spec::tunables::Errors::NotSet: {
            /** NOP */
        } break;
        case spec::tunables::Errors::NoConv: {
            logger.loge("Could not parse `VKSATR_DEVICE`!");
        } break;
        case spec::tunables::Errors::OutRange: {
            logger.loge("Invalid value for `VKSATR_DEVICE`!");
        } break;
    }
    // Automatic device selection.
    if (device_index == -1U) {
        for (uint32_t i = 0; i < _devices_caps_len; i++) {
            // Select first device which matches.
            if (_devices_caps[i].match(requirements, features)) {
                device_index = i;
                break;
            }
        }
    }
    // Confirm/validate selected device is usable.
    if (device_index == -1U) {
        logger.loge("No vulkan device has been selected!");
        return false;
    }
    DeviceCapabilities* selected_device = &_devices_caps[device_index];
    if (!selected_device->match(requirements, features)) {
        logger.loge("Selected vulkan device is not compatible!");
        return false;
    }
    // Create (logical) device.
    logger.logi("Using device #", device_index, " `", *selected_device, "`.");
    Extensions extensions;
    {
        // Filter optional device extensions.
        Extensions optional_extensions = get_optional_device_extensions(_instance_version);
        Extensions supported_extensions = selected_device->get_extensions();
        optional_extensions &= supported_extensions;
        extensions += optional_extensions;
    }
    extensions += requirements.get_required_extensions();
    uint32_t layer_count;
    const char* const* layer_names;
    if (enable_validation_layers(layer_count, layer_names)) {
        extensions += VK_EXT_TOOLING_INFO_EXTENSION_NAME;
    }
    auto device_opt =
         selected_device->create_logical(features, extensions, {layer_count, layer_names},
                                         {QUEUE_SLOT_COUNT, QUEUE_SLOT_ROLES, _queue_indexes});
    if (!device_opt) {
        logger.loge("Could not allocate selected vulkan device!");
        return false;
    }
    else {
        // Commit state.
        _device_index = device_index;
        _device = *device_opt;
    }
    // Upgrade dispatcher.
    _dispatcher.init(_instance, vkGetInstanceProcAddr, _device);
    // Print validation layer info if enabled.
    if (_validation_layer_enabled) {
        ::vk::PhysicalDevice physdev = selected_device->get_handle();
        auto [r, tools] = physdev.getToolPropertiesEXT(_dispatcher);
        if (r == ::vk::Result::eSuccess) {
            logger.logd("Enabled tools(", tools.size(), "):");
            for (const auto& tool : tools) {
                if (tool.layer != nullptr && tool.layer[0] != '\0') {
                    logger.logd('\t', "layer: ", tool.layer, ", name: ", tool.name,
                                ", version: ", tool.version, ", purposes: ", tool.purposes);
                }
                else {
                    logger.logd('\t', "name: ", tool.name, ", version: ", tool.version,
                                ", purposes: ", tool.purposes);
                }
            }
        }
        else {
            logger.loge("Unable to get tool properties (", to_string(r), ").");
        }
    }
    // Initialize vulkan allocator.
    allocator_init(_instance_version, _instance, selected_device->get_handle(), _device,
                   extensions);
    // Get Queue's handles and allocate SharedQueue(s).
    for (size_t slot_index = 0; slot_index < QUEUE_SLOT_COUNT; slot_index++) {
        QueueSlotIndex& current_queue_slot = _queue_indexes[slot_index];
        SharedQueue& pool_slot = _queues_pool[current_queue_slot.slot_index];
        if (!pool_slot) {
            pool_slot = SharedQueue(_device, current_queue_slot.phys);
        }
    }
    return true;
}

bool Context::select_device(const DeviceCapabilities::Filter& requirements,
                            const DeviceCapabilities::Requests& features,
                            nui::GLFW& glfw_instance) {
    if (select_device(requirements, features)) {
        auto [presentation_queue_candidate_index, presentation_queue_candidate] =
             get_queue_slot(QueueSlotRole::Graphics);
        if (glfw_instance.get_physical_device_presentation_support(
                 _instance, _devices_caps[_device_index].get_handle(),
                 presentation_queue_candidate_index.phys.family_index)) {
            return true;
        }
        else {
            logger.loge("Selected graphics queue family cannot be used for presentation!");
            return false;
        }
    }
    return false;
}

::vk::CommandPool Context::create_command_pool(QueueSlotRole role, size_t sub_index) const {
    auto queue_index = get_queue_slot(role, sub_index).first;
    ::vk::CommandPoolCreateInfo cinfo(::vk::CommandPoolCreateFlagBits::eResetCommandBuffer,
                                      queue_index.phys.family_index);
    auto [r, cmd_pool] = _device.createCommandPool(cinfo);
    if (r != ::vk::Result::eSuccess) {
        logger.loge("Couldn't create command pool(", to_string(r), ")!");
    }
    return cmd_pool;
}
::vk::CommandBuffer Context::create_command_buffer(::vk::CommandPool cmd_pool) const {
    ::vk::CommandBufferAllocateInfo ainfo(cmd_pool, ::vk::CommandBufferLevel::ePrimary, 1);
    ::vk::CommandBuffer cmd_buf;
    ::vk::Result r = get_active_device().allocateCommandBuffers(&ainfo, &cmd_buf);
    if (r != ::vk::Result::eSuccess) {
        logger.logf("Couldn't allocate command buffer(", to_string(r), ")!");
    }
    return cmd_buf;
}
::vk::Fence Context::create_fence(bool signaled) const {
    ::vk::FenceCreateInfo cinfo(signaled ? ::vk::FenceCreateFlagBits::eSignaled :
                                           ::vk::FenceCreateFlags{});
    auto [r, h] = get_active_device().createFence(cinfo);
    if (r != ::vk::Result::eSuccess) {
        logger.logf("Couldn't allocate fence(", to_string(r), ")!");
    }
    return h;
}
::vk::Semaphore Context::create_semaphore() const {
    auto [r, s] = get_active_device().createSemaphore({});
    if (r != ::vk::Result::eSuccess) {
        logger.logf("Couldn't allocate semaphore(", to_string(r), ")!");
    }
    return s;
}

DescriptorSetPack Context::new_descriptor_set_pack(
     std::span<::vk::DescriptorSetLayout> set_layouts) {
    ::vk::DescriptorSetAllocateInfo alloc_info;
    alloc_info.descriptorSetCount = set_layouts.size();
    alloc_info.pSetLayouts = set_layouts.data();

    std::scoped_lock _g(_desc_pool_mutex);
    // First try to allocate from already available pools.
    for (auto it = _desc_pools.rbegin(); it != _desc_pools.rend(); ++it) {
        alloc_info.descriptorPool = *it;
        auto [r, sets] = _device.allocateDescriptorSets(alloc_info);
        if (r != ::vk::Result::eSuccess) [[unlikely]] {
            logger.logd("Failure to allocate descriptor sets from available pool (",
                        to_string(r), ").");
        }
        return DescriptorSetPack(*this, *it, std::move(sets));
    }

    // Allocate from new pool.
    ::vk::DescriptorPoolCreateInfo cinfo(::vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet,
                                         DESC_POOL_MAX_SETS, DESC_POOL_SUPPORTED_TYPES.size(),
                                         DESC_POOL_SUPPORTED_TYPES.data());
    auto [r, new_pool] = _device.createDescriptorPool(cinfo);
    if (r != ::vk::Result::eSuccess) {
        logger.logf("Failure to allocate new descriptor pool(", to_string(r), ")!");
    }
    _desc_pools.push_back(new_pool);

    {
        alloc_info.descriptorPool = new_pool;
        auto [r, sets] = _device.allocateDescriptorSets(alloc_info);
        if (r != ::vk::Result::eSuccess) [[unlikely]] {
            logger.loge("Failure to allocate descriptor sets from new pool(", to_string(r),
                        ")!");
        }
        return DescriptorSetPack(*this, new_pool, std::move(sets));
    }

    // Give up.
    return DescriptorSetPack();
}

void Context::free_descriptor_set_pack(::vk::DescriptorPool pool,
                                       const std::vector<::vk::DescriptorSet>& sets) {
    std::scoped_lock _g(_desc_pool_mutex);
    _device.freeDescriptorSets(pool, sets);
}

void Context::associate_handle_with_name(uint64_t handle, ::vk::ObjectType type,
                                         const char* name) {
    if (_validation_layer_enabled) {
        ::vk::DebugUtilsObjectNameInfoEXT assoc_info(type, handle, name);
        ::vk::Device dev = get_active_device();
        ::vk::Result r = dev.setDebugUtilsObjectNameEXT(assoc_info, _dispatcher);
        if (r != ::vk::Result::eSuccess) {
            logger.loge("Unable to associate handle with name (", to_string(r), ").");
        }
    }
}

bool Context::enable_validation_layers(uint32_t& layer_count, const char* const*& layer_names) {
    // Quick path after first call.
    if (_validation_layer_initialized) {
        if (_validation_layer_enabled) {
            layer_count = VALIDATION_LAYERS.size();
            layer_names = VALIDATION_LAYERS.data();
        }
        return _validation_layer_enabled;
    }
    // Enable validation layers on DEBUG builds by default.
    if constexpr (FORCE_ENABLE_VALIDATION_LAYERS) {
        _validation_layer_enabled = true;
    }
    else {
        auto [user_error, user_enable] = spec::tunables::get_bool("VKSATR_VALIDATION");
        switch (user_error) {
            case spec::tunables::Errors::Ok: {
                _validation_layer_enabled = user_enable;
            } break;
            case spec::tunables::Errors::NotSet: {
                /** NOP */
            } break;
            case spec::tunables::Errors::OutRange:
            case spec::tunables::Errors::NoConv: {
                logger.loge("Unknown value for `VKSATR_VALIDATION`!");
            } break;
        }
    }
    if (_validation_layer_enabled) {
        // Check if the validation layer is available.
        bool available = false;
        uint32_t available_count = 0;
        ::vk::Result r = ::vk::enumerateInstanceLayerProperties(&available_count, nullptr);
        if (r != ::vk::Result::eSuccess) {
            spec::trace("Context::enable_validation_layers"
                        "::enumerateInstanceLayerProperties(&, null) -> ",
                        to_string(r), ".");
        }
        else {
            ::vk::LayerProperties* available_layers =
                 new ::vk::LayerProperties[available_count];
            ::vk::Result r =
                 ::vk::enumerateInstanceLayerProperties(&available_count, available_layers);
            if (r != ::vk::Result::eSuccess) {
                spec::trace("Context::enable_validation_layers"
                            "::enumerateInstanceLayerProperties(&, &) -> ",
                            to_string(r), ".");
            }
            else {
                std::bitset<VALIDATION_LAYERS.size()> availability_mask;
                for (uint32_t i = 0; i < available_count; i++) {
                    ::vk::LayerProperties& current_layer_props = available_layers[i];
                    for (uint32_t j = 0; j < VALIDATION_LAYERS.size(); j++) {
                        // Skip already found.
                        if (!availability_mask[j]) {
                            if (strequal(VALIDATION_LAYERS[j], current_layer_props.layerName)) {
                                availability_mask[j] = true;
                                // Current layer has been found, move to next.
                                break;
                            }
                        }
                    }
                }
                available = availability_mask.all();
            }
            delete[] available_layers;
        }
        if (!available) {
            logger.logw("Validation layers have been enabled but are unavailable!");
            _validation_layer_enabled = false;
        }
        else {
            _validation_layer_enabled = true;
            layer_count = VALIDATION_LAYERS.size();
            layer_names = VALIDATION_LAYERS.data();
        }
    }
    _validation_layer_initialized = true;
    return _validation_layer_enabled;
}

VKAPI_ATTR ::vk::Bool32 VKAPI_CALL Context::__on_validation_layer_log(
     ::vk::DebugUtilsMessageSeverityFlagBitsEXT severity,
     ::vk::DebugUtilsMessageTypeFlagsEXT types,
     const ::vk::DebugUtilsMessengerCallbackDataEXT* data, void*) {
    spec::Logger::Level level;
    switch (severity) {
        case ::vk::DebugUtilsMessageSeverityFlagBitsEXT::eError: {
            level = spec::Logger::Level::Error;
        } break;
        case ::vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning: {
            level = spec::Logger::Level::Warn;
        } break;
        case ::vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo: {
            level = spec::Logger::Level::Info;
        } break;
        case ::vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose: {
            level = spec::Logger::Level::Verbose;
        } break;
        default: {
            level = spec::Logger::Level::Verbose;
        }
    }
    std::string types_string = "";
    if (types &
        ::vk::DebugUtilsMessageTypeFlagsEXT(::vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral)) {
        types_string += 'G';
    }
    if (types & ::vk::DebugUtilsMessageTypeFlagsEXT(
                     ::vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance)) {
        types_string += 'P';
    }
    if (types & ::vk::DebugUtilsMessageTypeFlagsEXT(
                     ::vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation)) {
        types_string += 'V';
    }
    if (types & ::vk::DebugUtilsMessageTypeFlagsEXT(
                     ::vk::DebugUtilsMessageTypeFlagBitsEXT::eDeviceAddressBinding)) {
        types_string += 'B';
    }
    logger.log(level, "VKV(", types_string, "): ", data->pMessage);
    return VK_FALSE;
}

}  // namespace satr::vk
