#version 450

layout(location = 0) out vec2 frag_pos;
layout(location = 1) out flat int instance_index;

/**
 * Triangle strips are assembled in the following order:
 *   1. p_0 = {v_0, v_1, v_2}
 *   2. p_1 = {v_1, v_3, v_2}
 */
const vec2 QUAD[4] = {
    vec2(0, 1), // Bottom left
    vec2(0, 0), // Top left
    vec2(1, 1), // Bottom right
    vec2(1, 0)  // Top right
};

struct SceneProperties {
    vec2 offset;
    vec2 extent;
} scene_properties;

#define MAX_SCENE_COUNT 8

layout(set = 0, binding = 0) uniform Scenes {
    SceneProperties properties[MAX_SCENE_COUNT];
    vec4 visibilities[((MAX_SCENE_COUNT-1)/4)+1];
} scenes;

void main() {
    vec2 offset = scenes.properties[gl_InstanceIndex].offset;
    vec2 extent = scenes.properties[gl_InstanceIndex].extent;
    frag_pos = QUAD[gl_VertexIndex];
    instance_index = gl_InstanceIndex;
    gl_Position = vec4(offset*2 + ((frag_pos * extent)*2 - 1), 0.0f, 1.0f);
}
