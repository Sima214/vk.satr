#ifndef SATR_VK_SCENE_HPP
#define SATR_VK_SCENE_HPP
/**
 * @file
 * @brief Manages communication between logic and render threads,
 * while also cycling command buffers and framebuffers.
 */

#include <Context.hpp>
#include <Framebuffer.hpp>
#include <GlfwWindow.hpp>
#include <Image.hpp>
#include <SharedQueue.hpp>
#include <Utils.hpp>
#include <events/MultiViewObject.hpp>
#include <glm/vec2.hpp>
#include <vulkan/vulkan.hpp>

#include <atomic>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <thread>

namespace satr::vk {

/* Fwd decls */
class RenderLoop;

class SceneImpl;
class Scene;
using SceneWeak = std::weak_ptr<SceneImpl>;

const Format SETUP_FRAMEBUFFER_DEFAULT_FORMAT = decompose(::vk::Format::eR32G32B32A32Sfloat);

class SceneImpl : public spec::INonCopyable {
   public:
    struct RenderData {
        ::vk::CommandBuffer cmd_buf;
        ::vk::Semaphore rdy_sig;
        uint64_t last_timeline = 0;

        /* Helper operations */
        void init(Context& engine, ::vk::CommandPool cmd_pool);
        void reset();
        void do_resize(FramebufferChain& framebuffer, size_t chain_index,
                       glm::ivec2 target_size);
        bool start(FramebufferChain& framebuffer, size_t chain_index);
        bool end(FramebufferChain& framebuffer);
        void submit(const SharedQueue& queue);
        void destroy(::vk::Device dev);
    };

   protected:
    RenderLoop& _root;
    std::string _name;

    ::vk::CommandPool _cmd_pool;
    FramebufferChain _framebuffer;
    std::atomic<glm::ivec2> _framebuffer_target_size;

    /* State sequencing/synchronization */
    std::unique_ptr<RenderData[]> _render_data;
    uint64_t _timeline_counter = 0;
    uint64_t _timeline_target = 0;

   public:
    SceneImpl(const char* name, RenderLoop& root);
    SceneImpl(const std::string& name, RenderLoop& root) : SceneImpl(name.c_str(), root) {}
    virtual ~SceneImpl();

    operator bool() const {
        return static_cast<bool>(_framebuffer);
    }

    /* Getters/Setters */
    RenderLoop& get_root() const {
        return _root;
    }
    Context& get_engine() const;
    ::vk::Device get_device() const;

    FramebufferChain& get_framebuffer() {
        return _framebuffer;
    }
    const FramebufferChain& get_framebuffer() const {
        return _framebuffer;
    }
    size_t get_chain_size() const;
    const std::string& get_name() const {
        return _name;
    }

    /** Trigger a new frame, if rendering is paused. */
    void mark_dirty();

    /* Events/Callbacks */
    virtual bool on_setup(size_t chain_size, glm::ivec2 scene_size, glm::vec2& offset,
                          glm::vec2& extent) = 0;
    virtual std::tuple<RenderData*, bool /*reused*/> on_render(size_t index);

    /**
     * Build & submit command buffer.
     * Return false to pause rendering.
     */
    virtual bool render(size_t chain_index, RenderData& slot_data);
    /**
     * Build command buffer.
     * Return false to pause rendering.
     */
    virtual bool render(size_t chain_index, ::vk::CommandBuffer cmd_buf) = 0;

    virtual void on_resize(glm::ivec2 size, glm::vec2& offset, glm::vec2& extent);

    virtual void destroy_render_data();
    virtual void on_remove();
    virtual void on_close();
    virtual void on_focus(bool focused);

    virtual bool on_key_press(satr::nui::Window::Keys key, int scancode,
                              satr::nui::Window::ButtonState action,
                              satr::nui::Window::Modifiers mods);
    virtual bool on_text_input(const std::string& text);
    virtual bool on_mouse_press(satr::nui::Window::MouseButtons button,
                                satr::nui::Window::ButtonState action,
                                satr::nui::Window::Modifiers mods);
    virtual bool on_mouse_scroll(glm::dvec2 offset);
    virtual bool on_mouse_move(glm::dvec2 pos);
    virtual bool on_mouse_motion(glm::dvec2 delta);

   protected:
    /* Helpers */
    bool setup_framebuffer(
         size_t chain_size, glm::ivec2 fb_size, size_t color_attachment_count = 1,
         const Format* color_attachment_format = &SETUP_FRAMEBUFFER_DEFAULT_FORMAT,
         bool use_depth_attachment = false);
    bool setup_framebuffer(size_t chain_size, glm::ivec2 fb_size,
                           ::vk::Format color_attachment_format,
                           bool use_depth_attachment = false) {
        const Format fmt = decompose(color_attachment_format);
        return setup_framebuffer(chain_size, fb_size, 1, &fmt, use_depth_attachment);
    }
    bool setup_render_data(size_t chain_size);
    /**
     * NOTE: Must be called at the end of the overridden on_setup() callback.
     */
    bool on_setup_complete();
};

class Scene : public std::shared_ptr<SceneImpl> {
   public:
    /* For convenience */
    using Weak = SceneWeak;
    using RenderData = SceneImpl::RenderData;

   public:
    template<typename S>
    constexpr Scene(std::shared_ptr<S> r) noexcept : std::shared_ptr<SceneImpl>(r) {}

    // inherit shared_ptr's constructors
    constexpr Scene() noexcept : std::shared_ptr<SceneImpl>() {}
    constexpr Scene(std::nullptr_t _) noexcept : std::shared_ptr<SceneImpl>(_) {}
    Scene(const Scene& r) noexcept = default;
    Scene(Scene&& r) noexcept = default;

    Scene& operator=(const Scene& r) noexcept = default;
    Scene& operator=(Scene&& r) noexcept = default;

    /* Getters */
    operator bool() const {
        return static_cast<bool>((std::shared_ptr<SceneImpl>) *this) &&
               static_cast<bool>((SceneImpl&) **this);
    }
    RenderLoop& get_root() const {
        return (*this)->get_root();
    }
    Context& get_engine() const {
        return (*this)->get_engine();
    }
    FramebufferChain& get_framebuffer() const {
        return (*this)->get_framebuffer();
    }
    const std::string& get_name() const {
        return (*this)->get_name();
    }

    /* --------- Callbacks --------- */
    /**
     * Begin loading process. Called when added in the RenderLoop.
     */
    bool on_setup(size_t chain_size, glm::ivec2 scene_size, glm::vec2& offset,
                  glm::vec2& extent) const {
        return (*this)->on_setup(chain_size, scene_size, offset, extent);
    }
    std::tuple<Scene::RenderData*, bool /*reused*/> on_render(size_t index) const {
        return (*this)->on_render(index);
    }
    /**
     * When a scene removal is triggered from RenderLoop.
     * Post-removal handling should happen in SceneImpl destructor.
     */
    void on_remove() const {
        return (*this)->on_remove();
    }

    void on_close() const {
        return (*this)->on_close();
    }
    void on_resize(glm::ivec2 size, glm::vec2& offset, glm::vec2& extent) const {
        return (*this)->on_resize(size, offset, extent);
    }
    void on_focus(bool focused) const {
        return (*this)->on_focus(focused);
    }

    bool on_key_press(satr::nui::Window::Keys key, int scancode,
                      satr::nui::Window::ButtonState action,
                      satr::nui::Window::Modifiers mods) const {
        return (*this)->on_key_press(key, scancode, action, mods);
    }
    bool on_text_input(const std::string& text) const {
        return (*this)->on_text_input(text);
    }
    bool on_mouse_press(satr::nui::Window::MouseButtons button,
                        satr::nui::Window::ButtonState action,
                        satr::nui::Window::Modifiers mods) const {
        return (*this)->on_mouse_press(button, action, mods);
    }
    bool on_mouse_scroll(glm::dvec2 offset) const {
        return (*this)->on_mouse_scroll(offset);
    }
    bool on_mouse_move(glm::dvec2 pos) const {
        return (*this)->on_mouse_move(pos);
    }
    bool on_mouse_motion(glm::dvec2 delta) const {
        return (*this)->on_mouse_motion(delta);
    }
};

}  // namespace satr::vk

#endif /*SATR_VK_SCENE_HPP*/