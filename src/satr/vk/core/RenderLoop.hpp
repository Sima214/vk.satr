#ifndef SATR_VK_RENDERLOOP_HPP
#define SATR_VK_RENDERLOOP_HPP
/**
 * @file
 * @brief Bridge between a vulkan context and a glfw window.
 */

#include <CommandBuffer.hpp>
#include <Context.hpp>
#include <DescriptorSetPack.hpp>
#include <Framebuffer.hpp>
#include <Glfw.hpp>
#include <GlfwWindow.hpp>
#include <Image.hpp>
#include <Presenter.hpp>
#include <Samplers.hpp>
#include <Scene.hpp>
#include <Utils.hpp>
#include <vulkan/vulkan.hpp>

#include <atomic>
#include <list>
#include <memory>
#include <mutex>

namespace satr::vk {

class RenderLoop : protected Presenter {
   private:
    static const uint32_t VERTEX_SHADER_CODE[];
    static const uint32_t FRAGMENT_SHADER_CODE[];
    static constexpr size_t sizeof_pipeline_scene_props_state(size_t max_scene_count) {
        size_t vertex_props_size = (sizeof(glm::vec2) * 2) * max_scene_count;
        size_t visibilities_size = (sizeof(glm::vec4)) * (((max_scene_count - 1) / 4) + 1);
        return vertex_props_size + visibilities_size;
    }
    static constexpr size_t offsetof_pipeline_scene_props_visibility_state(
         size_t max_scene_count) {
        size_t vertex_props_size = (sizeof(glm::vec2) * 2) * max_scene_count;
        return vertex_props_size;
    }

   public:
    struct RenderData {
        ::vk::CommandBuffer cmd_buf;
        ::vk::Fence frame_complete;
        /** Surface ready to be drawn onto. */
        ::vk::Semaphore window_surface_ready;
        /** Surface ready to be presented. */
        ::vk::Semaphore rendering_complete;

        std::unique_ptr<Scene[]> scenes_in_flight;
        /** cmd_buf is in use, frame_complete is waiting to be signaled. */
        bool in_flight;

        ::vk::DescriptorSet scene_state_set;
        Buffer scene_props_buffer;

        RenderData(size_t i, satr::vk::Context& engine, ::vk::CommandPool cmd_pool,
                   ::vk::DescriptorSet render_state_set, uint32_t scene_data_binding,
                   size_t max_scene_count);

        void wait(::vk::Device dev, size_t max_scene_count);
        void destroy(::vk::Device dev);
    };

    struct SceneProperties {
        glm::vec2 offset = {0.0f, 0.0f};
        glm::vec2 extent = {1.0f, 1.0f};
        float visibility = 1.0f;
        float priority = 0.0f;
    };

    struct SceneData {
        Scene scene;
        SceneProperties properties;

        SceneData() : scene(nullptr) {}
        SceneData(Scene scene_) : scene(scene_) {}
        SceneData(Scene scene_, SceneProperties properties_) :
            scene(scene_), properties(properties_) {}

        SceneData(SceneData&&) noexcept = default;
        SceneData& operator=(SceneData&&) noexcept = default;
        SceneData(const SceneData&) noexcept = default;
        SceneData& operator=(const SceneData&) noexcept = default;

        bool operator==(const SceneData& rhs) const {
            return scene == rhs.scene;
        }
        bool operator<(const SceneData& rhs) const {
            return properties.priority > rhs.properties.priority;
        }
    };

   protected:
    Context& _engine;
    satr::nui::Window& _window;

    ::vk::CommandPool _render_data_cmd_buf_pool;
    /** Rendering data for each frame in flight. */
    std::vector<RenderData> _render_data;
    /** The index into the _render_data vector that the next render() call will use. */
    size_t _render_data_index;
    Pipeline _pipeline;
    size_t _max_scene_count;
    Sampler _scene_state_sampler;
    Image2D::View _scene_attachment_invalid;
    uint32_t _scene_attachments_binding;
    DescriptorSetPack _scene_state_set_pack;

    /**
     * Scenes are kept in reverse rendering order.
     * Which is also:
     *   - Sorted front to back (as visible to the user).
     *   - Sorted in descending priority.
     */
    std::unique_ptr<SceneData[]> _scenes;
    std::atomic<size_t> _scenes_count;
    std::unique_ptr<std::tuple<Scene::RenderData*, bool /*reused*/>[]> _scenes_staging_data;
    std::list<std::pair<SceneData, bool /*delete*/>> _scenes_changes_queue;
    std::recursive_mutex _scenes_changes_mutex;

   private:
    size_t _cb_uid_fbsize;
    size_t _cb_uid_close;
    size_t _cb_uid_focus;
    size_t _cb_uid_keys;
    size_t _cb_uid_chars;
    size_t _cb_uid_buttons;
    size_t _cb_uid_scrolls;
    size_t _cb_uid_move;

    std::string _character_buffer;
    glm::dvec2 _mouse_last_relative;
    double _loop_timer_offset;

   public:
    RenderLoop(Context& engine, satr::nui::Window& window);
    RenderLoop(RenderLoop&&) = delete;
    RenderLoop& operator=(RenderLoop&&) = delete;
    ~RenderLoop();

    /** --------- Getters/Setters --------- */
    Context& get_engine() const {
        return _engine;
    }
    satr::nui::Window& get_window() const {
        return _window;
    }
    ::vk::Device get_device() const {
        return _engine.get_active_device();
    }
    FramebufferChain& get_framebuffer() {
        return Presenter::get_framebuffer();
    }

    size_t get_active_scene_count() const {
        return _scenes_count.load();
    }
    size_t get_max_scene_count() const;
    /**
     * Registers a new scene for rendering and event handling.
     */
    bool append_scene(Scene scene, SceneProperties props);
    /**
     * Update a scene's rendering properties.
     */
    bool update_scene(Scene scene, SceneProperties props);
    /**
     * Removes an existing scene from rendering and event handling.
     */
    bool remove_scene(const Scene scene);
    /**
     * Retrieves SceneData based on the implementation base pointer.
     */
    std::optional<SceneData> retrieve_scene(SceneImpl* impl);

    /**
     * Gets currently elapsed time
     * since the start of the
     * main loop in seconds.
     */
    double get_loop_time() const;
    bool is_mouse_captured() const;
    /**
     * Start relative cursor mode input.
     */
    void capture_mouse();
    /**
     * End relative cursor mode input.
     */
    void release_mouse();
    /**
     * Closes all scenes and finally the window.
     */
    void close();

    /** --------- Operations --------- */
    void setup();
    void render();
    /**
     * Enter the mainloop:
     *   1. Call RenderLoop::setup().
     *   2. While window shouldn't close, call RenderLoop::render(),
     *   3. RenderLoop::render() will also call
     *      RenderLoop::dispatch_window_events() when appropriate.
     */
    void loop();

    /** Event handling. */
    /**
     * Called right after having submitted for presentation.
     * Poll window for events and handle size, keyboard and close events.
     */
    void on_frame_submitted();
    /**
     * Called after having waited on and reset a vk::Fence.
     *
     * Lets all the scenes to build and submit their command buffers.
     */
    void render_scenes(size_t index);

   protected:
    void _render(RenderData& render_data, uint32_t current_chain_index);

    void _dispatch_window_events();
    void _update_scenes();

    void _framebuffer_size_callback(glm::ivec2 size);
    void _close_callback();
    void _focus_callback(bool focused);
    void _leave_callback(bool cursor_left);
    void _on_keyboard_button_press(satr::nui::Window::Keys key, int scancode,
                                   satr::nui::Window::ButtonState action,
                                   satr::nui::Window::Modifiers mods);
    void _on_char_input(spec::utf8_codepoint c);
    void _on_mouse_button_press(satr::nui::Window::MouseButtons button,
                                satr::nui::Window::ButtonState action,
                                satr::nui::Window::Modifiers mods);
    void _on_mouse_scroll(glm::dvec2 offset);
    void _on_mouse_movement(glm::dvec2 pos);

   private:
    /** Callbacks. */
    static void __framebuffer_size_callback(satr::nui::Window&, glm::ivec2 size);
    static void __close_callback(satr::nui::Window&);
    static void __focus_callback(satr::nui::Window& window, bool focused);
    static void __on_keyboard_button_press(satr::nui::Window&, satr::nui::Window::Keys key,
                                           int scancode, satr::nui::Window::ButtonState action,
                                           satr::nui::Window::Modifiers mods);
    static void __on_char_input(satr::nui::Window&, spec::utf8_codepoint c);
    static void __on_mouse_button_press(satr::nui::Window&,
                                        satr::nui::Window::MouseButtons button,
                                        satr::nui::Window::ButtonState action,
                                        satr::nui::Window::Modifiers mods);
    static void __on_mouse_scroll(satr::nui::Window&, glm::dvec2 offset);
    static void __on_mouse_movement(satr::nui::Window&, glm::dvec2 pos);
};

}  // namespace satr::vk

#endif /*SATR_VK_RENDERLOOP_HPP*/