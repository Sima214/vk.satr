# SAT-R vulkan version

Under development.

TODO: Either limit scope of dynamic dispatcher for extensions, or use global dynamic dispatcher.

TODO: On queue work commit, attach resource dependencies (which are shared_ptrs) to the fence?

## Object which wrap vulkan objects lifecycle notes

If the object is usually a singleton and/or the instance is managed primarily by the object that created it, then the object is part of the parent object, passed as a reference and optionally moveable to aid in construction (ex: Context).

If the object is potentially changing ownership and/or passed down to other objects with different lifecycle requirements, then the object is going to be created as shared_ptr and not be copy-able nor moveable (ex. Memory, Buffer, Image).

TODO: Remove non-moveable constraint to allow allocation in arenas and afterward migration to general heap with a special shared_ptr.


## Summer 2023
