#include <Glfw.hpp>
#include <GlfwWindow.hpp>
#include <glm/vec2.hpp>
#include <logger/Logger.hpp>
#include <satr/vk/core/Context.hpp>
#include <satr/vk/core/Presenter.hpp>
#include <satr/vk/pipeline/Framebuffer.hpp>
#include <satr/vk/resources/LoaderUtils.hpp>
#include <satr/vk/utils/CommandBuffer.hpp>
#include <satr/vk/utils/Extensions.hpp>
#include <satr/vk/utils/SharedQueue.hpp>
#include <satr/vk/utils/TypeConversions.hpp>
#include <stopwatch/StopWatch.hpp>
#include <vulkan/vulkan.hpp>
#include <vulkan/vulkan_to_string.hpp>

#include <cmath>
#include <cstdlib>
#include <cstring>
#include <limits>
#include <numbers>
#include <string>

#include <pthread.h>
#include <results.h>

using namespace std::string_literals;

static const uint32_t VERTEX_SHADER_CODE[] =
#include "AnimatedTriangle.vert.spirv.h"
     ;

static const uint32_t FRAG_SHADER_CODE[] =
#include "AnimatedTriangle.frag.spirv.h"
     ;

struct VertexData {
    glm::vec2 pos;
    glm::vec3 color;
};

struct RenderState {
    glm::mat4 transformation_matrix;
};
static const float SQRT_5 = std::sqrt(5.0f);
static const VertexData VERTICES[] = {{{0.0f, SQRT_5 / 4.0f}, {1.0f, 0.0f, 0.0f}},
                                      {{0.5f, -SQRT_5 / 4.0f}, {0.0f, 1.0f, 0.0f}},
                                      {{-0.5f, -SQRT_5 / 4.0f}, {0.0f, 0.0f, 1.0f}}};
static constexpr size_t VERTICES_COUNT = sizeof(VERTICES) / sizeof(VertexData);

class DirectPresenter : public satr::vk::Presenter {
   protected:
    satr::vk::Context& _context;

   public:
    DirectPresenter(satr::vk::Context& context, satr::nui::Window& window) : _context(context) {
        if (!set_frame_limit(-1)) {
            std::abort();
        }
        if (!init(context, window)) {
            std::abort();
        }
    }
    ~DirectPresenter() {
        deinit(_context);
    }

    bool recreate(satr::nui::Window& window) {
        return satr::vk::Presenter::recreate(_context, window);
    }

    uint32_t acquire(::vk::Semaphore window_surface_ready_signal) {
        return satr::vk::Presenter::acquire(_context.get_active_device(),
                                            window_surface_ready_signal);
    }
};

struct FrameData {
   private:
    ::vk::Device _dev = nullptr;

   public:
    ::vk::CommandBuffer cmd_buf;
    ::vk::Semaphore window_surface_ready_signal; /* Surface ready to be drawn onto. */
    ::vk::Semaphore rendering_complete_signal;   /* Surface ready to be presented. */
    ::vk::Fence frame_complete_barrier;

    /** Descriptor sets/Uniform buffers */
    ::vk::DescriptorSet render_state_set;
    satr::vk::Buffer render_state_buffer;

    FrameData() = default;
    FrameData(size_t i, satr::vk::Context& engine, ::vk::CommandPool cmd_pool,
              ::vk::DescriptorSet render_state_set_, uint32_t render_state_binding_) :
        _dev(engine.get_active_device()) {
        {
            ::vk::CommandBufferAllocateInfo ainfo(cmd_pool, ::vk::CommandBufferLevel::ePrimary,
                                                  1);
            ::vk::Result r = _dev.allocateCommandBuffers(&ainfo, &cmd_buf);
            if (r != ::vk::Result::eSuccess) {
                logger.logf("Couldn't allocate command buffer(", to_string(r), ")!");
            }
        }
        {
            ::vk::Result r;
            std::tie(r, window_surface_ready_signal) = _dev.createSemaphore({});
            if (r != ::vk::Result::eSuccess) {
                logger.logf("Couldn't allocate semaphore(", to_string(r), ")!");
            }
        }
        {
            ::vk::Result r;
            std::tie(r, rendering_complete_signal) = _dev.createSemaphore({});
            if (r != ::vk::Result::eSuccess) {
                logger.logf("Couldn't allocate semaphore(", to_string(r), ")!");
            }
        }
        {
            ::vk::FenceCreateInfo cinfo(::vk::FenceCreateFlagBits::eSignaled);
            ::vk::Result r;
            std::tie(r, frame_complete_barrier) = _dev.createFence(cinfo);
            if (r != ::vk::Result::eSuccess) {
                logger.logf("Couldn't allocate fence(", to_string(r), ")!");
            }
        }
        {
            render_state_set = render_state_set_;
            render_state_buffer =
                 engine.new_uniform_buffer("render_state_"s + std::to_string(i),
                                           sizeof(RenderState), {}, {}, false, true);
            if (!render_state_buffer) {
                spec::fatal("Couldn't allocate uniform buffer!");
            }
            render_state_buffer.update_uniform(render_state_set, render_state_binding_);
        }
    }
    ~FrameData() {
        if (_dev) {
            _dev.destroy(window_surface_ready_signal);
            _dev.destroy(rendering_complete_signal);
            _dev.destroy(frame_complete_barrier);
        }
    }
};

int main() {
    pthread_setname_np(pthread_self(), "animated_triangle");
    logger.configure(true, spec::Logger::Level::Debug, "animated_triangle.log",
                     spec::Logger::Level::All);
    // Initialization.
    logger.logi("Vk:Satr in startup...");
    satr::nui::GLFW::init_vulkan_loader(vkGetInstanceProcAddr);
    satr::nui::GLFW& glfw = satr::nui::GLFW::get_instance();
    logger.logi("Loaded GLFW, compiled version: ", glfw.get_compiled_version(),
                ", runtime version: ", glfw.get_runtime_version());
    if (!glfw.vulkan_supported()) {
        return TEST_ERROR;
    }
    // Window.
    satr::nui::WindowHints window_hints;
    // window_hints.set_maximized(true);
    // window_hints.set_srgb_capable(true);
    window_hints.set_client_api(satr::nui::WindowHints::ClientApi::None);
    satr::nui::Window window = glfw.create_window({640, 480}, "VK:Satr Triangle", window_hints);
    // Engine setup.
    auto ui_vk_ext = satr::vk::Extensions(glfw);
    logger.logd("Using GLFW's vulkan extensions: ", ui_vk_ext);
    satr::vk::Context engine("animated_triangle", ui_vk_ext);
    satr::vk::DeviceCapabilities::Filter requirements;
    requirements.append_required_extension("VK_KHR_swapchain");
    satr::vk::DeviceCapabilities::Requests features;
    if (!engine.select_device(requirements, features, glfw)) {
        logger.logf_rec("No appropriate vulkan device found!");
        return TEST_ERROR;
    }

    // Window surface setup.
    DirectPresenter presenter(engine, window);
    satr::vk::FramebufferChain& framebuffer = presenter.get_framebuffer();
    auto dev = engine.get_active_device();

    // Initialization/Data Transfer step.
    satr::vk::Buffer vertices;
    satr::vk::Pipeline pipeline;
    {
        const auto& transfer_queue = engine.get_queue(satr::vk::QueueSlotRole::Transfer);
        ::vk::CommandPool transfer_command_pool =
             engine.create_command_pool(satr::vk::QueueSlotRole::Transfer);

        ::vk::CommandBuffer trans_cmd_buf;
        {
            ::vk::CommandBufferAllocateInfo ainfo(transfer_command_pool,
                                                  ::vk::CommandBufferLevel::ePrimary, 1);
            ::vk::Result r = dev.allocateCommandBuffers(&ainfo, &trans_cmd_buf);
            if (r != ::vk::Result::eSuccess) {
                logger.logf("Couldn't allocate command buffer(", to_string(r), ")!");
            }
        }
        ::vk::Fence transfer_complete_barrier;
        {
            ::vk::FenceCreateInfo cinfo;
            ::vk::Result r;
            std::tie(r, transfer_complete_barrier) = dev.createFence(cinfo);
            if (r != ::vk::Result::eSuccess) {
                logger.logf("Couldn't allocate fence(", to_string(r), ")!");
            }
        }
        satr::vk::CommandBufferContext trans_cmd_ctx(trans_cmd_buf, transfer_complete_barrier);
        if (!trans_cmd_ctx) {
            return TEST_FAIL;
        }

        // Shader creation.
        {
            auto vertex_shader = engine.new_shader(VERTEX_SHADER_CODE);
            auto fragment_shader = engine.new_shader(FRAG_SHADER_CODE);
            satr::vk::Module shaders[] = {vertex_shader, fragment_shader};
            satr::vk::GraphicsPipelineOptions pipeline_options;
            pipeline_options.dynamic_state_add(::vk::DynamicState::eViewport);
            pipeline_options.dynamic_state_add(::vk::DynamicState::eScissor);
            pipeline_options.vertex_input_bindings_add(0, sizeof(VertexData));
            pipeline_options.vertex_input_attributes_add("inPosition"s, 0,
                                                         offsetof(VertexData, pos));
            pipeline_options.vertex_input_attributes_add("inColor"s, 0,
                                                         offsetof(VertexData, color));
            pipeline = engine.new_pipeline(framebuffer, shaders, pipeline_options);
        }

        // Vertex data preparation.
        vertices = satr::vk::load_vertex_buffer(engine, trans_cmd_ctx, "triangle",
                                                sizeof(VERTICES), VERTICES);

        // Async host to device loading.
        trans_cmd_ctx.submit(transfer_queue);

        // Wait for host to device loading to finish.
        {
            if (!trans_cmd_ctx.wait(dev)) {
                return TEST_FAIL;
            }
            dev.destroy(transfer_complete_barrier);
            dev.destroy(transfer_command_pool);
        }
    }

    // Scene creation.
    auto [queue_index, queue] = engine.get_queue_slot(satr::vk::QueueSlotRole::Graphics);

    ::vk::CommandPool command_pool;
    {
        ::vk::CommandPoolCreateInfo cinfo(::vk::CommandPoolCreateFlagBits::eResetCommandBuffer,
                                          queue_index.phys.family_index);
        ::vk::Result r;
        std::tie(r, command_pool) = dev.createCommandPool(cinfo);
        if (r != ::vk::Result::eSuccess) {
            logger.loge("Couldn't create command pool(", to_string(r), ")!");
            return TEST_ERROR;
        }
    }

    // Main loop setup.
    const uint32_t max_frames_in_flight = framebuffer.get_chain_size() - 1;
    logger.logi("max_frames_in_flight = ", max_frames_in_flight);

    auto opt_render_state_desc = pipeline.get_descriptors().retrieve_by_name("render_state");
    if (!opt_render_state_desc) {
        logger.loge("Couldn't find render_state uniform block!");
        return TEST_FAIL;
    }
    satr::vk::Descriptor render_state_desc = opt_render_state_desc.value();
    auto render_state_layout =
         pipeline.get_descriptors().get_set_layout(render_state_desc.get_set());
    auto render_state_set_pack =
         engine.new_descriptor_set_pack(max_frames_in_flight, render_state_layout);
    if (!render_state_set_pack) {
        return TEST_FAIL;
    }

    std::vector<FrameData> frames_in_flight;
    frames_in_flight.reserve(max_frames_in_flight);
    for (uint32_t i = 0; i < max_frames_in_flight; i++) {
        frames_in_flight.emplace_back(i, engine, command_pool, render_state_set_pack[i],
                                      render_state_desc.get_binding());
    }

    framebuffer.set_color_clear_value(0, satr::vk::rgba_hex_to_vec(0xcb7f7fff));

    // Main Loop.
    {
        size_t frame_count = 0;
        spec::StopWatch frametimes;
        frametimes.start();

        uint32_t next_frame_slot = 0;
        while (!window.should_close()) {
            const FrameData& frame_data = frames_in_flight[next_frame_slot];
            // Acquire next window surface.
            uint32_t current_chain_index =
                 presenter.acquire(frame_data.window_surface_ready_signal);
            bool chain_index_acquired = current_chain_index < framebuffer.get_chain_size();

            if (chain_index_acquired) {
                // Wait for previous frame completion.
                ::vk::Result fence_result;
                do {
                    fence_result = dev.waitForFences({frame_data.frame_complete_barrier}, true,
                                                     std::numeric_limits<uint64_t>::max());
                } while (fence_result == ::vk::Result::eTimeout);
                if (fence_result != ::vk::Result::eSuccess) {
                    logger.loge("Error while waiting on fence(", to_string(fence_result), ")!");
                    return TEST_FAIL;
                }
                dev.resetFences({frame_data.frame_complete_barrier});

                // Statistics.
                frametimes.stop();
                if ((frame_count % 30) == 0) {
                    logger.logd("fps=", 1 / (frametimes.get_avg_time() / 1000));
                }
                if ((frame_count % 300) == 0) {
                    frametimes.reset();
                }
                frametimes.start();

                next_frame_slot = (next_frame_slot + 1) % max_frames_in_flight;

                frame_data.cmd_buf.reset();
                // Rendering setup.
                {
                    ::vk::CommandBufferBeginInfo binfo(
                         ::vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
                    ::vk::Result r = frame_data.cmd_buf.begin(binfo);
                    if (r != ::vk::Result::eSuccess) {
                        logger.loge("Couldn't begin command recording(", to_string(r), ")!");
                        return TEST_FAIL;
                    }
                }
                framebuffer.start_pass(frame_data.cmd_buf, current_chain_index);

                // Update shader state.
                float animation_state;
                {
                    double integral;
                    animation_state = std::modf(glfw.get_time() / 8, &integral);
                }
                RenderState render_state = {glm::identity<glm::mat4>()};
                render_state.transformation_matrix =
                     glm::rotate(render_state.transformation_matrix,
                                 animation_state * (std::numbers::pi_v<float> * 2.0f),
                                 glm::vec3(0.0f, 0.0f, 1.0f));
                memcpy(frame_data.render_state_buffer.get_memory().get_mapping(), &render_state,
                       sizeof(render_state));
                frame_data.render_state_buffer.get_memory().flush();

                // Rendering.
                pipeline.bind(frame_data.cmd_buf);
                pipeline.update_viewport(frame_data.cmd_buf, framebuffer, current_chain_index);
                pipeline.bind_sets(frame_data.cmd_buf,
                                   std::span(&frame_data.render_state_set, 1));
                vertices.bind_vertex(frame_data.cmd_buf, 0);
                frame_data.cmd_buf.draw(VERTICES_COUNT, 1, 0, 0);

                // Rendering finalize.
                framebuffer.end_pass(frame_data.cmd_buf);
                {
                    ::vk::Result r = frame_data.cmd_buf.end();
                    if (r != ::vk::Result::eSuccess) {
                        logger.loge("Couldn't end command recording(", to_string(r), ")!");
                        return TEST_FAIL;
                    }
                }

                // Command submission.
                {
                    ::vk::PipelineStageFlags wait_stages[] = {
                         ::vk::PipelineStageFlagBits::eColorAttachmentOutput};
                    ::vk::SubmitInfo info;
                    info.waitSemaphoreCount = 1;
                    info.pWaitSemaphores = &frame_data.window_surface_ready_signal;
                    info.pWaitDstStageMask = wait_stages;
                    info.commandBufferCount = 1;
                    info.pCommandBuffers = &frame_data.cmd_buf;
                    info.signalSemaphoreCount = 1;
                    info.pSignalSemaphores = &frame_data.rendering_complete_signal;
                    if (!queue.submit(info, frame_data.frame_complete_barrier)) {
                        return TEST_FAIL;
                    }
                }
                // Present submission.
                if (!presenter.submit(queue, frame_data.rendering_complete_signal,
                                      current_chain_index)) {
                    return TEST_FAIL;
                }
                frame_count++;
            }
            else {
                logger.logd("Couldn't acquire swapchain image: skipping frame.");
            }

            // Basic window event handling.
            window.poll_events();

            // Handle swapchain re-creation.
            presenter.recreate(window);
        }
    }

    // Cleanup.
    // TODO: Instead wait on last submitted fence.
    (void) dev.waitIdle();
    logger.logd("Cleaning up...");
    dev.destroyCommandPool(command_pool);

    return TEST_OK;
}
