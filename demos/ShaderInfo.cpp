#include <Glfw.hpp>
#include <GlfwWindow.hpp>
#include <glm/vec2.hpp>
#include <logger/Logger.hpp>
#include <satr/vk/core/Context.hpp>
#include <satr/vk/pipeline/Shaders.hpp>
#include <satr/vk/utils/Extensions.hpp>
#include <vulkan/vulkan.hpp>
#include <vulkan/vulkan_to_string.hpp>

#include <cstdint>
#include <ios>

#include <pthread.h>
#include <results.h>

static const uint32_t VERTEX_SHADER_CODE[] =
#include "ShaderInfo.vert.spirv.h"
     ;

static const uint32_t FRAG_SHADER_CODE[] =
#include "ShaderInfo.frag.spirv.h"
     ;

int main(int, char*[]) {

    pthread_setname_np(pthread_self(), "shader_info");
    logger.configure(true, spec::Logger::Level::Debug, "shader_info.log",
                     spec::Logger::Level::All);

    // Engine setup.
    logger.logi("Vk:Satr in startup...");
    satr::vk::Context engine("ShaderInfo");
    satr::vk::DeviceCapabilities::Filter requirements;
    satr::vk::DeviceCapabilities::Requests features;
    if (!engine.select_device(requirements, features)) {
        logger.logf_rec("No appropriate vulkan device found!");
        return TEST_ERROR;
    }

    // Module print reflection info.
    satr::vk::Module shader1 = engine.new_shader(VERTEX_SHADER_CODE);
    auto& refl1 = shader1->get_reflection();
    auto& mod1 = refl1.GetShaderModule();
    logger.logi("Entry point name: ", shader1->get_entry_point());
    logger.logi("Stage: ", ::vk::to_string(shader1->get_stage_type()));

    logger.logi("Descriptor Binding count: ", mod1.descriptor_binding_count);
    for (size_t i = 0; i < mod1.descriptor_binding_count; i++) {
        logger.logi("\t", i, ". Name: ", mod1.descriptor_bindings[i].name);
        logger.logi("\t", i, ". Binding: ", mod1.descriptor_bindings[i].binding);
        logger.logi("\t", i, ". Set: ", mod1.descriptor_bindings[i].set);
        logger.logi("\t", i, ". Descriptor type: ",
                    ::vk::to_string(static_cast<::vk::DescriptorType>(
                         mod1.descriptor_bindings[i].descriptor_type)));
        logger.logi("\t", i, ". Resource type: ", mod1.descriptor_bindings[i].resource_type);
        logger.logi("\t", i, ". Count: ", mod1.descriptor_bindings[i].count);
        logger.logi("\t", i, ". Type info - name: ",
                    mod1.descriptor_bindings[i].type_description->type_name);
        logger.logi("\t", i, ". Type info - storage class: ",
                    mod1.descriptor_bindings[i].type_description->storage_class);
        logger.logi("\t", i, ". Type info - member count: ",
                    mod1.descriptor_bindings[i].type_description->member_count);
        logger.logi("\t", i, ". Type info - type flags: 0x", std::hex,
                    mod1.descriptor_bindings[i].type_description->type_flags);
        logger.logi("\t", i, ". Type info - decoration flags: 0x", std::hex,
                    mod1.descriptor_bindings[i].type_description->decoration_flags);
    }

    logger.logi("Descriptor Set count: ", mod1.descriptor_set_count);
    for (size_t i = 0; i < mod1.descriptor_set_count; i++) {
        logger.logi("\t", i, ". Set: ", mod1.descriptor_sets[i].set);
        logger.logi("\t", i, ". Bindings: ", mod1.descriptor_sets[i].binding_count);
        for (size_t j = 0; j < mod1.descriptor_sets[i].binding_count; j++) {
            logger.logi("\t", i, ".", j,
                        " Bindings: ", mod1.descriptor_sets[i].bindings[j]->binding);
        }
    }

    logger.logi("Push constant blocks: ", mod1.push_constant_block_count);
    for (size_t i = 0; i < mod1.push_constant_block_count; i++) {
        logger.logi("\t", i, ". Name: ", mod1.push_constant_blocks[i].name);
        logger.logi("\t", i, ". Size: ", mod1.push_constant_blocks[i].size);
        logger.logi("\t", i, ". Member count: ", mod1.push_constant_blocks[i].member_count);
    }

    logger.logi("Input variable count: ", mod1.input_variable_count);
    for (size_t i = 0; i < mod1.input_variable_count; i++) {
        logger.logi("\t", i, ". Name: ", mod1.input_variables[i]->name);
        logger.logi("\t", i, ". Semantic: ", mod1.input_variables[i]->semantic);
        logger.logi("\t", i, ". Location: ", mod1.input_variables[i]->location);
        logger.logi("\t", i, ". Built-in: ", mod1.input_variables[i]->built_in);
        logger.logi("\t", i, ". Format: 0x", std::hex, mod1.input_variables[i]->format);
        logger.logi("\t", i, ". Component: 0x", std::hex, mod1.input_variables[i]->component);
        logger.logi("\t", i, ". Storage class: 0x", std::hex,
                    mod1.input_variables[i]->storage_class);
        logger.logi("\t", i, ". Decoration Flags: 0x", std::hex,
                    mod1.input_variables[i]->decoration_flags);
        logger.logi("\t", i, ". Member count: ", mod1.input_variables[i]->member_count);
    }

    logger.logi("Output variable count: ", mod1.output_variable_count);
    for (size_t i = 0; i < mod1.output_variable_count; i++) {
        logger.logi("\t", i, ". Name: ", mod1.output_variables[i]->name);
        logger.logi("\t", i, ". Semantic: ", mod1.output_variables[i]->semantic);
        logger.logi("\t", i, ". Location: ", mod1.output_variables[i]->location);
        logger.logi("\t", i, ". Built-in: ", mod1.output_variables[i]->built_in);
        logger.logi("\t", i, ". Format: 0x", std::hex, mod1.output_variables[i]->format);
        logger.logi("\t", i, ". Component: 0x", std::hex, mod1.output_variables[i]->component);
        logger.logi("\t", i, ". Storage class: 0x", std::hex,
                    mod1.output_variables[i]->storage_class);
        logger.logi("\t", i, ". Decoration Flags: 0x", std::hex,
                    mod1.output_variables[i]->decoration_flags);
        logger.logi("\t", i, ". Member count: ", mod1.output_variables[i]->member_count);
    }

    // logger.logi("Interface variable count: ", mod.interface_variable_count);
    // for (size_t i = 0; i < mod.interface_variable_count; i++) {
    //     logger.logi("\t", i, ". Name: ", mod.interface_variables[i].name);
    //     logger.logi("\t", i, ". Semantic: ", mod.interface_variables[i].semantic);
    //     logger.logi("\t", i, ". Location: ", mod.interface_variables[i].location);
    //     logger.logi("\t", i, ". Format: 0x", std::hex, mod.interface_variables[i].format);
    // }

    satr::vk::Module shader2 = engine.new_shader(FRAG_SHADER_CODE);
    auto& refl2 = shader2->get_reflection();
    auto& mod2 = refl2.GetShaderModule();
    logger.logi("Entry point name: ", shader2->get_entry_point());
    logger.logi("Stage: ", ::vk::to_string(shader2->get_stage_type()));

    logger.logi("Descriptor Binding count: ", mod2.descriptor_binding_count);
    for (size_t i = 0; i < mod2.descriptor_binding_count; i++) {
        logger.logi("\t", i, ". Name: ", mod2.descriptor_bindings[i].name);
        logger.logi("\t", i, ". Binding: ", mod2.descriptor_bindings[i].binding);
        logger.logi("\t", i, ". Set: ", mod2.descriptor_bindings[i].set);
        logger.logi("\t", i, ". Descriptor type: ",
                    ::vk::to_string(static_cast<::vk::DescriptorType>(
                         mod2.descriptor_bindings[i].descriptor_type)));
        logger.logi("\t", i, ". Resource type: ", mod2.descriptor_bindings[i].resource_type);
        logger.logi("\t", i, ". Count: ", mod2.descriptor_bindings[i].count);
        logger.logi("\t", i, ". Type info - name: ",
                    mod2.descriptor_bindings[i].type_description->type_name);
        logger.logi("\t", i, ". Type info - storage class: ",
                    mod2.descriptor_bindings[i].type_description->storage_class);
        logger.logi("\t", i, ". Type info - member count: ",
                    mod2.descriptor_bindings[i].type_description->member_count);
        logger.logi("\t", i, ". Type info - type flags: 0x", std::hex,
                    mod2.descriptor_bindings[i].type_description->type_flags);
        logger.logi("\t", i, ". Type info - decoration flags: 0x", std::hex,
                    mod2.descriptor_bindings[i].type_description->decoration_flags);
    }

    logger.logi("Descriptor Set count: ", mod2.descriptor_set_count);
    for (size_t i = 0; i < mod2.descriptor_set_count; i++) {
        logger.logi("\t", i, ". Set: ", mod2.descriptor_sets[i].set);
        logger.logi("\t", i, ". Bindings: ", mod2.descriptor_sets[i].binding_count);
        for (size_t j = 0; j < mod2.descriptor_sets[i].binding_count; j++) {
            logger.logi("\t", i, ".", j,
                        " Bindings: ", mod2.descriptor_sets[i].bindings[j]->binding);
        }
    }

    logger.logi("Push constant blocks: ", mod2.push_constant_block_count);
    for (size_t i = 0; i < mod2.push_constant_block_count; i++) {
        logger.logi("\t", i, ". Name: ", mod2.push_constant_blocks[i].name);
        logger.logi("\t", i, ". Size: ", mod2.push_constant_blocks[i].size);
        logger.logi("\t", i, ". Member count: ", mod2.push_constant_blocks[i].member_count);
    }

    logger.logi("Input variable count: ", mod2.input_variable_count);
    for (size_t i = 0; i < mod2.input_variable_count; i++) {
        logger.logi("\t", i, ". Name: ", mod2.input_variables[i]->name);
        logger.logi("\t", i, ". Semantic: ", mod2.input_variables[i]->semantic);
        logger.logi("\t", i, ". Location: ", mod2.input_variables[i]->location);
        logger.logi("\t", i, ". Built-in: ", mod2.input_variables[i]->built_in);
        logger.logi("\t", i, ". Format: 0x", std::hex, mod2.input_variables[i]->format);
        logger.logi("\t", i, ". Component: 0x", std::hex, mod2.input_variables[i]->component);
        logger.logi("\t", i, ". Storage class: 0x", std::hex,
                    mod2.input_variables[i]->storage_class);
        logger.logi("\t", i, ". Decoration Flags: 0x", std::hex,
                    mod2.input_variables[i]->decoration_flags);
        logger.logi("\t", i, ". Member count: ", mod2.input_variables[i]->member_count);
    }

    logger.logi("Output variable count: ", mod2.output_variable_count);
    for (size_t i = 0; i < mod2.output_variable_count; i++) {
        logger.logi("\t", i, ". Name: ", mod2.output_variables[i]->name);
        logger.logi("\t", i, ". Semantic: ", mod2.output_variables[i]->semantic);
        logger.logi("\t", i, ". Location: ", mod2.output_variables[i]->location);
        logger.logi("\t", i, ". Built-in: ", mod2.output_variables[i]->built_in);
        logger.logi("\t", i, ". Format: 0x", std::hex, mod2.output_variables[i]->format);
        logger.logi("\t", i, ". Component: 0x", std::hex, mod2.output_variables[i]->component);
        logger.logi("\t", i, ". Storage class: 0x", std::hex,
                    mod2.output_variables[i]->storage_class);
        logger.logi("\t", i, ". Decoration Flags: 0x", std::hex,
                    mod2.output_variables[i]->decoration_flags);
        logger.logi("\t", i, ". Member count: ", mod2.output_variables[i]->member_count);
    }

    // logger.logi("Interface variable count: ", mod.interface_variable_count);
    // for (size_t i = 0; i < mod.interface_variable_count; i++) {
    //     logger.logi("\t", i, ". Name: ", mod.interface_variables[i].name);
    //     logger.logi("\t", i, ". Semantic: ", mod.interface_variables[i].semantic);
    //     logger.logi("\t", i, ". Location: ", mod.interface_variables[i].location);
    //     logger.logi("\t", i, ". Format: 0x", std::hex, mod.interface_variables[i].format);
    // }

    // Cleanup.

    return TEST_OK;
}