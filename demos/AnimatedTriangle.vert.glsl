#version 450

layout(set=0, binding=0) uniform RenderState {
    mat4 transformation_matrix;
} render_state;

layout(location = 0) in vec2 inPosition;
layout(location = 1) in vec3 inColor;

layout(location = 0) out vec3 fragColor;

void main() {
    vec4 pos = vec4(inPosition, 0.0, 1.0);
    gl_Position = render_state.transformation_matrix * pos;
    fragColor = inColor;
}
