#ifndef MESON_TEST_RESULTS
#define MESON_TEST_RESULTS
/**
 * @file
 * @brief Definitions for executable return values recognized by meson.
 */

/**
 * Test completed successfully.
 */
#define TEST_OK 0

/**
 * Test failed.
 */
#define TEST_FAIL 1

/**
 * Test could not be setup.
 */
#define TEST_ERROR 99

/**
 * Test could not be run.
 */
#define TEST_SKIP 77

#endif /*MESON_TEST_RESULTS*/
