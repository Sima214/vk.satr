#include <Glfw.hpp>
#include <GlfwWindow.hpp>
#include <logger/Logger.hpp>
#include <satr/vk/core/Context.hpp>
#include <satr/vk/core/RenderLoop.hpp>
#include <satr/vk/core/Scene.hpp>
#include <satr/vk/imgui/DearImguiScene.hpp>
#include <satr/vk/pipeline/Pipelines.hpp>

#include <memory>

#include <imgui.h>
#include <pthread.h>
#include <results.h>

static const uint32_t VERTEX_SHADER_CODE[] =
#include "Triangle.vert.spirv.h"
     ;

static const uint32_t FRAG_SHADER_CODE[] =
#include "Triangle.frag.spirv.h"
     ;

class TriangleScene final : public satr::vk::SceneImpl {
   protected:
    satr::vk::Pipeline _pipeline;

   public:
    TriangleScene(satr::vk::RenderLoop& root) : satr::vk::SceneImpl("triangle", root) {}
    virtual ~TriangleScene() = default;

    virtual bool on_setup(size_t chain_size, glm::ivec2 scene_size,
                          [[maybe_unused]] glm::vec2& offset,
                          [[maybe_unused]] glm::vec2& extent) override {
        satr::vk::SceneImpl::setup_framebuffer(chain_size, scene_size);
        satr::vk::SceneImpl::setup_render_data(chain_size);

        auto vertex_shader = get_engine().new_shader(VERTEX_SHADER_CODE);
        auto fragment_shader = get_engine().new_shader(FRAG_SHADER_CODE);
        satr::vk::Module shaders[] = {vertex_shader, fragment_shader};
        satr::vk::GraphicsPipelineOptions pipeline_options;
        pipeline_options.dynamic_state_add(::vk::DynamicState::eViewport);
        pipeline_options.dynamic_state_add(::vk::DynamicState::eScissor);
        _pipeline = get_engine().new_pipeline(get_framebuffer(), shaders, pipeline_options);

        return satr::vk::SceneImpl::on_setup_complete();
    }
    virtual bool render(size_t chain_index, ::vk::CommandBuffer cmd_buf) override {
        _pipeline.bind(cmd_buf);
        _pipeline.update_viewport(cmd_buf, get_framebuffer(), chain_index);
        cmd_buf.draw(3, 1, 0, 0);
        return true;
    }
};

class ImguiDemo final : public satr::vk::DearImguiScene {
   protected:
   public:
    ImguiDemo(satr::vk::RenderLoop& root) : satr::vk::DearImguiScene(root) {}
    virtual ~ImguiDemo() = default;

    virtual bool on_setup(size_t chain_size, glm::ivec2 scene_size, glm::vec2& offset,
                          glm::vec2& extent) override {
        bool ret = satr::vk::DearImguiScene::on_setup(chain_size, scene_size, offset, extent);
        // get_framebuffer().set_color_clear_value(0, glm::vec4(0.1, 0.1, 0.1, 0.0));
        return ret;
    }

    virtual void render_ui(ImGuiContext*) override {
        ImGui::ShowDemoWindow();
        // ImGui::Begin("Hello Imgui. I want your plots.");
        // ImGui::Text("This could be some useful text.");
        // ImGui::End();
    }
};

int main() {
    pthread_setname_np(pthread_self(), "imgui_scene");
    logger.configure(true, spec::Logger::Level::Debug, "imgui_scene.log",
                     spec::Logger::Level::All);
    // Initialization.
    logger.logi("Vk:Satr in startup...");
    satr::nui::GLFW::init_vulkan_loader(vkGetInstanceProcAddr);
    satr::nui::GLFW& glfw = satr::nui::GLFW::get_instance();
    logger.logi("Loaded GLFW, compiled version: ", glfw.get_compiled_version(),
                ", runtime version: ", glfw.get_runtime_version());
    if (!glfw.vulkan_supported()) {
        return TEST_ERROR;
    }
    // Window.
    satr::nui::WindowHints window_hints;
    // window_hints.set_maximized(true);
    // window_hints.set_srgb_capable(true);
    window_hints.set_client_api(satr::nui::WindowHints::ClientApi::None);
    satr::nui::Window window =
         glfw.create_window({640, 480}, "VK:Satr's DearImGui Demo", window_hints);
    // Engine setup.
    auto ui_vk_ext = satr::vk::Extensions(glfw);
    logger.logd("Using GLFW's vulkan extensions: ", ui_vk_ext);
    satr::vk::Context engine("dearimgui", ui_vk_ext);
    satr::vk::DeviceCapabilities::Filter requirements;
    requirements.append_required_extension("VK_KHR_swapchain");
    satr::vk::DeviceCapabilities::Requests features;
    if (!engine.select_device(requirements, features, glfw)) {
        logger.logf_rec("No appropriate vulkan device found!");
        return TEST_ERROR;
    }

    satr::vk::RenderLoop compositor(engine, window);
    satr::vk::Scene triangle_scene(std::make_shared<TriangleScene>(compositor));
    satr::vk::Scene imgui_scene(std::make_shared<ImguiDemo>(compositor));
    satr::vk::RenderLoop::SceneProperties triangle_scene_props = {
         glm::vec2(0.02, 0.02), glm::vec2(0.96, 0.96), 1.0, 0.5};
    satr::vk::RenderLoop::SceneProperties imgui_scene_props = {.visibility = 1.0,
                                                               .priority = 1.0};
    compositor.append_scene(imgui_scene, imgui_scene_props);
    compositor.append_scene(triangle_scene, triangle_scene_props);

    compositor.loop();

    return TEST_OK;
}
