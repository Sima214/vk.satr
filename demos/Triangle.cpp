#include <Glfw.hpp>
#include <GlfwWindow.hpp>
#include <glm/vec2.hpp>
#include <logger/Logger.hpp>
#include <satr/vk/core/Context.hpp>
#include <satr/vk/core/Presenter.hpp>
#include <satr/vk/pipeline/Framebuffer.hpp>
#include <satr/vk/utils/CommandBuffer.hpp>
#include <satr/vk/utils/Extensions.hpp>
#include <satr/vk/utils/SharedQueue.hpp>
#include <satr/vk/utils/TypeConversions.hpp>
#include <stopwatch/StopWatch.hpp>
#include <vulkan/vulkan.hpp>
#include <vulkan/vulkan_to_string.hpp>

#include <cstdlib>

#include <pthread.h>
#include <results.h>

static const uint32_t VERTEX_SHADER_CODE[] =
#include "Triangle.vert.spirv.h"
     ;

static const uint32_t FRAG_SHADER_CODE[] =
#include "Triangle.frag.spirv.h"
     ;

class DirectPresenter : public satr::vk::Presenter {
   protected:
    satr::vk::Context& _context;

   public:
    DirectPresenter(satr::vk::Context& context, satr::nui::Window& window) : _context(context) {
        if (!set_frame_limit(-1)) {
            std::abort();
        }
        if (!init(context, window)) {
            std::abort();
        }
    }
    ~DirectPresenter() {
        deinit(_context);
    }

    bool recreate(satr::nui::Window& window) {
        return satr::vk::Presenter::recreate(_context, window);
    }

    uint32_t acquire(::vk::Semaphore window_surface_ready_signal) {
        return satr::vk::Presenter::acquire(_context.get_active_device(),
                                            window_surface_ready_signal);
    }
};

struct FrameData {
   private:
    ::vk::Device _dev = nullptr;

   public:
    ::vk::CommandBuffer cmd_buf;
    ::vk::Semaphore window_surface_ready_signal; /* Surface ready to be drawn onto. */
    ::vk::Semaphore rendering_complete_signal;   /* Surface ready to be presented. */
    ::vk::Fence frame_complete_barrier;

    satr::vk::CommandBufferContext cmd_ctx;

    FrameData() = default;
    FrameData(FrameData&&) noexcept = default;
    FrameData& operator=(FrameData&&) noexcept = default;

    FrameData(::vk::Device dev, ::vk::CommandPool cmd_pool) : _dev(dev) {
        {
            ::vk::CommandBufferAllocateInfo ainfo(cmd_pool, ::vk::CommandBufferLevel::ePrimary,
                                                  1);
            ::vk::Result r = dev.allocateCommandBuffers(&ainfo, &cmd_buf);
            if (r != ::vk::Result::eSuccess) {
                logger.logf("Couldn't allocate command buffer(", to_string(r), ")!");
            }
        }
        {
            ::vk::Result r;
            std::tie(r, window_surface_ready_signal) = dev.createSemaphore({});
            if (r != ::vk::Result::eSuccess) {
                logger.logf("Couldn't allocate semaphore(", to_string(r), ")!");
            }
        }
        {
            ::vk::Result r;
            std::tie(r, rendering_complete_signal) = dev.createSemaphore({});
            if (r != ::vk::Result::eSuccess) {
                logger.logf("Couldn't allocate semaphore(", to_string(r), ")!");
            }
        }
        {
            ::vk::FenceCreateInfo cinfo(vk::FenceCreateFlagBits::eSignaled);
            ::vk::Result r;
            std::tie(r, frame_complete_barrier) = dev.createFence(cinfo);
            if (r != ::vk::Result::eSuccess) {
                logger.logf("Couldn't allocate fence(", to_string(r), ")!");
            }
        }
    }
    ~FrameData() {
        if (_dev) {
            _dev.destroy(window_surface_ready_signal);
            _dev.destroy(rendering_complete_signal);
            _dev.destroy(frame_complete_barrier);
        }
    }
    bool start_cmd_ctx() {
        cmd_ctx = satr::vk::CommandBufferContext(cmd_buf, frame_complete_barrier);
        return static_cast<bool>(cmd_ctx);
    }
};

int main() {
    pthread_setname_np(pthread_self(), "triangle");
    logger.configure(true, spec::Logger::Level::Debug, "triangle.log",
                     spec::Logger::Level::All);
    // Initialization.
    logger.logi("Vk:Satr in startup...");
    satr::nui::GLFW::init_vulkan_loader(vkGetInstanceProcAddr);
    satr::nui::GLFW& glfw = satr::nui::GLFW::get_instance();
    logger.logi("Loaded GLFW, compiled version: ", glfw.get_compiled_version(),
                ", runtime version: ", glfw.get_runtime_version());
    if (!glfw.vulkan_supported()) {
        return TEST_ERROR;
    }
    // Window.
    satr::nui::WindowHints window_hints;
    // window_hints.set_maximized(true);
    // window_hints.set_srgb_capable(true);
    window_hints.set_client_api(satr::nui::WindowHints::ClientApi::None);
    satr::nui::Window window = glfw.create_window({640, 480}, "VK:Satr Triangle", window_hints);
    // Engine setup.
    auto ui_vk_ext = satr::vk::Extensions(glfw);
    logger.logd("Using GLFW's vulkan extensions: ", ui_vk_ext);
    satr::vk::Context engine("triangle", ui_vk_ext);
    satr::vk::DeviceCapabilities::Filter requirements;
    requirements.append_required_extension("VK_KHR_swapchain");
    satr::vk::DeviceCapabilities::Requests features;
    if (!engine.select_device(requirements, features, glfw)) {
        logger.logf_rec("No appropriate vulkan device found!");
        return TEST_ERROR;
    }

    // Window surface setup.
    DirectPresenter presenter(engine, window);
    satr::vk::FramebufferChain& framebuffer = presenter.get_framebuffer();

    // Shader creation.
    satr::vk::Pipeline pipeline;
    {
        auto vertex_shader = engine.new_shader(VERTEX_SHADER_CODE);
        auto fragment_shader = engine.new_shader(FRAG_SHADER_CODE);
        satr::vk::Module shaders[] = {vertex_shader, fragment_shader};
        satr::vk::GraphicsPipelineOptions pipeline_options;
        pipeline_options.dynamic_state_add(::vk::DynamicState::eViewport);
        pipeline_options.dynamic_state_add(::vk::DynamicState::eScissor);
        pipeline = engine.new_pipeline(framebuffer, shaders, pipeline_options);
    }

    // Scene creation.
    auto dev = engine.get_active_device();
    const auto& queue = engine.get_queue(satr::vk::QueueSlotRole::Graphics);

    ::vk::CommandPool command_pool =
         engine.create_command_pool(satr::vk::QueueSlotRole::Graphics);
    if (!command_pool) {
        return TEST_ERROR;
    }

    // Main loop setup.
    framebuffer.set_color_clear_value(0, satr::vk::rgba_hex_to_vec(0xcb7f7fff));

    const uint32_t max_frames_in_flight = framebuffer.get_chain_size() - 1;
    std::vector<FrameData> frames_in_flight;
    frames_in_flight.reserve(max_frames_in_flight);
    logger.logi("max_frames_in_flight = ", max_frames_in_flight);
    for (uint32_t i = 0; i < max_frames_in_flight; i++) {
        frames_in_flight.emplace_back(dev, command_pool);
    }

    // Main Loop.
    {
        size_t frame_count = 0;
        spec::StopWatch frametimes;
        frametimes.start();

        uint32_t next_frame_slot = 0;
        while (!window.should_close()) {
            FrameData& frame_data = frames_in_flight[next_frame_slot];
            // Acquire next window surface.
            uint32_t current_chain_index =
                 presenter.acquire(frame_data.window_surface_ready_signal);
            bool chain_index_acquired = current_chain_index < framebuffer.get_chain_size();

            if (chain_index_acquired) [[likely]] {
                // Wait for previous frame completion.
                if (!frame_data.cmd_ctx.wait(dev)) {
                    return TEST_FAIL;
                }

                // Statistics.
                frametimes.stop();
                if ((frame_count % 30) == 0) {
                    logger.logd("fps=", 1 / (frametimes.get_avg_time() / 1000));
                }
                if ((frame_count % 300) == 0) {
                    frametimes.reset();
                }
                frametimes.start();

                next_frame_slot = (next_frame_slot + 1) % max_frames_in_flight;

                // Rendering setup.
                if (!frame_data.start_cmd_ctx()) {
                    return TEST_FAIL;
                }
                framebuffer.start_pass(frame_data.cmd_buf, current_chain_index);

                // Rendering.
                pipeline.bind(frame_data.cmd_buf);
                pipeline.update_viewport(frame_data.cmd_buf, framebuffer, current_chain_index);
                frame_data.cmd_buf.draw(3, 1, 0, 0);

                // Rendering finalize & submission.
                framebuffer.end_pass(frame_data.cmd_buf);
                {
                    ::vk::PipelineStageFlags wait_stages[] = {
                         ::vk::PipelineStageFlagBits::eColorAttachmentOutput};
                    ::vk::SubmitInfo info;
                    info.waitSemaphoreCount = 1;
                    info.pWaitSemaphores = &frame_data.window_surface_ready_signal;
                    info.pWaitDstStageMask = wait_stages;
                    info.signalSemaphoreCount = 1;
                    info.pSignalSemaphores = &frame_data.rendering_complete_signal;
                    if (!frame_data.cmd_ctx.submit(queue, info)) {
                        return TEST_FAIL;
                    }
                }
                // Present submission.
                if (!presenter.submit(queue, frame_data.rendering_complete_signal,
                                      current_chain_index)) {
                    return TEST_FAIL;
                }
                frame_count++;
            }
            else {
                logger.logd("Couldn't acquire swapchain image: skipping frame.");
            }

            // Basic window event handling.
            window.poll_events();

            // Handle swapchain re-creation.
            presenter.recreate(window);
        }
    }

    // Cleanup.
    (void) dev.waitIdle();
    logger.logd("Cleaning up...");
    dev.destroyCommandPool(command_pool);

    return TEST_OK;
}
