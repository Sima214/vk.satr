#version 450

layout(set = 0, binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

layout(set = 1, binding = 0) uniform ExtraPos {
    vec3 model_offset;
} pos;

layout(push_constant) uniform Constants {
    vec2 data;
} push_const;

layout(location = 0) in vec2 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 1, component = 3) in float comp;
layout(location = 2) in float array[4];

layout(location = 0) out vec3 fragColor;
layout(location = 2) out Test {
    vec3 test1;
    vec2 test2;
} test;

void main() {
    test.test1 = vec3(0);
    test.test2 = vec2(0);
    gl_Position = ubo.proj * ubo.view * ubo.model * (vec4(pos.model_offset, 0.0) * array[3] + vec4(inPosition + push_const.data, 0.0, 1.0) * comp);
    fragColor = inColor;
}
