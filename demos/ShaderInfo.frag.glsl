#version 450

layout(set = 1, binding = 1) uniform ExtraPos {
    vec3 base_color;
} pos;

layout(location = 0) in vec3 fragColor;
layout(location = 2) in Test {
    vec3 test1;
    vec2 test2;
} test;

layout(push_constant) uniform Constants2 {
    vec3 tmp;
    vec2 data;
} push_const2;

layout(location = 0) out vec4 outColor;

void main() {
    vec4 data = vec4(push_const2.data, 1.0, 0.0);
    outColor = vec4(pos.base_color + fragColor, 1.0 - (test.test1.z * test.test2.y)) - data;
}
