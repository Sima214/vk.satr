# Subprojects.
satr_spec = subproject('satr_spec')
nui = subproject('satr_nui')

satr_spec_dep = satr_spec.get_variable('satr_dep')
glm_dep = nui.get_variable('glm_dep')
glfw_dep = [glm_dep, nui.get_variable('glfwpp_dep')]
vulkan_dep = [nui.get_variable('vulkan_dep'), nui.get_variable('vma_dep')]
vulkan_pch = nui.get_variable('vulkan_pch')

glslc_vert_cmd = nui.get_variable('glslc_vert_cmd')
glslc_frag_cmd = nui.get_variable('glslc_frag_cmd')
glslh_gen = nui.get_variable('glslh_gen')

# External tools.
re2c_exec = find_program('re2c', native: true, required: true)

# Core libraries.
math_dep = compiler.find_library('m', required: false)
# zlib_dep = dependency('zlib', required: true)
# png_dep = dependency('libpng', required: true)
cfg_dep = dependency('libconfig++', required: true)

# Additional vulkan libraries.
spirv_reflect_inc = include_directories('spirv_reflect')
spirv_reflect_conf = [
    # ADD_DEFINE_TEMPLATE.format('SPIRV_REFLECT_USE_SYSTEM_SPIRV_H'),
]
spirv_reflect_lib = static_library(
    'spirv_reflect',
    'spirv_reflect/spirv_reflect.c',
    include_directories: spirv_reflect_inc,
    dependencies: vulkan_dep,
    c_args: spirv_reflect_conf,
    cpp_args: spirv_reflect_conf,
    override_options: ['warning_level=0'],
    build_by_default: false
)
spirv_reflect_dep = declare_dependency(
    include_directories: spirv_reflect_inc,
    link_with: spirv_reflect_lib, compile_args: spirv_reflect_conf
)

# UI Library
dear_imgui_inc = include_directories(
    'imgui', 'imgui/misc/cpp',
    'imgui_toggle', 'implot'
)
dear_imgui_conf = [
    ADD_DEFINE_TEMPLATE.format('IMGUI_DISABLE_OBSOLETE_FUNCTIONS'),
    ADD_DEFINE_TEMPLATE.format('IMGUI_DISABLE_OBSOLETE_KEYIO'),
    ADD_DEFINE_TEMPLATE.format('IMGUI_DISABLE_DEBUG_TOOLS')
]
dear_imgui_src = files(
    # Core
    'imgui/imgui.cpp',
    'imgui/imgui_draw.cpp',
    'imgui/imgui_tables.cpp',
    'imgui/imgui_widgets.cpp',
    # Core extra
    'imgui/misc/cpp/imgui_stdlib.cpp',
    # Extension: toggle
    'imgui_toggle/imgui_toggle.cpp',
    'imgui_toggle/imgui_toggle_palette.cpp',
    'imgui_toggle/imgui_toggle_presets.cpp',
    'imgui_toggle/imgui_toggle_renderer.cpp',
    # Extension: plot
    'implot/implot.cpp',
    'implot/implot_items.cpp'
)
dear_imgui_lib = static_library(
    'dear_imgui', dear_imgui_src,
    include_directories: dear_imgui_inc,
    c_args: dear_imgui_conf,
    cpp_args: dear_imgui_conf,
    override_options: ['warning_level=0'],
    build_by_default: false
)
dear_imgui_dep = declare_dependency(
    include_directories: dear_imgui_inc,
    link_with: dear_imgui_lib,
    compile_args: dear_imgui_conf
)

dear_imgui_demo_lib = static_library(
    'dear_imgui_demo', 'imgui/imgui_demo.cpp', 'implot/implot_demo.cpp',
    include_directories: dear_imgui_inc,
    c_args: dear_imgui_conf,
    cpp_args: dear_imgui_conf,
    override_options: ['warning_level=0'],
    build_by_default: false
)
dear_imgui_demo_dep = declare_dependency(
    include_directories: dear_imgui_inc,
    link_with: dear_imgui_demo_lib, compile_args: dear_imgui_conf
)

# Extra libraries used mostly demos.
argparse_inc = include_directories('argparse/include/argparse')
argparse_dep = declare_dependency(include_directories: argparse_inc)
perlin_noise_inc = include_directories('perlin_noise')
perlin_noise_dep = declare_dependency(include_directories: perlin_noise_inc)

hdf5_dep = dependency('hdf5_cpp', required: true)

kissfft_sp = subproject('kissfft')
kissfft_dep = kissfft_sp.get_variable('kissfft_dep')
